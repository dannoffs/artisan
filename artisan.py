#!/usr/bin/env python

import sys
import imp
import os

# needs to be done before any other PyQt import
import sip
sip.setapi('QString', 2)
sip.setapi('QVariant', 2)

from platform import system
from artisanlib import main


# on Qt5, the platform plugin cocoa/windows is not found in the plugin directory (dispite the qt.conf file) if we do not
# extend the libraryPath accordingly
if system() == 'Darwin':
            from PyQt5.QtWidgets import QApplication
            QApplication.addLibraryPath(str(os.path.dirname(os.path.abspath( __file__ ))) + "/qt_plugins/")

elif system().startswith("Windows"):
	ib = (hasattr(sys, "frozen") or # new py2exe
    	hasattr(sys, "importers") # old py2exe
    	or imp.is_frozen("__main__")) # tools/freeze
	from PyQt5.QtWidgets import QApplication

	if ib:
    		QApplication.addLibraryPath(str(os.path.dirname(os.path.abspath( __file__ ))) + "\\qt_plugins\\")
	else:
    		QApplication.addLibraryPath("c:\\Python34\\Lib\\site-packages\\PyQt5\\plugins")


import numpy
from multiprocessing import freeze_support

if system() == "Windows" and (hasattr(sys, "frozen") # new py2exe
                            or hasattr(sys, "importers") # old py2exe
                            or imp.is_frozen("__main__")): # tools/freeze
    from multiprocessing import set_executable
    executable = os.path.join(os.path.dirname(sys.executable), 'artisan.exe')
    set_executable(executable)    
    del executable

if __name__ == '__main__':
    freeze_support()
    with numpy.errstate(invalid='ignore'):
        main.app.exec_()
