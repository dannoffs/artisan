

#######################################################################################
#################### GRAPH DRAWING WINDOW  ############################################
#######################################################################################

import matplotlib
matplotlib.use("Qt5Agg")

from PyQt5.QtCore import QLibraryInfo

from PyQt5.QtWidgets import (QLayout,QAction, QApplication, QWidget, QMessageBox, QLabel, QMainWindow, QFileDialog,
                             QInputDialog, QGroupBox, QDialog, QLineEdit, QTimeEdit, QTableWidgetSelectionRange,
                             QSizePolicy, QGridLayout, QVBoxLayout, QHBoxLayout, QPushButton, QDialogButtonBox,
                             QLCDNumber, QSpinBox, QComboBox, QHeaderView,
                             QSlider, QTabWidget, QStackedWidget, QTextEdit, QRadioButton,
                             QColorDialog, QFrame, QCheckBox,QStatusBar, 
                             QStyleFactory, QTableWidget, QTableWidgetItem, QMenu, QDoubleSpinBox)
from PyQt5.QtGui import (QImageReader, 
                         QKeySequence,QStandardItem,QImage,QPixmap,QColor,QPalette,QDesktopServices,QIcon,
                         QRegExpValidator,QDoubleValidator, QIntValidator,QPainter, QFont,QBrush, QRadialGradient,QCursor,QTextDocument)
from PyQt5.QtPrintSupport import (QPrinter,QPrintDialog)
from PyQt5.QtCore import (QLibraryInfo, QTranslator, QLocale, QFileInfo, PYQT_VERSION_STR, pyqtSignal,
                              QT_VERSION_STR,QTime, QTimer, QFile, QIODevice, QTextStream, QSettings, 
                              QRegExp, QDate, QUrl, QDir, QVariant, Qt, QPoint, QEvent, QDateTime, QThread, QSemaphore)

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

class tgraphcanvas(FigureCanvas):
    def __init__(self,parent):

        #default palette of colors
        self.palette = {"background":'white',"grid":'green',"ylabel":'0.20',"xlabel":'0.20',"title":'0.20',"rect1":'green',
                        "rect2":'orange',"rect3":'#996633',"rect4":'lightblue',"et":'red',"bt":'#00007f',"xt":'green',"deltaet":'orange',
                        "deltabt":'blue',"markers":'black',"text":'black',"watermarks":'yellow',"Cline":'blue'}

        self.artisanflavordefaultlabels = [QApplication.translate("Textbox", "Acidity",None),
                                            QApplication.translate("Textbox", "Aftertaste",None),
                                            QApplication.translate("Textbox", "Clean Cup",None),
                                            QApplication.translate("Textbox", "Head",None),
                                            QApplication.translate("Textbox", "Fragrance",None),
                                            QApplication.translate("Textbox", "Sweetness",None),
                                            QApplication.translate("Textbox", "Aroma",None),
                                            QApplication.translate("Textbox", "Balance",None),
                                            QApplication.translate("Textbox", "Body",None)]

        # custom labels are stored in the application settings and can be edited by the user
        self.customflavorlabels = self.artisanflavordefaultlabels

        self.SCCAflavordefaultlabels = [QApplication.translate("Textbox", "Sour",None),
                                        QApplication.translate("Textbox", "Flavor",None),
                                        QApplication.translate("Textbox", "Critical\nStimulus",None),
                                        QApplication.translate("Textbox", "Aftertaste",None),
                                        QApplication.translate("Textbox", "Bitter",None),
                                        QApplication.translate("Textbox", "Astringency",None),
                                        QApplication.translate("Textbox", "Solubles\nConcentration",None),
                                        QApplication.translate("Textbox", "Mouthfeel",None),
                                        QApplication.translate("Textbox", "Other",None),
                                        QApplication.translate("Textbox", "Aromatic\nComplexity",None),
                                        QApplication.translate("Textbox", "Roast\nColor",None),
                                        QApplication.translate("Textbox", "Aromatic\nPungency",None),
                                        QApplication.translate("Textbox", "Sweet",None),
                                        QApplication.translate("Textbox", "Acidity",None),
                                        QApplication.translate("Textbox", "pH",None),
                                        QApplication.translate("Textbox", "Balance",None)]

        self.CQIflavordefaultlabels =  [QApplication.translate("Textbox", "Fragance",None),
                                        QApplication.translate("Textbox", "Aroma",None),
                                        QApplication.translate("Textbox", "Flavor",None),
                                        QApplication.translate("Textbox", "Acidity",None),
                                        QApplication.translate("Textbox", "Body",None),
                                        QApplication.translate("Textbox", "Aftertaste",None)]

        self.SweetMariasflavordefaultlabels = [QApplication.translate("Textbox", "Dry Fragrance",None),
                                            QApplication.translate("Textbox", "Uniformity",None),
                                            QApplication.translate("Textbox", "Complexity",None),
                                            QApplication.translate("Textbox", "Clean Cup",None),
                                            QApplication.translate("Textbox", "Sweetness",None),
                                            QApplication.translate("Textbox", "Finish",None),
                                            QApplication.translate("Textbox", "Body",None),
                                            QApplication.translate("Textbox", "Flavor",None),
                                            QApplication.translate("Textbox", "Brightness",None),
                                            QApplication.translate("Textbox", "Wet Aroma",None)]

        self.Cflavordefaultlabels = [QApplication.translate("Textbox", "Fragrance",None),
                                            QApplication.translate("Textbox", "Aroma",None),
                                            QApplication.translate("Textbox", "Taste",None),
                                            QApplication.translate("Textbox", "Nose",None),
                                            QApplication.translate("Textbox", "Aftertaste",None),
                                            QApplication.translate("Textbox", "Body",None),
                                            QApplication.translate("Textbox", "Acidity",None)]

        self.Eflavordefaultlabels = [QApplication.translate("Textbox", "Fragrance-Aroma",None),
                                            QApplication.translate("Textbox", "Acidity",None),
                                            QApplication.translate("Textbox", "Flavor",None),
                                            QApplication.translate("Textbox", "Body",None),
                                            QApplication.translate("Textbox", "Aftertaste",None),
                                            QApplication.translate("Textbox", "Balance",None)]


        self.coffeegeekflavordefaultlabels = [QApplication.translate("Textbox", "Aroma",None),
                                            QApplication.translate("Textbox", "Acidity",None),
                                            QApplication.translate("Textbox", "Mouthfeel",None),
                                            QApplication.translate("Textbox", "Flavour",None),
                                            QApplication.translate("Textbox", "Aftertaste",None),
                                            QApplication.translate("Textbox", "Balance",None)]


        self.Intelligentsiaflavordefaultlabels = [QApplication.translate("Textbox", "Sweetness",None),
                                            QApplication.translate("Textbox", "Acidity",None),
                                            QApplication.translate("Textbox", "Body",None),
                                            QApplication.translate("Textbox", "Finish",None)]

        self.IstitutoInternazionaleAssaggiatoriCaffe = [QApplication.translate("Textbox", "Roast Color",None),
                                            QApplication.translate("Textbox", "Crema Texture",None),
                                            QApplication.translate("Textbox", "Crema Volume",None),
                                            QApplication.translate("Textbox", "Fragrance",None),
                                            QApplication.translate("Textbox", "Body",None),
                                            QApplication.translate("Textbox", "Acidity",None),
                                            QApplication.translate("Textbox", "Bitterness",None),
                                            QApplication.translate("Textbox", "Defects",None),
                                            QApplication.translate("Textbox", "Aroma Intensity",None),
                                            QApplication.translate("Textbox", "Aroma Persistence",None),
                                            QApplication.translate("Textbox", "Balance",None)]

        self.ax1 = self.ax2 = None

        self.flavorlabels = list(self.artisanflavordefaultlabels)
        #Initial flavor parameters. 
        self.flavors = [5., 5., 5., 5., 5., 5., 5., 5., 5.]
        self.flavorstartangle = 90
        self.flavoraspect = 1.0  #aspect ratio

        #F = Fahrenheit; C = Celsius
        self.mode = "F"
        self.errorlog = []

        # default delay between readings in miliseconds
        self.default_delay = 3000 # default 3s
        self.delay = self.default_delay
        self.min_delay = 1000
        
        # oversampling flag
        self.oversampling = False
        self.oversampling_min_delay = 3000
        
        # extra event sampling interval in miliseconds. 
	# If 0, then extra sampling commands are sent "in sync" with the standard sampling commands

        self.extra_event_sampling_delay = 0 # sync, 0.5s, 1.0s, 1.5s,.., 5s => 0, 500, 1000, 1500, ..

        #watermarks limits: dryphase1, dryphase2, midphase, and finish phase Y limits
        self.phases_fahrenheit_defaults = [200,300,390,450]
        self.phases_celsius_defaults = [95,150,200,230]
        self.phases = list(self.phases_fahrenheit_defaults) # contains either the phases_filter or phases_espresso, depending on the mode
        self.phases_mode = 0 # indicates the active phases mode: 0: filter; 1: espresso
        self.phases_filter = self.phases[:]
        self.phases_espresso = self.phases[:]
        #this flag makes the main push buttons DryEnd, and FCstart change the phases[1] and phases[2] respectively
        self.phasesbuttonflag = True #False no change; True make the DRY and FC buttons change the phases during roast automatically
        self.watermarksflag = True

        #show phases LCDs during roasts
        self.phasesLCDflag = True
        self.phasesLCDmode = 0 # one of 0: time, 1: percentage, 2: temp mode
        

        #statistics flags selects to display: stat. time, stat. bar, stat. flavors, stat. area, stat. deg/min, stat. ETBTarea
        self.statisticsflags = [1,1,0,1,1,0]
        self.statisticsmode = 0 # one of 0: standard computed values, 1: roast properties
        #conditions to estimate bad flavor:dry[min,max],mid[min,max],finish[min,max] in seconds
        self.defaultstatisticsconditions = [180,360,180,600,180,360,180,300]
        self.statisticsconditions = self.defaultstatisticsconditions
        #records length in seconds of total time [0], dry phase [1],mid phase[2],finish phase[3], cool phase[4]
        self.statisticstimes = [0,0,0,0,0]

        #DEVICES
        self.device = 18                                    # default device selected to None (18). Calls appropiate function
        
        # Phidget variables
        self.phidget1048_types = [
            ThermocoupleType.PHIDGET_TEMPERATURE_SENSOR_K_TYPE,
            ThermocoupleType.PHIDGET_TEMPERATURE_SENSOR_K_TYPE,
            ThermocoupleType.PHIDGET_TEMPERATURE_SENSOR_K_TYPE,
            ThermocoupleType.PHIDGET_TEMPERATURE_SENSOR_K_TYPE] # probe types (ThermocoupleType)
        self.phidget1048_async = [False]*4
        self.phidget1048_changeTriggers = [1.0]*4
        self.phidget1048_changeTriggersValues = [x / 10.0 for x in range(1, 11, 1)]
        self.phidget1048_changeTriggersStrings = list(map(lambda x:str(x) + "C",self.phidget1048_changeTriggersValues))
        self.phidget1045_async = False
        self.phidget1045_changeTrigger = 1.0
        self.phidget1045_changeTriggersValues = [x / 10.0 for x in range(1, 11, 1)]
        self.phidget1045_changeTriggersStrings = list(map(lambda x:str(x) + "C",self.phidget1045_changeTriggersValues))
        self.phidget1045_emissivity = 1.0

        self.phidget_dataRatesStrings = ["0.25s","0.5s","0.75s","1s"] # too fast: "8ms","16ms","32ms","64ms","0.12s",
        self.phidget_dataRatesValues = [256,512,768,1000] # 8,16,32,64,128,

        self.phidget1046_async = [False]*4
        self.phidget1046_gain = [1]*4
        self.phidget1046_gainValues = ["1", "8","16","32","64","128"] # 1 for no gain
        self.phidget1046_formula = [1]*4 # 0: 1K Ohm Wheatstone Bridge, 1: 1K Ohm Voltage Divider, 2: raw
        self.phidget1046_formulaValues = ["WS", "Div","raw"]
        self.phidget1046_dataRate = 1000 # in ms; (Phidgets default 8ms, 16ms if wireless is active)
        
        self.phidgetRemoteFlag = False
        self.phidgetServerID = ""
        self.phidgetPassword = ""
        
        self.phidget1018_async = [False]*8
        self.phidget1018_raws = [False]*8
        self.phidget1018_dataRates = [1000]*8 # in ms; (Phidgets default 8ms, 16ms if wireless is active)
        self.phidget1018_changeTriggers = [10]*8
        self.phidget1018_changeTriggersValues = range(0,51,1)
        self.phidget1018_changeTriggersStrings = list(map(lambda x:str(x),self.phidget1018_changeTriggersValues))
        self.phidget1018Ratiometric = True

        #menu of thermocouple devices
        #device with first letter + only shows in extra device tab
        #device with first letter - does not show in any tab (but its position in the list is important)
        # device labels (used in Dialog config).

        # ADD DEVICE: to add a device you have to modify several places. Search for the tag "ADD DEVICE:" in the code
        # - add to self.devices
        self.devices = [#Fuji PID               #0
                       "Omega HH806AU",         #1
                       "Omega HH506RA",         #2
                       "CENTER 309",            #3
                       "CENTER 306",            #4
                       "CENTER 305",            #5
                       "CENTER 304",            #6
                       "CENTER 303",            #7
                       "CENTER 302",            #8
                       "CENTER 301",            #9
                       "CENTER 300",            #10
                       "VOLTCRAFT K204",        #11
                       "VOLTCRAFT K202",        #12
                       "VOLTCRAFT 300K",        #13
                       "VOLTCRAFT 302KJ",       #14
                       "EXTECH 421509",         #15
                       "Omega HH802U",          #16
                       "Omega HH309",           #17
                       "NONE",                  #18
                       "-ARDUINOTC4",           #19
                       "TE VA18B",              #20
                       "+CENTER 309_34",        #21
                       "+FUJI DUTY %/SV",       #22
                       "Omega HHM28[6]",        #23
                       "+VOLTCRAFT K204_34",    #24
                       "+Virtual",              #25
                       "-DTAtemperature",       #26
                       "Program",               #27
                       "+ArduinoTC4_34",        #28
                       "MODBUS",                #29
                       "VOLTCRAFT K201",        #30
                       "Amprobe TMD-56",        #31
                       "+ArduinoTC4_56",        #32
                       "+MODBUS_34",            #33
                       "Phidget 1048",          #34
                       "+Phidget 1048_34",      #35
                       "+Phidget 1048_AT",      #36
                       "Phidget 1046 RTD",      #37
                       "+Phidget 1046_34 RTD",  #38
                       "Mastech MS6514",        #39
                       "Phidget IO 12",         #40
                       "+Phidget IO 34",        #41
                       "+Phidget IO 56",        #42
                       "+Phidget IO 78",        #43
                       "+ArduinoTC4_78",        #44
                       "Yocto Thermocouple",    #45
                       "Yocto PT100",           #46
                       "Phidget 1045 IR",       #47
                       "+Program_34",           #48
                       "+Program_56",           #49
                       "DUMMY",                 #50
                       "+CENTER 304_34",        #51
                       "Phidget 1051",          #52
                       "Hottop BT/ET",          #53
                       "+Hottop Heater/Fan",     #54
                       "-Omega HH806W"          #55 NOT WORKING 
                       ]

        #extra devices
        self.extradevices = []                                      # list with indexes for extra devices
        self.extratimex = []                                        # individual time for each extra device (more accurate).
								    # List of lists (2 dimension)
        self.extradevicecolor1 = []                                 # extra line 1 color. list with colors.
        self.extradevicecolor2 = []                                 # extra line 2 color. list with colors.
        self.extratemp1,self.extratemp2 = [],[]                     # extra temp1, temp2. List of lists
        self.extrastemp1,self.extrastemp2 = [],[]                   # smoothed extra temp1, temp2. List of lists
        self.extratemp1lines,self.extratemp2lines = [],[]           # lists with extra lines for speed drawing
        self.extraname1,self.extraname2 = [],[]                     # name of labels for line (like ET or BT) - legend
        self.extramathexpression1,self.extramathexpression2 = [],[] # list with user defined math evaluating strings. Example "2*cos(x)"
        self.extralinestyles1,self.extralinestyles2 = [],[]         # list of extra curve line styles
        self.extradrawstyles1,self.extradrawstyles2 = [],[]         # list of extra curve drawing styles
        self.extralinewidths1,self.extralinewidths2 = [],[]         # list of extra curve linewidth
        self.extramarkers1,self.extramarkers2 = [],[]               # list of extra curve marker styles
        self.extramarkersizes1,self.extramarkersizes2 = [],[]       # list of extra curve marker size

        #holds math expressions to plot
        self.plotcurves=["", "", "", "", "", ""]
        self.plotcurvecolor = ["black","black","black","black","black","black"]

        self.fig = Figure(tight_layout={"pad":.2},frameon=True) # ,"h_pad":0.0,"w_pad":0.0
        # with tight_layout=True, the matplotlib canvas expands to the maximum using figure.autolayout

        #figure back color
        if platf == 'Darwin':
            self.backcolor ="#EEEEEE"
        else:
            self.backcolor = "white"
        self.fig.patch.set_facecolor(self.backcolor)
        self.fig.patch.set_edgecolor(self.backcolor)

        self.ax = self.fig.add_subplot(111, axisbg= self.palette["background"])
        self.delta_ax = self.ax.twinx()

        #legend location
        self.legendloc = 2
        self.fig.subplots_adjust(
            # all values in percent
            top=0.93, # the top of the subplots of the figure (default: 0.9)
            bottom=0.1, # the bottom of the subplots of the figure (default: 0.1)
            left=0.067, # the left side of the subplots of the figure (default: 0.125)
            right=.925) # the right side of the subplots of the figure (default: 0.9
        FigureCanvas.__init__(self, self.fig)

        self.fig.canvas.mpl_connect('button_press_event', self.onclick)
        self.fig.canvas.mpl_connect('pick_event', self.onpick)

        # set the parent widget
        self.setParent(parent)
        # we define the widget as
        FigureCanvas.setSizePolicy(self,QSizePolicy.Expanding,QSizePolicy.Expanding)
        # notify the system of updated policy
        FigureCanvas.updateGeometry(self)

        # the rate of chage of temperature
        self.rateofchange1 = 0.0
        self.rateofchange2 = 0.0

        #read and plot on/off flag
        self.flagon = False  # Artisan turned on
        self.flagstart = False # Artisan logging
        self.flagsampling = False # if True, Artisan is still in the sampling phase and one has to wait for its end to turn OFF
        self.flagsamplingthreadrunning = False
        #log flag that tells to log ET when using device 18 (manual mode)
        self.manuallogETflag = 0
        
        self.flagalignFCs = False

        self.roastpropertiesflag = 1  #resets roast properties if not zero
        self.title = QApplication.translate("Scope Title", "Roaster Scope",None)
        self.ambientTemp = 0.
        self.ambientTempSource = 0 # indicates the temperature curve that is used to automatically fill the ambient temperature on DROP
                                   # 0 : None; 1 : ET, 2 : BT, 3 : 0xT1, 4 : 0xT2,
        self.ambient_humidity = 0.
        #relative humidity percentage [0], corresponding temperature [1]
        
        self.moisture_greens = 0.
        self.moisture_roasted = 0.
        
        self.beansize = 0.0

        self.whole_color = 0
        self.ground_color = 0
        self.color_systems = ["","Tonino","ColorTest","Colorette","ColorTrack","Agtron"]
        self.color_system_idx = 0

        # roast property flags
        self.heavyFC_flag = False
        self.lowFC_flag = False
        self.lightCut_flag = False
        self.darkCut_flag = False
        self.drops_flag = False
        self.oily_flag = False
        self.uneven_flag = False
        self.tipping_flag = False
        self.scorching_flag = False
        self.divots_flag = False

        #list to store the time of each reading. Most IMPORTANT variable.
        self.timex = []

        #lists to store temps and rates of change. Second most IMPORTANT variables. All need same dimension.
        #self.temp1 = ET ; self.temp2 = BT; self.delta1 = deltaMET; self.delta2 = deltaBT
        self.temp1,self.temp2,self.delta1, self.delta2 = [],[],[],[]
        self.stemp1,self.stemp2 = [],[] # smoothed versions of temp1/temp2 usind in redraw()
        self.unfiltereddelta1, self.unfiltereddelta2 = [],[] # used in sample()

        #indexes for CHARGE[0],DRYe[1],FCs[2],FCe[3],SCs[4],SCe[5],DROP[6] and COOLe[7]
        #Example: Use as self.timex[self.timeindex[1]] to get the time of DryEnd
        #Example: Use self.temp2[self.timeindex[4]] to get the BT temperature of SCs

        self.timeindex = [-1,0,0,0,0,0,0,0] #CHARGE index init set to -1 as 0 could be an actal index used

        #applies a Y(x) function to ET or BT 
        self.ETfunction,self.BTfunction = "",""

        #put a "aw.qmc.safesaveflag = True" whenever there is a change of a profile like at [DROP], edit properties Dialog, etc
        #prevents accidentally deleting a modified profile.
        self.safesaveflag = False

        #background profile
        self.background = False
        self.backgroundDetails = False
        self.backgroundeventsflag = False
        self.backgroundpath = ""
        self.titleB = ""
        self.roastbatchnrB = 0
        self.roastbatchprefixB = u("")
        self.roastbatchposB = 0
        self.temp1B,self.temp2B,self.temp1BX,self.temp2BX,self.timeB = [],[],[],[],[]
        self.stemp1B,self.stemp2B,self.stemp1BX,self.stemp2BX = [],[],[],[] # smoothed versions of the background courves
        self.extraname1B,self.extraname2B = [],[]
        self.extratimexB = []
        self.xtcurveidx = 0 # the selected extra background courve to be displayed
        self.delta1B,self.delta2B = [],[]
        self.timeindexB = [-1,0,0,0,0,0,0,0]
        self.backgroundEvents = [] #indexes of background events
        self.backgroundEtypes = []
        self.backgroundEvalues = []
        self.backgroundEStrings = []
        self.backgroundalpha = 0.3
        self.backgroundmetcolor = self.palette["et"]
        self.backgroundbtcolor = self.palette["bt"]
        self.backgroundxtcolor = self.palette["xt"]
        self.backgrounddeltaetcolor = self.palette["deltaet"]
        self.backgrounddeltabtcolor = self.palette["deltabt"]
        self.backmoveflag = 1
        self.detectBackgroundEventTime = 20 #seconds
        self.backgroundReproduce = False
        self.Betypes = [QApplication.translate("Scope Annotation", "Speed",None),
                        QApplication.translate("Scope Annotation", "Heater",None),
                        QApplication.translate("Scope Annotation", "Damper",None),
                        QApplication.translate("Scope Annotation", "Fan",None),
                        "--"]
        self.backgroundFlavors = []
        self.flavorbackgroundflag = False
        #background by value
        self.E1backgroundtimex,self.E2backgroundtimex,self.E3backgroundtimex,self.E4backgroundtimex = [],[],[],[]
        self.E1backgroundvalues,self.E2backgroundvalues,self.E3backgroundvalues,self.E4backgroundvalues = [],[],[],[]
        self.l_backgroundeventtype1dots, = self.ax.plot(self.E1backgroundtimex, self.E1backgroundvalues, color="grey")
        self.l_backgroundeventtype2dots, = self.ax.plot(self.E2backgroundtimex, self.E2backgroundvalues, color="darkgrey")
        self.l_backgroundeventtype3dots, = self.ax.plot(self.E3backgroundtimex, self.E3backgroundvalues, color="slategrey")
        self.l_backgroundeventtype4dots, = self.ax.plot(self.E4backgroundtimex, self.E4backgroundvalues, color="slateblue")
        # background Deltas
        self.DeltaETBflag = False
        self.DeltaBTBflag = False

        # projection variables of change of rate
        self.HUDflag = False
        self.hudresizeflag = False
        self.ETtarget = 300
        self.ET2target = 350
        self.BTtarget = 200
        self.BT2target = 250
        self.hudETpid = [5,240,60]    # HUD pid: p = 20, i = 60, d = 13
        self.pidpreviouserror = 0  # temporary storage of pid error

        #General notes. Accessible through "edit graph properties" of graph menu. WYSIWYG viewer/editor.
        self.roastertype = ""
        self.operator = ""
        self.roastingnotes = ""
        self.cuppingnotes = ""
        self.roastdate = QDateTime.currentDateTime()
        # system batch nr system
        self.roastepoch = self.roastdate.toTime_t() # in seconds
        self.lastroastepoch = self.roastepoch # the epoch of the last roast in seconds
        self.batchcounter = -1 # global batch counter; if batchcounter is -1, batchcounter system is inactive
        self.batchsequence = 0 # global counter of position in sequence of batches of one session
        self.batchprefix = u("")
        # profile batch nr
        self.roastbatchnr = 0 # batch number of the roast; if roastbatchnr=0, prefix/counter is hidden/inactiv (initialized to 0 on roast START)
        self.roastbatchprefix = self.batchprefix # batch prefix of the roast
        self.roastbatchpos = 0 # position of the roast in the roast session (first batch, second batch,..)
        self.roasttzoffset = libtime.timezone # timezone offset to be added to roastepoch to get time in local timezone
        self.beans = ""

        #flags to show projections, draw Delta ET, and draw Delta BT
        self.projectFlag = False
        self.ETcurve = True
        self.BTcurve = True
        self.ETlcd = True
        self.BTlcd = True
        self.LCDdecimalplaces = 1
        self.DeltaETflag = False
        self.DeltaBTflag = False
        self.DeltaETlcdflag = True
        self.DeltaBTlcdflag = True
        self.HUDbuttonflag = False
        self.PIDbuttonflag = True
        # user filter values x are translated as follows to internal filter values: y = x*2 + 1 (to go the other direction: x = y/2)
        # this is to ensure, that only uneven window values are used and no wrong shift is happening through smoothing
        self.deltafilter = 5 # => corresponds to 2 on the user interface
        self.curvefilter = 3 # => corresponds to 1 on the user interface
        self.deltaspan = 6 # the time period taken to compute one deltaBT/ET value (1-15sec)
        self.deltasamples = 2 # the number of samples that make up the delta span, to be used in the delta computations (> 0!)
        self.profile_sampling_interval = None # will be updated on loading a profile
        
        self.altsmoothing = False # toggle between standard and alternative smoothing approach
        self.smoothingwindowsize = 3 # window size of the alternative smoothing approach

        self.patheffects = 3
        self.graphstyle = 0
        self.graphfont = 0

        #variables to configure the 8 default buttons
        # button = 0:CHARGE, 1:DRY_END, 2:FC_START, 3:FC_END, 4:SC_START, 5:SC_END, 6:DROP, 7:COOL_END
        self.buttonvisibility = [True]*8
        self.buttonactions = [0]*8
        self.buttonactionstrings = [""]*8
        #variables to configure the 0: ON, 1: OFF buttons and 2: SAMPLE action
        self.extrabuttonactions = [0]*3
        self.extrabuttonactionstrings = [""]*3

        #flag to activate the automatic marking of the CHARGE and DROP events
        #self.autoChargeDropFlag = False # has been replaced by the following two separate flags
        self.autoChargeFlag = False
        self.autoDropFlag = False
        #autodetected CHARGE and DROP index
        self.autoChargeIdx = 0
        self.autoDropIdx = 0

        self.markTPflag = True
        self.autoTPIdx = 0 # set by sample() on recognition and cleared once TP is marked

        # flags to control automatic DRY and FCs events based on phases limits
        self.autoDRYflag = False
        self.autoFCsFlag = False
        self.autoDryIdx = 0 # set by sample() on recognition and cleared once DRY is marked
        self.autoFCsIdx = 0 # set by sample() on recognition and cleared once FCs is marked

        # projection variables of change of rate
        self.projectionconstant = 1
        self.projectionmode = 0     # 0 = linear; 1 = newton

        #[0]weight in, [1]weight out, [2]units (string)
        self.weight = [0,0,"g"]
        #[0]volume in, [1]volume out, [2]units (string)
        self.volume = [0,0,"l"]
        #[0]probe weight, [1]weight unit, [2]probe volume, [3]volume unit
        self.density = [0.,"g",1.,"l"]
        
        self.volumeCalcUnit = 0
        self.volumeCalcWeightInStr = ""
        self.volumeCalcWeightOutStr = ""
        
        # container scale tare
        self.container_names = []
        self.container_weights = [] # all weights in g and as int
        self.container_idx = -1 # the empty field (as -1 + 2 = 1)

        #stores _indexes_ of self.timex to record events. 
        # Use as self.timex[self.specialevents[x]] to get the time of an event
        # use self.temp2[self.specialevents[x]] to get the BT temperature of an event.
        self.specialevents = []
        #ComboBox text event types. They can be modified in eventsDlg()
        self.etypes = [QApplication.translate("ComboBox", "Speed",None),
                       QApplication.translate("ComboBox", "Power",None),
                       QApplication.translate("ComboBox", "Damper",None),
                       QApplication.translate("ComboBox", "Fan",None),
                       "--"]
        #default etype settings to restore
        self.etypesdefault = [QApplication.translate("ComboBox", "Speed",None),
                              QApplication.translate("ComboBox", "Power",None),
                              QApplication.translate("ComboBox", "Damper",None),
                              QApplication.translate("ComboBox", "Fan",None),
                              "--"]
        #stores the type of each event as index of self.etypes. None = 0, Power = 1, etc.
        self.specialeventstype = []
        #stores text string descriptions for each event.
        self.specialeventsStrings = []
        #event values are from 0-10
        #stores the value for each event
        self.specialeventsvalue = []
        #flag that makes the events location type bars (horizontal bars) appear on the plot. flag read on redraw()
        # 0 = no event bars; 1 = type bars (4 bars); 2 = value bars (10 bars)
        self.eventsGraphflag = 0
        #flag that shows events in the graph
        self.eventsshowflag = 0
        #flag that shows major event annotations in the graph
        self.annotationsflag = 1
        #plot events by value
        self.E1timex,self.E2timex,self.E3timex,self.E4timex = [],[],[],[]
        self.E1values,self.E2values,self.E3values,self.E4values = [],[],[],[]
        self.EvalueColor = ["brown","blue","purple","grey"]
        self.EvalueMarker = ["o","s","h","D"]
        self.EvalueMarkerSize = [8,8,8,8]
        self.Evaluelinethickness = [2,2,2,2]
        self.Evaluealpha = [.8,.8,.8,.8]
        #the event value position bars are calculated at redraw()
        self.eventpositionbars = [0.]*12

        #curve styles
        self.linestyle_default = "-"
        self.drawstyle_default = "default"
        self.linewidth_default = 2
        self.marker_default = None
        self.markersize_default = 6

        self.BTlinestyle = self.linestyle_default
        self.BTdrawstyle = self.drawstyle_default
        self.BTlinewidth = self.linewidth_default
        self.BTmarker = self.marker_default
        self.BTmarkersize = self.markersize_default
        self.ETlinestyle = self.linestyle_default
        self.ETdrawstyle = self.drawstyle_default
        self.ETlinewidth = self.linewidth_default
        self.ETmarker = self.marker_default
        self.ETmarkersize = self.markersize_default
        self.BTdeltalinestyle = self.linestyle_default
        self.BTdeltadrawstyle = self.drawstyle_default
        self.BTdeltalinewidth = self.linewidth_default
        self.BTdeltamarker = self.marker_default
        self.BTdeltamarkersize = self.markersize_default
        self.ETdeltalinestyle = self.linestyle_default
        self.ETdeltadrawstyle = self.drawstyle_default
        self.ETdeltalinewidth = self.linewidth_default
        self.ETdeltamarker = self.marker_default
        self.ETdeltamarkersize = self.markersize_default
        self.BTbacklinestyle = self.linestyle_default
        self.BTbackdrawstyle = self.drawstyle_default
        self.BTbacklinewidth = self.linewidth_default
        self.BTbackmarker = self.marker_default
        self.BTbackmarkersize = self.markersize_default
        self.ETbacklinestyle = self.linestyle_default
        self.ETbackdrawstyle = self.drawstyle_default
        self.ETbacklinewidth = self.linewidth_default
        self.ETbackmarker = self.marker_default
        self.ETbackmarkersize = self.markersize_default
        self.XTbacklinestyle = self.linestyle_default
        self.XTbackdrawstyle = self.drawstyle_default
        self.XTbacklinewidth = self.linewidth_default
        self.XTbackmarker = self.marker_default
        self.XTbackmarkersize = self.markersize_default                
        self.BTBdeltalinestyle = self.linestyle_default
        self.BTBdeltadrawstyle = self.drawstyle_default
        self.BTBdeltalinewidth = self.linewidth_default
        self.BTBdeltamarker = self.marker_default
        self.BTBdeltamarkersize = self.markersize_default
        self.ETBdeltalinestyle = self.linestyle_default
        self.ETBdeltadrawstyle = self.drawstyle_default
        self.ETBdeltalinewidth = self.linewidth_default
        self.ETBdeltamarker = self.marker_default
        self.ETBdeltamarkersize = self.markersize_default
        self.BTBdeltalinestyle = self.linestyle_default
        self.BTBdeltadrawstyle = self.drawstyle_default
        self.BTBdeltalinewidth = self.linewidth_default
        self.BTBdeltamarker = self.marker_default
        self.BTBdeltamarkersize = self.markersize_default
        self.ETBdeltalinestyle = self.linestyle_default
        self.ETBdeltadrawstyle = self.drawstyle_default
        self.ETBdeltalinewidth = self.linewidth_default
        self.ETBdeltamarker = self.marker_default
        self.ETBdeltamarkersize = self.markersize_default

        #Temperature Alarms lists. Data is written in  alarmDlg 
        self.alarmflag = []    # 0 = OFF; 1 = ON flags
        self.alarmguard = []   # points to another alarm by index that has to be triggered before; -1 indicates no guard
        self.alarmnegguard = []   # points to another alarm by index that should not has been triggered before; -1 indicates no guard
        self.alarmtime = []    # times after which each alarm becomes efective. Usage: self.timeindex[self.alarmtime[i]]
                               # -1 equals None
        self.alarmoffset = []  # for timed alarms, the seconds after alarmtime the alarm is triggered
        self.alarmtime2menuidx = [2,4,5,6,7,8,9,10,3,0,1] # maps self.alarmtime index to menu idx (to move TP in menu from index 9 to 3)
        self.menuidx2alarmtime = [9,-1,0,8,1,2,3,4,5,6,7] # inverse of above (note that those two are only inverse in one direction!)
        self.alarmcond = []    # 0 = falls below; 1 = rises above
        # alarmstate is set to 'not triggered' on reset()

        self.alarmstate = []   # 1=triggered, 0=not triggered. 
				# Needed so that the user does not have to turn the alarms ON next roast after alarm being used once.

        self.alarmsource = []   # -3=None, -2=DeltaET, -1=DeltaBT, 0=ET , 1=BT, 2=extratemp1[0], 3=extratemp2[0], 4=extratemp2[1],....
        self.alarmtemperature = []  # set temperature number (example 500)
        self.alarmaction = []       # -1 = no action; 0 = open a window;
                                    # 1 = call program with a filepath equal to alarmstring; 
                                    # 2 = activate button with number given in description; 
                                    # 3,4,5,6 = move slider with value given in description
                                    # 7 (START), 8 (DRY), 9 (FCs), 10 (FCe), 11 (SCs), 12 (SCe), 13 (DROP), 14 (COOL), 15 (OFF)
                                    # 16 (CHARGE),
                                    # 17 (RampSoak_ON), 18 (RampSoak_OFF), 19 (PID_ON), 20 (PID_OFF)
        self.alarmbeep = []    # 0 = OFF; 1 = ON flags
        self.alarmstrings = []      # text descriptions, action to take, or filepath to call another program

        self.loadalarmsfromprofile = False # if set, alarms are loaded from profile (even background profiles)
        self.alarmsfile = "" # filename alarms were loaded from
        self.temporaryalarmflag = -3 #holds temporary index value of triggered alarm in updategraphics()
        self.TPalarmtimeindex = None # is set to the current  aw.qmc.timeindex by sample(), if alarms are defined and once the TP is detected
        
        self.quantifiedEvent = [] # holds an event quantified during sample(), a tuple [<eventnr>,<value>]q

        # set initial limits for X and Y axes. But they change after reading the previous seetings at aw.settingsload()
        self.ylimit = 600
        self.ylimit_min = 0
        self.zlimit = 100
        self.zlimit_min = 0
        self.RoRlimitF = 80
        self.RoRlimitC = 45
        self.endofx = 60
        self.startofx = 0
        self.resetmaxtime = 60  #time when pressing reset
        self.fixmaxtime = False # if true, do not automatically extend the endofx by if needed because the measurements get out of the x-axis
        self.locktimex = False # if true, do not set time axis min and max from profile on load
        self.locktimex_start = 0 # seconds of x-axis min as locked by locktimex (needs to be interpreted wrt. CHARGE index)
        self.xgrid = 60   #initial time separation; 60 = 1 minute
        self.ygrid = 100  #initial temperature separation
        self.zgrid = 10   #initial RoR separation
        self.gridstyles =    ["-","--","-.",":"," "]  #solid,dashed,dash-dot,dotted,None
        self.gridlinestyle = 0
        self.gridthickness = 1
        self.gridalpha = .2
        self.xrotation = 0

        #height of statistics bar
        self.statisticsheight = 650
        self.statisticsupper = 655
        self.statisticslower = 617

        # autosave
        self.autosaveflag = 0
        self.autosaveprefix = ""
        self.autosavepath = ""

        #used to place correct height of text to avoid placing text over text (annotations)
        self.ystep_down = 0
        self.ystep_up = 0

        self.ax.set_xlim(self.startofx, self.endofx)
        self.ax.set_ylim(self.ylimit_min,self.ylimit)
        
        if self.delta_ax:
            self.delta_ax.set_xlim(self.startofx, self.endofx)
            self.delta_ax.set_ylim(self.zlimit_min,self.zlimit)
            self.delta_ax.set_autoscale_on(False)

        # disable figure autoscale
        self.ax.set_autoscale_on(False)

        #set grid + axle labels + title
        self.ax.grid(True,color=self.palette["grid"],linestyle = self.gridstyles[self.gridlinestyle],linewidth = self.gridthickness,alpha = self.gridalpha)

        #change label colors
        for label in self.ax.yaxis.get_ticklabels():
            label.set_color(self.palette["ylabel"])

        for label in self.ax.xaxis.get_ticklabels():
            label.set_color(self.palette["xlabel"])

        # generates first "empty" plot (lists are empty) of temperature and deltaT
        self.l_temp1, = self.ax.plot(self.timex,self.temp1,markersize=self.ETmarkersize,marker=self.ETmarker,linewidth=self.ETlinewidth,linestyle=self.ETlinestyle,drawstyle=self.ETdrawstyle,color=self.palette["et"],label=u(QApplication.translate("Label", "ET", None)))
        self.l_temp2, = self.ax.plot(self.timex,self.temp2,markersize=self.BTmarkersize,marker=self.BTmarker,linewidth=self.BTlinewidth,linestyle=self.BTlinestyle,drawstyle=self.BTlinestyle,color=self.palette["bt"],label=u(QApplication.translate("Label", "BT", None)))
        self.l_delta1, = self.ax.plot(self.timex,self.delta1,markersize=self.ETdeltamarkersize,marker=self.ETdeltamarker,linewidth=self.ETdeltalinewidth,linestyle=self.ETdeltalinestyle,drawstyle=self.ETdeltadrawstyle,color=self.palette["deltaet"],label=u(QApplication.translate("Label", "DeltaET", None)))
        self.l_delta2, = self.ax.plot(self.timex,self.delta2,markersize=self.BTdeltamarkersize,marker=self.BTdeltamarker,linewidth=self.BTdeltalinewidth,linestyle=self.BTdeltalinestyle,drawstyle=self.BTdeltadrawstyle,color=self.palette["deltabt"],label=u(QApplication.translate("Label", "DeltaBT", None)))
        self.l_back1 = None
        self.l_back2 = None
        self.l_back3 = None # one extra background curve
        self.l_delta1B = None
        self.l_delta2B = None

        self.l_BTprojection = None
        self.l_ETprojection = None

        self.l_horizontalcrossline = None
        self.l_verticalcrossline = None

        self.l_timeline = None

        self.l_eventtype1dots, = self.ax.plot(self.E1timex, self.E1values, color=self.EvalueColor[0], marker=self.EvalueMarker[0])
        self.l_eventtype2dots, = self.ax.plot(self.E2timex, self.E2values, color=self.EvalueColor[1], marker=self.EvalueMarker[1])
        self.l_eventtype3dots, = self.ax.plot(self.E3timex, self.E3values, color=self.EvalueColor[2], marker=self.EvalueMarker[2])
        self.l_eventtype4dots, = self.ax.plot(self.E4timex, self.E4values, color=self.EvalueColor[3], marker=self.EvalueMarker[3])

        ###########################  TIME  CLOCK     ##########################
        # create an object time to measure and record time (in miliseconds)

        self.timeclock = QTime()
        self.timeclock.setHMS(0,0,0,0)

        ############################  Thread Server #################################################
        #server that spawns a thread dynamically to sample temperature (press button ON to make a thread press OFF button to kill it)
        self.threadserver = Athreadserver()

        ##########################     Designer variables       #################################
        self.designerflag = False
        self.designerconnections = [0,0,0,0]   #mouse event ids
        self.mousepress = None
        self.indexpoint = 0
        self.workingline = 2  #selects ET or BT
        self.eventtimecopy = []
        self.specialeventsStringscopy = []
        self.specialeventsvaluecopy = []
        self.specialeventstypecopy = []
        self.currentx = 0               #used to add point when right click
        self.currenty = 0               #used to add point when right click
        self.designertimeinit = [50,300,540,560,660,700,800,900]
        if self.mode == "C":
                                     #CH, DE, Fcs,Fce,Scs,Sce,Drop
            self.designertemp1init = [290,290,290,290,290,290,290,290]
            self.designertemp2init = [200,150,200,210,220,225,240,240]   #CHARGE,DRY END,FCs, FCe,SCs,SCe,DROP,COOL
        elif self.mode == "F":
            self.designertemp1init = [500,500,500,500,500,500,500,500]
            self.designertemp2init = [380,300,390,395,410,412,420,420]
        self.BTsplinedegree = 3
        self.ETsplinedegree = 3
        self.reproducedesigner = 0      #flag to add events to help reproduce (replay) the profile: 0 = none; 1 = sv; 2 = ramp

        ###########################         filterDropOut variables     ################################

        # constants

        self.filterDropOut_replaceRoR_period = 3
        self.filterDropOut_spikeRoR_period = 3

        # defaults

        self.filterDropOut_tmin_C_default = 10
        self.filterDropOut_tmax_C_default = 700
        self.filterDropOut_tmin_F_default = 50
        self.filterDropOut_tmax_F_default = 1292
        self.filterDropOut_spikeRoR_dRoR_limit_C_default = 4.2
        self.filterDropOut_spikeRoR_dRoR_limit_F_default = 7

        # variables

        self.filterDropOuts = True
        self.filterDropOut_tmin = self.filterDropOut_tmin_F_default
        self.filterDropOut_tmax = self.filterDropOut_tmax_F_default

	# the limit of additional RoR in temp/sec compared to previous readings
        self.filterDropOut_spikeRoR_dRoR_limit = self.filterDropOut_spikeRoR_dRoR_limit_F_default

	self.minmaxLimits = False
        self.dropSpikes = False

        self.swapETBT = False

        ###########################         wheel graph variables     ################################
        self.wheelflag = False
        #data containers for wheel
        self.wheelnames,self.segmentlengths,self.segmentsalpha,self.wheellabelparent,self.wheelcolor = [],[],[],[],[]

        #crerate starting wheel
        wheels = [4,6,12,50]
        for i in range(len(wheels)):
            w,a,c,co = [],[],[],[]
            for x in range(wheels[i]):
                w.append("W%i %i"%(i+1,x+1))
                a.append(0.3)
                c.append(0)
                color = QColor()
                color.setHsv((360/wheels[i])*x,255,255,255)
                co.append(str(color.name()))

            self.wheelnames.append(w)
            self.segmentsalpha.append(a)
            self.segmentlengths.append([100./len(self.wheelnames[i])]*len(self.wheelnames[i]))
            self.wheellabelparent.append(c)
            self.wheelcolor.append(co)

        #properties
        #store radius of each circle as percentage(sum of all must at all times add up to 100.0%)
        self.wradii = [25.,20.,20.,35.]
        #starting angle for each circle (0-360).
        self.startangle = [0,0,0,0]
        #text projection: 0 = Flat, 1 = perpendicular to center, 2 = radial from center
        self.projection = [0,1,1,2]
        self.wheeltextsize = [10,10,10,10]
        self.wheelcolorpattern = 0                  #pattern
        self.wheeledge = .02                        #overlaping decorative edge
        self.wheellinewidth = 1
        self.wheellinecolor = "black"               #initial color of lines
        self.wheelconnections = [0,0,0]
        self.wheelx,self.wheelz = 0,0                   #temp variables to pass index values
        self.wheellocationx,self.wheellocationz = 0.,0.  #temp vars to pass mouse location (angleX+radiusZ)
        self.wheelaspect = 1.0

        self.samplingsemaphore = QSemaphore(1)
        self.messagesemaphore = QSemaphore(1)
        self.errorsemaphore = QSemaphore(1)
        self.serialsemaphore = QSemaphore(1)

        #flag to plot cross lines from mouse
        self.crossmarker = False
        self.crossmouseid = 0

        #########  temporary serial variables
        #temporary storage to pass values. Holds extra T3 and T4 values for center 309
        self.extra309T3 = -1
        self.extra309T4 = -1
        self.extra309TX = 0.
        
        #temporary storage to pass values. Holds all values retrieved from a Hottop roaster
        self.hottop_ET = -1
        self.hottop_BT = -1
        self.hottop_HEATER = 0 # 0-100
        self.hottop_MAIN_FAN = 0 # 0-10 (!)
        self.hottop_TX = 0.
        
        #temporary storage to pass values. Holds extra T3 and T4 values for MODBUS connected devices
        self.extraMODBUSt3 = -1
        self.extraMODBUSt4 = -1
        self.extraMODBUStx = 0.

        #used by extra device +ArduinoTC4_XX to pass values
        self.extraArduinoT1 = 0.
        self.extraArduinoT2 = 0.
        self.extraArduinoT3 = 0. # heater duty %
        self.extraArduinoT4 = 0. # fan duty %
        self.extraArduinoT5 = 0. # SV
        self.extraArduinoT6 = 0. # TC4 internal ambient temperature
        
        #used by extra device +Program_34 and +Program_56 to pass values
        self.program_t3 = -1
        self.program_t4 = -1
        self.program_t5 = -1
        self.program_t6 = -1

        #temporary storage to pass values. Holds the power % ducty cycle of Fuji PIDs  and ET-BT
        self.dutycycle = 0.
        self.dutycycleTX = 0.
        self.currentpidsv = 0.

        self.linecount = None # linecount cache for resetlines(); has to be reseted if visibility of ET/BT or extra lines or background ET/BT changes
        self.deltalinecount = None # deltalinecoutn cache for resetdeltalines(); has to be reseted if visibility of deltaET/deltaBT or background deltaET/deltaBT

        #variables to organize the delayed update of the backgrounds for bitblitting
        self.ax_background = None
        self.redrawEnabled = True
        self._resizeTimer = QTimer()
        self._resizeTimer.timeout.connect(self.updateBackground)
        self.delayTimeout = 100
        
        # flag to toggle between Temp and RoR scale of xy-display
        self.fmt_data_RoR = False

    #NOTE: empty Figure is initialy drawn at the end of aw.settingsload()
    #################################    FUNCTIONS    ###################################
    #####################################################################################
    
    # update the aw.qmc.deltaspan from the given sampling interval and aw.qmc.deltasamples
    # interval is expected in seconds (either from the profile on load or from the sampling interval set for recording)
    def updateDeltaSamples(self):
        if self.flagstart or self.profile_sampling_interval == None:
            interval = self.delay
        else:
            interval = self.profile_sampling_interval
        self.deltasamples = max(1,self.deltaspan / int(interval))
    
    # hack to make self.ax receive onPick events although it is drawn behind self.delta_ax
    def draw(self):
        if self.ax and self.delta_ax: #self.designerflag and self.ax and self.delta_ax:
            self.ax.set_zorder(0)
            self.delta_ax.set_zorder(0.1)
        FigureCanvas.draw(self)
        if self.ax and self.delta_ax: #self.designerflag and self.ax and self.delta_ax:
            self.ax.set_zorder(0.1)
            self.delta_ax.set_zorder(0)
    
    # returns the prefix of length l of s and adds eclipse
    def abbrevString(self,s,l):
        if len(s) > l:
            return s[:l-1] + "..."
        else:
            return s

    def delayedUpdateBackground(self):
        self._resizeTimer.start(self.delayTimeout) # triggeres the updateBackground after timeout

    def resizeEvent(self,event):
        self.redrawEnabled = False
        super(tgraphcanvas,self).resizeEvent(event)
        self.delayedUpdateBackground()

    def updateBackground(self):
        self._resizeTimer.stop()
        if not self.designerflag:
            self.redrawEnabled = False
            self.resetlinecountcaches() # ensure that the line counts are up to date
            self.resetlines() # get rid of HUD, projection and cross lines
            self.resetdeltalines() # just in case
            self.fig.canvas.draw()
            self.ax_background = self.fig.canvas.copy_from_bbox(aw.qmc.ax.bbox)
            self.redrawEnabled = True

    def getetypes(self):
        if len(self.etypes) == 4:
            self.etypes.append("--")
        return self.etypes

    def etypesf(self, i):
        if len(self.etypes) == 4:
            self.etypes.append("--")
        return self.etypes[i]

    def Betypesf(self, i):
        if len(self.Betypes) == 4:
            self.Betypes.append("--")
        return self.Betypes[i]

    def ambientTempSourceAvg(self):
        res = None
        if self.ambientTempSource:
            start = 0
            end = len(aw.qmc.temp1) - 1
            if self.timeindex[1] > -1: # CHARGE
                start = self.timeindex[1]
            if self.timeindex[6] > -1: # DROP
                end = self.timeindex[6]
            if self.ambientTempSource == 1: # from ET
                res = numpy.mean(aw.qmc.temp1[start:end])
            elif self.ambientTempSource == 2: # from BT
                res = numpy.mean(aw.qmc.temp2[start:end])
            elif self.ambientTempSource > 2 and ((self.ambientTempSource - 3) < (2*len(aw.qmc.extradevices))): 
                # from an extra device
                if (self.ambientTempSource)%2==0:
                    res = numpy.mean(aw.qmc.extratemp2[(self.ambientTempSource - 3)//2][start:end])
                else:
                    res = numpy.mean(aw.qmc.extratemp1[(self.ambientTempSource - 3)//2][start:end])
        if res:
            res = aw.float2float(res)
        return res

    def updateAmbientTemp(self):
        if aw.qmc.device == 34: # Phidget 1048 (use internal temp)
            try:
                if aw.ser.PhidgetTemperatureSensor != None and aw.ser.PhidgetTemperatureSensor.isAttached():
                    t = aw.ser.PhidgetTemperatureSensor.getAmbientTemperature()
                    if aw.qmc.mode == "F":
                        aw.qmc.ambientTemp = aw.float2float(aw.qmc.fromCtoF(t))
                    else:
                        aw.qmc.ambientTemp = aw.float2float(t)
            except:
                pass
        else:
            res = aw.qmc.ambientTempSourceAvg()
            if res != None and (isinstance(res, float) or isinstance(res, int)) and not math.isnan(res):
                aw.qmc.ambientTemp = aw.float2float(float(res))

    # eventsvalues maps the given number v to a string to be displayed to the user as special event value
    # v is expected to be float value of range [0.0-10.0]
    # negative values are mapped to -1
    def eventsInternal2ExternalValue(self,v):
        if v == None:
            return -1
        else:
            if v < 1:
                return -1
        return int(round(v*10)) - 10

    # eventsvalues maps the given number v to a string to be displayed to the user as special event value
    # v is expected to be float value of range [0-10]
    # negative values are mapped to ""
    # 0.1 to "1"
    # ..
    # 1.0 to "10"
    # .. 
    # 10.0 to "100"
    def eventsvalues(self,v):
        value = self.eventsInternal2ExternalValue(v)
        if value < 0:
            return ""
        else:
            return u(value)

    # 100.0 to "10" and 10.1 to "1"
    def eventsvaluesShort(self,v):
        value = v*10. - 10.
        if value < 0:
            return ""
        else:
            if aw.qmc.LCDdecimalplaces:
                return u(int(round(value)))
            else:
                return u(int(round(value / 10)))

    # the inverse to eventsvalues above (string -> value)
    def str2eventsvalue(self,s):
        st = s.strip()
        if st == None or len(st) == 0:
            return -1
        else:
            return aw.float2float(float(st)/10. + 1.0)

    def onpick(self,event):
        if event.artist in [self.l_backgroundeventtype1dots,self.l_backgroundeventtype2dots,self.l_backgroundeventtype3dots,self.l_backgroundeventtype4dots]:
            timex = self.backgroundtime2index(event.artist.get_xdata()[event.ind][0])
            idxs = []
            for i in range(len(self.backgroundEvents)):
                if self.backgroundEvents[i] == timex:
                    idxs.append(i)
            message = u("")
            for i in idxs:
                if len(message) != 0:
                    message += u(", ")
                else:
                    message += u("Background: ")
                message += u(self.Betypesf(self.backgroundEtypes[i])) + " = " + self.eventsvalues(self.backgroundEvalues[i]) 
                if self.backgroundEStrings[i] and self.backgroundEStrings[i]!="":
                    message += u(" (") + u(self.backgroundEStrings[i]) + u(")")
            if len(message) != 0:
                message += u(" @ ") + self.stringfromseconds(event.artist.get_xdata()[event.ind][0])
                aw.sendmessage(message,append=False)
        else:
            timex = self.time2index(event.artist.get_xdata()[event.ind][0])
            idxs = []
            for i in range(len(self.specialevents)):
                if self.specialevents[i] == timex:
                    idxs.append(i)
            message = u("")
            for i in idxs:
                if len(message) != 0:
                    message += u(", ")
                message += u(self.etypesf(self.specialeventstype[i])) + " = " + self.eventsvalues(self.specialeventsvalue[i])
                if self.specialeventsStrings[i] and self.specialeventsStrings[i]!="":
                    message += u(" (") + u(self.specialeventsStrings[i]) + u(")")
            if len(message) != 0:
                message += u(" @ ") + self.stringfromseconds(event.artist.get_xdata()[event.ind][0])
                aw.sendmessage(message,append=False)

    def onclick(self,event):
        try:
            if event.inaxes == None and not aw.qmc.flagstart and not aw.qmc.flagon and event.button==3:
                aw.qmc.statisticsmode = (aw.qmc.statisticsmode + 1)%2
                aw.qmc.writecharacteristics()
                aw.qmc.fig.canvas.draw()
            elif event.button==3 and event.inaxes and not self.designerflag and not self.wheelflag:# and not self.flagon:
                timex = self.time2index(event.xdata)
                if timex > 0:
                    menu = QMenu(self) 
                    # populate menu
                    ac = QAction(menu)
                    if self.timeindex[0] > -1:
                        ac.setText(u(QApplication.translate("Label", "at")) + u(" ") + self.stringfromseconds(event.xdata - self.timex[self.timeindex[0]]))
                    else:
                        ac.setText(u(QApplication.translate("Label", "at")) + u(" ") + self.stringfromseconds(event.xdata))
                    ac.setEnabled(False)
                    menu.addAction(ac)
                    for k in [(u(QApplication.translate("Label","CHARGE")),0),
                              (u(QApplication.translate("Label","DRY END")),1),
                              (u(QApplication.translate("Label","FC START")),2),
                              (u(QApplication.translate("Label","FC END")),3),
                              (u(QApplication.translate("Label","SC START")),4),
                              (u(QApplication.translate("Label","SC END")),5),
                              (u(QApplication.translate("Label","DROP")),6),
                              (u(QApplication.translate("Label","COOL")),7)]:
                        idx_before = idx_after = 0
                        for i in range(k[1]):
                            if self.timeindex[i] and self.timeindex[i] != -1:
                                idx_before = self.timeindex[i]
                        for i in range(6,k[1],-1) :
                            if self.timeindex[i] and self.timeindex[i] != -1:
                                idx_after = self.timeindex[i]
                        if ((not idx_before) or timex > idx_before) and ((not idx_after) or timex < idx_after):
                            ac = QAction(menu)
                            ac.key = (k[1],timex)
                            ac.setText(" " + k[0])
                            menu.addAction(ac)
                    # add user EVENT entry
                    ac = QAction(menu)
                    ac.setText(u(" ") + u(QApplication.translate("Label", "EVENT")))
                    ac.key = (-1,timex)
                    menu.addAction(ac)
                    # show menu
                    menu.triggered.connect(self.event_popup_action)
                    menu.popup(QCursor.pos())
        except Exception as e:
	#traceback.print_exc(file=sys.stdout)
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " onclick() {0}").format(str(e)),exc_tb.tb_lineno)

    def event_popup_action(self,action):
        if action.key[0] >= 0:
            self.timeindex[action.key[0]] = action.key[1]
            if action.key[0] == 0: # CHARGE
                # realign to background
                aw.qmc.timealign(redraw=True,recompute=False) # redraws at least the canvas if redraw=True, so no need here for doing another canvas.draw()
        else:
            # add a special event at the current timepoint
            self.specialevents.append(action.key[1]) # absolut time index
            self.specialeventstype.append(4) # "--"
            self.specialeventsStrings.append("")
            self.specialeventsvalue.append(0)
        aw.qmc.safesaveflag = True
        self.redraw()
        
    def updateWebLCDs(self,bt=None,et=None,time=None,alertTitle=None,alertText=None,alertTimeout=None):
        try:
            url = "http://127.0.0.1:" + str(aw.WebLCDsPort) + "/send"
            headers = {'content-type': 'application/json'}
            payload = {'data': {}}
            if bt != None:
                payload['data']['bt'] = bt
            if et != None:
                payload['data']['et'] = et
            if time != None:
                payload['data']['time'] = time
            if alertText != None:
                payload['alert'] = {}
                payload['alert']['text'] = alertText
                if alertTitle:
                    payload['alert']['title'] = alertTitle
                if alertTimeout:
                    payload['alert']['timeout'] = alertTimeout
            requests.post(url, data=json.dumps(payload),headers=headers,timeout=0.1)
        except:
            pass
            
    def updateLargeLCDs(self,bt=None,et=None,time=None):
        try:
            if time:
                aw.largeLCDs_dialog.lcd1.display(time)
            if et:
                aw.largeLCDs_dialog.lcd2.display(et)
            if bt:
                aw.largeLCDs_dialog.lcd3.display(bt)
        except:
            pass

    # runs from GUI thread.
    # this function is called by a signal at the end of the thread sample()
    # during sample, updates to GUI widgets or anything GUI must be done here (never from thread)
    def updategraphics(self):
        try:
            if self.flagon:
                if len(self.timex):
                    if self.LCDdecimalplaces:
                        lcdformat = "%.1f"
                    else:
                        lcdformat = "%.0f"
                    if -100 < self.temp1[-1] < 1000:
                        aw.lcd2.display(lcdformat%float(self.temp1[-1]))            # ET
                    else:
                        aw.lcd2.display("--")
                    if -100 < self.temp2[-1] < 1000:
                        aw.lcd3.display(lcdformat%float(self.temp2[-1]))            # BT
                    else:
                        aw.lcd3.display("--")
                    if -100 < self.rateofchange1 < 1000:
                        aw.lcd4.display(lcdformat%float(self.rateofchange1))        # rate of change MET (degress per minute)
                    else:
                        aw.lcd4.display("--")
                    if -100 < self.rateofchange2 < 1000:
                        aw.lcd5.display(lcdformat%float(self.rateofchange2))        # rate of change BT (degrees per minute)
                    else:
                        aw.lcd5.display("--")
                    
                    if self.device == 0 or self.device == 26:         #extra LCDs for Fuji or DTA pid  
                        aw.lcd6.display(self.currentpidsv)
                        aw.lcd7.display(self.dutycycle)

                    ndev = len(self.extradevices)
                    for i in range(ndev):
                        if i < aw.nLCDS:
                            if self.extratemp1[i]:
                                if -100 < self.extratemp1[i][-1] < 1000:
                                    aw.extraLCD1[i].display(lcdformat%float(self.extratemp1[i][-1]))
                                else:
                                    aw.extraLCD1[i].display("--")
                            if self.extratemp2[i]:
                                if -100 < self.extratemp2[i][-1] < 1000:
                                    aw.extraLCD2[i].display(lcdformat%float(self.extratemp2[i][-1]))
                                else:
                                    aw.extraLCD2[i].display("--")
                                    
                    # update large LCDs (incl. Web LCDs)
                    timestr = None
                    if not self.flagstart:
                        timestr = "00:00"
                    digits = (1 if aw.qmc.LCDdecimalplaces else 0)
                    btstr = str(aw.float2float(self.temp2[-1],digits))
                    etstr = str(aw.float2float(self.temp1[-1],digits))
                    if aw.WebLCDs:                       
                        self.updateWebLCDs(bt=btstr,et=etstr,time=timestr)
                    if aw.largeLCDs_dialog:
                        self.updateLargeLCDs(bt=btstr,et=etstr,time=timestr)

                if self.flagstart:          
                    if aw.qmc.patheffects:
                        rcParams['path.effects'] = [PathEffects.withStroke(linewidth=aw.qmc.patheffects, foreground="w")]
                    else:
                        rcParams['path.effects'] = []

                    ##### updated canvas
                    if self.redrawEnabled:
                        if self.ax_background:
                            self.fig.canvas.restore_region(self.ax_background)
                            # draw eventtypes
                            if self.eventsshowflag and self.eventsGraphflag == 2:
                                aw.qmc.ax.draw_artist(self.l_eventtype1dots)
                                aw.qmc.ax.draw_artist(self.l_eventtype2dots)
                                aw.qmc.ax.draw_artist(self.l_eventtype3dots)
                                aw.qmc.ax.draw_artist(self.l_eventtype4dots)
                            # draw extra curves
                            xtra_dev_lines1 = 0
                            xtra_dev_lines2 = 0
                            for i in range(min(len(aw.extraCurveVisibility1),len(aw.extraCurveVisibility1),len(self.extratimex),len(self.extratemp1),len(self.extradevicecolor1),len(self.extraname1),len(self.extratemp2),len(self.extradevicecolor2),len(self.extraname2))):
                                if aw.extraCurveVisibility1[i] and len(self.extratemp1lines) > xtra_dev_lines1:
                                    aw.qmc.ax.draw_artist(self.extratemp1lines[xtra_dev_lines1])
                                    xtra_dev_lines1 = xtra_dev_lines1 + 1
                                if aw.extraCurveVisibility2[i] and len(self.extratemp2lines) > xtra_dev_lines2:
                                    aw.qmc.ax.draw_artist(self.extratemp2lines[xtra_dev_lines2])
                                    xtra_dev_lines2 = xtra_dev_lines2 + 1
                            # draw ET
                            if aw.qmc.ETcurve:
                                aw.qmc.ax.draw_artist(self.l_temp1)
                            # draw BT
                            if aw.qmc.BTcurve:
                                aw.qmc.ax.draw_artist(self.l_temp2)
                             

                            if aw.qmc.device == 18 and aw.qmc.l_timeline != None: # not NONE device
                                aw.qmc.ax.draw_artist(aw.qmc.l_timeline)

                            if aw.qmc.projectFlag:
                                if self.l_BTprojection != None:
                                    aw.qmc.ax.draw_artist(self.l_BTprojection)
                                if self.l_ETprojection != None:
                                    aw.qmc.ax.draw_artist(self.l_ETprojection)
                            # draw delta lines
                            if self.DeltaETflag and self.l_delta1 != None:
                                aw.qmc.delta_ax.draw_artist(self.l_delta1)
                            if self.DeltaBTflag and self.l_delta2 != None:
                                aw.qmc.delta_ax.draw_artist(self.l_delta2)

                            self.fig.canvas.blit(aw.qmc.ax.bbox)
                        else:
                            # we do not have a background to bitblit, so do a full redraw
                            #self.fig.canvas.draw()
                            self.delayedUpdateBackground() # does the canvas draw, but also fills the ax_background cache
                    #####

                    #update phase lcds
                    if aw.qmc.phasesLCDflag:
                        aw.updatePhasesLCDs()

                    #check if HUD is ON (done after self.fig.canvas.draw())
                    if self.HUDflag:
                        aw.showHUD[aw.HUDfunction]()

                    #auto mark CHARGE/TP/DRY/FCs/DROP
                    if self.autoChargeIdx and aw.qmc.timeindex[0] < 0:
                        self.markCharge() # we do not reset the autoChargeIdx to avoid another trigger
                    if self.autoTPIdx != 0:
                        self.autoTPIdx = 0
                        self.markTP()
                    if self.autoDryIdx != 0:
                        self.autoDryIdx = 0
                        self.markDryEnd()
                    if self.autoFCsIdx != 0:
                        self.autoFCsIdx = 0
                        self.mark1Cstart()
                    if self.autoDropIdx != 0 and aw.qmc.timeindex[0] > -1 and not aw.qmc.timeindex[6]:
                        self.markDrop() # we do not reset the autoDropIdx to avoid another trigger

                #check triggered alarms
                if self.temporaryalarmflag > -3:
                    i = self.temporaryalarmflag  # reset self.temporaryalarmflag before calling alarm
                    self.temporaryalarmflag = -3 # self.setalarm(i) can take longer to run than the sampling interval 
                    self.setalarm(i)
                    
                #check quantified events
                for el in self.quantifiedEvent:
                    aw.moveslider(el[0],el[1])
                    if aw.qmc.flagstart:
                        value = aw.float2float((el[1] + 10.0) / 10.0)
                        aw.qmc.EventRecordAction(extraevent = 1,eventtype=el[0],eventvalue=value,eventdescription=u("Q"))
                self.quantifiedEvent = []

                    

        except Exception as e:
            self.flagon = False
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " updategraphics() {0}").format(str(e)),exc_tb.tb_lineno)

    def updateLCDtime(self):
        if self.flagon and self.flagstart:
            tx = self.timeclock.elapsed()/1000.
            if self.timeindex[0] != -1:
                ts = tx - self.timex[self.timeindex[0]]
            else:
                ts = tx
            nextreading = 1000. - 1000.*(tx%1.)

            # if more than max cool (from statistics) past DROP and not yet COOLend turn the time LCD red:
            if aw.qmc.timeindex[0]!=-1 and aw.qmc.timeindex[6] and not aw.qmc.timeindex[7] and (tx - aw.qmc.timex[aw.qmc.timeindex[6]]) > aw.qmc.statisticsconditions[7]:
                aw.lcd1.setStyleSheet("QLCDNumber { color: %s; background-color: %s;}"%("red",aw.lcdpaletteB["timer"]))

            timestr = self.stringfromseconds(int(round(ts)))
            aw.lcd1.display(u(timestr))
            
            # update connected WebLCDs
            if aw.WebLCDs:
                self.updateWebLCDs(time=timestr)
            if aw.largeLCDs_dialog:
                self.updateLargeLCDs(time=timestr)
            
            QTimer.singleShot(nextreading,self.updateLCDtime)

    def toggleHUD(self):
        aw.soundpop()
        #OFF
        if self.HUDflag:
            self.HUDflag = False
            aw.HUD.clear()
            aw.button_18.setStyleSheet("QPushButton { background-color: #b5baff }")
            aw.stack.setCurrentIndex(0)
            self.resetlines()
            aw.sendmessage(QApplication.translate("Message","HUD OFF", None))
            
        #ON
        else:
            #load
            img = self.grab()
            aw.HUD.setPixmap(img)
            
            self.HUDflag = True
            aw.button_18.setStyleSheet("QPushButton { background-color: #60ffed }")
            aw.stack.setCurrentIndex(1)
            aw.sendmessage(QApplication.translate("Message","HUD ON", None))


    # redraws at least the canvas if redraw=True, if FCs=True align to FCs if set, otherwise CHARGE (if set)
    def timealign(self,redraw=True,recompute=False,FCs=False):
        try:
            ptime = None
            btime = None
            if FCs and self.timeindexB[2] and self.timeindex[2]:
                ptime = self.timex[self.timeindex[2]]
                btime = self.timeB[self.timeindexB[2]]
            elif self.timeindexB[0] != -1 and self.timeindex[0] != -1:
                ptime = self.timex[self.timeindex[0]]
                btime = self.timeB[self.timeindexB[0]]
            if ptime and btime:
                difference = ptime - btime
                if difference > 0:
                    self.movebackground("right",abs(difference))
                    self.backmoveflag = 0
                    if redraw:
                        self.redraw(recompute)
                elif difference < 0:
                    self.movebackground("left",abs(difference))
                    self.backmoveflag = 0
                    if redraw:
                        self.redraw(recompute)
                elif redraw and not FCs: # ensure that we at least redraw the canvas
                    self.delayedUpdateBackground()
            elif redraw and not FCs: # only on aligning with CHARGE we redraw even if nothing is moved to redraw the time axis
                    self.delayedUpdateBackground()
        except Exception as ex:
	    #traceback.print_exc(file=sys.stdout)
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " timealign() {0}").format(str(ex)),exc_tb.tb_lineno)

    def lenaxlines(self):
        active_curves = len(self.extratimex)
        curves = aw.extraCurveVisibility1[0:active_curves] + aw.extraCurveVisibility2[0:active_curves] + [aw.qmc.ETcurve,aw.qmc.BTcurve]
        c = curves.count(True)
        if aw.qmc.background:
            c += 2
            if aw.qmc.xtcurveidx > 0: # 3rd background curve set?
                idx3 = aw.qmc.xtcurveidx - 1
                n3 = idx3 // 2
                if len(self.stemp1BX) > n3 and len(self.extratimexB) > n3:
                    c += 1
            if aw.qmc.backgroundeventsflag and aw.qmc.eventsGraphflag == 2:
                c += 4
        if aw.qmc.eventsshowflag and aw.qmc.eventsGraphflag == 2:
            c += 4
        return c

    def lendeltaaxlines(self):
        linecount = 0 
        if self.DeltaETflag:
            linecount += 1
        if  self.DeltaBTflag:
            linecount += 1
        if aw.qmc.background:
            if self.DeltaETBflag:
                linecount += 1
            if self.DeltaBTBflag:
                linecount += 1
        return linecount

    def resetlinecountcaches(self):
        aw.qmc.linecount = None
        aw.qmc.deltalinecount = None

    def resetlines(self):
        #note: delta curves are now in self.delta_ax and have been removed from the count of resetlines()
        if self.linecount == None:
            self.linecount = self.lenaxlines()
        self.ax.lines = self.ax.lines[0:self.linecount]

    def resetdeltalines(self):
        if self.deltalinecount == None:
            self.deltalinecount = self.lendeltaaxlines()
        if self.delta_ax:
            self.delta_ax.lines = self.delta_ax.lines[0:self.deltalinecount]

    def setalarm(self,alarmnumber):
        self.alarmstate[alarmnumber] = 1    #turn off flag as it has been read
        aw.sendmessage(QApplication.translate("Message","Alarm {0} triggered", None).format(alarmnumber + 1))
        if len(self.alarmbeep) > alarmnumber and self.alarmbeep[alarmnumber]:
            QApplication.beep()
        try:
            if self.alarmaction[alarmnumber] == 0:
                # alarm popup message
                #QMessageBox.information(self,QApplication.translate("Message", "Alarm notice",None),self.alarmstrings[alarmnumber])
                # alarm popup message with 10sec timeout
                amb = ArtisanMessageBox(self,QApplication.translate("Message", "Alarm notice",None),u(self.alarmstrings[alarmnumber]),timeout=10)
                amb.show()
                #send alarm also to connected WebLCDs clients
                if aw.WebLCDs and aw.WebLCDsAlerts:
                    aw.qmc.updateWebLCDs(alertText=u(self.alarmstrings[alarmnumber]),alertTimeout=10)
            elif self.alarmaction[alarmnumber] == 1:
                # alarm call program
                fname = u(self.alarmstrings[alarmnumber])
		# take care, the QDir().current() directory changes with loads and saves                
		#QDesktopServices.openUrl(QUrl("file:///" + u(QDir().current().absolutePath()) + "/" + fname, QUrl.TolerantMode))
                if platf == 'Windows':
                    f = u("file:///") + u(QApplication.applicationDirPath()) + "/" + u(fname)
                    res = QDesktopServices.openUrl(QUrl(f, QUrl.TolerantMode))
                else:
                    # MacOS X: script is expected to sit next to the Artisan.app or being specified with its full path
                    # Linux: script is expected to sit next to the artisan binary or being specified with its full path
                    #
                    # to get the effect of speaking alarms a text containing the following two lines called "say.sh" could do
                    #                #!/bin/sh
                    #                say "Hello" &
                    # don't forget to do
                    #                # cd 
                    #                # chmod +x say.sh
                    #
                    # alternatively use "say $@ &" as command and send text strings along
                    # Voices:
                    #  -v Alex (male english)
                    #  -v Viki (female english)
                    #  -v Victoria (female english)
                    #  -v Yannick (male german)
                    #  -v Anna (female german)
                    #  -v Paolo (male italian)
                    #  -v Silvia (female italian)
                    aw.call_prog_with_args(fname)
                    res = True
                if res:
                    aw.sendmessage(QApplication.translate("Message","Alarm is calling: {0}",None).format(u(self.alarmstrings[alarmnumber])))
                else:
                    aw.qmc.adderror(QApplication.translate("Message","Calling alarm failed on {0}",None).format(f))
            elif self.alarmaction[alarmnumber] == 2:
                # alarm event button
                button_number = None
                try:
                    button_number = int(str(self.alarmstrings[alarmnumber])) - 1 # the event buttons presented to the user are numbered from 1 on
                except:
                    aw.sendmessage(QApplication.translate("Message","Alarm trigger button error, description '{0}' not a number",None).format(u(self.alarmstrings[alarmnumber])))
                if button_number != None:
                    if button_number > -1 and button_number < len(aw.buttonlist):
                        aw.recordextraevent(button_number)
            elif self.alarmaction[alarmnumber] in [3,4,5,6]:
                # alarm slider 1-4
                slidernr = None
                try:
                    slidervalue = max(0,min(100,int(str(self.alarmstrings[alarmnumber]))))
                    if slidervalue < 0 or slidervalue > 100:
                        raise Exception()
                    if self.alarmaction[alarmnumber] == 3:
                        slidernr = 0
                    elif self.alarmaction[alarmnumber] == 4:
                        slidernr = 1
                    elif self.alarmaction[alarmnumber] == 5:
                        slidernr = 2
                    elif self.alarmaction[alarmnumber] == 6:
                        slidernr = 3
                    if slidernr != None:
                        aw.moveslider(slidernr,slidervalue)
                        if aw.qmc.flagstart:
                            value = aw.float2float((slidervalue + 10.0) / 10.0)
                            aw.qmc.EventRecordAction(extraevent = 1,eventtype=slidernr,eventvalue=value,eventdescription=str("A%d (S%d)"%(alarmnumber,slidernr)))
                        aw.fireslideraction(slidernr)
                except Exception as e:
                    _, _, exc_tb = sys.exc_info()
                    aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " setalarm() {0}").format(str(e)),exc_tb.tb_lineno)
                    aw.sendmessage(QApplication.translate("Message","Alarm trigger slider error, description '{0}' not a valid number [0-100]",None).format(u(self.alarmstrings[alarmnumber])))
                    
            elif self.alarmaction[alarmnumber] == 7:
                # START
                if aw.button_2.isEnabled():
                    aw.qmc.ToggleRecorder()
            elif self.alarmaction[alarmnumber] == 8:
                # DRY
                if aw.button_19.isEnabled():
                    aw.qmc.markDryEnd()
            elif self.alarmaction[alarmnumber] == 9:
                # FCs
                if aw.button_3.isEnabled():
                    aw.qmc.mark1Cstart()
            elif self.alarmaction[alarmnumber] == 10:
                # FCe
                if aw.button_4.isEnabled():
                    aw.qmc.mark1Cend()
            elif self.alarmaction[alarmnumber] == 11:
                # SCs
                if aw.button_5.isEnabled():
                    aw.qmc.mark2Cstart()
            elif self.alarmaction[alarmnumber] == 12:
                # SCe
                if aw.button_6.isEnabled():
                    aw.qmc.mark2Cend()
            elif self.alarmaction[alarmnumber] == 13:
                # DROP
                if aw.button_9.isEnabled():
                    aw.qmc.markDrop()
            elif self.alarmaction[alarmnumber] == 14:
                # COOL
                if aw.button_20.isEnabled():
                    aw.qmc.markCoolEnd()
            elif self.alarmaction[alarmnumber] == 15:
                # OFF
                if aw.button_1.isEnabled():
                    aw.qmc.ToggleMonitor()
            elif self.alarmaction[alarmnumber] == 16:
                # CHARGE
                if aw.qmc.timeindex[0] > -1:
                    aw.qmc.autoChargeIdx = len(aw.qmc.timex)
            elif self.alarmaction[alarmnumber] == 17:
                # RampSoak ON
                if aw.qmc.device == 0 and aw.fujipid: # FUJI PID
                    aw.fujipid.setrampsoak(1)
                elif aw.qmc.device == 19 and aw.arduino: # Arduino TC4
                    aw.arduino.svMode = 1
                    aw.arduino.pidOn()
            elif self.alarmaction[alarmnumber] == 18:
                # RampSoak OFF
                if aw.qmc.device == 0 and aw.fujipid: # FUJI PID
                    aw.fujipid.setrampsoak(0)
                elif aw.qmc.device == 19 and aw.arduino: # Arduino TC4
                    aw.arduino.svMode = 0
                    aw.arduino.pidOff()
            elif self.alarmaction[alarmnumber] == 19:
                # PID ON
                if aw.qmc.device == 0 and aw.fujipid: # FUJI PID
                    aw.fujipid.setONOFFstandby(0)
                elif aw.qmc.device == 19 and aw.arduino: # Arduino TC4
                    aw.arduino.pidOn()
            elif self.alarmaction[alarmnumber] == 20:
                # PID OFF
                if aw.qmc.device == 0 and aw.fujipid: # FUJI PID
                    aw.fujipid.setONOFFstandby(1)
                elif aw.qmc.device == 19 and aw.arduino: # Arduino TC4
                    aw.arduino.pidOff()
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " setalarm() {0}").format(str(ex)),exc_tb.tb_lineno)

    def playbackevent(self):
        try:
            #needed when using device NONE
            if len(self.timex):
                #find time distances
                for i in range(len(self.backgroundEvents)):
                    timed = int(self.timeB[self.backgroundEvents[i]] - self.timeclock.elapsed()/1000.)
                    if  timed > 0 and timed < self.detectBackgroundEventTime:
                        #write text message
                        message = "> " +  self.stringfromseconds(timed) + " [" + u(self.Betypesf(self.backgroundEtypes[i]))
                        message += "] [" + self.eventsvalues(self.backgroundEvalues[i]) + "] : " + self.backgroundEStrings[i]
                        #rotate colors to get attention
                        if timed%2:
                            aw.messagelabel.setStyleSheet("background-color:'transparent';")
                        else:
                            aw.messagelabel.setStyleSheet("background-color:'yellow';")
                            
                        aw.sendmessage(message)
                        break

                    elif timed == 0:
                        #for devices that support automatic roaster control
                        #if Fuji PID
                        if self.device == 0:

                            # COMMAND SET STRINGS
                            #  (adjust the SV PID to the float VALUE1)
                            # SETRS::VALUE1::VALUE2::VALUE3  (VALUE1 = target SV. float VALUE2 = time to reach int VALUE 1 (ramp) in minutes. int VALUE3 = hold (soak) time in minutes)

                            # IMPORTANT: VALUES are for controlling ET only (not BT). The PID should control ET not BT. The PID should be connected to ET only.
                            # Therefore, these values don't reflect a BT defined profile. They define an ET profile.
                            # They reflect the changes in ET, which indirectly define BT after some time lag

                            # There are two ways to record a roast. One is by changing Set Values (SV) during the roast,
                            # the other is by using ramp/soaks segments (RS). 
                            # Examples:

                            # SETSV::560.3           sets an SV value of 560.3F in the PID at the time of the recorded background event

                            # SETRS::440.2::2::0     starts Ramp Soak mode so that it reaches 440.2F in 2 minutes and holds (soaks) 440.2F for zero minutes

                            # SETRS::300.0::2::3::SETRS::540.0::6::0::SETRS::560.0::4::0::SETRS::560::0::0
                            #       this command has 4 comsecutive commands inside (4 segments)
                            #       1 SETRS::300.0::2::3 reach 300.0F in 2 minutes and hold it for 3 minutes (ie. total dry phase time = 5 minutes)
                            #       2 SETRS::540.0::6::0 then reach 540.0F in 6 minutes and hold it there 0 minutes (ie. total mid phase time = 6 minutes )
                            #       3 SETRS::560.0::4::0 then reach 560.0F in 4 minutes and hold it there 0 minutes (ie. total finish phase time = 4 minutes)
                            #       4 SETRS::560::0::0 then do nothing (because ramp time and soak time are both 0)
                            #       END ramp soak mode

                            if "::" in self.backgroundEStrings[i]:
                                aw.fujipid.replay(self.backgroundEStrings[i])
                                libtime.sleep(.5)  #avoid possible close times (rounding off)

                        #future Arduino
                        #if self.device == 19:

                    #delete message
                    else:
                        text = str(aw.messagelabel.text())
                        if len(text):
                            if text[0] == ">":
                                aw.sendmessage("")
                                aw.messagelabel.setStyleSheet("background-color:'transparent';")
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " playbackevent() {0}").format(str(ex)),exc_tb.tb_lineno)

    #make a projection of change of rate of BT on the graph
    def updateProjection(self):
        try:
            if len(aw.qmc.temp2) > 1:  #Need this because viewProjections use rate of change (two values needed)
                #self.resetlines()
                if self.timeindex[0] != -1:
                    starttime = self.timex[self.timeindex[0]]
                else:
                    starttime = 0
                if self.projectionmode == 0:
                    #calculate the temperature endpoint at endofx acording to the latest rate of change
                    if self.l_BTprojection != None:
                        if aw.qmc.BTcurve and len(aw.qmc.delta2) > 0 and aw.qmc.delta2[-1] != None:
                            BTprojection = self.temp2[-1] + aw.qmc.delta2[-1]*(self.endofx - self.timex[-1]+ starttime)/60.
                            #plot projections
                            self.l_BTprojection.set_data([self.timex[-1],self.endofx+starttime], [self.temp2[-1], BTprojection])
                        elif self.l_BTprojection:
                            self.l_BTprojection.set_data([],[])
                    if self.l_ETprojection != None:
                        if aw.qmc.ETcurve and len(aw.qmc.delta1) > 0 and aw.qmc.delta1[-1] != None:
                            ETprojection = self.temp1[-1] + aw.qmc.delta1[-1]*(self.endofx - self.timex[-1]+ starttime)/60.
                            self.l_ETprojection.set_data([self.timex[-1],self.endofx+starttime], [self.temp1[-1], ETprojection])
                        elif self.l_ETprojection:
                            self.l_ETprojection.set_data([],[])
                elif self.projectionmode == 1:
                    # Under Test. Newton's Law of Cooling
                    # This comes from the formula of heating (with ET) a cool (colder) object (BT).
                    # The difference equation (discrete with n elements) is: DeltaT = T(n+1) - T(n) = K*(ET - BT)
                    # The formula is a natural decay towards ET. The closer BT to ET, the smaller the change in DeltaT
                    # projectionconstant is a multiplier factor. It depends on
                    # 1 Damper or fan. Heating by convection is _faster_ than heat by conduction,
                    # 2 Mass of beans. The heavier the mass, the _slower_ the heating of BT
                    # 3 Gas or electric power: gas heats BT _faster_ because of hoter air.
                    # Every roaster will have a different constantN.

                    den = self.temp1[-1] - self.temp2[-1]  #denominator ETn - BTn 
                    if den > 0 and len(aw.qmc.delta2)>0 and aw.qmc.delta2[-1]: # if ETn > BTn
                        #get x points
                        xpoints = list(numpy.arange(self.timex[-1],self.endofx + starttime, self.delay/1000.))  #do two minutes after endofx (+ 120 seconds); why? now +starttime
                        #get y points
                        ypoints = [self.temp2[-1]]                                  # start initializing with last BT
                        K =  self.projectionconstant*aw.qmc.delta2[-1]/den/60.                 # multiplier
                        for _ in arange(len(xpoints)-1):                                     # create new points from previous points
                            DeltaT = K*(self.temp1[-1]- ypoints[-1])                        # DeltaT = K*(ET - BT)
                            ypoints.append(ypoints[-1]+ DeltaT)                             # add DeltaT to the next ypoint

                        #plot ET level (straight line) and BT curve
                        if self.l_ETprojection != None:
                            self.l_ETprojection.set_data([self.timex[-1],self.endofx + starttime], [self.temp1[-1], self.temp1[-1]])
                        if self.l_BTprojection != None:
                            self.l_BTprojection.set_data(xpoints, ypoints)
                    else:
                        if self.l_ETprojection:
                            self.l_ETprojection.set_data([],[])
                        if self.l_BTprojection:
                            self.l_BTprojection.set_data([],[])
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " updateProjection() {0}").format(str(ex)),exc_tb.tb_lineno)

    # this function is called from the HUD DLg and reports the linear time (straight line) it would take to reach a temperature target
    # acording to the current rate of change
    def getTargetTime(self):

        if self.rateofchange1 > 0:
            ETreachTime = (self.ETtarget - self.temp1[-1])/(self.rateofchange1/60.)
            ET2reachTime = (self.ET2target - self.temp1[-1])/(self.rateofchange1/60.)
        else:
            ETreachTime = -1
            ET2reachTime = -1
            
        if self.rateofchange2 > 0:
            BTreachTime = (self.BTtarget - self.temp2[-1])/(self.rateofchange2/60.)
            BT2reachTime = (self.BT2target - self.temp2[-1])/(self.rateofchange2/60.)
        else:
            BTreachTime = -1
            BT2reachTime = -1

        return ETreachTime, BTreachTime, ET2reachTime, BT2reachTime

    #single variable (x) mathematical expression evaluator for user defined functions to convert sensor readings from HHM28 multimeter
    #example: eval_math_expression("pow(e,2*cos(x))",.3) returns 6.75763501
    def eval_math_expression(self,mathexpression,x,tx):
        if mathexpression == None or len(mathexpression) == 0 or (x == -1 and "x" in mathexpression):
            return x

        #Since eval() is very powerful, for security reasons, only the functions in this dictionary will be allowed
        mathdictionary = {"min":min,"max":max,"sin":math.sin,"cos":math.cos,"tan":math.tan,"pow":math.pow,"exp":math.exp,"pi":math.pi,"e":math.e,
                          "abs":abs,"acos":math.acos,"asin":math.asin,"atan":math.atan,"log":math.log,"radians":math.radians,
                          "sqrt":math.sqrt,"atan2":math.atan,"degrees":math.degrees}
        try:
            x = float(x)
            mathdictionary['x'] = x         #add x to the math dictionary assigning the key "x" to its float value
            #add ETB and BTB (background ET and BT)
            etb = btb = 0
            try:
                if aw.qmc.background and ("ETB" in mathexpression or "BTB" in mathexpression):
                    #first compute closest index at that time point in the background data
                    j = aw.qmc.backgroundtime2index(tx)
                    etb = aw.qmc.temp1B[j]
                    btb = aw.qmc.temp2B[j]
            except:
                pass
            mathdictionary["ETB"] = etb
            mathdictionary["BTB"] = btb
            #if Ys in expression
            if "Y" in mathexpression:
                #extract Ys
                Yval = []                   #extract value number example Y9 = 9
                mlen = len(mathexpression)
                for i in range(mlen):
                    if mathexpression[i] == "Y":
                        #find Y number
                        if i+1 < mlen:                          #check for out of range
                            if mathexpression[i+1].isdigit():
                                number = mathexpression[i+1]
                            else:
                                number = "1"
                        #check for double digit
                        if i+2 < mlen:
                            if mathexpression[i+2].isdigit() and mathexpression[i+1].isdigit():
                                number += mathexpression[i+2]
                        Yval.append(number)
                #build Ys float values
                if len(self.timex) > 0:
                    Y = [self.temp1[-1],self.temp2[-1]]
                else:
                    Y = [-1,-1]
                for i in range(len(self.extradevices)):
                    if len(self.extratimex[i]):
                        try:
                            Y.append(self.extratemp1[i][-1])
                        except:
                            Y.append(-1)
                        try:
                            Y.append(self.extratemp2[i][-1])
                        except:
                            Y.append(-1)
                    else:
                        Y.append(-1)
                        Y.append(-1)
                    #add Ys and their value to math dictionary 
                    for i in range(len(Yval)):
                        idx = int(Yval[i])-1
                        if idx >= len(Y):
                            mathdictionary["Y"+ Yval[i]] = 0
                        else:
                            mathdictionary["Y"+ Yval[i]] = Y[idx]
            return round(eval(mathexpression,{"__builtins__":None},mathdictionary),3)

        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " eval_math_expression() {0}").format(str(e)),exc_tb.tb_lineno)
            return 0

    #format X axis labels
    def xaxistosm(self,redraw=True):

        if self.timeindex[0] != -1 and self.timeindex[0] < len(self.timex):
            starttime = self.timex[self.timeindex[0]]
        else:
            starttime = 0

        endtime = self.endofx + starttime
        self.ax.set_xlim(self.startofx,endtime)

        if not self.xgrid:
            self.xgrid = 60.

        mfactor1 =  round(float(2. + int(starttime)/int(self.xgrid)))
        mfactor2 =  round(float(2. + int(self.endofx)/int(self.xgrid)))

        majorloc = numpy.arange(starttime-(self.xgrid*mfactor1),starttime+(self.xgrid*mfactor2), self.xgrid)
        if self.xgrid == 60:
            minorloc = numpy.arange(starttime-(self.xgrid*mfactor1),starttime+(self.xgrid*mfactor2), 30)
        else:
            minorloc = numpy.arange(starttime-(self.xgrid*mfactor1),starttime+(self.xgrid*mfactor2), 60)
        
        majorlocator = ticker.FixedLocator(majorloc)
        minorlocator = ticker.FixedLocator(minorloc)

        self.ax.xaxis.set_major_locator(majorlocator)
        self.ax.xaxis.set_minor_locator(minorlocator)

        formatter = ticker.FuncFormatter(self.formtime)
        self.ax.xaxis.set_major_formatter(formatter)

        #adjust the length of the minor ticks
        for i in self.ax.xaxis.get_minorticklines() + self.ax.yaxis.get_minorticklines():
            i.set_markersize(5)

        #adjust the length of the major ticks
        for i in self.ax.get_xticklines() + self.ax.get_yticklines():
            i.set_markersize(10)
            #i.set_markeredgewidth(2)   #adjust the width

        # check x labels rotation
        if self.xrotation:     
            for label in self.ax.xaxis.get_ticklabels():
                label.set_rotation(self.xrotation)
        # we have to update the canvas cache
        if redraw:
            self.delayedUpdateBackground()
        else:
            self.ax_background = None

    def fmt_timedata(self,x):
        if self.timeindex[0] != -1 and self.timeindex[0] < len(self.timex):
            starttime = self.timex[self.timeindex[0]]
        else:
            starttime = 0
        if x >=  starttime:
            sign = ""
        else:
            sign = "-"
        m,s = divmod(abs(x - round(starttime)), 60)
        s = int(round(s))
        m = int(m)
        return '%s%d:%02d'%(sign,m,s)
        
    def fmt_data(self,x):
        res = x
        if self.fmt_data_RoR and not aw.qmc.designerflag and self.delta_ax:
            try:
                # depending on the z-order of ax vs delta_ax the one or the other one is correct
                #res = (self.ax.transData.inverted().transform((0,self.delta_ax.transData.transform((0,x))[1]))[1])
                res = (self.delta_ax.transData.inverted().transform((0,self.ax.transData.transform((0,x))[1]))[1])
            except Exception:
                pass
        elif not self.fmt_data_RoR and aw.qmc.designerflag and self.delta_ax:
            try:
                res = (self.delta_ax.transData.inverted().transform((0,self.ax.transData.transform((0,x))[1]))[1])
            except Exception:
                pass
        if aw.qmc.LCDdecimalplaces:
            return aw.float2float(res)
        else:
            return int(round(res))

    #used by xaxistosm(). Provides also negative time
    def formtime(self,x,pos):
        if self.timeindex[0] != -1 and self.timeindex[0] < len(self.timex):
            starttime = self.timex[self.timeindex[0]]
        else:
            starttime = 0

        if x >=  starttime:
            m,s = divmod((x - round(starttime)), 60)  #**NOTE**: divmod() returns here type numpy.float64, which could create problems
            #print type(m),type(s)                    #it is used in: formatter = ticker.FuncFormatter(self.formtime) in xaxistosm()
            s = int(round(s))
            m = int(m)

            if s >= 59:
                return '%d'%(m+1)
            elif abs(s - 30) < 1:
                return '%d.5'%m
            elif s > 1:
                return  '%d:%02d'%(m,s)
            else:
                return '%d'%m

        else:
            m,s = divmod(abs(x - round(starttime)), 60)
            s = int(round(s))
            m = int(m)
            
            if s >= 59:
                return '-%d'%(m+1)
            elif abs(s-30) < 1:
                return '-%d.5'%m
            elif s > 1:
                return  '-%d:%02d'%(m,s)
            else:
                if m == 0:
                    return '0'
                else:
                    return '-%d'%m

    # returns True if nothing to save, discard or save was selected and False if canceled by the user
    def checkSaved(self):
        #prevents deleting accidentally a finished roast
        if self.safesaveflag == True:
            string = QApplication.translate("Message","Save the profile, Discard the profile (Reset), or Cancel?", None)
            reply = QMessageBox.warning(self,QApplication.translate("Message","Profile unsaved", None),string,
                                QMessageBox.Discard |QMessageBox.Save|QMessageBox.Cancel)
            if reply == QMessageBox.Save:
                aw.fileSave(None)  #if accepted, makes safesaveflag = False
                return True
            elif reply == QMessageBox.Discard:
                self.safesaveflag = False
                return True
            elif reply == QMessageBox.Cancel:
                aw.sendmessage(QApplication.translate("Message","Action canceled",None))
                return False
        else:
            # nothing to be saved
            return True

    def clearMeasurements(self,andLCDs=True):
        try:
            #### lock shared resources #####
            aw.qmc.samplingsemaphore.acquire(1)
            self.safesaveflag = False  #now flag is cleared (OFF)
            self.rateofchange1 = 0.0
            self.rateofchange2 = 0.0
            self.temp1, self.temp2, self.delta1, self.delta2, self.timex, self.stemp1, self.stemp2 = [],[],[],[],[],[],[]
            self.unfiltereddelta1,self.unfiltereddelta2 = [],[]
            self.timeindex = [-1,0,0,0,0,0,0,0]
            #extra devices
            for i in range(min(len(self.extradevices),len(self.extratimex),len(self.extratemp1),len(self.extratemp2),len(self.extrastemp1),len(self.extrastemp2))):
                self.extratimex[i],self.extratemp1[i],self.extratemp2[i],self.extrastemp1[i],self.extrastemp2[i] = [],[],[],[],[]            #reset all variables that need to be reset (but for the actually measurements that will be treated separately at the end of this function)
            self.specialevents=[]
            aw.lcd1.display("00:00")
            if aw.WebLCDs:
                self.updateWebLCDs(time="00:00")
            if aw.largeLCDs_dialog:
                self.updateLargeLCDs(time="00:00")
            if andLCDs:
                if self.LCDdecimalplaces:
                    zz = "0.0"
                else:
                    zz = "0"
                aw.lcd2.display(zz)
                aw.lcd3.display(zz)
                aw.lcd4.display(zz)
                aw.lcd5.display(zz)
                aw.lcd6.display(zz)
                aw.lcd7.display(zz)
                for i in range(aw.nLCDS):
                    aw.extraLCD1[i].display(zz)
                    aw.extraLCD2[i].display(zz)
        except Exception as ex:
            #traceback.print_exc(file=sys.stdout)
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " reset() {0}").format(str(ex)),exc_tb.tb_lineno)
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)

    #Resets graph. Called from reset button. Deletes all data. Calls redraw() at the end
    # returns False if action was canceled, True otherwise
    def reset(self,redraw=True,soundOn=True):
        if not self.checkSaved():
            return False
        else:
            if soundOn:
                aw.soundpop()
            try:
                
                #### lock shared resources #####
                aw.qmc.samplingsemaphore.acquire(1)
                if self.flagon:
                    self.OffMonitor()

                #reset time
                aw.qmc.timeclock.start()
                
                aw.qmc.roastbatchnr = 0 # initialized to 0, set to increased batchcounter on DROP
                aw.qmc.roastbatchpos = 0 # initialized to 0, set to increased batchsequence on DROP
                aw.qmc.roastbatchprefix = aw.qmc.batchprefix

                if self.HUDflag:
                    self.toggleHUD()
                self.hudresizeflag = False

                aw.sendmessage(QApplication.translate("Message","Scope has been reset",None))
                aw.button_3.setDisabled(False)
                aw.button_4.setDisabled(False)
                aw.button_5.setDisabled(False)
                aw.button_6.setDisabled(False)
                aw.button_7.setDisabled(False)
                aw.button_8.setDisabled(False)
                aw.button_9.setDisabled(False)
                aw.button_19.setDisabled(False)
                aw.button_20.setDisabled(False)
                aw.button_3.setFlat(False)
                aw.button_4.setFlat(False)
                aw.button_5.setFlat(False)
                aw.button_6.setFlat(False)
                aw.button_7.setFlat(False)
                aw.button_8.setFlat(False)
                aw.button_9.setFlat(False)
                aw.button_19.setFlat(False)
                aw.button_20.setFlat(False)
                aw.button_1.setText(QApplication.translate("Button", "ON",None))
                aw.button_1.setStyleSheet(aw.pushbuttonstyles["OFF"])
                aw.button_2.setText(QApplication.translate("Button", "START",None))
                aw.button_2.setStyleSheet(aw.pushbuttonstyles["OFF"])

                aw.setWindowTitle(aw.windowTitle)

                if self.roastpropertiesflag:
                    self.title = QApplication.translate("Scope Title", "Roaster Scope",None)
                    self.roastingnotes = ""
                    self.cuppingnotes = ""
                    self.beans = ""
                    self.weight = [0,0,self.weight[2]]
                    self.volume = [0,0,self.volume[2]]
                    self.density = [0,self.density[1],0,self.density[3]]
                    self.ambientTemp = 0.
                    self.ambient_humidity = 0.
                    self.beansize = 0.
                    self.whole_color = 0
                    self.ground_color = 0
                    self.moisture_greens = 0.
                    self.moisture_roasted = 0.
                    self.volumeCalcWeightInStr = ""
                    self.volumeCalcWeightOutStr = ""
                else:
                    self.weight = [self.weight[0],0,self.weight[2]]
                    self.volume = [self.volume[0],0,self.volume[2]]
                    

                self.roastdate = QDateTime.currentDateTime()
                self.roastepoch = QDateTime.currentDateTime().toTime_t()
                self.roasttzoffset = libtime.timezone
                self.errorlog = []
                aw.seriallog = []

                self.specialevents = []
                self.specialeventstype = []
                self.specialeventsStrings = []
                self.specialeventsvalue = []
                self.E1timex,self.E2timex,self.E3timex,self.E4timex = [],[],[],[]
                self.E1values,self.E2values,self.E3values,self.E4values = [],[],[],[]
                aw.eNumberSpinBox.setValue(0)
                aw.lineEvent.setText("")
                aw.etypeComboBox.setCurrentIndex(0)
                aw.valueEdit.setText("")
                aw.curFile = None                 #current file name
                #used to find length of arms in annotations
                self.ystep_down = 0
                self.ystep_up = 0

                # reset keyboard mode
                aw.keyboardmoveindex = 0
                aw.keyboardmoveflag = 0
                aw.resetKeyboardButtonMarks()
                
                if not self.locktimex:
                    self.startofx = 0
                    self.endofx = self.resetmaxtime
                if self.endofx < 1:
                    self.endofx = 60

                #roast flags
                aw.qmc.heavyFC_flag = False
                aw.qmc.lowFC_flag = False
                aw.qmc.lightCut_flag = False
                aw.qmc.darkCut_flag = False
                aw.qmc.drops_flag = False
                aw.qmc.oily_flag = False
                aw.qmc.uneven_flag = False
                aw.qmc.tipping_flag = False
                aw.qmc.scorching_flag = False
                aw.qmc.divots_flag = False

                #color variables
                aw.qmc.whole_color = 0
                aw.qmc.ground_color = 0

                #Designer variables
                self.indexpoint = 0
                self.workingline = 2            #selects ET or BT
                self.currentx = 0               #used to add point when right click
                self.currenty = 0               #used to add point when right click
                self.designertemp1init = []
                self.designertemp2init = []
                if self.mode == "C":
                                             #CH, DE, Fcs,Fce,Scs,Sce,Drop
                    self.designertemp1init = [290,290,290,290,290,290,290]
                    self.designertemp2init = [200,150,200,210,220,225,240]
                elif self.mode == "F":
                    self.designertemp1init = [500,500,500,500,500,500,500]
                    self.designertemp2init = [380,300,390,395,410,412,420]
                self.disconnect_designer()  #sets designer flag false
                self.setCursor(Qt.ArrowCursor)

                #reset cupping flavor values
                self.flavors = [5.]*len(self.flavorlabels)

                try:
                    # reset color of last pressed button
                    if aw.lastbuttonpressed != -1:
                        normalstyle = "QPushButton {font-size: 10pt; font-weight: bold; color: %s; background-color: %s}"%(aw.extraeventbuttontextcolor[aw.lastbuttonpressed],aw.extraeventbuttoncolor[aw.lastbuttonpressed])
                        aw.buttonlist[aw.lastbuttonpressed].setStyleSheet(normalstyle)
                    # reset lastbuttonpressed
                    aw.lastbuttonpressed = -1
                except:
                    pass

                # reset sliders
                aw.moveslider(0,0)
                aw.moveslider(1,0)
                aw.moveslider(2,0)
                aw.moveslider(3,0)
                
                # reset Arduino/TC4 PID SV
                aw.moveSVslider(0)
                aw.arduino.sv = 0

                #autodetected CHARGE and DROP index
                self.autoChargeIdx = 0
                self.autoDropIdx = 0
                self.autoTPIdx = 0
                self.autoDryIdx = 0
                self.autoFCsIdx = 0

                aw.hideDefaultButtons()
                aw.hideExtraButtons(changeDefault=False)
                aw.hideLCDs()
                aw.hideSliders(changeDefault=False)
                
                aw.arduino.pidActive = False

                self.wheelflag = False
                self.designerflag = False

                #check and turn off mouse cross marker
                if self.crossmarker:
                    self.togglecrosslines()
            except Exception as ex:
                #traceback.print_exc(file=sys.stdout)
                _, _, exc_tb = sys.exc_info()
                aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " reset() {0}").format(str(ex)),exc_tb.tb_lineno)
            finally:
                if aw.qmc.samplingsemaphore.available() < 1:
                    aw.qmc.samplingsemaphore.release(1)
            # now clear all measurements and redraw
            self.clearMeasurements()
            ### REDRAW  ##
            if redraw:
                self.redraw(False)
            return True

    def medfilt(self, x, k):
        """Apply a length-k median filter to a 1D array x.
        Boundaries are extended by repeating endpoints.
        """
        assert k % 2 == 1, "Median filter length must be odd."
        assert x.ndim == 1, "Input must be one-dimensional."
        k2 = (k - 1) // 2
        y = numpy.zeros ((len (x), k), dtype=x.dtype)
        y[:,k2] = x
        for i in range (k2):
            j = k2 - i
            y[j:,i] = x[:-j]
            y[:j,i] = x[0]
            y[:-j,-(i+1)] = x[j:]
            y[-j:,-(i+1)] = x[-1]
        return numpy.median(y, axis=1)

    # smoothes a list of values 'y' at taken at times indicated by the numbers in list 'x'
    # 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
    # 'flat' results in moving average
    # window_len should be odd
    # based on http://wiki.scipy.org/Cookbook/SignalSmooth
    def smooth(self, x, y, window_len=15, window='hanning'):
        try:
            if len(x) == len(y) and len(x) > 1:
                # filter spikes
                if aw.qmc.filterDropOuts:
                    y = self.medfilt(y,5) # k=3 seems not to catch all spikes in all cases
                if window_len > 2 and len(x) == len(y) and len(x) > 1:
                    # smooth curves
                    #s = numpy.r_[2*x[0]-y[window_len:1:-1],y,2*y[-1]-y[-1:-window_len:-1]]
                    s=numpy.r_[y[window_len-1:0:-1],y,y[-1:-window_len:-1]]
                    if window == 'flat': #moving average
                        w = numpy.ones(window_len,'d')
                    else:
                        w = eval('numpy.'+window+'(window_len)')
                    ys = numpy.convolve(w/w.sum(),s,mode='valid')
                    res = ys[(window_len/2):-(window_len/2)]
                    if len(res) != len(y):
                        return y
                    else:
                        return res
                else:
                    return y
            else:
                return y
        except Exception as ex:
            #traceback.print_exc(file=sys.stdout)
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " smooth() {0}").format(str(ex)),exc_tb.tb_lineno)
            return x

    def smooth_list(self, a, b, window_len=7, window='hanning',fromIndex=-1):  # default 'hanning'
        #pylint: disable=E1103        
        win_len = max(0,window_len)
        if win_len != 1: # at the lowest level we turn smoothing completely off
            if fromIndex > -1: # if fromIndex is set, replace prefix up to fromIndex by None
                return numpy.concatenate(([None]*(fromIndex),
                    self.smooth(numpy.array(a),numpy.array(b),window_len,window).tolist()[fromIndex:])).tolist()
            elif aw.qmc.timeindex[0] != -1: # we do not smooth before CHARGE
                return numpy.concatenate((b[:aw.qmc.timeindex[0]+1],
                    self.smooth(numpy.array(a),numpy.array(b),win_len,window).tolist()[aw.qmc.timeindex[0]+1:])).tolist()            
            else:
                return self.smooth(numpy.array(a),numpy.array(b),win_len,window).tolist()
        else:
            return b

    def annotate(self, temp, time_str, x, y, yup, ydown,e=0,a=1.):                
        if aw.qmc.patheffects:
            rcParams['path.effects'] = [PathEffects.withStroke(linewidth=aw.qmc.patheffects, foreground="w")]
        else:
            rcParams['path.effects'] = []
        #annotate temp
        if aw.qmc.LCDdecimalplaces:
            fmtstr = "%.1f"
        else:
            fmtstr = "%.0f"        
        self.ax.annotate(fmtstr%(temp), xy=(x,y),xytext=(x+e,y + yup),
                            color=self.palette["text"],arrowprops=dict(arrowstyle='-',color=self.palette["text"],alpha=a),fontsize="x-small",alpha=a,fontproperties=aw.mpl_fontproperties)
        #anotate time
        self.ax.annotate(time_str,xy=(x,y),xytext=(x+e,y - ydown),
                             color=self.palette["text"],arrowprops=dict(arrowstyle='-',color=self.palette["text"],alpha=a),fontsize="x-small",alpha=a,fontproperties=aw.mpl_fontproperties)

    def place_annotations(self,TP_index,d,timex,timeindex,temp,stemp,startB=None,time2=None,timeindex2=None,path_effects=None):
        ystep_down = ystep_up = 0
        #Add markers for CHARGE
        try: 
            if timeindex[0] != -1 and len(timex) > timeindex[0]:
                t0idx = timeindex[0]
                t0 = timex[t0idx]
                y = stemp[t0idx]
                ystep_down,ystep_up = self.findtextgap(ystep_down,ystep_up,y,y,d)
                if startB != None:
                    st1 = str(self.stringfromseconds(t0 - startB))
                    t0 = startB
                    e = 40
                    a = aw.qmc.backgroundalpha
                else:
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation", "CHARGE", None))
                    e = 0
                    a = 1.  
                self.annotate(temp[t0idx],st1,t0,y,ystep_up,ystep_down,e,a)
                #Add TP marker
                if self.markTPflag and TP_index and TP_index > 0:
                    ystep_down,ystep_up = self.findtextgap(ystep_down,ystep_up,stemp[t0idx],stemp[TP_index],d)
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","TP {0}", None),u(self.stringfromseconds(timex[TP_index]-t0,False)))
                    a = 1.
                    e = 0
                    self.annotate(temp[TP_index],st1,timex[TP_index],stemp[TP_index],ystep_up,ystep_down,e,a)
                #Add Dry End markers
                if timeindex[1]:
                    tidx = timeindex[1]
                    ystep_down,ystep_up = self.findtextgap(ystep_down,ystep_up,stemp[t0idx],stemp[tidx],d)
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","DE {0}", None),u(self.stringfromseconds(timex[tidx]-t0,False)))
                    if timeindex2:
                        a = aw.qmc.backgroundalpha
                    else:
                        a = 1.
                    if timeindex2 and timeindex2[1] and timex[timeindex[1]] < time2[timeindex2[1]]:
                        e = -80
                    else:
                        e = 0
                    self.annotate(temp[tidx],st1,timex[tidx],stemp[tidx],ystep_up,ystep_down,e,a)
                #Add 1Cs markers            
                if timeindex[2]:
                    tidx = timeindex[2]
                    if timeindex[1]: #if dryend
                        ystep_down,ystep_up = self.findtextgap(ystep_down,ystep_up,stemp[timeindex[1]],stemp[tidx],d)
                    else:
                        ystep_down,ystep_up = self.findtextgap(0,0,stemp[tidx],stemp[tidx],d)
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","FCs {0}", None),u(self.stringfromseconds(timex[tidx]-t0,False)))
                    if timeindex2:
                        a = aw.qmc.backgroundalpha
                    else:
                        a = 1.
                    if timeindex2 and timeindex2[2] and timex[timeindex[2]] < time2[timeindex2[2]]:
                        e = 0
                    else:
                        e = -80
                    self.annotate(temp[tidx],st1,timex[tidx],stemp[tidx],ystep_up,ystep_down,e,a)
                #Add 1Ce markers
                if timeindex[3]:
                    tidx = timeindex[3]
                    ystep_down,ystep_up = self.findtextgap(ystep_down,ystep_up,stemp[timeindex[2]],stemp[tidx],d)
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","FCe {0}", None),u(self.stringfromseconds(timex[tidx]-t0,False)))
                    if timeindex2:
                        a = aw.qmc.backgroundalpha
                    else:
                        a = 1.
                    if timeindex2 and timeindex2[3] and timex[timeindex[3]] < time2[timeindex2[3]]:
                        e = -80
                    else:
                        e = 0
                    self.annotate(temp[tidx],st1,timex[tidx],stemp[tidx],ystep_up,ystep_down,e,a)
                    #add a water mark if FCs
                    if timeindex[2] and not timeindex2:
                        self.ax.axvspan(timex[timeindex[2]],timex[tidx], facecolor=self.palette["watermarks"], alpha=0.2)
                #Add 2Cs markers
                if timeindex[4]:
                    tidx = timeindex[4]
                    if timeindex[3]:
                        ystep_down,ystep_up = self.findtextgap(ystep_down,ystep_up,stemp[timeindex[3]],stemp[tidx],d)
                    else:
                        ystep_down,ystep_up = self.findtextgap(0,0,stemp[tidx],stemp[tidx],d)
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","SCs {0}", None),u(self.stringfromseconds(timex[tidx]-t0,False)))
                    if timeindex2:
                        a = aw.qmc.backgroundalpha
                    else:
                        a = 1.
                    if timeindex2 and timeindex2[4] and timex[timeindex[4]] < time2[timeindex2[4]]:
                        e = -80
                    else:
                        e = 0
                    self.annotate(temp[tidx],st1,timex[tidx],stemp[tidx],ystep_up,ystep_down,e,a)
                #Add 2Ce markers
                if timeindex[5]:
                    tidx = timeindex[5]
                    ystep_down,ystep_up = self.findtextgap(ystep_down,ystep_up,stemp[timeindex[4]],stemp[tidx],d)
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","SCe {0}", None),u(self.stringfromseconds(timex[tidx]-t0,False)))
                    if timeindex2:
                        a = aw.qmc.backgroundalpha
                    else:
                        a = 1.
                    if timeindex2 and timeindex2[5] and timex[timeindex[5]] < time2[timeindex2[5]]:
                        e = -80
                    else:
                        e = 0
                    self.annotate(temp[tidx],st1,timex[tidx],stemp[tidx],ystep_up,ystep_down,e,a)
                    #do water mark if SCs
                    if timeindex[4] and not timeindex2:
                        self.ax.axvspan(timex[timeindex[4]],timex[tidx], facecolor=self.palette["watermarks"], alpha=0.2)
                #Add DROP markers
                if timeindex[6]:
                    tidx = timeindex[6]
                    if timeindex[5]:
                        tx = timeindex[5]
                    elif timeindex[4]:
                        tx = timeindex[4]
                    elif timeindex[3]:
                        tx = timeindex[3]
                    elif timeindex[2]:
                        tx = timeindex[2]
                    elif timeindex[1]:
                        tx = timeindex[1]
                    else:
                        tx = t0idx
                    ystep_down,ystep_up = self.findtextgap(ystep_down,ystep_up,stemp[tx],stemp[tidx],d)
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","DROP {0}", None),str(self.stringfromseconds(timex[tidx]-t0,False)))
                    if timeindex2:
                        a = aw.qmc.backgroundalpha
                    else:
                        a = 1.
                    if timeindex2 and timeindex2[6] and timex[timeindex[6]] < time2[timeindex2[6]]:
                        e = -80
                    else:
                        e = 0
                    self.annotate(temp[tidx],st1,timex[tidx],stemp[tidx],ystep_up,ystep_down,e,a)
                    
                    #do water mark if FCs, but no FCe nor SCs nor SCe
                    if timeindex[2] and not timeindex[3] and not timeindex[4] and not timeindex[5] and not timeindex2:
                        self.ax.axvspan(timex[timeindex[2]],timex[tidx], facecolor=self.palette["watermarks"], alpha=0.2)
                    #do water mark if SCs, but no SCe
                    if timeindex[4] and not timeindex[5] and not timeindex2:
                        self.ax.axvspan(timex[timeindex[4]],timex[tidx], facecolor=self.palette["watermarks"], alpha=0.2)
                # add COOL mark
                if timeindex[7] and not timeindex2:
                    tidx = timeindex[7]
                    endidx = self.ax.get_xlim()[1] # or timex[-1]
                    if timex[tidx] < endidx:
                        self.ax.axvspan(timex[tidx],endidx, facecolor=self.palette["rect4"], ec='none', alpha=0.3, clip_on=False, clip_path=None, lw=None,lod=False)
        except Exception as e:
            #traceback.print_exc(file=sys.stdout)
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " place_annotations() {0}").format(str(e)),exc_tb.tb_lineno)

    #Redraws data
    # if recomputeAllDeltas, the delta arrays; if smooth the smoothed line arrays are recomputed
    def redraw(self, recomputeAllDeltas=True, smooth=False):
        try:
            #### lock shared resources   ####
            aw.qmc.samplingsemaphore.acquire(1)

            rcParams['path.effects'] = []
            if aw.qmc.graphstyle == 1:
                scale = 1
            else:
                scale = 0            
            length = 700 # 100 (128 the default)
            randomness = 12 # 2 (16 default)
            rcParams['path.sketch'] = (scale, length, randomness)

            rcParams['axes.linewidth'] = 1.5
            rcParams['xtick.major.size'] = 8
            rcParams['xtick.major.width'] = 1.5
            rcParams['xtick.minor.width'] = 1
            rcParams['ytick.major.size'] = 8
            rcParams['ytick.major.width'] = 1.5
            rcParams['ytick.minor.width'] = 1
            rcParams['xtick.color'] = self.palette["xlabel"]
            rcParams['ytick.color'] = self.palette["ylabel"]

            self.fig.clf()   #wipe out figure. keep_observers=False

            self.ax = self.fig.add_subplot(111,axisbg=self.palette["background"])

            #Set axes same as in __init__
            if self.endofx == 0:            #fixes possible condition of endofx being ZERO when application starts (after aw.settingsload)
                self.endofx = 60
            self.ax.set_ylim(self.ylimit_min, self.ylimit)
            self.ax.set_autoscale_on(False)
            
            fontprop_small = aw.mpl_fontproperties.copy()
            fontprop_small.set_size("xx-small")
            fontprop_medium = aw.mpl_fontproperties.copy()
            fontprop_medium.set_size("medium")
            fontprop_large = aw.mpl_fontproperties.copy()
            fontprop_large.set_size("large")
            fontprop_xlarge = aw.mpl_fontproperties.copy()
            fontprop_xlarge.set_size("x-large")
            self.ax.grid(True,color=self.palette["grid"],linestyle=self.gridstyles[self.gridlinestyle],linewidth = self.gridthickness,alpha = self.gridalpha,sketch_params=0,path_effects=[])
            if aw.qmc.flagstart:
                self.ax.set_ylabel("")
                self.ax.set_xlabel("")
                self.ax.set_title("")
                self.fig.suptitle("")
            else:
                self.ax.set_ylabel(self.mode,color=self.palette["ylabel"],rotation=0,labelpad=10,fontproperties=fontprop_large)
                self.ax.set_xlabel(aw.arabicReshape(QApplication.translate("Label", "min",None)),color = self.palette["xlabel"],fontproperties=fontprop_medium)
                if self.roastbatchnr == 0:
                    title = self.title
                else:
                    title = self.roastbatchprefix + u(self.roastbatchnr) + u(" ") + self.title
                self.ax.set_title(aw.arabicReshape(title), color=self.palette["title"],
                    fontproperties=fontprop_xlarge,horizontalalignment="left",x=0)
            
            two_ax_mode = (self.DeltaETflag or self.DeltaBTflag or (aw.qmc.background and (self.DeltaETBflag or self.DeltaBTBflag)))

            if self.background:
                if len(self.title) > 20:
                    stl = 25
                else:
                    stl = 30
                if two_ax_mode:
                    suptitleX = 0.93
                else:
                    suptitleX = 1
                if aw.qmc.flagstart:
                    self.fig.suptitle("")
                else:
                    if self.roastbatchnrB == 0:
                        titleB = self.titleB
                    else:
                        titleB = self.roastbatchprefixB + u(self.roastbatchnrB) + u(" ") + self.titleB
                    if self.title == None or u(self.title).strip() == "":
                        self.fig.suptitle(aw.qmc.abbrevString(titleB,stl),
                            horizontalalignment="right",fontproperties=fontprop_small,x=suptitleX,y=1)
                    else:
                        self.fig.suptitle("\n" + aw.qmc.abbrevString(titleB,stl),
                            horizontalalignment="right",fontproperties=fontprop_small,x=suptitleX,y=1)
            
            #self.fig.patch.set_facecolor(self.palette["background"]) # facecolor='lightgrey'
            #self.ax.spines['top'].set_color('none')
            self.ax.tick_params(\
                axis='x',           # changes apply to the x-axis
                which='both',       # both major and minor ticks are affected
                bottom='on',        # ticks along the bottom edge are on
                top='off',          # ticks along the top edge are off
                direction='out',
                labelbottom='on')   # labels along the bottom edge are on
            self.ax.tick_params(\
                axis='y',           # changes apply to the x-axis
                which='both',       # both major and minor ticks are affected
                bottom='on',        # ticks along the bottom edge are on
                top='off',          # ticks along the top edge are off
                direction='out',
                labelbottom='on')   # labels along the bottom edge are on

            prop = aw.mpl_fontproperties.copy()
            prop.set_size("medium")
            for label in self.ax.get_xticklabels() :
                label.set_fontproperties(prop)
            for label in self.ax.get_yticklabels() :
                label.set_fontproperties(prop)

            # format temperature as int, not float in the cursor position coordinate indicator
            self.ax.fmt_ydata = self.fmt_data
            self.ax.fmt_xdata = self.fmt_timedata

            if two_ax_mode:
                #create a second set of axes in the same position as self.ax
                self.delta_ax = self.ax.twinx()
                self.delta_ax.tick_params(\
                    axis='y',           # changes apply to the x-axis
                    which='both',       # both major and minor ticks are affected
                    bottom='on',        # ticks along the bottom edge are on
                    top='off',          # ticks along the top edge are off
                    direction='out',
                    labelbottom='on')   # labels along the bottom edge are on
                self.ax.set_zorder(self.delta_ax.get_zorder()-1) # put delta_ax in front of ax
                self.ax.patch.set_visible(True)
                if aw.qmc.flagstart:
                    self.delta_ax.set_ylabel("")
                else:
                    self.delta_ax.set_ylabel(aw.qmc.mode + aw.arabicReshape(QApplication.translate("Label", "/min", None)),color = self.palette["ylabel"],fontproperties=fontprop_large)
                self.delta_ax.set_ylim(self.zlimit_min,self.zlimit)
                self.delta_ax.yaxis.set_major_locator(ticker.MultipleLocator(self.zgrid))
                self.delta_ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
                for i in self.delta_ax.get_yticklines():
                    i.set_markersize(10)
                for i in self.delta_ax.yaxis.get_minorticklines():
                    i.set_markersize(5)
                for label in self.delta_ax.get_yticklabels() :
                    label.set_fontproperties(prop)

                # translate y-coordinate from delta into temp range to ensure the cursor position display (x,y) coordinate in the temp axis
                self.delta_ax.fmt_ydata = self.fmt_data
                self.delta_ax.fmt_xdata = self.fmt_timedata
            #put a right tick on the graph
            else:
                self.delta_ax = None
                self.ax.tick_params(\
                    axis='y', 
                    which='both',
                    right='off',
                    labelright='off') 

            self.ax.spines['top'].set_color("0.40")
            self.ax.spines['bottom'].set_color("0.40")
            self.ax.spines['left'].set_color("0.40")
            self.ax.spines['right'].set_color("0.40")

            self.ax.yaxis.set_major_locator(ticker.MultipleLocator(self.ygrid))
            self.ax.yaxis.set_minor_locator(ticker.AutoMinorLocator())
            for i in self.ax.get_yticklines():
                i.set_markersize(10)
            for i in self.ax.yaxis.get_minorticklines():
                i.set_markersize(5)

            #update X ticks, labels, and colors
            self.xaxistosm(redraw=False)

            rcParams['path.sketch'] = (0,0,0)
            trans = transforms.blended_transform_factory(self.ax.transAxes,self.ax.transData)

            #draw water marks for dry phase region, mid phase region, and finish phase region
            if aw.qmc.watermarksflag:
                rect1 = patches.Rectangle((0,self.phases[0]), width=1, height=(self.phases[1]-self.phases[0]),
                                          transform=trans, color=self.palette["rect1"],alpha=0.15)
                rect2 = patches.Rectangle((0,self.phases[1]), width=1, height=(self.phases[2]-self.phases[1]),
                                          transform=trans, color=self.palette["rect2"],alpha=0.15)
                rect3 = patches.Rectangle((0,self.phases[2]), width=1, height=(self.phases[3] - self.phases[2]),
                                          transform=trans, color=self.palette["rect3"],alpha=0.15)
                self.ax.add_patch(rect1)
                self.ax.add_patch(rect2)
                self.ax.add_patch(rect3)

            #if self.eventsGraphflag == 0 then that means don't plot event bars

            if self.eventsGraphflag == 1: #plot event bars by type
                # make blended transformations to help identify EVENT types
                if self.mode == "C":
                    step = 5
                    start = 20
                else:
                    step = 10
                    start = 60
                jump = 20
                for i in range(4):
                    rectEvent = patches.Rectangle((0,self.phases[0]-start-jump), width=1, height = step, transform=trans, color=self.palette["rect1"],alpha=.15)
                    self.ax.add_patch(rectEvent)
                    if self.mode == "C":
                        jump -= 10
                    else:
                        jump -= 20

            #plot events bars by value
            elif self.eventsGraphflag == 2:
                # make blended transformations to help identify EVENT types
                if self.mode == "C":
                    step = 2
                    start = 40
                else:
                    step = 5
                    start = 100
                jump = 20

                for i in range(12):
                    if i == 0:
                        color = self.palette["rect3"]
                    elif i%2:
                        color = self.palette["rect2"]
                    else:
                        color = self.palette["rect1"]
                    barposition = self.phases[0]-start-jump    
                    rectEvent = patches.Rectangle((0,barposition), width=1, height = step, transform=trans, color=color,alpha=.15)
                    self.ax.add_patch(rectEvent)
                    self.eventpositionbars[i] = barposition
                    if self.mode == "C":
                        jump -= 5
                    else:
                        jump -= 10

            rcParams['path.sketch'] = (scale, length, randomness)

            #check BACKGROUND flag
            if self.background: 
                #check to see if there is both a profile loaded and a background loaded
                if self.backmoveflag:
                    self.timealign(redraw=False,recompute=False,FCs=self.flagalignFCs)
                    
                #draw one extra device on background stemp1BX
                if aw.qmc.xtcurveidx > 0:
                    idx3 = aw.qmc.xtcurveidx - 1
                    n3 = idx3 // 2
                    if len(self.stemp1BX) > n3 and len(self.extratimexB) > n3:
                        if aw.qmc.xtcurveidx % 2:
                            stemp3B = self.stemp1BX[n3]
                        else:
                            stemp3B = self.stemp2BX[n3]
                        self.l_back3, = self.ax.plot(self.extratimexB[n3], stemp3B, markersize=self.XTbackmarkersize,marker=self.XTbackmarker,
                                                    sketch_params=None,path_effects=[],
                                                    linewidth=self.XTbacklinewidth,linestyle=self.XTbacklinestyle,drawstyle=self.XTbackdrawstyle,color=self.backgroundxtcolor,
                                                    alpha=self.backgroundalpha,label=aw.arabicReshape(QApplication.translate("Label", "BackgroundXT", None)))                                    

                #draw background
                self.l_back1, = self.ax.plot(self.timeB, self.stemp1B,markersize=self.ETbackmarkersize,marker=self.ETbackmarker,
                                            sketch_params=None,path_effects=[],
                                            linewidth=self.ETbacklinewidth,linestyle=self.ETbacklinestyle,drawstyle=self.ETbackdrawstyle,color=self.backgroundmetcolor,
                                            alpha=self.backgroundalpha,label=aw.arabicReshape(QApplication.translate("Label", "BackgroundET", None)))
                self.l_back2, = self.ax.plot(self.timeB, self.stemp2B,markersize=self.BTbackmarkersize,marker=self.BTbackmarker, 
                                            linewidth=self.BTbacklinewidth,linestyle=self.BTbacklinestyle,drawstyle=self.BTbackdrawstyle,color=self.backgroundbtcolor,
                                            sketch_params=None,path_effects=[],
                                            alpha=self.backgroundalpha,label=aw.arabicReshape(QApplication.translate("Label", "BackgroundBT", None)))

                #populate background delta ET (self.delta1B) and delta BT (self.delta2B)
                if self.DeltaETBflag or self.DeltaBTBflag:
                    if True: # recomputeAllDeltas:
                        tx = numpy.array(self.timeB)
                        with numpy.errstate(divide='ignore'):
                            nt1 = numpy.array(self.stemp1B)
                            z1 = (nt1[aw.qmc.deltasamples:] - nt1[:-aw.qmc.deltasamples]) / ((tx[aw.qmc.deltasamples:] - tx[:-aw.qmc.deltasamples])/60.)                            
                        with numpy.errstate(divide='ignore'):
                            nt2 = numpy.array(self.stemp2B)
                            z2 = (nt2[aw.qmc.deltasamples:] - nt2[:-aw.qmc.deltasamples]) / ((tx[aw.qmc.deltasamples:] - tx[:-aw.qmc.deltasamples])/60.)
                        lt,ld1,ld2 = len(self.timeB),len(z1),len(z2)
                        if lt > ld1:
                            z1 = numpy.append(z1,[z1[-1] if ld1 else 0.]*(lt - ld1))
                        if lt > ld2:
                            z2 = numpy.append(z2,[z2[-1] if ld2 else 0.]*(lt - ld2))
                        self.delta1B = self.smooth_list(tx,z1,window_len=self.deltafilter,fromIndex=self.timeindexB[0]) # CHARGE is the charge for the foreground, so we have to disable this here
                        self.delta2B = self.smooth_list(tx,z2,window_len=self.deltafilter,fromIndex=self.timeindexB[0])
                        # cut out the part after DROP
                        if aw.qmc.timeindexB[6]:
                            self.delta1B = numpy.append(self.delta1B[:self.timeindexB[6]+1],[None]*(len(self.delta1B)-self.timeindexB[6]-1))
                            self.delta2B = numpy.append(self.delta2B[:self.timeindexB[6]+1],[None]*(len(self.delta2B)-self.timeindexB[6]-1))
                        # cut out the part before CHARGE
                        if aw.qmc.timeindexB[0] > -1 and aw.qmc.timeindexB[0] < aw.qmc.timeindexB[6]:
                            self.delta1B = numpy.append([None]*(aw.qmc.timeindexB[0]),self.delta1B[aw.qmc.timeindexB[0]:])
                            self.delta2B = numpy.append([None]*(aw.qmc.timeindexB[0]),self.delta2B[aw.qmc.timeindexB[0]:])
                        # filter out values beyond the delta limits
                        if aw.qmc.mode == "C":
                            rorlimit = aw.qmc.RoRlimitC
                        else:
                            rorlimit = aw.qmc.RoRlimitF
                        self.delta1B = [d if d and (-rorlimit < d < rorlimit) else None for d in self.delta1B]
                        self.delta2B = [d if d and (-rorlimit < d < rorlimit) else None for d in self.delta2B]
                    
                    ##### DeltaETB,DeltaBTB curves
                    if self.DeltaETBflag:
                        self.l_delta1B, = self.delta_ax.plot(self.timeB, self.delta1B,markersize=self.ETBdeltamarkersize,
                        sketch_params=None,path_effects=[],
                        marker=self.ETBdeltamarker,linewidth=self.ETBdeltalinewidth,linestyle=self.ETBdeltalinestyle,drawstyle=self.ETBdeltadrawstyle,color=self.backgrounddeltaetcolor,alpha=self.backgroundalpha,label=aw.arabicReshape(QApplication.translate("Label", "BackgroundDeltaET", None)))
                    if self.DeltaBTBflag:
                        self.l_delta2B, = self.delta_ax.plot(self.timeB, self.delta2B,markersize=self.BTBdeltamarkersize,
                        sketch_params=None,path_effects=[],
                        marker=self.BTBdeltamarker,linewidth=self.BTBdeltalinewidth,linestyle=self.BTBdeltalinestyle,drawstyle=self.BTBdeltadrawstyle,color=self.backgrounddeltabtcolor,alpha=self.backgroundalpha,label=aw.arabicReshape(QApplication.translate("Label", "BackgroundDeltaBT", None)))

                #check backgroundevents flag
                if self.backgroundeventsflag:
                    if self.eventsGraphflag != 2:
                        if self.mode == "F":
                            height = 50
                        else:
                            height = 20

                        for p in range(len(self.backgroundEvents)):
                            st1 = u(self.Betypesf(self.backgroundEtypes[p])[0] + self.eventsvaluesShort(self.backgroundEvalues[p]))
                            if self.temp1B[self.backgroundEvents[p]] > self.temp2B[self.backgroundEvents[p]]:
                                temp = self.temp1B[self.backgroundEvents[p]]
                            else:
                                temp = self.temp2B[self.backgroundEvents[p]]
                            self.ax.annotate(st1, xy=(self.timeB[self.backgroundEvents[p]], temp),path_effects=[],
                                                xytext=(self.timeB[self.backgroundEvents[p]], temp+height),
                                                fontsize="x-small",fontproperties=aw.mpl_fontproperties,color=self.palette["text"],arrowprops=dict(arrowstyle='wedge',color="yellow",
                                                alpha=self.backgroundalpha,relpos=(0,0)),alpha=self.backgroundalpha)
                    #background events by value
                    else:
                        self.E1backgroundtimex,self.E2backgroundtimex,self.E3backgroundtimex,self.E4backgroundtimex = [],[],[],[]
                        self.E1backgroundvalues,self.E2backgroundvalues,self.E3backgroundvalues,self.E4backgroundvalues = [],[],[],[]
                        for i in range(len(self.backgroundEvents)):
                            if self.backgroundEtypes[i] == 0:
                                self.E1backgroundtimex.append(self.timeB[self.backgroundEvents[i]])
                                self.E1backgroundvalues.append(self.eventpositionbars[int(self.backgroundEvalues[i])])
                            elif self.backgroundEtypes[i] == 1:
                                self.E2backgroundtimex.append(self.timeB[self.backgroundEvents[i]])
                                self.E2backgroundvalues.append(self.eventpositionbars[int(self.backgroundEvalues[i])])
                            elif self.backgroundEtypes[i] == 2:
                                self.E3backgroundtimex.append(self.timeB[self.backgroundEvents[i]])
                                self.E3backgroundvalues.append(self.eventpositionbars[int(self.backgroundEvalues[i])])
                            elif self.backgroundEtypes[i] == 3:
                                self.E4backgroundtimex.append(self.timeB[self.backgroundEvents[i]])
                                self.E4backgroundvalues.append(self.eventpositionbars[int(self.backgroundEvalues[i])])

                        self.l_backgroundeventtype1dots, = self.ax.plot(self.E1backgroundtimex, self.E1backgroundvalues, color=self.EvalueColor[0], marker=self.EvalueMarker[0],markersize = self.EvalueMarkerSize[0],
                                                                        picker=3,linestyle="steps-post",linewidth = self.Evaluelinethickness[0],alpha = aw.qmc.backgroundalpha)
                        self.l_backgroundeventtype2dots, = self.ax.plot(self.E2backgroundtimex, self.E2backgroundvalues, color=self.EvalueColor[1], marker=self.EvalueMarker[1],markersize = self.EvalueMarkerSize[1],
                                                                        picker=3,linestyle="steps-post",linewidth = self.Evaluelinethickness[1],alpha = aw.qmc.backgroundalpha)
                        self.l_backgroundeventtype3dots, = self.ax.plot(self.E3backgroundtimex, self.E3backgroundvalues, color=self.EvalueColor[2], marker=self.EvalueMarker[2],markersize = self.EvalueMarkerSize[2],
                                                                        picker=3,linestyle="steps-post",linewidth = self.Evaluelinethickness[2],alpha = aw.qmc.backgroundalpha)
                        self.l_backgroundeventtype4dots, = self.ax.plot(self.E4backgroundtimex, self.E4backgroundvalues, color=self.EvalueColor[3], marker=self.EvalueMarker[3],markersize = self.EvalueMarkerSize[3],
                                                                        picker=3,linestyle="steps-post",linewidth = self.Evaluelinethickness[3],alpha = aw.qmc.backgroundalpha)                                                                        
                                                                          
                #check backgroundDetails flag
                if self.backgroundDetails:
                    d = aw.qmc.ylimit - aw.qmc.ylimit_min 
                    d = d - d/5
                    #if there is a profile loaded with CHARGE, then save time to get the relative time
                    if self.timeindex[0] != -1:   #verify it exists before loading it, otherwise the list could go out of index
                        startB = self.timex[self.timeindex[0]]
                    else:
                        if self.timeindexB[0] > 0:
                            startB = self.timeB[self.timeindexB[0]]
                        else:
                            startB = 0
                    self.place_annotations(-1,d,self.timeB,self.timeindexB,self.temp2B,self.stemp2B,startB,self.timex,self.timeindex)
                    
                #END of Background
                
            if aw.qmc.patheffects:
                rcParams['path.effects'] = [PathEffects.withStroke(linewidth=aw.qmc.patheffects, foreground="w")]
                
                
            handles = []
            labels = []
            
            if smooth or len(self.stemp1) != len(self.timex):
                self.stemp1 = self.smooth_list(self.timex,self.temp1,window_len=self.curvefilter)
            if smooth or len(self.stemp2) != len(self.timex):
                self.stemp2 = self.smooth_list(self.timex,self.temp2,window_len=self.curvefilter)

            if self.eventsshowflag:
                Nevents = len(self.specialevents)
                #three modes of drawing events.
                # the first mode just places annotations. They are text annotations.
                # The second mode aligns the events types to a bar height so that they can be visually identified by type. They are text annotations
                # the third mode plots the events by value. They are not annotations but actual lines.

                if Nevents:
                    for i in range(Nevents):
                        if self.specialeventstype[i] == 4 or self.eventsGraphflag == 0:
                            if self.specialeventstype[i] < 4:
                                etype = self.etypesf(self.specialeventstype[i])
                                firstletter = u(etype[0])
                                secondletter = self.eventsvaluesShort(self.specialeventsvalue[i])
                            else:
                                firstletter = "E"
                                secondletter = ""
                            if self.mode == "F":
                                height = 50
                            else:
                                height = 20
                            #some times ET is not drawn (ET = 0) when using device NONE
                            if self.temp1[int(self.specialevents[i])] > self.temp2[int(self.specialevents[i])] and aw.qmc.ETcurve:
                                if aw.qmc.flagon:
                                    temp = self.temp1[int(self.specialevents[i])]
                                else:
                                    temp = self.stemp1[int(self.specialevents[i])]
                            elif aw.qmc.BTcurve:
                                if aw.qmc.flagon:
                                    temp = self.temp2[int(self.specialevents[i])]
                                else:
                                    temp = self.stemp2[int(self.specialevents[i])]
                            else:
                                temp = None
                            if temp:
                                if self.specialeventstype[i] == 0:
                                    boxstyle = 'square,pad=0.2'
                                    boxcolor = self.EvalueColor[0]
                                    textcolor = 'white'
                                elif self.specialeventstype[i] == 1:
                                    boxstyle = 'circle,pad=0.1'
                                    boxcolor = self.EvalueColor[1]
                                    textcolor = 'white'
                                elif self.specialeventstype[i] == 2:
                                    boxstyle = 'sawtooth,pad=0.4,tooth_size=0.8'
                                    boxcolor = self.EvalueColor[2]
                                    textcolor = 'white'
                                elif self.specialeventstype[i] == 3:
                                    boxstyle = 'round4,pad=0.3,rounding_size=0.15'
                                    boxcolor = self.EvalueColor[3]
                                    textcolor = 'white'
                                elif self.specialeventstype[i] == 4:
                                    boxstyle = 'square,pad=0.2'
                                    boxcolor = 'yellow'
                                    textcolor = self.palette["text"]
                                self.ax.annotate(firstletter + secondletter, xy=(self.timex[int(self.specialevents[i])], temp),
                                             xytext=(self.timex[int(self.specialevents[i])],temp+height),
                                             alpha=0.9,
                                             color=textcolor,
                                             arrowprops=dict(arrowstyle='-',color=self.palette["bt"],alpha=0.4,relpos=(0,0)),
                                             bbox=dict(boxstyle=boxstyle, fc=boxcolor, ec='none'),
                                             fontproperties=fontprop_small,
                                             path_effects=[PathEffects.withStroke(linewidth=0.5,foreground="w")],
                                             )

                if self.eventsGraphflag == 1 and Nevents:
                    char1 = self.etypes[0][0]
                    char2 = self.etypes[1][0]
                    char3 = self.etypes[2][0]
                    char4 = self.etypes[3][0]

                    if self.mode == "F":
                        row = {char1:self.phases[0]-20,char2:self.phases[0]-40,char3:self.phases[0]-60,char4:self.phases[0]-80}
                    else:
                        row = {char1:self.phases[0]-10,char2:self.phases[0]-20,char3:self.phases[0]-30,char4:self.phases[0]-40}

                    #draw lines of color between events of the same type to help identify areas of events.
                    #count (as length of the list) and collect their times for each different type. Each type will have a different plot heigh
                    netypes=[[],[],[],[]]
                    for i in range(Nevents):
                        if self.specialeventstype[i] == 0:
                            netypes[0].append(self.timex[self.specialevents[i]])
                        elif self.specialeventstype[i] == 1:
                            netypes[1].append(self.timex[self.specialevents[i]])
                        elif self.specialeventstype[i] == 2:
                            netypes[2].append(self.timex[self.specialevents[i]])
                        elif self.specialeventstype[i] == 3:
                            netypes[3].append(self.timex[self.specialevents[i]])
                            
                    letters = char1+char2+char3+char4   #"NPDF" first letter for each type (None, Power, Damper, Fan)
                    colors = [self.palette["rect2"],self.palette["rect3"]] #rotating colors
                    for p in range(len(letters)):
                        if len(netypes[p]) > 1:
                            for i in range(len(netypes[p])-1):
                                #draw differentiating color bars between events and place then in a different height acording with type
                                rect = patches.Rectangle((netypes[p][i], row[letters[p]]), width = (netypes[p][i+1]-netypes[p][i]), height = step, color = colors[i%2],alpha=0.5)
                                self.ax.add_patch(rect)

                    # annotate event
                    for i in range(Nevents):
                        if self.specialeventstype[i] > 3:
                            # a special event of type "--"
                            pass
                        else:
                            firstletter = self.etypes[self.specialeventstype[i]][0]
                            secondletter = self.eventsvaluesShort(self.specialeventsvalue[i])
                            #some times ET is not drawn (ET = 0) when using device NONE
                            if aw.qmc.ETcurve or aw.qmc.BTcurve:
                                if (aw.qmc.ETcurve and self.temp1[int(self.specialevents[i])] >= self.temp2[int(self.specialevents[i])]) or (not aw.qmc.BTcurve):
                                    col = self.palette["et"]
                                    if aw.qmc.flagon:
                                        temps = self.temp1
                                    else:
                                        temps = self.stemp1
                                else:
                                    col = self.palette["bt"]
                                    if aw.qmc.flagon:
                                        temps = self.temp2
                                    else:
                                        temps = self.stemp2
                                self.ax.annotate(firstletter + secondletter, xy=(self.timex[int(self.specialevents[i])], temps[int(self.specialevents[i])]),
                                                 xytext=(self.timex[int(self.specialevents[i])],row[firstletter]),alpha=1.,
                                                 bbox=dict(boxstyle='square,pad=0.1', fc='yellow', ec='none'),
                                                 path_effects=[PathEffects.withStroke(linewidth=0.5,foreground="w")],
                                                 color=self.palette["text"],arrowprops=dict(arrowstyle='-',color=col,alpha=0.4,relpos=(0,0)),
                                                 fontsize="xx-small",
                                                 fontproperties=fontprop_small)

                elif self.eventsGraphflag == 2: # in this mode we have to generate the plots even if Nevents=0 to avoid redraw issues resulting from an incorrect number of plot count
                    self.E1timex,self.E2timex,self.E3timex,self.E4timex = [],[],[],[]
                    self.E1values,self.E2values,self.E3values,self.E4values = [],[],[],[]
                    E1_nonempty = E2_nonempty = E3_nonempty = E4_nonempty = False
                    for i in range(Nevents):
                        if self.specialeventstype[i] == 0:           
                            self.E1timex.append(self.timex[self.specialevents[i]])
                            self.E1values.append(self.eventpositionbars[min(11,max(0,int(self.specialeventsvalue[i])))])
                            E1_nonempty = True
                        elif self.specialeventstype[i] == 1:
                            self.E2timex.append(self.timex[self.specialevents[i]])
                            self.E2values.append(self.eventpositionbars[min(11,max(0,int(self.specialeventsvalue[i])))])
                            E2_nonempty = True
                        elif self.specialeventstype[i] == 2:
                            self.E3timex.append(self.timex[self.specialevents[i]])
                            self.E3values.append(self.eventpositionbars[min(11,max(0,int(self.specialeventsvalue[i])))])
                            E3_nonempty = True
                        elif self.specialeventstype[i] == 3:
                            self.E4timex.append(self.timex[self.specialevents[i]])
                            self.E4values.append(self.eventpositionbars[min(11,max(0,int(self.specialeventsvalue[i])))])
                            E4_nonempty = True

                    self.l_eventtype1dots, = self.ax.plot(self.E1timex, self.E1values, color=self.EvalueColor[0], marker=self.EvalueMarker[0],markersize = self.EvalueMarkerSize[0],
                                                          picker=3,linestyle="steps-post",linewidth = self.Evaluelinethickness[0],alpha = self.Evaluealpha[0],label=self.etypesf(0))
                    self.l_eventtype2dots, = self.ax.plot(self.E2timex, self.E2values, color=self.EvalueColor[1], marker=self.EvalueMarker[1],markersize = self.EvalueMarkerSize[1],
                                                          picker=3,linestyle="steps-post",linewidth = self.Evaluelinethickness[1],alpha = self.Evaluealpha[1],label=self.etypesf(1))
                    self.l_eventtype3dots, = self.ax.plot(self.E3timex, self.E3values, color=self.EvalueColor[2], marker=self.EvalueMarker[2],markersize = self.EvalueMarkerSize[2],
                                                          picker=3,linestyle="steps-post",linewidth = self.Evaluelinethickness[2],alpha = self.Evaluealpha[2],label=self.etypesf(2))
                    self.l_eventtype4dots, = self.ax.plot(self.E4timex, self.E4values, color=self.EvalueColor[3], marker=self.EvalueMarker[3],markersize = self.EvalueMarkerSize[3],
                                                          picker=3,linestyle="steps-post",linewidth = self.Evaluelinethickness[3],alpha = self.Evaluealpha[3],label=self.etypesf(3))
                        
            ##### Extra devices-curves
            self.extratemp1lines,self.extratemp2lines = [],[]
            for i in range(min(len(self.extratimex),len(self.extratemp1),len(self.extradevicecolor1),len(self.extraname1),len(self.extratemp2),len(self.extradevicecolor2),len(self.extraname2))):
                if aw.extraCurveVisibility1[i]:
                    if aw.qmc.flagon:
                        self.extratemp1lines.append(self.ax.plot(self.extratimex[i], self.extratemp1[i],color=self.extradevicecolor1[i],
                        sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.extralinewidths1[i]+aw.qmc.patheffects,foreground="w")],
                        markersize=self.extramarkersizes1[i],marker=self.extramarkers1[i],linewidth=self.extralinewidths1[i],linestyle=self.extralinestyles1[i],drawstyle=self.extradrawstyles1[i],label= self.extraname1[i])[0])
                    else:
                        if smooth or len(self.extrastemp1[i]) != len(self.extratimex[i]):
                            self.extrastemp1[i] = self.smooth_list(self.extratimex[i],self.extratemp1[i],window_len=self.curvefilter)
                        self.extratemp1lines.append(self.ax.plot(self.extratimex[i], self.extrastemp1[i],color=self.extradevicecolor1[i],                        
                        sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.extralinewidths1[i]+aw.qmc.patheffects,foreground="w")],
                        markersize=self.extramarkersizes1[i],marker=self.extramarkers1[i],linewidth=self.extralinewidths1[i],linestyle=self.extralinestyles1[i],drawstyle=self.extradrawstyles1[i],label= self.extraname1[i])[0])
                if aw.extraCurveVisibility2[i]:
                    if aw.qmc.flagon:
                        self.extratemp2lines.append(self.ax.plot(self.extratimex[i], self.extratemp2[i],color=self.extradevicecolor2[i],
                        sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.extralinewidths2[i]+aw.qmc.patheffects,foreground="w")],
                        markersize=self.extramarkersizes2[i],marker=self.extramarkers2[i],linewidth=self.extralinewidths2[i],linestyle=self.extralinestyles2[i],drawstyle=self.extradrawstyles2[i],label= self.extraname2[i])[0])
                    else:
                        if smooth or len(self.extrastemp2[i]) != len(self.extratimex[i]):
                            self.extrastemp2[i] = self.smooth_list(self.extratimex[i],self.extratemp2[i],window_len=self.curvefilter)
                        self.extratemp2lines.append(self.ax.plot(self.extratimex[i], self.extrastemp2[i],color=self.extradevicecolor2[i],
                        sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.extralinewidths2[i]+aw.qmc.patheffects,foreground="w")],
                        markersize=self.extramarkersizes2[i],marker=self.extramarkers2[i],linewidth=self.extralinewidths2[i],linestyle=self.extralinestyles2[i],drawstyle=self.extradrawstyles2[i],label= self.extraname2[i])[0])

            #populate delta ET (self.delta1) and delta BT (self.delta2)
            if self.DeltaETflag or self.DeltaBTflag:
                if recomputeAllDeltas:
                    tx = numpy.array(self.timex)
                    if aw.qmc.flagon or len(tx) != len(self.stemp1):
                        t1 = self.temp1
                    else:
                        t1 = self.stemp1
                    with numpy.errstate(divide='ignore'):
                        nt1 = numpy.array(t1)
                        z1 = (nt1[aw.qmc.deltasamples:] - nt1[:-aw.qmc.deltasamples]) / ((tx[aw.qmc.deltasamples:] - tx[:-aw.qmc.deltasamples])/60.)
                    if aw.qmc.flagon or len(tx) != len(self.stemp2):
                        t2 = self.temp2
                    else:
                        t2 = self.stemp2
                    with numpy.errstate(divide='ignore'):
                        nt2 = numpy.array(t2)
                        z2 = (nt2[aw.qmc.deltasamples:] - nt2[:-aw.qmc.deltasamples]) / ((tx[aw.qmc.deltasamples:] - tx[:-aw.qmc.deltasamples])/60.)
                    lt,ld1,ld2 = len(self.timex),len(z1),len(z2)
                    # make lists equal in length
                    if lt > ld1:
                        z1 = numpy.append(z1,[z1[-1] if ld1 else 0.]*(lt - ld1))
                    if lt > ld2:
                        z2 = numpy.append(z2,[z2[-1] if ld2 else 0.]*(lt - ld2))
                    self.delta1 = self.smooth_list(tx,z1,window_len=self.deltafilter,fromIndex=aw.qmc.timeindex[0])
                    self.delta2 = self.smooth_list(tx,z2,window_len=self.deltafilter,fromIndex=aw.qmc.timeindex[0])
                    # filter out values beyond the delta limits
                    # cut out the part after DROP
                    if aw.qmc.timeindex[6]:
                        self.delta1 = numpy.append(self.delta1[:aw.qmc.timeindex[6]+1],[None]*(len(self.delta1)-aw.qmc.timeindex[6]-1))
                        self.delta2 = numpy.append(self.delta2[:aw.qmc.timeindex[6]+1],[None]*(len(self.delta2)-aw.qmc.timeindex[6]-1))
                    # cut out the part before CHARGE
                    if aw.qmc.timeindex[0] > -1 and aw.qmc.timeindex[0] < aw.qmc.timeindex[6]:
                        self.delta1 = numpy.append([None]*(aw.qmc.timeindex[0]),self.delta1[aw.qmc.timeindex[0]:])
                        self.delta2 = numpy.append([None]*(aw.qmc.timeindex[0]),self.delta2[aw.qmc.timeindex[0]:])
                    # remove values beyond the RoRlimit
                    if aw.qmc.mode == "C":
                        rorlimit = aw.qmc.RoRlimitC
                    else:
                        rorlimit = aw.qmc.RoRlimitF
                    self.delta1 = [d if d and (-rorlimit < d < rorlimit) else None for d in self.delta1]
                    self.delta2 = [d if d and (-rorlimit < d < rorlimit) else None for d in self.delta2]
                    
                ##### DeltaET,DeltaBT curves
                if self.DeltaETflag: 
                    self.l_delta1, = self.delta_ax.plot(self.timex, self.delta1,markersize=self.ETdeltamarkersize,marker=self.ETdeltamarker,
                    sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.ETdeltalinewidth+aw.qmc.patheffects,foreground="w")],
                    linewidth=self.ETdeltalinewidth,linestyle=self.ETdeltalinestyle,drawstyle=self.ETdeltadrawstyle,color=self.palette["deltaet"],label=aw.arabicReshape(QApplication.translate("Label", "DeltaET", None)))                    
                if self.DeltaBTflag:
                    self.l_delta2, = self.delta_ax.plot(self.timex, self.delta2,markersize=self.BTdeltamarkersize,marker=self.BTdeltamarker,
                    sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.BTdeltalinewidth+aw.qmc.patheffects,foreground="w")],
                    linewidth=self.BTdeltalinewidth,linestyle=self.BTdeltalinestyle,drawstyle=self.BTdeltadrawstyle,color=self.palette["deltabt"],label=aw.arabicReshape(QApplication.translate("Label", "DeltaBT", None)))

            ##### ET,BT curves
            if aw.qmc.ETcurve:
                if aw.qmc.flagon:
                    self.l_temp1, = self.ax.plot(self.timex,self.temp1,markersize=self.ETmarkersize,marker=self.ETmarker,
                    sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.ETlinewidth+aw.qmc.patheffects,foreground="w")],
                    linewidth=self.ETlinewidth,linestyle=self.ETlinestyle,drawstyle=self.ETdrawstyle,color=self.palette["et"],label=aw.arabicReshape(QApplication.translate("Label", "ET", None)))
                else:
                    self.l_temp1, = self.ax.plot(self.timex,self.stemp1,markersize=self.ETmarkersize,marker=self.ETmarker,
                    sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.ETlinewidth+aw.qmc.patheffects,foreground="w")],
                    linewidth=self.ETlinewidth,linestyle=self.ETlinestyle,drawstyle=self.ETdrawstyle,color=self.palette["et"],label=aw.arabicReshape(QApplication.translate("Label", "ET", None)))
            if aw.qmc.BTcurve:
                if aw.qmc.flagon:
                    self.l_temp2, = self.ax.plot(self.timex,self.temp2,markersize=self.BTmarkersize,marker=self.BTmarker,
                    sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.BTlinewidth+aw.qmc.patheffects,foreground="w")],
                    linewidth=self.BTlinewidth,linestyle=self.BTlinestyle,drawstyle=self.BTdrawstyle,color=self.palette["bt"],label=aw.arabicReshape(QApplication.translate("Label", "BT", None)))
                else:
                    self.l_temp2, = self.ax.plot(self.timex,self.stemp2,markersize=self.BTmarkersize,marker=self.BTmarker,
                    sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.BTlinewidth+aw.qmc.patheffects,foreground="w")],
                    linewidth=self.BTlinewidth,linestyle=self.BTlinestyle,drawstyle=self.BTdrawstyle,color=self.palette["bt"],label=aw.arabicReshape(QApplication.translate("Label", "BT", None)))

            if aw.qmc.ETcurve:
                handles.append(self.l_temp1)
                labels.append(aw.arabicReshape(QApplication.translate("Label", "ET", None)))
            if aw.qmc.BTcurve:
                handles.append(self.l_temp2)
                labels.append(aw.arabicReshape(QApplication.translate("Label", "BT", None)))

            if self.DeltaETflag: 
                handles.append(self.l_delta1)
                labels.append(aw.arabicReshape(QApplication.translate("Label", "DeltaET", None)))
            if self.DeltaBTflag:
                handles.append(self.l_delta2)
                labels.append(aw.arabicReshape(QApplication.translate("Label", "DeltaBT", None)))


            nrdevices = len(self.extradevices)
            
            if nrdevices and not self.designerflag:
                xtmpl1idx = 0
                xtmpl2idx = 0
                for i in range(nrdevices):
                    if aw.extraCurveVisibility1[i]:
                        handles.append(self.extratemp1lines[xtmpl1idx])
                        xtmpl1idx = xtmpl1idx + 1
                        labels.append(aw.arabicReshape(self.extraname1[i]))
                    if aw.extraCurveVisibility2[i]:
                        handles.append(self.extratemp2lines[xtmpl2idx])
                        xtmpl2idx = xtmpl2idx + 1
                        labels.append(aw.arabicReshape(self.extraname2[i]))

            if self.eventsshowflag and self.eventsGraphflag == 2 and Nevents:
                if E1_nonempty:
                    handles.append(self.l_eventtype1dots)
                    labels.append(aw.arabicReshape(self.etypesf(0)))
                if E2_nonempty:
                    handles.append(self.l_eventtype2dots)
                    labels.append(aw.arabicReshape(self.etypesf(1)))
                if E3_nonempty:
                    handles.append(self.l_eventtype3dots)
                    labels.append(aw.arabicReshape(self.etypesf(2)))
                if E4_nonempty:
                    handles.append(self.l_eventtype4dots)
                    labels.append(aw.arabicReshape(self.etypesf(3)))                        
                        
            if not self.designerflag and aw.qmc.BTcurve:
                if self.flagon: # no smoothed lines in this case, pass normal BT
                    self.place_annotations(aw.qmc.TPalarmtimeindex,aw.qmc.ylimit - aw.qmc.ylimit_min,self.timex,self.timeindex,self.temp2,self.temp2)
                else:
                    TP_index = aw.findTP()
                    if aw.qmc.annotationsflag:
                        self.place_annotations(TP_index,aw.qmc.ylimit - aw.qmc.ylimit_min,self.timex,self.timeindex,self.temp2,self.stemp2)
                    if self.timeindex[6]:
                        self.writestatistics(TP_index)


                #if recorder on
                if self.flagon and self.eventsshowflag:
                    if not self.eventsshowflag:
                        Nevents = len(self.specialevents)
                    #update to last event
                    if Nevents:
                        aw.etypeComboBox.setCurrentIndex(self.specialeventstype[Nevents-1])
                        aw.valueEdit.setText(aw.qmc.eventsvalues(self.specialeventsvalue[Nevents-1]))
                    else:
                        aw.etypeComboBox.setCurrentIndex(0)
                        aw.valueEdit.setText("")
                    aw.eNumberSpinBox.setValue(Nevents)

            #update label colors
            for label in self.ax.xaxis.get_ticklabels():
                label.set_color(self.palette["xlabel"])
            for label in self.ax.yaxis.get_ticklabels():
                label.set_color(self.palette["ylabel"])
            if two_ax_mode:
                for label in self.delta_ax.yaxis.get_ticklabels():
                    label.set_color(self.palette["ylabel"])

            #write legend
            if self.legendloc:
                rcParams['path.effects'] = []
                prop = aw.mpl_fontproperties.copy()
                prop.set_size("x-small")
                if len(handles) > 3:
                    ncol = int(math.ceil(len(handles)/2.))
                else:
                    ncol = int(math.ceil(len(handles)))
                if two_ax_mode:
                    leg = self.delta_ax.legend(handles,labels,loc=self.legendloc,ncol=ncol,fancybox=True,prop=prop)
                else:
                    leg = self.ax.legend(handles,labels,loc=self.legendloc,ncol=ncol,fancybox=True,prop=prop)
                leg.draggable(state=True)
                if aw.qmc.graphstyle == 1:
                    leg.legendPatch.set_path_effects([PathEffects.withSimplePatchShadow(offset_xy=(8,-8),patch_alpha=0.9, shadow_rgbFace=(0.25,0.25,0.25))])

            # we create here the project line plots to have the accurate time axis after CHARGE
            self.l_BTprojection, = self.ax.plot([], [],color = self.palette["bt"],
                                            linestyle = '-.', linewidth= 8, alpha = .3,sketch_params=None,path_effects=[])
            self.l_ETprojection, = self.ax.plot([], [],color = self.palette["et"],
                                            linestyle = '-.', linewidth= 8, alpha = .3,sketch_params=None,path_effects=[])

            ############  ready to plot ############
            #self.fig.canvas.draw() # done by updateBackground()
            self.delayedUpdateBackground() # update bitlblit backgrounds
            #######################################

            # if designer ON
            if self.designerflag:
                if self.background:
                    self.ax.lines = self.ax.lines[2:]
                if len(self.timex):
                    self.xaxistosm()
                    self.redrawdesigner()

        except Exception as ex:
            #traceback.print_exc(file=sys.stdout)
            _, _, exc_tb = sys.exc_info()    
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " redraw() {0}").format(str(ex)),exc_tb.tb_lineno)
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)

    # adjusts height of annotations
    #supporting function for self.redraw() used to find best height of annotations in graph to avoid annotating over previous annotations (unreadable) when close to each other
    def findtextgap(self,ystep_down,ystep_up,height1,height2,dd=0):
        if dd <= 0:
            d = aw.qmc.ylimit - aw.qmc.ylimit_min
        else:
            d = dd
        init = int(d/15.0)
        gap = int(d/20.0)
        maxx = int(d/3.6)
        for i in range(init,maxx):
            if abs((height1 + ystep_up) - (height2+i)) > gap:
                break      
        for j in range(init,maxx):
            if abs((height1 - ystep_down) - (height2 - j)) > gap:
                break
        return j,i  #return height of arm

    # used to convert time from int seconds to string (like in the LCD clock timer). input int, output string xx:xx
    def stringfromseconds(self, seconds, leadingzero=True):
        if seconds >= 0:
            if leadingzero:
                return "%02d:%02d"% divmod(seconds, 60)
            else:
                return ("%2d:%02d"% divmod(seconds, 60)).strip()
        else:
            #usually the timex[timeindex[0]] is alreday taken away in seconds before calling stringfromseconds()
            negtime = abs(seconds)
            return "-%02d:%02d"% divmod(negtime, 60)

    #Converts a string into a seconds integer. Use for example to interpret times from Roaster Properties Dlg inputs
    #acepted formats: "00:00","-00:00"
    def stringtoseconds(self, string,errormsg=False):
        timeparts = string.split(":")
        if len(timeparts) != 2:
            if errormsg:
                aw.sendmessage(QApplication.translate("Message","Time format error encountered", None))
            return -1
        else:
            if timeparts[0][0] != "-":  #if number is positive
                seconds = int(timeparts[1])
                seconds += int(timeparts[0])*60
                return seconds
            else:
                seconds = int(timeparts[0])*60
                seconds -= int(timeparts[1])
                return seconds    #return negative number

    def fromFtoC(self,Ffloat):
        if Ffloat != None:
            return (Ffloat-32.0)*(5.0/9.0)
        else:
            return None

    def fromCtoF(self,CFloat):
        if CFloat != None:
            return (CFloat*9.0/5.0)+32.0
        else:
            return None

    #sets the graph display in Fahrenheit mode
    def fahrenheitMode(self):
        # just set it to the defaults to avoid strange conversion issues
        self.ylimit = 600
        self.ylimit_min = 0
        self.ygrid = 100
        self.zlimit = 50
        self.zlimit_min = 0
        self.zgrid = 10
        if self.mode == "C":
            #change watermarks limits. dryphase1, dryphase2, midphase, and finish phase Y limits
            for i in range(4):
                self.phases[i] = int(round(self.fromCtoF(self.phases[i])))
                self.phases_espresso[i] = int(round(self.fromCtoF(self.phases_espresso[i])))
                self.phases_filter[i] = int(round(self.fromCtoF(self.phases_filter[i])))
            self.ETtarget = int(round(self.fromCtoF(self.ETtarget)))
            self.ET2target = int(round(self.fromCtoF(self.ET2target)))
            self.BTtarget = int(round(self.fromCtoF(self.BTtarget)))
            self.BT2target = int(round(self.fromCtoF(self.BT2target)))
            # conv Arduino mode
            if aw:
                aw.arduino.conv2fahrenheit()
        self.ax.set_ylabel("F",size=16,color = self.palette["ylabel"]) #Write "F" on Y axis
        self.mode = "F"
        if aw: # during initialization aw is still None!
            aw.FahrenheitAction.setDisabled(True)
            aw.CelsiusAction.setEnabled(True)
            aw.ConvertToFahrenheitAction.setDisabled(True)
            aw.ConvertToCelsiusAction.setEnabled(True)
            # configure dropfilter
            aw.qmc.filterDropOut_tmin = aw.qmc.filterDropOut_tmin_F_default
            aw.qmc.filterDropOut_tmax = aw.qmc.filterDropOut_tmax_F_default
            aw.qmc.filterDropOut_spikeRoR_dRoR_limit = aw.qmc.filterDropOut_spikeRoR_dRoR_limit_F_default

    #sets the graph display in Celsius mode
    def celsiusMode(self):
        self.ylimit = 350
        self.ylimit_min = 0
        self.ygrid = 50
        self.zlimit = 50
        self.zlimit_min = 0
        self.zgrid = 10
        if self.mode == "F":
            #change watermarks limits. dryphase1, dryphase2, midphase, and finish phase Y limits
            for i in range(4):
                self.phases[i] = int(round(self.fromFtoC(self.phases[i])))
                self.phases_espresso[i] = int(round(self.fromFtoC(self.phases_espresso[i])))
                self.phases_filter[i] = int(round(self.fromFtoC(self.phases_filter[i])))
            self.ETtarget = int(round(self.fromFtoC(self.ETtarget)))
            self.ET2target = int(round(self.fromFtoC(self.ET2target)))
            self.BTtarget = int(round(self.fromFtoC(self.BTtarget)))
            self.BT2target = int(round(self.fromFtoC(self.BT2target)))
            
            # conv Arduino mode
            if aw:
                aw.arduino.conv2celsius()
        self.ax.set_ylabel("C",size=16,color = self.palette["ylabel"]) #Write "C" on Y axis
        self.mode = "C"
        if aw: # during initialization aw is still None
            aw.CelsiusAction.setDisabled(True)
            aw.FahrenheitAction.setEnabled(True)
            aw.ConvertToCelsiusAction.setDisabled(True)
            aw.ConvertToFahrenheitAction.setEnabled(True)
            # configure dropfilter
            aw.qmc.filterDropOut_tmin = aw.qmc.filterDropOut_tmin_C_default
            aw.qmc.filterDropOut_tmax = aw.qmc.filterDropOut_tmax_C_default
            aw.qmc.filterDropOut_spikeRoR_dRoR_limit = aw.qmc.filterDropOut_spikeRoR_dRoR_limit_C_default

    def fahrenheitModeRedraw(self):
        self.fahrenheitMode()
        self.redraw()

    def celsiusModeRedraw(self):
        self.celsiusMode()
        self.redraw()

    #converts a loaded profile to a different temperature scale. t input is the requested mode (F or C).
    def convertTemperature(self,t):
        #verify there is a loaded profile
        profilelength = len(self.timex)
        if profilelength > 0:
            if t == "F":
                string = QApplication.translate("Message", "Convert profile data to Fahrenheit?",None)
                reply = QMessageBox.question(self,QApplication.translate("Message", "Convert Profile Temperature",None),string,
                        QMessageBox.Yes|QMessageBox.Cancel)
                if reply == QMessageBox.Cancel:
                    return 
                elif reply == QMessageBox.Yes:
                    if self.mode == "C":
                        aw.CelsiusAction.setDisabled(True)
                        aw.FahrenheitAction.setEnabled(True)
                        aw.ConvertToCelsiusAction.setDisabled(True)
                        aw.ConvertToFahrenheitAction.setEnabled(True)
                        for i in arange(profilelength):
                            self.temp1[i] = self.fromCtoF(self.temp1[i])    #ET
                            self.temp2[i] = self.fromCtoF(self.temp2[i])    #BT
                            if len(self.delta1):
                                self.delta1[i] = self.fromCtoF(self.delta1[i])  #Delta ET
                            if len(self.delta2):
                                self.delta2[i] = self.fromCtoF(self.delta2[i])  #Delta BT
                            #extra devices curves
                            nextra = len(aw.qmc.extratemp1)   
                            if nextra:
                                for e in range(nextra):
                                    try:
                                        aw.qmc.extratemp1[e][i] = self.fromCtoF(aw.qmc.extratemp1[e][i])
                                        aw.qmc.extratemp2[e][i] = self.fromCtoF(aw.qmc.extratemp2[e][i])
                                    except:
                                        pass

                        self.ambientTemp = self.fromCtoF(self.ambientTemp)  #ambient temperature

                        #prevents accidentally deleting a modified profile. 
                        self.safesaveflag = True

                        #background
                        for i in arange(len(self.timeB)):
                            self.temp1B[i] = self.fromCtoF(self.temp1B[i])
                            self.temp2B[i] = self.fromCtoF(self.temp2B[i])
                            self.stemp1B[i] = self.fromCtoF(self.stemp1B[i])
                            self.stemp2B[i] = self.fromCtoF(self.stemp2B[i])

                        self.fahrenheitMode()
                        aw.sendmessage(QApplication.translate("Message","Profile changed to Fahrenheit", None))

                    else:
                        QMessageBox.information(self,QApplication.translate("Message", "Convert Profile Temperature",None),
                                                QApplication.translate("Message", "Unable to comply. You already are in Fahrenheit", None))
                        aw.sendmessage(QApplication.translate("Message","Profile not changed", None))
                        return

            elif t == "C":
                string = QApplication.translate("Message", "Convert profile data to Celsius?",None)
                reply = QMessageBox.question(self,QApplication.translate("Message", "Convert Profile Temperature",None),string,
                        QMessageBox.Yes|QMessageBox.Cancel)
                if reply == QMessageBox.Cancel:
                    return 
                elif reply == QMessageBox.Yes:
                    if self.mode == "F":    
                        aw.ConvertToFahrenheitAction.setDisabled(True)
                        aw.ConvertToCelsiusAction.setEnabled(True) 
                        aw.FahrenheitAction.setDisabled(True)
                        aw.CelsiusAction.setEnabled(True)   
                        for i in arange(profilelength):
                            self.temp1[i] = self.fromFtoC(self.temp1[i])    #ET
                            self.temp2[i] = self.fromFtoC(self.temp2[i])    #BT
                            if self.device != 18:
                                if len(self.delta1):
                                    self.delta1[i] = self.fromFtoC(self.delta1[i])  #Delta ET
                                if len(self.delta2):
                                    self.delta2[i] = self.fromFtoC(self.delta2[i])  #Delta BT
                            #extra devices curves
                            nextra = len(aw.qmc.extratemp1)
                            if nextra:
                                for e in range(nextra):
                                    aw.qmc.extratemp1[e][i] = self.fromFtoC(aw.qmc.extratemp1[e][i])
                                    aw.qmc.extratemp2[e][i] = self.fromFtoC(aw.qmc.extratemp2[e][i])

                        self.ambientTemp = self.fromFtoC(self.ambientTemp)  #ambient temperature

                        for i in arange(len(self.timeB)):
                            self.temp1B[i] = self.fromFtoC(self.temp1B[i]) #ET B
                            self.temp2B[i] = self.fromFtoC(self.temp2B[i]) #BT B
                            self.stemp1B[i] = self.fromFtoC(self.stemp1B[i])
                            self.stemp2B[i] = self.fromFtoC(self.stemp2B[i])

                    else:
                        QMessageBox.information(self,QApplication.translate("Message", "Convert Profile Temperature",None),
                                                QApplication.translate("Message", "Unable to comply. You already are in Celsius",None))
                        aw.sendmessage(QApplication.translate("Message","Profile not changed", None))
                        return

                    self.celsiusMode()
                    aw.sendmessage(QApplication.translate("Message","Profile changed to Celsius", None))

            self.redraw(recomputeAllDeltas=True,smooth=True)

        else:
            QMessageBox.information(self,QApplication.translate("Message", "Convert Profile Scale",None),
                                          QApplication.translate("Message", "No profile data found",None))

    #selects color mode: input 1=color mode; input 2=black and white mode (printing); input 3 = customize colors
    def changeGColor(self,color):
        #COLOR (option 1) Default
        palette1 = {"background":'white',"grid":'green',"ylabel":'black',"xlabel":'black',"title":'black',"rect1":'green',
                        "rect2":'orange',"rect3":'#996633',"rect4":'lightblue',"et":'red',"bt":'#00007f',"deltaet":'orange',
                        "deltabt":'blue',"markers":'black',"text":'black',"watermarks":'yellow',"Cline":'blue'}

        #BLACK & WHITE (option 2) best for printing
        palette2 = {"background":'white',"grid":'grey',"ylabel":'black',"xlabel":'black',"title":'black',"rect1":'lightgrey',
                   "rect2":'darkgrey',"rect3":'grey',"rect4":'lightgrey',"et":'black',"bt":'black',"deltaet":'grey',
                   "deltabt":'grey',"markers":'grey',"text":'black',"watermarks":'lightgrey',"Cline":'grey'}

        #load selected dictionary
        if color == 1:
            aw.sendmessage(QApplication.translate("Message","Colors set to defaults", None))
            for key in list(palette1.keys()):
                self.palette[key] = palette1[key]
            
        if color == 2:
            aw.sendmessage(QApplication.translate("Message","Colors set to grey", None))
            for key in list(palette1.keys()):
                self.palette[key] = palette2[key]
                
        if color == 3:
            dialog = graphColorDlg(self)
            if dialog.exec_():
                self.palette["background"] = str(dialog.backgroundLabel.text())
                self.palette["grid"] = str(dialog.gridLabel.text())
                self.palette["ylabel"] = str(dialog.yLabel.text())
                self.palette["xlabel"] = str(dialog.xLabel.text())
                self.palette["title"] = str(dialog.titleLabel.text())
                self.palette["rect1"] = str(dialog.rect1Label.text())
                self.palette["rect2"] = str(dialog.rect2Label.text())
                self.palette["rect3"] = str(dialog.rect3Label.text())
                self.palette["rect4"] = str(dialog.rect4Label.text())
                self.palette["et"] = str(dialog.metLabel.text())
                self.palette["bt"] = str(dialog.btLabel.text())
                self.palette["deltaet"] = str(dialog.deltametLabel.text())
                self.palette["deltabt"] = str(dialog.deltabtLabel.text())
                self.palette["markers"] = str(dialog.markersLabel.text())
                self.palette["text"] = str(dialog.textLabel.text())
                self.palette["watermarks"] = str(dialog.watermarksLabel.text())
                self.palette["Cline"] = str(dialog.ClineLabel.text())

        #update screen with new colors
        self.fig.canvas.redraw()

    #draws a polar star graph to score cupping. It does not delete any profile data.
    def flavorchart(self):

        pi = math.pi
        self.fig.clf()
        #create a new name ax1 instead of ax (ax is used when plotting profiles)

        self.ax1 = self.fig.add_subplot(111,projection='polar', axisbg=self.backcolor) #) radar green axisbg='#d5de9c'
        self.ax1.set_aspect(self.flavoraspect)

        #find number of divisions
        nflavors = len(self.flavors)      #last value of nflavors is used to close circle (same as flavors[0])

        g_angle = numpy.arange(self.flavorstartangle,(360.+self.flavorstartangle),(360./nflavors))  #angles in degree
        self.ax1.set_thetagrids(g_angle)
        self.ax1.set_rmax(1.)
        self.ax1.set_autoscale_on(False)
        self.ax1.grid(True,linewidth=1.,color='green', linestyle = "-",alpha=.3)
        # hack to make flavor labels visible also on top and bottom
        self.ax1.set_xlabel(" -\n ", alpha=0.0)
        self.ax1.set_title(" -\n ", alpha=0.0)

        #create water marks 6-7 anf 8-9
        self.ax1.bar(.1, .1, width=2.*pi, bottom=.6,color="green",linewidth=0.,alpha = .1)
        self.ax1.bar(.1, .1, width=2.*pi, bottom=.8,color="green",linewidth=0.,alpha = .1)

        #delete degrees ticks to anotate flavor characteristics 
        for tick in self.ax1.xaxis.get_major_ticks():
            tick.label1On = False
        
        fontprop_small = aw.mpl_fontproperties.copy()
        fontprop_small.set_size("x-small")

        #rename yaxis 
        locs = self.ax1.get_yticks()
        labels = []
        for i in range(len(locs)):
            stringlabel = str(int(locs[i]*10))
            labels.append(stringlabel)
        self.ax1.set_yticklabels(labels,color=self.palette["xlabel"],fontproperties=fontprop_small)

        step = 2.*pi/nflavors
        angles = [math.radians(self.flavorstartangle)]   #angles in radians
        for i in range(nflavors-1): angles.append(angles[-1] + step)

        #To close circle we need one more element. angle and values need same dimension in order to plot.
        plotf = self.flavors[:]
        plotf.append(self.flavors[0])
        #normalize flavor values to 0-1 range
        for i in range(len(plotf)):
            plotf[i] /= 10.
        angles.append(angles[-1]+step)
            
        #anotate labels
        for i in range(len(self.flavorlabels)):
            if angles[i] > 2.*pi or angles[i] < 0.:
                _,angles[i] = divmod(angles[i],(2.*pi))
            if angles[i] <= (pi/2.) or angles[i] >= (1.5*pi): #if < 90 or smaller than 270 degress
                ha = "left"
            else:
                ha = "right"
            self.ax1.annotate(aw.arabicReshape(self.flavorlabels[i]) + "\n" + str("%.2f"%self.flavors[i]),xy =(angles[i],.9),
                                fontproperties=fontprop_small,
                                xytext=(angles[i],1.1),horizontalalignment=ha,verticalalignment='center')

        score = 0.
        for i in range(nflavors):
            score += self.flavors[i]
        score /= (nflavors)
        score *= 10.

        txt = "%.2f" %score
        self.ax1.text(0.,0.,txt,fontsize="x-large",fontproperties=aw.mpl_fontproperties,color="blue",horizontalalignment="center",bbox={"facecolor":"yellow", "alpha":0.3, "pad":10})

        #add background to plot if found
        if self.background:
            if self.flavorbackgroundflag:
                backgroundplotf = self.backgroundFlavors[:]
                backgroundplotf.append(self.backgroundFlavors[0])
                #normalize flavor values to 0-1 range
                for i in range(len(backgroundplotf)):
                    backgroundplotf[i] /= 10.

                self.ax1.plot(angles,backgroundplotf,color="orange",marker="o",alpha=.5)
                #needs matplotlib 1.0.0+
                self.ax1.fill_between(angles,0,backgroundplotf, facecolor="yellow", alpha=0.1, interpolate=True)

        #add to plot
        self.ax1.plot(angles,plotf,color="blue",marker="o")
        
        self.ax1.fill_between(angles,0,plotf, facecolor='green', alpha=0.1, interpolate=True)

        self.fig.canvas.draw()
        
    def samplingAction(self):
        try:
            ###  lock resources ##
            aw.qmc.samplingsemaphore.acquire(1)
            if aw.qmc.extra_event_sampling_delay != 0:
                a = aw.qmc.extrabuttonactions[2]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.extrabuttonactionstrings[2])
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)
    
    def AsyncSamplingActionTrigger(self):
        if aw.AsyncSamplingAction and aw.qmc.extra_event_sampling_delay and aw.qmc.extrabuttonactions[2]:
            self.samplingAction()       
            QTimer.singleShot(aw.qmc.extra_event_sampling_delay,self.AsyncSamplingActionTrigger)
        
    def StartAsyncSamplingAction(self):
        if aw.qmc.flagon and aw.qmc.extra_event_sampling_delay != 0:
            aw.AsyncSamplingAction = True
            self.AsyncSamplingActionTrigger()
            
    def StopAsyncSamplingAction(self):
        aw.AsyncSamplingAction = False
        
    def OnMonitor(self):
        try:
            try:
                appnope.nope()
            except:
                pass
            if aw.qmc.device == 53:
                startHottop(0.8,aw.ser.comport,aw.ser.baudrate,aw.ser.bytesize,aw.ser.parity,aw.ser.stopbits,aw.ser.timeout)
            try:
                a = aw.qmc.extrabuttonactions[0]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.extrabuttonactionstrings[0])
            except:
                pass
            aw.lcd1.setStyleSheet("QLCDNumber { color: %s; background-color: %s;}"%(aw.lcdpaletteF["timer"],aw.lcdpaletteB["timer"]))
            aw.qmc.clearMeasurements()
            
            #reset alarms
            self.temporaryalarmflag = -3
            self.alarmstate = [0]*len(self.alarmflag)  #0 = not triggered; 1 = triggered
            #reset TPalarmtimeindex to trigger a new TP recognition during alarm processing
            aw.qmc.TPalarmtimeindex = None
            
            self.timeclock.start()   #set time to the current computer time
            self.flagon = True
            if self.designerflag: return
            aw.sendmessage(QApplication.translate("Message","Scope monitoring...", None))
            #disable RESET button:
            aw.button_7.setEnabled(False)
            aw.button_7.setStyleSheet(aw.pushbuttonstyles["DISABLED"])
            aw.button_1.setStyleSheet(aw.pushbuttonstyles["ON"])
            aw.button_1.setText(QApplication.translate("Button", "OFF",None)) # text means click to turn OFF (it is ON)
            aw.button_1.setToolTip(QApplication.translate("Tooltip", "Stop monitoring", None))
            aw.button_2.setEnabled(True) # ensure that the START button is enabled
            aw.showLCDs()
            if aw.eventslidersflag:
                aw.showSliders()
            aw.disableEditMenus()
            if aw.extraeventsbuttonsflag:
                aw.update_extraeventbuttons_visibility()
                aw.showExtraButtons()
            aw.arduino.activateONOFFeasySV(aw.arduino.svButtons)
            aw.arduino.activateSVSlider(aw.arduino.svSlider)
            self.threadserver.createSampleThread()
            self.StartAsyncSamplingAction()
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " OnMonitor() {0}").format(str(ex)),exc_tb.tb_lineno)

    def OffMonitor(self):
        try:
            # first activate "Stopping Mode" to ensure that sample() is not reseting the timer now (independent of the flagstart state)
            if self.HUDflag:
                self.toggleHUD()
            # stop Recorder if still running
            if self.flagstart:
                self.OffRecorder()
            self.flagon = False
            # now wait until the current sampling round is done
            while self.flagsampling:
                libtime.sleep(.01)
            # clear data from monitoring-only mode
            if len(self.timex) == 1:
                aw.qmc.clearMeasurements()
            aw.arduino.pidOff()
            self.disconnectProbes()
            #enable RESET button:
            aw.button_7.setStyleSheet(aw.pushbuttonstyles["RESET"])
            aw.button_7.setEnabled(True)
            aw.button_1.setStyleSheet(aw.pushbuttonstyles["OFF"])
            aw.button_1.setToolTip(QApplication.translate("Tooltip", "Start monitoring", None))
            aw.sendmessage(QApplication.translate("Message","Scope stopped", None))
            aw.button_1.setText(QApplication.translate("Button", "ON",None)) # text means click to turn OFF (it is ON)
            # reset time LCD color to the default (might have been changed to red due to long cooling!)
            aw.hideLCDs()
            # reset WebLCDs
            if aw.qmc.LCDdecimalplaces:
                resLCD = "-.-"
            else:
                resLCD = "--"
            if aw.WebLCDs: 
                self.updateWebLCDs(bt=resLCD,et=resLCD)
            if aw.largeLCDs_dialog:
                self.updateLargeLCDs(bt=resLCD,et=resLCD)
            if not aw.HottopControlActive:
                aw.hideSliders(changeDefault=False)
                aw.hideExtraButtons(changeDefault=False)
            aw.enableEditMenus()
            aw.arduino.activateONOFFeasySV(False)
            self.StopAsyncSamplingAction()
            aw.qmc.redraw(recomputeAllDeltas=True,smooth=True)
            #appnope.nap()
            try:
                a = aw.qmc.extrabuttonactions[1]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.extrabuttonactionstrings[1])
            except:
                pass
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " OffMonitor() {0}").format(str(ex)),exc_tb.tb_lineno)

    # close serial port, Phidgets and Yocto ports
    def disconnectProbesFromSerialDevice(self,ser):
        # close main serial port
        try:
            ser.closeport()
        except:
            pass
        # disconnect phidgets
        if ser.PhidgetTemperatureSensor:
            try:
                ser.PhidgetTemperatureSensorAttached = False
                ser.PhidgetTemperatureSensor.closePhidget()
                ser.PhidgetTemperatureSensor = None
            except:
                pass
        if ser.PhidgetIRSensor:
            try:
                ser.PhidgetIRSensorSensorAttached = False
                ser.PhidgetIRSensor.closePhidget()
                ser.PhidgetIRSensor = None
            except:
                pass
        if ser.PhidgetBridgeSensor:
            try:
                ser.PhidgetBridgeSensorAttached = False
                ser.PhidgetBridgeSensor.closePhidget()
                ser.PhidgetBridgeSensor = None
            except:
                pass
        if ser.PhidgetIO:
            try:
                ser.PhidgetIOAttached = False
                ser.PhidgetIO.closePhidget()
                ser.PhidgetIO = None
            except:
                pass
        if ser.PhidgetManager:
            ser.PhidgetManager.closeManager()
            ser.PhidgetManager = None
        if ser.YOCTOsensor:
            try:
                YAPI.FreeAPI()
                ser.YOCTOsensor = None
                ser.YOCTOchan1 = None
                ser.YOCTOchan2 = None
            except:
                pass

    def disconnectProbes(self):
        # close ports of main device
        self.disconnectProbesFromSerialDevice(aw.ser)
        # close serial port of Modbus device
        aw.modbus.disconnect()
        # close ports of extra devices
        for i in range(len(aw.extraser)):
            self.disconnectProbesFromSerialDevice(aw.extraser[i])

    #Turns ON/OFF flag self.flagon to read and print values. Called from push button_1.
    def ToggleMonitor(self):
        #turn ON
        if not self.flagon:
            if not self.checkSaved():
                return False
            else:
                aw.soundpop()
                if self.timex != []:
                    aw.qmc.reset(True,False)
                self.OnMonitor()
        #turn OFF
        else:
            aw.soundpop()
            self.OffMonitor()

    def OnRecorder(self):
        try:
            # start Monitor if not yet running
            if not self.flagon:
                self.OnMonitor()
            self.flagstart = True
            aw.qmc.roastbatchnr = 0 # initialized to 0, set to increased batchcounter on DROP
            aw.qmc.roastbatchpos = 0 # initialized to 0, set to increased batchsequence on DROP
            aw.qmc.fig.suptitle("")
            aw.qmc.updateDeltaSamples()
            aw.disableSaveActions()
            aw.sendmessage(QApplication.translate("Message","Scope recording...", None))
            aw.button_2.setEnabled(False)
            aw.button_2.setStyleSheet(aw.pushbuttonstyles["DISABLED"])
            aw.button_1.setToolTip(QApplication.translate("Tooltip", "Stop recording", None))
            aw.button_1.setEnabled(True) # ensure that the OFF button is enabled
            #disable RESET button:
            aw.button_7.setEnabled(False)
            aw.button_7.setStyleSheet(aw.pushbuttonstyles["DISABLED"])
            aw.button_18.setEnabled(True)
            aw.button_18.setStyleSheet(aw.pushbuttonstyles["HUD_OFF"])
            self.updateLCDtime()
            aw.lowerbuttondialog.setVisible(True)
            aw.applyStandardButtonVisibility()
            if aw.qmc.phasesLCDflag:
                aw.phasesLCDs.show()
            aw.update_minieventline_visibility()
            aw.qmc.ax.set_xlabel("")
            aw.qmc.ax.set_ylabel("")
            aw.qmc.ax.set_title("")
            if aw.qmc.delta_ax:
                aw.qmc.delta_ax.set_ylabel("")
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " OffMonitor() {0}").format(str(ex)),exc_tb.tb_lineno)

    def OffRecorder(self):
        try:
            aw.enableSaveActions()
            self.flagstart = False
            aw.button_2.setStyleSheet(aw.pushbuttonstyles["STOP"])
            aw.button_2.setEnabled(True)   
            #enable RESET button:
            aw.button_7.setStyleSheet(aw.pushbuttonstyles["RESET"]) 
            aw.button_7.setEnabled(True)
            aw.button_18.setStyleSheet(aw.pushbuttonstyles["DISABLED"])
            aw.button_18.setEnabled(False)
            self.updateLCDtime()
            self.redraw(smooth=True)
            #prevents accidentally deleting a modified profile.
            if len(self.timex) > 2:
                self.safesaveflag = True
            aw.sendmessage(QApplication.translate("Message","Scope recording stopped", None))
            aw.button_2.setText(QApplication.translate("Button", "START",None))
            aw.lowerbuttondialog.setVisible(False)
            aw.phasesLCDs.hide()
            aw.hideEventsMinieditor()
            if aw.qmc.autosaveflag and aw.qmc.autosavepath:
                aw.automaticsave()
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " OffMonitor() {0}").format(str(ex)),exc_tb.tb_lineno)

    #Turns START/STOP flag self.flagon to read and plot. Called from push button_2.
    def ToggleRecorder(self):
        #turn START
        if not self.flagstart:
            if not self.checkSaved():
                return False
            else:
                aw.soundpop()
                if self.flagon and len(self.timex) == 1:
                    # we are already in monitoring mode, we just clear this first measurement and go
                    aw.qmc.clearMeasurements(andLCDs=False)
                elif self.timex != []: # there is a profile loaded, we have to reset
                    aw.qmc.reset(True,False)
                self.OnRecorder()
        #turn STOP
        else:
            aw.soundpop()
            self.OffRecorder()

    #Records charge (put beans in) marker. called from push button 'Charge'
    def markCharge(self):
        try:
            aw.qmc.samplingsemaphore.acquire(1)
            if self.flagstart:
                try:
                    aw.soundpop()
                    #prevents accidentally deleting a modified profile.
                    self.safesaveflag = True
                    if self.device == 18: #manual mode
                        tx,et,bt = aw.ser.NONE()
                        if bt != 1 and et != -1:  #cancel
                            self.drawmanual(et,bt,tx)
                            self.timeindex[0] = len(self.timex)-1
                        else:
                            return
                    else:
                        if self.autoChargeIdx:
                            self.timeindex[0] = self.autoChargeIdx
                        else:
                            if len(self.timex) > 0:
                                self.timeindex[0] = len(self.timex)-1
                            else:
                                message = QApplication.translate("Message","Not enough variables collected yet. Try again in a few seconds", None)
                        if self.device == 19 and aw.arduino.pidOnCHARGE and not aw.arduino.pidActive: # Arduino/TC4
                            aw.arduino.pidOn()
                    self.xaxistosm(redraw=False) # need to fix uneven x-axis labels like -0:13
                    d = aw.qmc.ylimit - aw.qmc.ylimit_min
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation", "CHARGE 00:00", None))
                    t2 = self.temp2[self.timeindex[0]]
                    tx = self.timex[self.timeindex[0]]
                    self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,t2,t2,d)
                    self.annotate(t2,st1,tx,t2,self.ystep_up,self.ystep_down)
                    # mark active slider values that are not zero                   
                    for slidernr in range(4):
                        #if aw.eventslidervisibilities[slidernr]:
                        # we record also for inactive sliders as some button press actions might have changed the event values also for those
                        if slidernr == 0:
                            slidervalue = aw.slider1.value()
                        elif slidernr == 1:
                            slidervalue = aw.slider2.value()
                        elif slidernr == 2:
                            slidervalue = aw.slider3.value()
                        elif slidernr == 3:
                            slidervalue = aw.slider4.value()
                        if slidervalue != 0:
                            value = aw.float2float((slidervalue + 10.0) / 10.0)
                            # note that EventRecordAction avoids to generate events were type and value matches to the previously recorded one
                            aw.qmc.EventRecordAction(extraevent = 1,eventtype=slidernr,eventvalue=value)
                except:
                    pass
            else:
                message = QApplication.translate("Message","Scope is OFF", None)
                aw.sendmessage(message)
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " markCharge() {0}").format(str(ex)),exc_tb.tb_lineno)
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)
        if self.flagstart:
            # redraw (within timealign) should not be called if semaphore is hold!
            # NOTE: the following aw.eventaction might do serial communication that accires a lock, so release it here
            aw.qmc.timealign(redraw=True,recompute=False) # redraws at least the canvas if redraw=True, so no need here for doing another canvas.draw()
            try:
                a = aw.qmc.buttonactions[0]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.buttonactionstrings[0])
            except:
                pass
            aw.button_8.setDisabled(True)
            aw.button_8.setFlat(True)
            try:
                if aw.qmc.LCDdecimalplaces:
                    fmt = "%.1f"
                else:
                    fmt = "%.0f"
                bt = fmt%self.temp2[self.timeindex[0]] + aw.qmc.mode
                message = QApplication.translate("Message","Roast time starts now 00:00 BT = {0}",None).format(bt)
                aw.sendmessage(message) 
            except:
                pass

    # called from sample() and marks the autodetected TP visually on the graph
    def markTP(self):
        try:
            self.samplingsemaphore.acquire(1)
            if self.flagstart and self.markTPflag:
                if aw.qmc.TPalarmtimeindex and self.timeindex[0] != -1 and len(self.timex) > aw.qmc.TPalarmtimeindex:
                    st = self.stringfromseconds(self.timex[aw.qmc.TPalarmtimeindex]-self.timex[self.timeindex[0]])
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","TP {0}", None).format(st))
                    #anotate temperature
                    d = aw.qmc.ylimit - aw.qmc.ylimit_min
                    self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[0]],self.temp2[aw.qmc.TPalarmtimeindex],d)
                    self.annotate(self.temp2[aw.qmc.TPalarmtimeindex],st1,self.timex[aw.qmc.TPalarmtimeindex],self.temp2[aw.qmc.TPalarmtimeindex],self.ystep_up,self.ystep_down)
                    #self.fig.canvas.draw() # not needed as self.annotate does the (partial) redraw
                    self.delayedUpdateBackground() # but we need
                    st2 = "%.1f "%self.temp2[aw.qmc.TPalarmtimeindex] + self.mode
                    message = QApplication.translate("Message","[TP] recorded at {0} BT = {1}", None).format(st,st2)
                    #set message at bottom
                    aw.sendmessage(message)
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " markTP() {0}").format(str(ex)),exc_tb.tb_lineno)
        finally:
            if self.samplingsemaphore.available() < 1:
                self.samplingsemaphore.release(1)

    def markDryEnd(self):
        try:
            self.samplingsemaphore.acquire(1)
            if self.flagstart:
                if len(self.timex) > 0:
                    aw.soundpop()
                    #prevents accidentally deleting a modified profile.
                    self.safesaveflag = True
                    if self.device != 18:
                        self.timeindex[1] = len(self.timex)-1
                    else:
                        tx,et,bt = aw.ser.NONE()
                        if et != -1 and bt != -1:
                            self.drawmanual(et,bt,tx)
                            self.timeindex[1] = len(self.timex)-1
                        else:
                            return
                    if aw.qmc.phasesbuttonflag:
                        self.phases[1] = int(round(self.temp2[self.timeindex[1]]))
                    #calculate time elapsed since charge time
                    st = self.stringfromseconds(self.timex[self.timeindex[1]]-self.timex[self.timeindex[0]])
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","DE {0}", None).format(st))
                    #anotate temperature
                    d = aw.qmc.ylimit - aw.qmc.ylimit_min
                    self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[0]],self.temp2[self.timeindex[1]],d)
                    self.annotate(self.temp2[self.timeindex[1]],st1,self.timex[self.timeindex[1]],self.temp2[self.timeindex[1]],self.ystep_up,self.ystep_down)
                    #self.fig.canvas.draw() # not needed as self.annotate does the (partial) redraw
                    self.delayedUpdateBackground() # but we need

            else:
                message = QApplication.translate("Message","Scope is OFF", None)
                aw.sendmessage(message)
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " markDryEnd() {0}").format(str(ex)),exc_tb.tb_lineno)
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)
        if self.flagstart:                
            # NOTE: the following aw.eventaction might do serial communication that accires a lock, so release it here
            aw.button_19.setDisabled(True) # deactivate DRY button
            aw.button_19.setFlat(True)
            aw.button_8.setDisabled(True) # also deactivate CHARGE button
            aw.button_8.setFlat(True)
            try:
                a = aw.qmc.buttonactions[1]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.buttonactionstrings[1])
            except:
                pass
            st = self.stringfromseconds(self.timex[self.timeindex[1]]-self.timex[self.timeindex[0]])
            st2 = "%.1f "%self.temp2[self.timeindex[1]] + self.mode
            message = QApplication.translate("Message","[DRY END] recorded at {0} BT = {1}", None).format(st,st2)
            #set message at bottom
            aw.sendmessage(message)

    #record 1C start markers of BT. called from push button_3 of application window
    def mark1Cstart(self):
        try:
            aw.qmc.samplingsemaphore.acquire(1)
            if self.flagstart:
                if len(self.timex) > 0:
                    aw.soundpop()
                    #prevents accidentally deleting a modified profile.
                    self.safesaveflag = True
                    # record 1Cs only if Charge mark has been done
                    if self.device != 18:                
                        self.timeindex[2] = len(self.timex)-1
                    else:
                        tx,et,bt = aw.ser.NONE()
                        if et != -1 and bt != -1:
                            self.drawmanual(et,bt,tx)
                            self.timeindex[2] = len(self.timex)-1
                        else:
                            return
                    if aw.qmc.phasesbuttonflag:
                        self.phases[2] = int(round(self.temp2[self.timeindex[2]]))
                    #calculate time elapsed since charge time
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","FCs {0}", None).format(self.stringfromseconds(self.timex[self.timeindex[2]]-self.timex[self.timeindex[0]])))
                    d = aw.qmc.ylimit - aw.qmc.ylimit_min
                    if self.timeindex[1]:
                        self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[1]],self.temp2[self.timeindex[2]],d)
                    else:
                        self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[0]],self.temp2[self.timeindex[2]],d)
                    self.annotate(self.temp2[self.timeindex[2]],st1,self.timex[self.timeindex[2]],self.temp2[self.timeindex[2]],self.ystep_up,self.ystep_down)
                    #self.fig.canvas.draw() # not needed as self.annotate does the (partial) redraw
                    self.delayedUpdateBackground() # but we need
            else:
                message = QApplication.translate("Message","Scope is OFF", None)
                aw.sendmessage(message)
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " mark1Cstart() {0}").format(str(ex)),exc_tb.tb_lineno)
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)
        if self.flagstart:
            # redraw (within timealign) should not be called if semaphore is hold!
            # NOTE: the following aw.eventaction might do serial communication that accires a lock, so release it here
            if aw.qmc.flagalignFCs:
                aw.qmc.timealign(redraw=True,recompute=False,FCs=True) # redraws at least the canvas if redraw=True, so no need here for doing another canvas.draw()
            # NOTE: the following aw.eventaction might do serial communication that accires a lock, so release it here
            aw.button_3.setDisabled(True) # deactivate FCs button
            aw.button_3.setFlat(True)
            aw.button_8.setDisabled(True) # also deactivate CHARGE button
            aw.button_8.setFlat(True)
            aw.button_19.setDisabled(True) # also deactivate DRY button
            aw.button_19.setFlat(True)
            try:
                a = aw.qmc.buttonactions[2]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.buttonactionstrings[2])
            except:
                pass
            st1 = self.stringfromseconds(self.timex[self.timeindex[2]]-self.timex[self.timeindex[0]])
            st2 = "%.1f "%self.temp2[self.timeindex[2]] + self.mode
            message = QApplication.translate("Message","[FC START] recorded at {0} BT = {1}", None).format(st1,st2)
            aw.sendmessage(message)            
            

    #record 1C end markers of BT. called from button_4 of application window
    def mark1Cend(self):
        try:
            aw.qmc.samplingsemaphore.acquire(1)
            if self.flagstart:
                if len(self.timex) > 0:
                    aw.soundpop()
                    #prevents accidentally deleting a modified profile.
                    self.safesaveflag = True
                    if self.device != 18:
                        self.timeindex[3] = len(self.timex)-1
                    else:
                        tx,et,bt = aw.ser.NONE()
                        if et != -1 and bt != -1:
                            self.drawmanual(et,bt,tx)
                            self.timeindex[3] = len(self.timex)-1
                        else:
                            return
                    #calculate time elapsed since charge time
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","FCe {0}", None).format(self.stringfromseconds(self.timex[self.timeindex[3]]-self.timex[self.timeindex[0]])))
                    d = aw.qmc.ylimit - aw.qmc.ylimit_min  
                    self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[2]],self.temp2[self.timeindex[3]],d)
                    self.annotate(self.temp2[self.timeindex[3]],st1,self.timex[self.timeindex[3]],self.temp2[self.timeindex[3]],self.ystep_up,self.ystep_down)
                    #self.fig.canvas.draw() # not needed as self.annotate does the (partial) redraw
                    self.delayedUpdateBackground() # but we need
            else:
                message = QApplication.translate("Message","Scope is OFF", None)
                aw.sendmessage(message)
        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " mark1Cend() {0}").format(str(e)),exc_tb.tb_lineno)
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)
        if self.flagstart:
            # NOTE: the following aw.eventaction might do serial communication that accires a lock, so release it here
            aw.button_4.setDisabled(True) # deactivate FCe button
            aw.button_4.setFlat(True)
            aw.button_8.setDisabled(True) # also deactivate CHARGE button
            aw.button_8.setFlat(True)
            aw.button_19.setDisabled(True) # also deactivate DRY button
            aw.button_19.setFlat(True)
            aw.button_3.setDisabled(True) # also deactivate FCs button
            aw.button_3.setFlat(True)
            try:
                a = aw.qmc.buttonactions[3]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.buttonactionstrings[3])
            except:
                pass
            st1 = self.stringfromseconds(self.timex[self.timeindex[3]]-self.timex[self.timeindex[0]])
            st2 = "%.1f "%self.temp2[self.timeindex[3]] + self.mode
            message = QApplication.translate("Message","[FC END] recorded at {0} BT = {1}", None).format(st1,st2)
            aw.sendmessage(message)

    #record 2C start markers of BT. Called from button_5 of application window
    def mark2Cstart(self):
        try:
            aw.qmc.samplingsemaphore.acquire(1)
            if self.flagstart:
                if len(self.timex) > 0:
                    aw.soundpop()
                    #prevents accidentally deleting a modified profile. 
                    self.safesaveflag = True
                    if self.device != 18:
                        self.timeindex[4] = len(self.timex)-1
                    else:
                        tx,et,bt = aw.ser.NONE()
                        if et != -1 and bt != -1:
                            self.drawmanual(et,bt,tx)
                            self.timeindex[4] = len(self.timex)-1
                        else:
                            return
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","SCs {0}", None).format(self.stringfromseconds(self.timex[self.timeindex[4]]-self.timex[self.timeindex[0]])))
                    d = aw.qmc.ylimit - aw.qmc.ylimit_min
                    if self.timeindex[3]:
                        self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[3]],self.temp2[self.timeindex[4]],d)
                    else:
                        self.ystep_down,self.ystep_up = self.findtextgap(0,0,self.temp2[self.timeindex[4]],self.temp2[self.timeindex[4]],d)
                    self.annotate(self.temp2[self.timeindex[4]],st1,self.timex[self.timeindex[4]],self.temp2[self.timeindex[4]],self.ystep_up,self.ystep_down)
                    #self.fig.canvas.draw() # not needed as self.annotate does the (partial) redraw
                    self.delayedUpdateBackground() # but we need
            else:
                message = QApplication.translate("Message","Scope is OFF", None)
                aw.sendmessage(message)
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " mark2Cstart() {0}").format(str(ex)),exc_tb.tb_lineno)
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)
        if self.flagstart:
            # NOTE: the following aw.eventaction might do serial communication that accires a lock, so release it here
            aw.button_5.setDisabled(True) # deactivate SCs button
            aw.button_5.setFlat(True)
            aw.button_8.setDisabled(True) # also deactivate CHARGE button
            aw.button_8.setFlat(True)
            aw.button_19.setDisabled(True) # also deactivate DRY button
            aw.button_19.setFlat(True)
            aw.button_3.setDisabled(True) # also deactivate FCs button
            aw.button_3.setFlat(True)
            aw.button_4.setDisabled(True) # also deactivate FCe button
            aw.button_4.setFlat(True)
            try:
                a = aw.qmc.buttonactions[4]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.buttonactionstrings[4])
            except:
                pass
            st1 = self.stringfromseconds(self.timex[self.timeindex[4]]-self.timex[self.timeindex[0]])
            st2 = "%.1f "%self.temp2[self.timeindex[4]] + self.mode
            message = QApplication.translate("Message","[SC START] recorded at {0} BT = {1}", None).format(st1,st2)
            aw.sendmessage(message)

    #record 2C end markers of BT. Called from button_6  of application window
    def mark2Cend(self):
        try:
            aw.qmc.samplingsemaphore.acquire(1)
            if self.flagstart:
                if len(self.timex) > 0:
                    aw.soundpop()
                    #prevents accidentally deleting a modified profile.
                    self.safesaveflag = True
                    if self.device != 18:
                        self.timeindex[5] = len(self.timex)-1
                    else:
                        tx,et,bt = aw.ser.NONE()
                        if et != -1 and bt != -1:
                            self.drawmanual(et,bt,tx)
                            self.timeindex[5] = len(self.timex)-1
                        else:
                            return
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","SCe {0}", None).format(self.stringfromseconds(self.timex[self.timeindex[5]]-self.timex[self.timeindex[0]])))
                    d = aw.qmc.ylimit - aw.qmc.ylimit_min  
                    self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[4]],self.temp2[self.timeindex[5]],d)
                    self.annotate(self.temp2[self.timeindex[5]],st1,self.timex[self.timeindex[5]],self.temp2[self.timeindex[5]],self.ystep_up,self.ystep_down)
                    #self.fig.canvas.draw() # not needed as self.annotate does the (partial) redraw
                    self.delayedUpdatedBackground() # but we need
            else:
                message = QApplication.translate("Message","Scope is OFF", None)
                aw.sendmessage(message)
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " mark2Cend() {0}").format(str(ex)),exc_tb.tb_lineno)
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)  
        if self.flagstart:                      
            # NOTE: the following aw.eventaction might do serial communication that accires a lock, so release it here
            aw.button_6.setDisabled(True) # deactivate SCe button
            aw.button_6.setFlat(True)
            aw.button_8.setDisabled(True) # also deactivate CHARGE button
            aw.button_8.setFlat(True)
            aw.button_19.setDisabled(True) # also deactivate DRY button
            aw.button_19.setFlat(True)
            aw.button_3.setDisabled(True) # also deactivate FCs button
            aw.button_3.setFlat(True)
            aw.button_4.setDisabled(True) # also deactivate FCe button
            aw.button_4.setFlat(True)
            aw.button_5.setDisabled(True) # also deactivate SCs button
            aw.button_5.setFlat(True)
            try:
                a = aw.qmc.buttonactions[5]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.buttonactionstrings[5])
            except:
                pass
            st1 = self.stringfromseconds(self.timex[self.timeindex[5]]-self.timex[self.timeindex[0]])
            st2 = "%.1f "%self.temp2[self.timeindex[5]] + self.mode
            message = QApplication.translate("Message","[SC END] recorded at {0} BT = {1}", None).format(st1,st2)
            aw.sendmessage(message)

    #record end of roast (drop of beans). Called from push button 'Drop'
    def markDrop(self,takeLock=True):
        try:
            if takeLock:
                aw.qmc.samplingsemaphore.acquire(1)
            if self.flagstart:
                if len(self.timex) > 0:
                    self.incBatchCounter()
                    aw.soundpop()
                    #prevents accidentally deleting a modified profile.
                    self.safesaveflag = True
                    if self.device != 18:
                        if self.autoDropIdx:
                            self.timeindex[6] = self.autoDropIdx
                        else:
                            self.timeindex[6] = len(self.timex)-1
                    else:
                        tx,et,bt = aw.ser.NONE()
                        if et != -1 and bt != -1:
                            self.drawmanual(et,bt,tx)
                            self.timeindex[6] = len(self.timex)-1
                        else:
                            return
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","DROP {0}", None).format(self.stringfromseconds(self.timex[self.timeindex[6]]-self.timex[self.timeindex[0]])))
                    d = aw.qmc.ylimit - aw.qmc.ylimit_min  
                    if self.timeindex[5]:
                        self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[5]],self.temp2[self.timeindex[6]],d)
                    elif self.timeindex[4]:
                        self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[4]],self.temp2[self.timeindex[6]],d)
                    elif self.timeindex[3]:
                        self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[3]],self.temp2[self.timeindex[6]],d)
                    elif self.timeindex[2]:
                        self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[2]],self.temp2[self.timeindex[6]],d)
                    elif self.timeindex[1]:
                        self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[1]],self.temp2[self.timeindex[6]],d)
                    self.annotate(self.temp2[self.timeindex[6]],st1,self.timex[self.timeindex[6]],self.temp2[self.timeindex[6]],self.ystep_up,self.ystep_down)
                    #self.fig.canvas.draw() # not needed as self.annotate does the (partial) redraw
                    self.delayedUpdateBackground() # but we need
                    try:
                        # update ambient temperature if a ambient temperature source is configured and no value yet established
                        if aw.qmc.ambientTemp == 0.0:
                            aw.qmc.updateAmbientTemp()
                    except:
                        pass
            else:
                message = QApplication.translate("Message","Scope is OFF", None)
                aw.sendmessage(message)
        except Exception as ex:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " markDrop() {0}").format(str(ex)),exc_tb.tb_lineno)
        finally:
            if takeLock and aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)
        if self.flagstart:
            # NOTE: the following aw.eventaction might do serial communication that accires a lock, so release it here
            try:
                aw.button_9.setDisabled(True) # deactivate DROP button
                aw.button_9.setFlat(True)
                aw.button_8.setDisabled(True) # also deactivate CHARGE button
                aw.button_8.setFlat(True)
                aw.button_19.setDisabled(True) # also deactivate DRY button
                aw.button_19.setFlat(True)
                aw.button_3.setDisabled(True) # also deactivate FCs button
                aw.button_3.setFlat(True)
                aw.button_4.setDisabled(True) # also deactivate FCe button
                aw.button_4.setFlat(True)
                aw.button_5.setDisabled(True) # also deactivate SCs button
                aw.button_5.setFlat(True)
                aw.button_6.setDisabled(True) # also deactivate SCe button
                aw.button_6.setFlat(True)
            except:
                pass
            try:
                a = aw.qmc.buttonactions[6]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.buttonactionstrings[6])
            except:
                pass
            st1 = self.stringfromseconds(self.timex[self.timeindex[6]]-self.timex[self.timeindex[0]])
            st2 = "%.1f "%self.temp2[self.timeindex[6]] + self.mode
            message = QApplication.translate("Message","Roast ended at {0} BT = {1}", None).format(st1,st2)
            aw.sendmessage(message)

    def incBatchCounter(self):
        if aw.qmc.batchcounter > -1:
            aw.qmc.batchcounter += 1 # we increase the batch counter
            # set the batchcounter of the current profile
            aw.qmc.roastbatchnr = aw.qmc.batchcounter
            # set the batchprefix of the current profile
            aw.qmc.roastbatchprefix = aw.qmc.batchprefix
            # incr. the batchcounter of the loaded app settings
            if aw.settingspath and aw.settingspath != "":
                try:
                    settings = QSettings(aw.settingspath,QSettings.IniFormat)
                    settings.beginGroup("Batch")
                    if settings.contains("batchcounter"):
                        bc = toInt(settings.value("batchcounter",aw.qmc.batchcounter))
                        if bc > -1:
                            settings.setValue("batchcounter",bc + 1)
                    settings.endGroup()
                except:
                    aw.settingspath = u("")
            # update batchsequence by estimating batch sequence (roastbatchpos) from lastroastepoch and roastepoch
            # if this roasts DROP is more than 1.5h after the last registered DROP, we assume a new session starts
            if aw.qmc.lastroastepoch + 5400 < aw.qmc.roastepoch:
                # reset the sequence counter
                aw.qmc.batchsequence = 0
            else:
                aw.qmc.batchsequence += 1
            # set roastbatchpos
            aw.qmc.roastbatchpos = aw.qmc.batchsequence
        else: # batch counter system inactive
            # set the batchcounter of the current profile
            aw.qmc.batchsequence = 0
            aw.qmc.roastbatchnr = 0
            aw.qmc.roastbatchpos = 0
        # update lastroastepoch to time of roastdate
        aw.qmc.lastroastepoch = aw.qmc.roastepoch
        
    def markCoolEnd(self):
        try:
            aw.qmc.samplingsemaphore.acquire(1)
            if self.flagstart:
                if len(self.timex) > 0:
                    aw.soundpop()
                    #prevents accidentally deleting a modified profile.
                    self.safesaveflag = True
                    if self.device != 18:
                        self.timeindex[7] = len(self.timex)-1
                    else:
                        tx,et,bt = aw.ser.NONE()
                        if et != -1 and bt != -1:
                            self.drawmanual(et,bt,tx)
                            self.timeindex[7] = len(self.timex)-1
                        else:
                            return
                    #calculate time elapsed since charge time
                    st1 = aw.arabicReshape(QApplication.translate("Scope Annotation","CE {0}", None).format(self.stringfromseconds(self.timex[self.timeindex[7]] - self.timex[self.timeindex[0]])))
                    #anotate temperature
                    d = aw.qmc.ylimit - aw.qmc.ylimit_min  
                    self.ystep_down,self.ystep_up = self.findtextgap(self.ystep_down,self.ystep_up,self.temp2[self.timeindex[6]],self.temp2[self.timeindex[7]],d)
                    self.annotate(self.temp2[self.timeindex[7]],st1,self.timex[self.timeindex[7]],self.temp2[self.timeindex[7]],self.ystep_up,self.ystep_down)
                    #self.fig.canvas.draw() # not needed as self.annotate does the (partial) redraw
                    self.delayedUpdateBackground() # but we need
            else:
                message = QApplication.translate("Message","Scope is OFF", None)
                aw.sendmessage(message)
        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " markCoolEnd() {0}").format(str(e)),exc_tb.tb_lineno)
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)
        if self.flagstart:
            # NOTE: the following aw.eventaction might do serial communication that accires a lock, so release it here
            aw.button_20.setDisabled(True) # deactivate COOL button
            aw.button_20.setFlat(True)
            aw.button_8.setDisabled(True) # also deactivate CHARGE button
            aw.button_8.setFlat(True)
            aw.button_19.setDisabled(True) # also deactivate DRY button
            aw.button_19.setFlat(True)
            aw.button_3.setDisabled(True) # also deactivate FCs button
            aw.button_3.setFlat(True)
            aw.button_4.setDisabled(True) # also deactivate FCe button
            aw.button_4.setFlat(True)
            aw.button_5.setDisabled(True) # also deactivate SCs button
            aw.button_5.setFlat(True)
            aw.button_6.setDisabled(True) # also deactivate SCe button
            aw.button_6.setFlat(True)
            aw.button_9.setDisabled(True) # also deactivate DROP button
            aw.button_9.setFlat(True)
            try:
                a = aw.qmc.buttonactions[7]
                aw.eventaction((a if (a < 3) else ((a + 2) if (a > 5) else (a + 1))),aw.qmc.buttonactionstrings[7])
            except:
                pass
            st1 = self.stringfromseconds(self.timex[self.timeindex[7]]-self.timex[self.timeindex[0]])
            st2 = "%.1f "%self.temp2[self.timeindex[7]] + self.mode
            message = QApplication.translate("Message","[COOL END] recorded at {0} BT = {1}", None).format(st1,st2)
            #set message at bottom
            aw.sendmessage(message)

    def EventRecord(self,extraevent=None):
        try:
            if extraevent!=None:
                self.EventRecordAction(
                    extraevent=extraevent,
                    eventtype=aw.extraeventstypes[extraevent],
                    eventvalue=aw.extraeventsvalues[extraevent],
                    eventdescription=aw.extraeventsdescriptions[extraevent])
            else:
                self.EventRecordAction(extraevent=extraevent)
        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " EventRecord() {0}").format(str(e)),exc_tb.tb_lineno)

    #Marks location in graph of special events. For example change a fan setting.
    #Uses the position of the time index (variable self.timex) as location in time
    # extraevent is given when called from aw.recordextraevent() from an extra Event Button
    def EventRecordAction(self,extraevent=None,eventtype=None,eventvalue=None,eventdescription="",takeLock=True):
        try:
            if takeLock:
                aw.qmc.samplingsemaphore.acquire(1)
            if self.flagstart:
                if len(self.timex) > 0 or self.device == 18:
                    aw.soundpop()
                    #prevents accidentally deleting a modified profile.
                    self.safesaveflag = True                    
                    Nevents = len(self.specialevents)
                    #if in manual mode record first the last point in self.timex[]
                    if self.device == 18:
                        tx,et,bt = aw.ser.NONE()
                        if bt != -1 or et != -1:
                            self.drawmanual(et,bt,tx)
                        else:
                            return
                    #i = index number of the event (current length of the time list)
                    i = len(self.timex)-1
                    # if Desciption, Type and Value of the new event equals the last recorded one, we do not record this again!
                    if not(self.specialeventstype) or not(self.specialeventsvalue) or not(self.specialeventsStrings) or not(self.specialeventstype[-1] == eventtype and self.specialeventsvalue[-1] == eventvalue and self.specialeventsStrings[-1] == eventdescription):
                        self.specialevents.append(i)
                        self.specialeventstype.append(4)
                        self.specialeventsStrings.append(str(Nevents+1))
                        self.specialeventsvalue.append(0)
                        #if event was initiated by an Extra Event Button then change the type,value,and string 
                        if extraevent != None:
                            self.specialeventstype[-1] = eventtype
                            self.specialeventsvalue[-1] = eventvalue
                            self.specialeventsStrings[-1] = eventdescription
                        etype = self.specialeventstype[-1]
                        if etype == 0:
                            self.E1timex.append(self.timex[self.specialevents[-1]])
                            self.E1values.append(self.eventpositionbars[int(self.specialeventsvalue[-1])])
                        elif etype == 1:
                            self.E2timex.append(self.timex[self.specialevents[-1]])
                            self.E2values.append(self.eventpositionbars[int(self.specialeventsvalue[-1])])
                        elif etype == 2:
                            self.E3timex.append(self.timex[self.specialevents[-1]])
                            self.E3values.append(self.eventpositionbars[int(self.specialeventsvalue[-1])])
                        elif etype == 3:
                            self.E4timex.append(self.timex[self.specialevents[-1]])
                            self.E4values.append(self.eventpositionbars[int(self.specialeventsvalue[-1])])
                        #if Event show flag
                        if self.eventsshowflag:
                            index = self.specialevents[-1]
                            if etype < 4:
                                firstletter = self.etypesf(self.specialeventstype[-1])[0]
                            else:
                                firstletter = "E"
                            secondletter = self.eventsvaluesShort(self.specialeventsvalue[-1])
                            if self.eventsGraphflag == 0:
                                if self.mode == "F":
                                    height = 50
                                else:
                                    height = 20
                                #some times ET is not drawn (ET = 0) when using device NONE
                                if self.temp1[index] > self.temp2[index]:
                                    temp = self.temp1[index]
                                else:
                                    temp = self.temp2[index]
                                self.ax.annotate(firstletter + secondletter, xy=(self.timex[index], temp),xytext=(self.timex[index],temp+height),alpha=0.9,
                                                 color=self.palette["text"],arrowprops=dict(arrowstyle='-',color=self.palette["bt"],alpha=0.4,relpos=(0,0)),fontsize="x-small",fontproperties=aw.mpl_fontproperties,backgroundcolor='yellow')
                            #if Event Type-Bars flag
                            elif self.eventsGraphflag == 1 and etype < 4:
                                char1 = self.etypesf(0)[0]
                                char2 = self.etypesf(1)[0]
                                char3 = self.etypesf(2)[0]
                                char4 = self.etypesf(3)[0]
                                if self.mode == "F":
                                    row = {char1:self.phases[0]-20,char2:self.phases[0]-40,char3:self.phases[0]-60,char4:self.phases[0]-80}
                                else:
                                    row = {char1:self.phases[0]-10,char2:self.phases[0]-20,char3:self.phases[0]-30,char4:self.phases[0]-40}
                                #some times ET is not drawn (ET = 0) when using device NONE
                                fontprop_small = aw.mpl_fontproperties.copy()
                                fontprop_small.set_size("xx-small")
                                if self.temp1[index] >= self.temp2[index]:
                                    self.ax.annotate(firstletter + secondletter, 
                                        xy=(self.timex[index], 
                                        self.temp1[index]),xytext=(self.timex[index],row[firstletter]),
                                        alpha=1.,
                                        bbox=dict(boxstyle='square,pad=0.1', fc='yellow', ec='none'),
                                        path_effects=[PathEffects.withStroke(linewidth=0.5,foreground="w")],
                                        color=self.palette["text"],
                                        arrowprops=dict(arrowstyle='-',color=self.palette["et"],alpha=0.4,relpos=(0,0)),
                                        fontsize="xx-small",
                                        fontproperties=fontprop_small)
                                else:
                                    self.ax.annotate(firstletter + secondletter, 
                                            xy=(self.timex[index], 
                                            self.temp2[index]),xytext=(self.timex[index],row[firstletter]),
                                            alpha=1.,
                                            bbox=dict(boxstyle='square,pad=0.1', fc='yellow', ec='none'),
                                            path_effects=[PathEffects.withStroke(linewidth=0.5,foreground="w")],
                                            color=self.palette["text"],
                                            arrowprops=dict(arrowstyle='-',color=self.palette["bt"],alpha=0.4,relpos=(0,0)),
                                            fontsize="xx-small",
                                            fontproperties=fontprop_small)
                            elif self.eventsGraphflag == 2 and etype < 4:
                                # update lines data using the lists with new data
                                if etype == 0:
                                    self.l_eventtype1dots.set_data(self.E1timex, self.E1values)
                                elif etype == 1:
                                    self.l_eventtype2dots.set_data(self.E2timex, self.E2values)
                                elif etype == 2:
                                    self.l_eventtype3dots.set_data(self.E3timex, self.E3values)
                                elif etype == 3:
                                    self.l_eventtype4dots.set_data(self.E4timex, self.E4values)
                        self.delayedUpdateBackground() # call to canvas.draw() not needed as self.annotate does the (partial) redraw, but updateBacground() needed
                        temp = "%.1f "%self.temp2[i]            
                        if aw.qmc.timeindex[0] != -1:
                            start = aw.qmc.timex[aw.qmc.timeindex[0]]
                        else:
                            start = 0
                        timed = self.stringfromseconds(self.timex[i] - start)
                        message = QApplication.translate("Message","Event # {0} recorded at BT = {1} Time = {2}", None).format(str(Nevents+1),temp,timed)
                        aw.sendmessage(message)
                        #write label in mini recorder if flag checked
                        if aw.minieventsflag:
                            aw.eventlabel.setText(QApplication.translate("Label", "Event #<b>{0} </b>",None).format(Nevents+1))
                            aw.eNumberSpinBox.blockSignals(True)
                            aw.eNumberSpinBox.setValue(Nevents+1)
                            aw.eNumberSpinBox.blockSignals(False)
                            if aw.qmc.timeindex[0] > -1:
                                timez = aw.qmc.stringfromseconds(int(aw.qmc.timex[aw.qmc.specialevents[Nevents]]-aw.qmc.timex[aw.qmc.timeindex[0]]))
                                aw.etimeline.setText(timez)
                            aw.etypeComboBox.setCurrentIndex(self.specialeventstype[Nevents])
                            aw.valueEdit.setText(aw.qmc.eventsvalues(self.specialeventsvalue[Nevents]))
                            aw.lineEvent.setText(self.specialeventsStrings[Nevents])
            else:
                aw.sendmessage(QApplication.translate("Message","Timer is OFF", None))
        except Exception as e:
            #traceback.print_exc(file=sys.stdout)
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " EventRecordAction() {0}").format(str(e)),exc_tb.tb_lineno)
        finally:
            if takeLock and aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)

    #called from controlling devices when roasting to record steps (commands) and produce a profile later
    def DeviceEventRecord(self,command):
        try:
            aw.qmc.samplingsemaphore.acquire(1)
            if self.flagstart:
                #prevents accidentally deleting a modified profile.
                self.safesaveflag = True
                #number of events
                Nevents = len(self.specialevents)
                #index number            
                i = len(self.timex)-1
                if i > 0:
                    self.specialevents.append(i)                                     # store absolute time index
                    self.specialeventstype.append(0)                                 # set type (to the first index 0)
                    self.specialeventsStrings.append(command)                        # store the command in the string section of events (not a binary string)
                    self.specialeventsvalue.append(0)                                # empty
                    temp = str(self.temp2[i])
                    if self.timeindex[0] != -1:
                        start = self.timex[self.timeindex[0]]
                    else:
                        start = 0
                    timed = self.stringfromseconds(self.timex[i]-start)
                    message = QApplication.translate("Message","Computer Event # {0} recorded at BT = {1} Time = {2}", None).format(str(Nevents+1),temp,timed)
                    aw.sendmessage(message)
                    #write label in mini recorder if flag checked
                    if aw.minieventsflag:
                        aw.eNumberSpinBox.setValue(Nevents+1)
                        aw.etypeComboBox.setCurrentIndex(self.specialeventstype[Nevents-1])
                        aw.valueEdit.setText(aw.qmc.eventsvalues(self.specialeventsvalue[Nevents-1]))
                        aw.lineEvent.setText(self.specialeventsStrings[Nevents])
                #if Event show flag
                if self.eventsshowflag:
                    index = self.specialevents[-1]
                    if self.specialeventstype[-1] < 4:
                        firstletter = self.etypesf(self.specialeventstype[-1])[0]
                        secondletter = self.eventsvaluesShort(self.specialeventsvalue[-1])
                        if self.eventsGraphflag == 0:
                            if self.mode == "F":
                                height = 50
                            else:
                                height = 20
                            #some times ET is not drawn (ET = 0) when using device NONE
                            if self.temp1[index] > self.temp2[index]:
                                temp = self.temp1[index]
                            else:
                                temp = self.temp2[index]
                            self.ax.annotate(firstletter + secondletter, xy=(self.timex[index], temp),xytext=(self.timex[index],temp+height),alpha=0.9,
                                             color=self.palette["text"],arrowprops=dict(arrowstyle='-',color=self.palette["bt"],alpha=0.4,relpos=(0,0)),fontsize="x-small",fontproperties=aw.mpl_fontproperties,backgroundcolor='yellow')
                        #if Event Type-Bars flag
                        if self.eventsGraphflag == 1:
                            char1 = self.etypesf(0)[0]
                            char2 = self.etypesf(1)[0]
                            char3 = self.etypesf(2)[0]
                            char4 = self.etypesf(3)[0]
                            if self.mode == "F":
                                row = {char1:self.phases[0]-20,char2:self.phases[0]-40,char3:self.phases[0]-60,char4:self.phases[0]-80}
                            else:
                                row = {char1:self.phases[0]-10,char2:self.phases[0]-20,char3:self.phases[0]-30,char4:self.phases[0]-40}
                            #some times ET is not drawn (ET = 0) when using device NONE
                            if self.temp1[index] >= self.temp2[index]:
                                self.ax.annotate(firstletter + secondletter, xy=(self.timex[index], self.temp1[index]),xytext=(self.timex[index],row[firstletter]),alpha=1.,
                                                 color=self.palette["text"],arrowprops=dict(arrowstyle='-',color=self.palette["et"],alpha=0.4,relpos=(0,0)),fontsize="x-small",fontproperties=aw.mpl_fontproperties,backgroundcolor='yellow')
                            else:
                                self.ax.annotate(firstletter + secondletter, xy=(self.timex[index], self.temp2[index]),xytext=(self.timex[index],row[firstletter]),alpha=1.,
                                             color=self.palette["text"],arrowprops=dict(arrowstyle='-',color=self.palette["bt"],alpha=0.4,relpos=(0,0)),fontsize="x-small",fontproperties=aw.mpl_fontproperties,backgroundcolor='yellow')
                        if self.eventsGraphflag == 2:
                            # update lines data using the lists with new data
                            etype = self.specialeventstype[-1]
                            if etype == 0:
                                self.l_eventtype1dots.set_data(self.E1timex, self.E1values)
                            elif etype == 1:
                                self.l_eventtype2dots.set_data(self.E2timex, self.E2values)
                            elif etype == 2:
                                self.l_eventtype3dots.set_data(self.E3timex, self.E3values)
                            elif etype == 3:
                                self.l_eventtype4dots.set_data(self.E4timex, self.E4values)
                    #self.fig.canvas.draw() # not needed as self.annotate does the (partial) redraw
                    self.delayedUpdateBackground() # but we need
        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " DeviceEventRecord() {0}").format(str(e)),exc_tb.tb_lineno)
        finally:
            if aw.qmc.samplingsemaphore.available() < 1:
                aw.qmc.samplingsemaphore.release(1)

    def writecharacteristics(self,TP_index=None,LP=None):
        try:
            if self.statisticsflags[3] and self.timeindex[0]>-1 and self.temp1 and self.temp2 and self.temp1[self.timeindex[0]:self.timeindex[6]+1] and self.temp2[self.timeindex[0]:self.timeindex[6]+1]:
                statsprop = aw.mpl_fontproperties.copy()
                statsprop.set_size("small")
                if aw.qmc.statisticsmode == 0:
                    if TP_index == None:
                        TP_index = aw.findTP()
                    if LP == None:
                        #find Lowest Point in BT
                        LP = 1000 
                        if TP_index >= 0:
                            LP = self.temp2[TP_index]
                    # compute max ET between TP and DROP
                    if TP_index != None:
                        temp1_values = self.temp1[TP_index:self.timeindex[6]]
                        if self.LCDdecimalplaces:
                            lcdformat = "%.1f"
                        else:
                            lcdformat = "%.0f"
                        ETmax = lcdformat%max(temp1_values) + aw.qmc.mode
                    else:
                        ETmax = "--"
                    ror = "%.1f"%(((self.temp2[self.timeindex[6]]-LP)/(self.timex[self.timeindex[6]]-self.timex[self.timeindex[0]]))*60.)
                    ts,tse,tsb = aw.ts()
                    
                    #curveSimilarity
                    det,dbt = aw.curveSimilarity(aw.qmc.phases[1]) # we analyze from DRY-END as specified in the phases dialog to DROP
                
                    #end temperature
                    if locale == "ar":
                        strline = u("[{3}-{4}]{2}=" + aw.arabicReshape(QApplication.translate("Label", "ETBTa", None)) \
                                    + " " + aw.arabicReshape(aw.qmc.mode + QApplication.translate("Label", "/min", None)) \
                                    + "{1}=" + aw.arabicReshape(QApplication.translate("Label", "RoR", None)) \
                                    + " {0}=" + aw.arabicReshape(QApplication.translate("Label", "MET", None))) \
                                    .format(u(ETmax), \
                                    u(ror), \
                                    u("%d"%ts), \
                                    u(int(tse)), \
                                    u(int(tsb)))
                        if det != None:
                            strline = u(("%.1f/%.1f" % (det,dbt)) + self.mode + "=" + QApplication.translate("Label", "CM", None) + " ") + strline
                    else:
                        strline = u(QApplication.translate("Label", "MET", None) + "={0}   " \
                                    + QApplication.translate("Label", "RoR", None) + "={1}" \
                                    + aw.qmc.mode + QApplication.translate("Label", "/min", None) + "   " \
                                    + QApplication.translate("Label", "ETBTa", None) + "={2}[{3}-{4}]") \
                                    .format(u(ETmax), \
                                    u(ror), \
                                    u("%d"%ts), \
                                    u(int(tse)), \
                                    u(int(tsb)))
                        if det != None:
                            strline = strline + "   " + u(QApplication.translate("Label", "CM", None) + ("=%.1f/%.1f" % (det,dbt)) + self.mode)
                    self.ax.set_xlabel(strline,color = aw.qmc.palette["text"],fontproperties=statsprop)
                else:
                    sep = u"   "
                    msg = aw.qmc.roastdate.date().toString(Qt.SystemLocaleShortDate)
                    tm = u(aw.qmc.roastdate.time().toString()[:-3])
                    if tm != "00:00":
                        msg += ", " + tm
                    if aw.qmc.beans and aw.qmc.beans != "":
                        msg += sep + aw.qmc.abbrevString(u(aw.qmc.beans),25)
                    if aw.qmc.weight[0]:
                        msg += sep + str(int(round(aw.qmc.weight[0]))) + aw.qmc.weight[2]
                        if aw.qmc.weight[1]:
                            msg += sep + str(-aw.float2float(aw.weight_loss(aw.qmc.weight[0],aw.qmc.weight[1]),1)) + "%"
                    if aw.qmc.volume[0] and aw.qmc.volume[1]:
                            msg += sep + str(aw.float2float(aw.weight_loss(aw.qmc.volume[1],aw.qmc.volume[0]),1)) + "%"
                    if aw.qmc.whole_color and aw.qmc.ground_color:
                        msg += sep + u"#" + str(aw.qmc.whole_color) + u"/" +  str(aw.qmc.ground_color)
                    elif aw.qmc.ground_color:
                        msg += sep + u"#" + str(aw.qmc.ground_color)
                    self.ax.set_xlabel(msg,color = aw.qmc.palette["text"],fontproperties=statsprop)
            else:
                self.ax.set_xlabel(aw.arabicReshape(QApplication.translate("Label", "min",None)),size=16,color = self.palette["xlabel"],fontproperties=aw.mpl_fontproperties)
        except Exception as ex:
            #traceback.print_exc(file=sys.stdout)
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " writecharacteristics() {0}").format(str(ex)),exc_tb.tb_lineno)
    

    # Writes information about the finished profile in the graph
    # TP_index is the TP index calculated by findTP and might be -1 if no TP could be detected
    def writestatistics(self,TP_index):
        try:
            if self.timeindex[1] and self.phasesbuttonflag:
                #manual dryend available
                dryEndIndex = self.timeindex[1]
            else:
                #find when dry phase ends 
                dryEndIndex = aw.findDryEnd(TP_index)
            dryEndTime = self.timex[dryEndIndex]

            #if DROP
            if self.timeindex[6] and self.timeindex[2]:
                totaltime = int(self.timex[self.timeindex[6]]-self.timex[self.timeindex[0]])
                if totaltime == 0:
                    aw.sendmessage(QApplication.translate("Message","Statistics cancelled: need complete profile [CHARGE] + [DROP]", None))
                    return

                self.statisticstimes[0] = totaltime
                dryphasetime = int(dryEndTime - self.timex[self.timeindex[0]])
                midphasetime = int(self.timex[self.timeindex[2]] - dryEndTime)
                finishphasetime = int(self.timex[self.timeindex[6]] - self.timex[self.timeindex[2]])

                if self.timeindex[7]:
                    coolphasetime = int(self.timex[self.timeindex[7]] - self.timex[self.timeindex[6]])
                else:
                    coolphasetime = 0

                self.statisticstimes[1] = dryphasetime
                self.statisticstimes[2] = midphasetime
                self.statisticstimes[3] = finishphasetime
                self.statisticstimes[4] = coolphasetime

                #dry time string
                st1 = self.stringfromseconds(dryphasetime,False)

                #mid time string
                st2 = self.stringfromseconds(midphasetime,False)

                #finish time string
                st3 = self.stringfromseconds(finishphasetime,False)
                
                if coolphasetime:
                    st4 = self.stringfromseconds(coolphasetime,False)
                else:
                    st4 = ""

                #calculate the positions for the statistics elements
                ydist = self.ylimit - self.ylimit_min
                statisticsbarheight = ydist/70

                if aw.qmc.legendloc in [1,2,9]:
                    # legend on top
                    statisticsheight = self.ylimit - (0.13 * ydist) # standard positioning
                else:
                    # legend not on top
                    statisticsheight = self.ylimit - (0.08 * ydist)

                statisticsupper = statisticsheight + statisticsbarheight + 4
                statisticslower = statisticsheight - 3.5*statisticsbarheight

                if self.statisticsflags[1]:

                    #Draw cool phase rectangle
                    if self.timeindex[7]:
                        rect = patches.Rectangle((self.timex[self.timeindex[6]], statisticsheight), width = coolphasetime, height = statisticsbarheight,
                                                color = self.palette["rect4"],alpha=0.5)
                        self.ax.add_patch(rect)

                    if self.timeindex[2]: # only if FCs exists
                        #Draw finish phase rectangle
                        #check to see if end of 1C exists. If so, use half between start of 1C and end of 1C. Otherwise use only the start of 1C
                        rect = patches.Rectangle((self.timex[self.timeindex[2]], statisticsheight), width = finishphasetime, height = statisticsbarheight,
                                                color = self.palette["rect3"],alpha=0.5)
                        self.ax.add_patch(rect)

                        # Draw mid phase rectangle
                        rect = patches.Rectangle((self.timex[self.timeindex[0]]+dryphasetime, statisticsheight), width = midphasetime, height = statisticsbarheight,
                                              color = self.palette["rect2"],alpha=0.5)
                        self.ax.add_patch(rect)

                    # Draw dry phase rectangle
                    rect = patches.Rectangle((self.timex[self.timeindex[0]], statisticsheight), width = dryphasetime, height = statisticsbarheight,
                                              color = self.palette["rect1"],alpha=0.5)
                    self.ax.add_patch(rect)

                dryphaseP = int(round(dryphasetime*100/totaltime))
                midphaseP = int(round(midphasetime*100/totaltime))
                finishphaseP = int(round(finishphasetime*100/totaltime))
                            
                #find Lowest Point in BT
                LP = 1000 
                if TP_index >= 0:
                    LP = self.temp2[TP_index]

                if self.statisticsflags[0]:
                    statsprop = aw.mpl_fontproperties.copy()
                    statsprop.set_size(13)
                    self.ax.text(self.timex[self.timeindex[0]]+ dryphasetime/2.,statisticsupper,st1 + "  "+ str(dryphaseP)+"%",color=self.palette["text"],ha="center",fontproperties=statsprop)
                    if self.timeindex[2]: # only if FCs exists
                        self.ax.text(self.timex[self.timeindex[0]]+ dryphasetime+midphasetime/2.,statisticsupper,st2+ "  " + str(midphaseP)+"%",color=self.palette["text"],ha="center",fontproperties=statsprop)
                        self.ax.text(self.timex[self.timeindex[0]]+ dryphasetime+midphasetime+finishphasetime/2.,statisticsupper,st3 + "  " + str(finishphaseP)+ "%",color=self.palette["text"],ha="center",fontproperties=statsprop)
                    if self.timeindex[7]: # only if COOL exists
                        self.ax.text(self.timex[self.timeindex[0]]+ dryphasetime+midphasetime+finishphasetime+coolphasetime/2.,statisticsupper,st4,color=self.palette["text"],ha="center",fontproperties=statsprop)

                if self.statisticsflags[2]:
                    (st1,st2,st3,st4) = aw.defect_estimation()
                    st1 = aw.arabicReshape(st1)
                    st2 = aw.arabicReshape(st2)
                    st3 = aw.arabicReshape(st3)
                    st4 = aw.arabicReshape(st4)
                else:
                    st1 = st2 = st3 = st4 = ""

                if self.statisticsflags[4] or self.statisticsflags[5]:
                    rates_of_changes = aw.RoR(TP_index,dryEndIndex)
                    if self.statisticsflags[2]:
                        st1 = st1 + u(" (")
                        st2 = st2 + u(" (")
                        st3 = st3 + u(" (")
                    if self.statisticsflags[4]:
                        st1 = st1 + "%.1f"%rates_of_changes[0] + aw.arabicReshape(aw.qmc.mode + QApplication.translate("Label", "/min",None))
                        st2 = st2 + "%.1f"%rates_of_changes[1] + aw.arabicReshape(aw.qmc.mode + QApplication.translate("Label", "/min",None))
                        st3 = st3 + "%.1f"%rates_of_changes[2] + aw.arabicReshape(aw.qmc.mode + QApplication.translate("Label", "/min",None))
                    if self.statisticsflags[5]:
                        if self.statisticsflags[4]:
                            st1 += u("  ")
                            st2 += u("  ")
                            st3 += u("  ")
                        ts1,ts1e,ts1b = aw.ts(self.timeindex[0],dryEndIndex)
                        ts2,ts2e,ts2b = aw.ts(dryEndIndex,self.timeindex[2])
                        ts3,ts3e,ts3b = aw.ts(self.timeindex[2],self.timeindex[6])
                        st1 += u(ts1) + u(self.mode) + u("m")
                        st2 += u(ts2) + u(self.mode) + u("m")
                        st3 += u(ts3) + u(self.mode) + u("m")
                        if not self.statisticsflags[4]:
                            st1 += u(" [" + str(ts1e) + "-" + str(ts1b) + "]")
                            st2 += u(" [" + str(ts2e) + "-" + str(ts2b) + "]")
                            st3 += u(" [" + str(ts3e) + "-" + str(ts3b) + "]")
                    if self.statisticsflags[2]:
                        st1 = st1 + u(")")
                        st2 = st2 + u(")")
                        st3 = st3 + u(")")

                if self.statisticsflags[2] or self.statisticsflags[4] or self.statisticsflags[5]:
                    #Write flavor estimation
                    statsprop = aw.mpl_fontproperties.copy()
                    statsprop.set_size(13)
                    self.ax.text(self.timex[self.timeindex[0]] + dryphasetime/2.,statisticslower,st1,color=self.palette["text"],ha="center",fontproperties=statsprop)
                    if self.timeindex[2]: # only if FCs exists
                        self.ax.text(self.timex[self.timeindex[0]] + dryphasetime+midphasetime/2.,statisticslower,st2,color=self.palette["text"],ha="center",fontproperties=statsprop)
                        self.ax.text(self.timex[self.timeindex[0]] + dryphasetime+midphasetime+finishphasetime/2.,statisticslower,st3,color=self.palette["text"],ha="center",fontproperties=statsprop)
                    if self.timeindex[7]: # only if COOL exists
                        self.ax.text(self.timex[self.timeindex[0]]+ dryphasetime+midphasetime+finishphasetime+max(coolphasetime/2.,coolphasetime/3.),statisticslower,st4,color=self.palette["text"],ha="center",fontproperties=statsprop)
                self.writecharacteristics(TP_index,LP)
        except Exception as ex:
            #traceback.print_exc(file=sys.stdout)
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " writestatistics() {0}").format(str(ex)),exc_tb.tb_lineno)

    #used in EventRecord()
    def restorebutton_11(self):
        aw.button_11.setDisabled(False)
        aw.button_11.setFlat(False)

    #called from markdryen(), markcharge(), mark1Cstart(), etc when using device 18 (manual mode)
    def drawmanual(self,et,bt,tx):
        self.timex.append(tx)
        self.temp1.append(et)
        self.l_temp1.set_data(self.timex, self.temp1)
        self.temp2.append(bt)
        self.l_temp2.set_data(self.timex, self.temp2)

    def movebackground(self,direction,step):
        lt = len(self.timeB)
        le = len(self.temp1B)
        lb = len(self.temp2B)
        #all background curves must have same dimension in order to plot. Check just in case.
        if lt > 1 and lt == le and lb == le:
            if  direction == "up":
                for i in range(lt):
                    self.temp1B[i] += step
                    self.temp2B[i] += step

            elif direction == "left":
                for i in range(lt):
                    self.timeB[i] -= step
                                   
            elif direction == "right":
                for i in range(lt):
                    self.timeB[i] += step

            elif direction == "down":
                for i in range(lt):
                    self.temp1B[i] -= step
                    self.temp2B[i] -= step
        else:
            aw.sendmessage(QApplication.translate("Message","Unable to move background", None))
            return

    #points are used to draw interpolation
    def findpoints(self):
        #if profile found
        if self.timeindex[0] != -1:
            Xpoints = []                        #make temporary lists to hold the values to return
            Ypoints = []

            #start point from begining of time
            Xpoints.append(self.timex[0])
            Ypoints.append(self.temp2[0])
            #input beans (CHARGE)
            Xpoints.append(self.timex[self.timeindex[0]])
            Ypoints.append(self.temp2[self.timeindex[0]])

            #find indexes of lowest point and dryend
            LPind = aw.findTP()
            DE = aw.findDryEnd()

            if LPind < DE:
                Xpoints.append(self.timex[LPind])
                Ypoints.append(self.temp2[LPind])
                Xpoints.append(self.timex[DE])
                Ypoints.append(self.temp2[DE])
            else:
                Xpoints.append(self.timex[DE])
                Ypoints.append(self.temp2[DE])
                Xpoints.append(self.timex[LPind])
                Ypoints.append(self.temp2[LPind])
                
            if self.temp2[self.timeindex[1]] > self.timex[DE] and self.temp2[self.timeindex[1]] > self.timex[LPind]:
                Xpoints.append(self.timex[self.timeindex[1]])
                Ypoints.append(self.temp2[self.timeindex[1]])
            if self.timeindex[2]:
                Xpoints.append(self.timex[self.timeindex[2]])
                Ypoints.append(self.temp2[self.timeindex[2]])
            if self.timeindex[3]:
                Xpoints.append(self.timex[self.timeindex[3]])
                Ypoints.append(self.temp2[self.timeindex[3]])
            if self.timeindex[4]:
                Xpoints.append(self.timex[self.timeindex[4]])
                Ypoints.append(self.temp2[self.timeindex[4]])
            if self.timeindex[5]:
                Xpoints.append(self.timex[self.timeindex[5]])
                Ypoints.append(self.temp2[self.timeindex[5]])
            if self.timeindex[6]:
                Xpoints.append(self.timex[self.timeindex[6]])
                Ypoints.append(self.temp2[self.timeindex[6]])

            #end point
            if self.timex[self.timeindex[6]] != self.timex[-1]:
                Xpoints.append(self.timex[-1])
                Ypoints.append(self.temp2[-1])

            return Xpoints,Ypoints

        else:
            aw.sendmessage(QApplication.translate("Message","No finished profile found", None))
            return [],[]

    #collects info about the univariate interpolation
    def univariateinfo(self):
        try:
            #pylint: disable=E0611
            from scipy.interpolate import UnivariateSpline
            Xpoints,Ypoints = self.findpoints()  #from lowest point to avoid many coeficients
            equ = UnivariateSpline(Xpoints, Ypoints)
            coeffs = equ.get_coeffs().tolist()
            knots = equ.get_knots().tolist()
            resid = equ.get_residual()
            roots = equ.roots().tolist()

            #interpretation of coefficients: http://www.sagenb.org/home/pub/1708/
            #spline=[ans[0,i]+(x-xi)*(ans[1,i]+(x-xi)*(ans[2,i]+(x-xi)*ans[3,i]/3)/2) for i,xi in enumerate(a[:-1])]
            
            string = "<b>" + u(QApplication.translate("Message","Polynomial coefficients (Horner form):",
                                                    None)) + "</b><br><br>"
            string += str(coeffs) + "<br><br>"
            string += "<b>" + u(QApplication.translate("Message","Knots:",
                                                     None)) + "</b><br><br>"
            string += str(knots) + "<br><br>"
            string += "<b>" + u(QApplication.translate("Message","Residual:",
                                                     None)) + "</b><br><br>"
            string += str(resid) + "<br><br>"      
            string += "<b>" + u(QApplication.translate("Message","Roots:",
                                                     None)) + "</b><br><br>"
            string += str(roots)

            QMessageBox.information(self,QApplication.translate("Message","Profile information",None),string)

        except ValueError as e:
            _, _, exc_tb = sys.exc_info() 
            aw.qmc.adderror((QApplication.translate("Error Message","Value Error:",None) + " univariateinfo() {0}").format(str(e)),exc_tb.tb_lineno)
            return

        except Exception as e:
            _, _, exc_tb = sys.exc_info() 
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " univariateinfo() {0}").format(str(e)),exc_tb.tb_lineno)
            return

    def polyfit(self,xarray,yarray,deg,startindex,endindex,deltacurvep):
        xa = xarray[startindex:endindex]
        ya = yarray[startindex:endindex]
        if len(xa) > 0 and len(xa) == len(ya):
            try:
                z = numpy.polyfit(xa,ya,deg)
                p = numpy.poly1d(z)
                x = p(xarray[startindex:endindex])
                pad = max(0,len(self.timex) - startindex - len(x))
                xx = numpy.append(numpy.append([None]*max(0,startindex), x), [None]*pad)
                if deltacurvep:
                    self.delta_ax.plot(self.timex, xx, linestyle = '--', linewidth=3)
                else:
                    self.ax.plot(self.timex, xx, linestyle = '--', linewidth=3)
                self.fig.canvas.draw()
                return z
            except:
                return None
        else:
            return None

    #interpolation type
    def univariate(self):
        try:
            #pylint: disable=E0611
            from scipy.interpolate import UnivariateSpline
            Xpoints,Ypoints = self.findpoints()

            func = UnivariateSpline(Xpoints, Ypoints)

            xa = numpy.array(self.timex)
            newX = func(xa).tolist()

            self.ax.plot(self.timex, newX, color="black", linestyle = '-.', linewidth=3)
            self.ax.plot(Xpoints, Ypoints, "ro")

            self.fig.canvas.draw()

        except ValueError:
            aw.qmc.adderror(QApplication.translate("Error Message","Value Error:",None) + " univariate()")
            return

        except Exception as e:
            #traceback.print_exc(file=sys.stdout)
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror(QApplication.translate("Error Message","Exception:",None) + " univariate() " + str(e),exc_tb.tb_lineno)
            return

    def drawinterp(self,mode):
        try:
            #pylint: disable=E1101
            from scipy import interpolate as inter
            Xpoints,Ypoints = self.findpoints() #from 0 origin
            func = inter.interp1d(Xpoints, Ypoints, kind=mode)
            newY = func(self.timex)
            self.ax.plot(self.timex, newY, color="black", linestyle = '-.', linewidth=3)
            self.ax.plot(Xpoints, Ypoints, "ro")

            self.fig.canvas.draw()

        except ValueError as e:
            _, _, exc_tb = sys.exc_info() 
            aw.qmc.adderror((QApplication.translate("Error Message","Value Error:",None) + " drawinterp() {0}").format(str(e)),exc_tb.tb_lineno)
            return

        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message","Exception:",None) + " drawinterp() {0}").format(str(e)),exc_tb.tb_lineno)
            return

    def timearray2index(self,timearray,seconds):
        #find where given seconds crosses timearray
        if len(timearray):                           #check that timearray is not empty just in case
            #if input seconds longer than available time return last index
            if  seconds > timearray[-1]:
                return len(timearray)-1
            #if given input seconds smaller than first time return first index
            if seconds < timearray[0]:
                return 0
            i = numpy.searchsorted(timearray,seconds,side='left')
            if i < len(self.timex) - 1:
                #look around (check if the value of the next index is closer
                choice1 = abs(self.timex[i] - seconds)
                choice2 = abs(self.timex[i-1] - seconds)
                #return closest (smallest) index
                if choice2 < choice1:
                    i = i - 1
            return i
        else:
            return -1

    #selects closest time INDEX in self.timex from a given input float seconds
    def time2index(self,seconds):
        #find where given seconds crosses aw.qmc.timex
        return self.timearray2index(self.timex,seconds)

    #selects closest time INDEX in self.timeB from a given input float seconds
    def backgroundtime2index(self,seconds):
        #find where given seconds crosses aw.qmc.timex
        return self.timearray2index(self.timeB,seconds)

    #updates list self.timeindex when found an _OLD_ profile without self.timeindex (new version)
    def timeindexupdate(self,times):
	##        #          START            DRYEND          FCs             FCe         SCs         SCe         DROP
	##        times = [self.startend[0],self.dryend[0],self.varC[0],self.varC[2],self.varC[4],self.varC[6],self.startend[2]]
        for i in arange(len(times)):               
            if times[i]:
                self.timeindex[i] = self.time2index(times[i])
            else:
                self.timeindex[i] = 0

    #updates list self.timeindexB when found an _OLD_ profile without self.timeindexB 
    def timebackgroundindexupdate(self,times):
	##        #          STARTB            DRYENDB          FCsB       FCeB         SCsB         SCeB               DROPB
	##        times = [self.startendB[0],self.dryendB[0],self.varCB[0],self.varCB[2],self.varCB[4],self.varCB[6],self.startendB[2]]
        for i in arange(len(times)):               
            if times[i]:
                self.timeindexB[i] = self.backgroundtime2index(times[i])
            else:
                self.timeindexB[i] = 0

    #adds errors
    def adderror(self,error,line=None):
        try:
            #### lock shared resources #####
            aw.qmc.errorsemaphore.acquire(1)
            timez = str(QDateTime.currentDateTime().toString(u("hh:mm:ss.zzz")))    #zzz = miliseconds
            #keep a max of 500 errors
            if len(self.errorlog) > 499:
                self.errorlog = self.errorlog[1:]
            if line:
                error = error + "@line " + str(line)
            self.errorlog.append(timez + " " + error)
            aw.sendmessage(error)
        except:
            pass
        finally:
            if aw.qmc.errorsemaphore.available() < 1:
                aw.qmc.errorsemaphore.release(1)

    ####################  PROFILE DESIGNER   ###################################################################################
    #launches designer
    def designer(self):
        #disconnect mouse cross if ON
        if self.crossmarker:
            self.togglecrosslines()

        if len(self.timex):
            reply = QMessageBox.question(self,QApplication.translate("Message","Designer Start",None),
                                         QApplication.translate("Message","Importing a profile in to Designer will decimate all data except the main [points].\nContinue?",None),
                                         QMessageBox.Yes|QMessageBox.Cancel)
            if reply == QMessageBox.Yes:
                self.initfromprofile()
                self.connect_designer()
                self.redraw(True)
            elif reply == QMessageBox.Cancel:
                aw.designerAction.setChecked(False)
        else:
            #if no profile found
            self.reset(redraw=False,soundOn=False)
            self.connect_designer()
            self.designerinit()

    #used to start designer from scratch (not from a loaded profile)
    def designerinit(self):
        #init start vars        #CH, DE,      Fcs,      Fce,       Scs,         Sce,         Drop,      COOL
        self.designertimeinit = [50,(5*60+50),(8*60+50),(10*60+50),(10.5*60+50),(11.5*60+50),(12*60+50),(16*60+50)]
        if self.mode == "C":
            self.designertemp1init = [290.,290.,290.,290.,290.,290.,290.,290.]   #CHARGE,DE,FCs,FCe,SCs,SCe,Drop
            self.designertemp2init = [230.,150.,190.,210.,220.,225.,230.,230.]   #CHARGE,DE,FCs,FCe,SCs,SCe,DROP
        elif self.mode == "F":
            self.designertemp1init = [500.,500.,500.,500.,500.,500.,500.,500.]
            self.designertemp2init = [440.,300.,385.,410.,430.,445.,460.,460.]
            
        #check x limits
        if self.endofx < 960:
            self.endofx = 960
            self.redraw()

        self.timex,self.temp1,self.temp2 = [],[],[]
        for i in arange(len(self.timeindex)):
            self.timex.append(self.designertimeinit[i])
            self.temp1.append(self.designertemp1init[i])
            self.temp2.append(self.designertemp2init[i])
            self.timeindex[i] = i

        self.xaxistosm(redraw=False)
        self.redrawdesigner()

    #loads main points from a profile so that they can be edited
    def initfromprofile(self):
        if self.timeindex[0] == -1 or self.timeindex[6] == 0:
            QMessageBox.information(self,QApplication.translate("Message","Designer Init",None),
                                    QApplication.translate("Message","Unable to start designer.\nProfile missing [CHARGE] or [DROP]",None))
            self.disconnect_designer()
            return()

        #save events. They will be deleted on qmc.reset()
        self.specialeventsStringscopy = self.specialeventsStrings[:]
        self.specialeventsvaluecopy = self.specialeventsvalue[:]
        self.specialeventstypecopy = self.specialeventstype[:]
        self.eventtimecopy = []
        for i in range(len(self.specialevents)):
            #save relative time of events
            self.eventtimecopy.append(self.timex[self.specialevents[i]]-self.timex[self.timeindex[0]])

        #find lowest point from profile to be converted
        lpindex = aw.findTP()
        if lpindex != -1 and not lpindex in self.timeindex:
            lptime = self.timex[lpindex]
            lptemp2 = self.temp2[lpindex]
        else:
            lpindex = -1

        timeindexhold = [self.timex[self.timeindex[0]],0,0,0,0,0,0,0]
        timez,t1,t2 = [self.timex[self.timeindex[0]]],[self.temp1[self.timeindex[0]]],[self.temp2[self.timeindex[0]]]    #first CHARGE point
        for i in range(1,len(self.timeindex)):
            if self.timeindex[i]:                           # fill up empty lists with main points (FCs, etc). match from timeindex
                timez.append(self.timex[self.timeindex[i]])  #add time
                t1.append(self.temp1[self.timeindex[i]])    #add temp1
                t2.append(self.temp2[self.timeindex[i]])    #add temp2
                timeindexhold[i] =  self.timex[self.timeindex[i]]

        self.reset()                                            #erase screen

        self.timex,self.temp1,self.temp2 = timez[:],t1[:],t2[:]  #copy lists back after reset() with the main points

        self.timeindexupdate(timeindexhold) #create new timeindex[]

        #add lowest point as extra point
        if lpindex != -1:
            self.currentx = lptime
            self.currenty = lptemp2
            self.addpoint()

        self.xaxistosm(redraw=False)
        self.redrawdesigner()                                   #redraw the designer screen

    #redraws designer
    def redrawdesigner(self):
        if aw.qmc.designerflag:
            #pylint: disable=E0611
            from scipy.interpolate import UnivariateSpline
            #reset (clear) plot
            if self.DeltaBTflag or self.DeltaETflag:
                self.delta_ax.lines = []
            if self.background:
                self.ax.lines = self.ax.lines[0:4]
            else:
                self.ax.lines = []
                
            #create statistics bar
            #calculate the positions for the statistics elements
            ydist = self.ylimit - self.ylimit_min
            statisticsheight = self.ylimit - (0.13 * ydist)

            #add statistics bar
            self.ax.plot([self.timex[self.timeindex[0]],self.timex[self.timeindex[1]]],[statisticsheight,statisticsheight],color = self.palette["rect1"],alpha=.5,linewidth=5)
            self.ax.plot([self.timex[self.timeindex[1]],self.timex[self.timeindex[2]]],[statisticsheight,statisticsheight],color = self.palette["rect2"],alpha=.5,linewidth=5)
            self.ax.plot([self.timex[self.timeindex[2]],self.timex[self.timeindex[6]]],[statisticsheight,statisticsheight],color = self.palette["rect3"],alpha=.5,linewidth=5)

            #add phase division lines
            ylist = [self.ylimit,0]
            self.ax.plot([self.timex[self.timeindex[0]],self.timex[self.timeindex[0]]],ylist,color = self.palette["grid"],alpha=.3,linewidth=3,linestyle="--")
            self.ax.plot([self.timex[self.timeindex[1]],self.timex[self.timeindex[1]]],ylist,color = self.palette["grid"],alpha=.3,linewidth=3,linestyle="--")
            self.ax.plot([self.timex[self.timeindex[2]],self.timex[self.timeindex[2]]],ylist,color = self.palette["grid"],alpha=.3,linewidth=3,linestyle="--")
            self.ax.plot([self.timex[self.timeindex[6]],self.timex[self.timeindex[6]]],ylist,color = self.palette["grid"],alpha=.3,linewidth=3,linestyle="--")

            if self.timex[-1] > self.endofx:
                self.endofx = self.timex[-1] + 120
                self.xaxistosm()

            if self.BTsplinedegree >= len(self.timex):  #max 5 or less. Cannot biger than points
                self.BTsplinedegree = len(self.timex)-1

            if self.ETsplinedegree >= len(self.timex):  #max 5 or less. Cannot biger than points
                self.ETsplinedegree = len(self.timex)-1

            timez = numpy.arange(self.timex[0],self.timex[-1],1).tolist()
            func = UnivariateSpline(self.timex,self.temp2, k = self.BTsplinedegree)
            btvals = func(timez).tolist()
            func2 = UnivariateSpline(self.timex,self.temp1, k = self.ETsplinedegree)
            etvals = func2(timez).tolist()
            #convert all time values to temperature

            #add markers (big circles) '0'
            if self.ETcurve:
                self.ax.plot(self.timex,self.temp1,color = self.palette["et"],marker = "o",picker=10,linestyle='',markersize=8)
            if self.BTcurve:
                self.ax.plot(self.timex,self.temp2,color = self.palette["bt"],marker = "o",picker=10,linestyle='',markersize=8)     #picker = 10 means 10 points tolerance

            rcParams['path.sketch'] = (0,0,0)
            #add curves
            if self.ETcurve:
                self.ax.plot(timez, etvals,markersize=self.ETmarkersize,marker=self.ETmarker,linewidth=self.ETlinewidth,
                    linestyle=self.ETlinestyle,drawstyle=self.ETdrawstyle,color=self.palette["et"],
                        label=u(QApplication.translate("Label", "ET", None)))
            if self.BTcurve:
                self.ax.plot(timez, btvals, markersize=self.BTmarkersize,marker=self.BTmarker,linewidth=self.BTlinewidth,
                    linestyle=self.BTlinestyle,drawstyle=self.BTlinestyle,color=self.palette["bt"],
                        label=u(QApplication.translate("Label", "BT", None)))
                        
                        
            if self.DeltaBTflag:
                funcDelta = func.derivative()
                deltabtvals = [x*60 for x in funcDelta(timez).tolist()]
                self.delta_ax.plot(timez,deltabtvals,markersize=self.BTdeltamarkersize,marker=self.BTdeltamarker,
                    sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.BTdeltalinewidth+aw.qmc.patheffects,foreground="w")],
                    linewidth=self.BTdeltalinewidth,linestyle=self.BTdeltalinestyle,drawstyle=self.BTdeltadrawstyle,color=self.palette["deltabt"],
                    label=aw.arabicReshape(QApplication.translate("Label", "DeltaBT", None)))
                    
            if self.DeltaETflag:
                funcDelta2 = func2.derivative()
                deltaetvals = [x*60 for x in funcDelta2(timez).tolist()]
                self.delta_ax.plot(timez,deltaetvals,markersize=self.ETdeltamarkersize,marker=self.ETdeltamarker,
                    sketch_params=None,path_effects=[PathEffects.withStroke(linewidth=self.ETdeltalinewidth+aw.qmc.patheffects,foreground="w")],
                    linewidth=self.ETdeltalinewidth,linestyle=self.ETdeltalinestyle,drawstyle=self.ETdeltadrawstyle,color=self.palette["deltaet"],
                    label=aw.arabicReshape(QApplication.translate("Label", "DeltaET", None)))                          
        
            #plot
            self.fig.canvas.draw()

    #CONTEXT MENU  = Right click
    def on_press(self,event):
        if event.inaxes != self.ax: return
        if event.button != 3: return   #select right click only

        self.releaseMouse()
        self.mousepress = False
        self.setCursor(Qt.OpenHandCursor)

        self.currentx = event.xdata
        self.currenty = event.ydata

        designermenu = QMenu(self)

        createAction = QAction(QApplication.translate("Contextual Menu", "Create",None),self)
        createAction.triggered.connect(self.convert_designer)
        designermenu.addAction(createAction)

        configAction = QAction(QApplication.translate("Contextual Menu", "Config...",None),self)
        configAction.triggered.connect(self.desconfig)
        designermenu.addAction(configAction)

        backgroundAction = QAction(UIconst.ROAST_MENU_BACKGROUND,self)
        backgroundAction.triggered.connect(aw.background)
        designermenu.addAction(backgroundAction)

        designermenu.addSeparator()

        addpointAction = QAction(QApplication.translate("Contextual Menu", "Add point",None),self)
        addpointAction.triggered.connect(self.addpoint)
        designermenu.addAction(addpointAction)

        removepointAction = QAction(QApplication.translate("Contextual Menu", "Remove point",None),self)
        removepointAction.triggered.connect(self.removepoint)
        designermenu.addAction(removepointAction)

        designermenu.addSeparator()

        resetAction = QAction(QApplication.translate("Contextual Menu", "Reset Designer",None),self)
        resetAction.triggered.connect(self.reset_designer)
        designermenu.addAction(resetAction)

        exitAction = QAction(QApplication.translate("Contextual Menu", "Exit Designer",None),self)
        exitAction.triggered.connect(aw.stopdesigner)
        designermenu.addAction(exitAction)

        designermenu.exec_(QCursor.pos())

    def on_pick(self,event):
        self.setCursor(Qt.ClosedHandCursor)

        self.indexpoint = event.ind
        self.mousepress = True

        line = event.artist
        #identify which line is being edited
        ydata = line.get_ydata()
        if ydata[1] == self.temp1[1]:
            self.workingline = 1
        else:
            self.workingline = 2

    #handles when releasing mouse
    def on_release(self,event):
        self.mousepress = False
        self.setCursor(Qt.OpenHandCursor)

    #handler for moving point
    def on_motion(self,event):
        if not event.inaxes: return
        
        ydata = event.ydata
        
        try:
            if self.mousepress:                                 #if mouse clicked
                self.timex[self.indexpoint] = event.xdata
                if self.workingline == 1:
                    self.temp1[self.indexpoint] = ydata
                else:
                    self.temp2[self.indexpoint] = ydata

                #check point going over point
                #check to the left    
                if self.indexpoint > 0:
                    if abs(self.timex[self.indexpoint] - self.timex[self.indexpoint - 1]) < 10.:
                        self.unrarefy_designer()
                        return
                #check to the right
                if self.indexpoint <= len(self.timex)-2:
                    if abs(self.timex[self.indexpoint] - self.timex[self.indexpoint + 1]) < 10.:
                        self.unrarefy_designer()
                        return

                #check for possible CHARGE time moving
                if self.indexpoint == self.timeindex[0]:
                    self.xaxistosm(redraw=False)

                #redraw
                self.redrawdesigner()
                return

            if type(event.xdata):                       #outside graph type is None
                for i in arange(len(self.timex)):
                    if abs(event.xdata - self.timex[i]) < 7.:
                        if i in self.timeindex:
                            if abs(self.temp2[i] - ydata) < 10:
                                self.ax.plot(self.timex[i],self.temp2[i],color = "orange",marker = "o",alpha = .3,markersize=30)
                                self.fig.canvas.draw()
                                QTimer.singleShot(600, self.redrawdesigner)
                            elif abs(self.temp1[i] - ydata) < 10:
                                self.ax.plot(self.timex[i],self.temp1[i],color = "orange",marker = "o",alpha = .3,markersize=30)
                                self.fig.canvas.draw()
                                QTimer.singleShot(600, self.redrawdesigner)
                            index = self.timeindex.index(i)
                            if index == 0:
                                timez = self.stringfromseconds(0)
                                aw.messagelabel.setText("<font style=\"BACKGROUND-COLOR: #f07800\">" + u(QApplication.translate("Message", "[ CHARGE ]",None)) + "</font> " + timez)
                            elif index == 1:
                                timez = self.stringfromseconds(self.timex[self.timeindex[1]] - self.timex[self.timeindex[0]])
                                aw.messagelabel.setText("<font style=\"BACKGROUND-COLOR: orange\">" + u(QApplication.translate("Message", "[ DRY END ]",None)) + "</font> " + timez)
                            elif index == 2:
                                timez = self.stringfromseconds(self.timex[self.timeindex[2]] - self.timex[self.timeindex[0]])
                                aw.messagelabel.setText("<font style=\"BACKGROUND-COLOR: orange\">" + u(QApplication.translate("Message", "[ FC START ]",None)) + "</font> " + timez)
                            elif index == 3:
                                timez = self.stringfromseconds(self.timex[self.timeindex[3]] - self.timex[self.timeindex[0]])                                
                                aw.messagelabel.setText("<font style=\"BACKGROUND-COLOR: orange\">" + u(QApplication.translate("Message", "[ FC END ]",None)) + "</font> " + timez)
                            elif index == 4:
                                timez = self.stringfromseconds(self.timex[self.timeindex[4]] - self.timex[self.timeindex[0]])
                                aw.messagelabel.setText("<font style=\"BACKGROUND-COLOR: orange\">" + u(QApplication.translate("Message", "[ SC START ]",None)) + "</font> " + timez)
                            elif index == 5:
                                timez = self.stringfromseconds(self.timex[self.timeindex[5]] - self.timex[self.timeindex[0]])
                                aw.messagelabel.setText("<font style=\"BACKGROUND-COLOR: orange\">" + u(QApplication.translate("Message", "[ SC END ]",None)) + "</font> " + timez)
                            elif index == 6:
                                timez = self.stringfromseconds(self.timex[self.timeindex[6]] - self.timex[self.timeindex[0]])
                                aw.messagelabel.setText("<font style=\"BACKGROUND-COLOR: #f07800\">" + u(QApplication.translate("Message", "[ DROP ]",None)) + "</font> " + timez)
                            break
                        else:
                            if abs(self.temp2[i] - ydata) < 10:
                                self.ax.plot(self.timex[i],self.temp2[i],color = "blue",marker = "o",alpha = .3,markersize=30)
                                self.fig.canvas.draw()
                                QTimer.singleShot(600, self.redrawdesigner)
                            elif abs(self.temp1[i] - ydata) < 10:
                                self.ax.plot(self.timex[i],self.temp1[i],color = "blue",marker = "o",alpha = .3,markersize=30)
                                self.fig.canvas.draw()
                                QTimer.singleShot(600, self.redrawdesigner)
                            timez = self.stringfromseconds(self.timex[i] - self.timex[self.timeindex[0]])
                            aw.messagelabel.setText("<font style=\"BACKGROUND-COLOR: lightblue\">%s</font> "%timez)
                            break
                    else:
                        totaltime = self.timex[self.timeindex[6]] - self.timex[self.timeindex[0]]
                        dryphasetime = self.timex[self.timeindex[1]] - self.timex[self.timeindex[0]]
                        midphasetime = self.timex[self.timeindex[2]] - self.timex[self.timeindex[1]]
                        finishphasetime = self.timex[self.timeindex[6]] - self.timex[self.timeindex[2]]

                        if totaltime:
                            dryphaseP = int(round(dryphasetime*100./totaltime))
                            midphaseP = int(round(midphasetime*100./totaltime))
                            finishphaseP = int(round(finishphasetime*100./totaltime))
                        else:
                            return

                        dryramp = self.temp2[self.timeindex[1]] - self.temp2[self.timeindex[0]]
                        midramp = self.temp2[self.timeindex[2]] - self.temp2[self.timeindex[1]]
                        finishramp = self.temp2[self.timeindex[6]] - self.temp2[self.timeindex[2]]

                        ts1,_,_ = aw.ts(self.timeindex[0],self.timeindex[1])
                        ts2,_,_ = aw.ts(self.timeindex[1],self.timeindex[2])
                        ts3,_,_ = aw.ts(self.timeindex[2],self.timeindex[6])
                        etbt1 = "%i"%(ts1)
                        etbt2 = "%i"%(ts2)
                        etbt3 = "%i"%(ts3)

                        if dryphasetime:
                            dryroc = (" %.1f " + aw.qmc.mode + "/min")%((dryramp/dryphasetime)*60.)
                        else:
                            dryroc = " 0 " + aw.qmc.mode + "/min"

                        if midphasetime:
                            midroc = (" %.1f " + aw.qmc.mode + "/min")%((midramp/midphasetime)*60.)
                        else:
                            midroc = " 0 " + aw.qmc.mode + "/min"

                        if finishphasetime:
                            finishroc = (" %.1f " + aw.qmc.mode + "/min")%((finishramp/finishphasetime)*60.)
                        else:
                            finishroc = 0

                        margin = "&nbsp;&nbsp;&nbsp;"
                        string1 = " <font color = \"white\" style=\"BACKGROUND-COLOR: %s\">%s %s %s %i%% %s %s %s %s %s</font>"%(self.palette["rect1"],
                                  margin,self.stringfromseconds(dryphasetime),margin, dryphaseP, margin,dryroc,margin,etbt1,margin)
                        string2 = " <font color = \"white\" style=\"BACKGROUND-COLOR: %s\">%s %s %s %i%% %s %s %s %s %s</font>"%(self.palette["rect2"],
                                  margin,self.stringfromseconds(midphasetime),margin,midphaseP,margin,midroc,margin,etbt2,margin)
                        string3 = " <font color = \"white\" style=\"BACKGROUND-COLOR: %s\">%s %s %s %i%% %s %s %s %s %s</font>"%(self.palette["rect3"],
                                  margin,self.stringfromseconds(finishphasetime),margin,finishphaseP,margin,finishroc,margin,etbt3,margin)
                        aw.messagelabel.setText(string1+string2+string3)

        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " on_motion() {0}").format(str(e)),exc_tb.tb_lineno)
            self.unrarefy_designer()
            return

    #this is used in on_motion() to try to prevent points crossing over points
    def unrarefy_designer(self):
        for i in range(len(self.timex)-1):
            if abs(self.timex[i]-self.timex[i+1]) < 20:
                self.timex[i+1] = self.timex[i] + 20
            self.disconnect_designer()
            self.connect_designer()

    def addpoint(self):
        try:
            #current x, and y is obtained when doing right click in mouse: on_press()

            if self.currentx > self.timex[-1]:       #if point is beyond max timex (all the way to the right)
                #find closest line
                d1 = abs(self.temp1[-1] - self.currenty)
                d2 = abs(self.temp2[-1] - self.currenty)
                if d2 < d1:
                    self.temp2.append(self.currenty)
                    self.temp1.append(self.temp1[-1])
                else:
                    self.temp2.append(self.temp2[-1])
                    self.temp1.append(self.currenty)
                    
                self.timex.append(self.currentx)
                #no need to update time index

                self.redrawdesigner()
                return 0

            elif self.currentx < self.timex[0]:         #if point is bellow min timex (all the way to the left)
                #find closest line
                d1 = abs(self.temp1[0] - self.currenty)
                d2 = abs(self.temp2[0] - self.currenty)
                if d2 < d1:
                    self.temp2.insert(0,self.currenty)
                    self.temp1.insert(0,self.temp1[0])
                else:
                    self.temp2.insert(0,self.temp2[0])
                    self.temp1.insert(0,self.currenty)

                self.timex.insert(0,self.currentx)

                #update timeindex
                if self.timeindex[0] != -1:   #we update timeindex[0] different
                    self.timeindex[0] += 1
                for u in range(1,len(self.timeindex)):
                    if self.timeindex[u]:
                        self.timeindex[u] += 1

                self.redrawdesigner()
                return len(self.timex)-1   #return index received from Designer Dialog Config to asign index to timeindex)

            else:                                           #mid range
                #find index
                for i in range(len(self.timex)):
                    if self.timex[i] > self.currentx:
                        break
                #find closest line
                d1 = abs(self.temp1[i] - self.currenty)
                d2 = abs(self.temp2[i] - self.currenty)
                if d2 < d1:
                    self.temp2.insert(i,self.currenty)
                    self.temp1.insert(i,self.temp1[i])
                else:
                    self.temp2.insert(i,self.temp2[i])
                    self.temp1.insert(i,self.currenty)

                self.timex.insert(i,self.currentx)

                #update timeindex
                for x in range(len(self.timeindex)):
                    if self.timeindex[x] >= i:
                        self.timeindex[x] += 1

                self.redrawdesigner()
                return i

        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " addpoint() {0}").format(str(e)),exc_tb.tb_lineno)
            return 

    #removes point
    def removepoint(self):
        try:
            #current x, and y is obtained when doing right click in mouse: on_press()
            #find index
            for i in range(len(self.timex)):
                if self.timex[i] > self.currentx:
                    break
            #find closest point
            if abs(self.timex[i]- self.currentx) < abs(self.timex[i-1] - self.currentx):
                index = i
            else:
                index = i-1

            #check if if it is a landmark point
            if index in self.timeindex:
                whichone = self.timeindex.index(index)
                if whichone == 0 or whichone == 6:  #if charge or drop
                    return
                self.timeindex[whichone] = 0

            self.timex.pop(index)
            self.temp1.pop(index)
            self.temp2.pop(index)

            for x in range(len(self.timeindex)):
                if self.timeindex[x] > index: #decrease time index by one when above the index taken out
                    self.timeindex[x] -= 1

            self.redrawdesigner()

        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " removepoint() {0}").format(str(e)),exc_tb.tb_lineno)
            return

    #finds a proper index location for a time that does not exists yet.
    def designerfindindex(self,time):
        if time < aw.qmc.timex[0]:
            return 0
        elif time > aw.qmc.timex[-1]:
            return len(aw.qmc.timex)-1
        else:
            for i in range(len(aw.qmc.timex)):
                if time == aw.qmc.timex[i]:
                    return i
                if aw.qmc.timex[i] > time:
                    return i-1

    #converts from a designer profile to a normal profile
    def convert_designer(self): 
        try:
            #pylint: disable=E0611
            from scipy.interpolate import UnivariateSpline
            #prevents accidentally deleting a modified profile.
            self.safesaveflag = True
            #create functions
            funcBT = UnivariateSpline(self.timex,self.temp2, k = self.BTsplinedegree)
            funcET = UnivariateSpline(self.timex,self.temp1, k = self.ETsplinedegree)

            #create longer list of time values
            timez = numpy.arange(self.timex[0],self.timex[-1],1).tolist()

            #convert all time values to temperature
            btvals = funcBT(timez).tolist()
            etvals = funcET(timez).tolist()

            #find new indexes for events
            for i in range(len(self.specialevents)):
                for p in range(len(timez)):
                    if timez[p] > self.timex[self.specialevents[i]]:
                        self.specialevents[i] = p
                        break

            #save landmarks
            maintimes = []
            for i in range(len(self.timeindex)):
                maintimes.append(self.timex[self.timeindex[i]])

            self.timex = timez[:]
            self.temp1 = etvals[:]
            self.temp2 = btvals[:]

            self.timeindexupdate(maintimes)

            #check and restore carried over events
            if len(self.eventtimecopy):
                for i in range(len(self.eventtimecopy)):
                    self.specialevents.append(self.time2index(self.eventtimecopy[i] + self.timex[self.timeindex[0]]))
                self.specialeventsStrings = self.specialeventsStringscopy[:]
                self.specialeventsvalue = self.specialeventsvaluecopy[:]
                self.specialeventstype = self.specialeventstypecopy[:]

            #check for extra devices
            num = len(self.timex)
            for i in range(len(self.extradevices)):
                self.extratemp1[i] = [-1.]*num
                self.extratemp2[i] = [-1.]*num
                self.extratimex[i] = self.timex[:]

            self.disconnect_designer()

            #create playback events
            if self.reproducedesigner:
                functioncall = [0,
                                self.designer_create_BT_rateofchange,
                                self.designer_create_ET_rateofchange,
                                self.designer_create_sv_command,
                                self.designer_create_ramp_command]

                functioncall[self.reproducedesigner]()

            self.redraw()
            aw.sendmessage(QApplication.translate("Message", "New profile created",None))

        except ValueError:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror(QApplication.translate("Error Message", "Value Error:",None) + " createFromDesigner()",exc_tb.tb_lineno)
            return

        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " createFromDesigner() {0}").format(str(e)),exc_tb.tb_lineno)
            return

    #activates mouse events
    def connect_designer(self):
        if not self.designerflag:
            self.designerflag = True
            aw.designerAction.setChecked(True)
            self.setCursor(Qt.OpenHandCursor)
            self.mousepress = None
            #create mouse events. Note: keeping the ids inside a list helps protect against extrange python behaviour.
            self.designerconnections = [0,0,0,0]
            self.designerconnections[0] = self.fig.canvas.mpl_connect('pick_event', self.on_pick) 
            self.designerconnections[1] = self.fig.canvas.mpl_connect('button_release_event', self.on_release)
            self.designerconnections[2] = self.fig.canvas.mpl_connect('motion_notify_event', self.on_motion)
            self.designerconnections[3] = self.fig.canvas.mpl_connect('button_press_event', self.on_press) #right click
            #this is needed to prevent complaints from UnivariateSpline() -used in redraw()- in extreme cases of difficulty
            warnings.simplefilter('ignore', UserWarning)

    #deactivates mouse events
    def disconnect_designer(self):
        for i in range(len(self.designerconnections)):
            if self.designerconnections[i]:
                self.fig.canvas.mpl_disconnect(self.designerconnections[i])
        self.setCursor(Qt.ArrowCursor)
        self.designerflag = False
        aw.designerAction.setChecked(False)
        warnings.simplefilter('default', UserWarning)

    #launches designer config Window
    def desconfig(self):
        dialog = designerconfigDlg(self)
        dialog.show()
        dialog.setFixedSize(dialog.size())

    def reset_designer(self):
        self.reset()
        self.disconnect_designer()
        self.connect_designer()
        self.designerinit()

    #saves next BT rate of change till next landmark as an event (example idea for arduino TC4)
    def designer_create_BT_rateofchange(self):
        self.deleteEvents()
        lastindexused = 0
        for i in range(1,len(self.timeindex)):
            if self.timeindex[i]:
                difftemp = self.temp2[self.timeindex[i]] - self.temp2[self.timeindex[lastindexused]]
                difftime = (self.timex[self.timeindex[i]] - self.timex[self.timeindex[lastindexused]])/60.
                if difftime:
                    string = u(QApplication.translate("Label", "BT {0} {1}/min for {2}",None).format("%.1f"%(difftemp/difftime),aw.qmc.mode,self.stringfromseconds(self.timex[self.timeindex[i]]-self.timex[self.timeindex[lastindexused]])))
                    self.specialevents.append(self.timeindex[lastindexused])
                    self.specialeventstype.append(0)
                    self.specialeventsStrings.append(string)
                    self.specialeventsvalue.append(0)
                    lastindexused = i

    #saves next BT rate of change till next landmark as an event (example idea for arduino TC4)
    def designer_create_ET_rateofchange(self):
        self.deleteEvents()
        lastindexused = 0
        for i in range(1,len(self.timeindex)):
            if self.timeindex[i]:
                difftemp = self.temp1[self.timeindex[i]] - self.temp1[self.timeindex[lastindexused]]
                difftime = (self.timex[self.timeindex[i]] - self.timex[self.timeindex[lastindexused]])/60.
                if difftime:
                    string = u(QApplication.translate("Label", "ET {0} {1}/min for {2}",None).format("%.1f"%(difftemp/difftime),aw.qmc.mode,self.stringfromseconds(self.timex[self.timeindex[i]]-self.timex[self.timeindex[lastindexused]])))
                    self.specialevents.append(self.timeindex[lastindexused])
                    self.specialeventstype.append(0)
                    self.specialeventsStrings.append(string)
                    self.specialeventsvalue.append(0)
                    lastindexused = i

    def deleteEvents(self):
        self.specialevents = []
        self.specialeventstype = []
        self.specialeventsStrings = []
        self.specialeventsvalue = []

    #this is used to create a string in pid language to reproduce the profile from Designer
    #NOTE: pid runs ET (temp1)    
    def designer_create_ramp_command(self):
        tempinits = []
        minutes_segments = []

        #ramp times in minutes
        minsDryPhase = str(int(abs(self.timex[self.timeindex[0]] - self.timex[self.timeindex[1]])/60))
        minsMidPhase = str(int(abs(self.timex[self.timeindex[1]] - self.timex[self.timeindex[2]])/60)) 
        minsFinishPhase = str(int(abs(self.timex[self.timeindex[2]] - self.timex[self.timeindex[6]])/60))

        #target temps for ET
        tempinits.append("%.1f"%self.temp1[self.timeindex[1]])
        tempinits.append("%.1f"%self.temp1[self.timeindex[2]])
        tempinits.append("%.1f"%self.temp1[self.timeindex[6]])

        minutes_segments.append(minsDryPhase)
        minutes_segments.append(minsMidPhase)
        minutes_segments.append(minsFinishPhase)

        command = ""
        for i in range(3):
            command += "SETRS::" + tempinits[i] + "::" + minutes_segments[i] + "::0::"
        command += "SETRS::" + tempinits[-1] + "::0::0"

        self.clean_old_pid_commands()

        #do only one event but with all segments
        self.specialevents.append(0)
        self.specialeventstype.append(0)
        self.specialeventsStrings.append(command)
        self.specialeventsvalue.append(0)

    #this is used to create a string in ET temp language to reproduce the profile from Designer
    def designer_create_sv_command(self):
        self.clean_old_pid_commands()
        for i in range(len(self.timeindex)-1):
            command = "SETSV::%.1f"%self.temp1[self.timeindex[i+1]]
            if i > 0 and self.timeindex[i]:
                self.specialevents.append(self.timeindex[i])
                self.specialeventstype.append(0)
                self.specialeventsStrings.append(command)
                self.specialeventsvalue.append(0)

    #verifies there are no previos machine commands on events
    def clean_old_pid_commands(self):
        #check for possible preloaded machine commands
        target = 0
        if len(self.specialevents):
            for i in range(len(self.specialevents)):
                if "::" in self.specialeventsStrings[i]:
                    self.specialevents.pop(i)
                    self.specialeventstype.pop(i)
                    self.specialeventsStrings.pop(i)
                    self.specialeventsvalue.pop(i)
                    target = 1
                    break     #break or the index i can become larger than the new shorted length of specialevents
        if target:
            self.clean_old_pid_commands()

    ###################################      WHEEL GRAPH  ####################################################

    def findCenterWheelTextAngle(self,t):
        if t > 360. or t < 0.:
            _,t = divmod(t,360.)
        if t == 360. or t == 0.:
            return 270.
        #check cuadrants
        elif t < 90. and t > 0. or t > 360.:        #quadrant 1
            return 270.+t
        elif t <= 180. and t >= 90.:                #quadrant 2
            return t-90.
        elif t < 270. and t > 180.:                 #quadrant 3
            return t+90.
        else:                                       #quadrant 4
            return t-270.

    def findRadialWheelTextAngle(self,t):
        if t > 360. or t < 0.:
            _,t = divmod(t,360.)  
        if t <= 90. and t > 0. or t > 270.:
            return t
        else:
            return 180.+t

    def loadselectorwheel(self,path):
        string = "Wheels" + "\\" + path
        direct = QDir()
        pathDir = direct.toNativeSeparators(u(string))
        filename = aw.ArtisanOpenFileDialog(msg=QApplication.translate("Message","Open Wheel Graph",None),path=pathDir,ext="*.wg")
        if filename:
            self.connectWheel()
            aw.loadWheel(filename)
            self.drawWheel()

    def addTocuppingnotes(self):
        descriptor =  u(self.wheelnames[self.wheelx][self.wheelz]) 
        self.cuppingnotes += "\n" + descriptor 
        string = u(QApplication.translate("Message", " added to cupping notes",None))
        aw.sendmessage(descriptor + string)

    def addToroastingnotes(self):
        descriptor =  u(self.wheelnames[self.wheelx][self.wheelz]) + " "
        self.roastingnotes +=  "\n" + descriptor + " "
        string = u(QApplication.translate("Message", " added to roasting notes",None))
        aw.sendmessage(descriptor + string)

    def wheel_pick(self,event):
        rect =  event.artist
        loc = rect.get_url().split("-")
        x = int(loc[0])
        z = int(loc[1])
        self.wheelx = x
        self.wheelz = z
        aw.sendmessage(self.wheelnames[x][z])
        self.segmentsalpha[x][z] += .3
        self.drawWheel()

    def wheel_release(self,event):
        newlocz = event.xdata
        if newlocz:
            if newlocz != self.wheellocationz:
                diff = math.degrees(self.wheellocationx - newlocz)
                for i in range(len(self.startangle)):
                    self.startangle[i] -= diff
                self.segmentsalpha[self.wheelx][self.wheelz] -= .3   #restore alpha in mouse selection 
                self.drawWheel()

    def wheel_menu(self,event):
        if str(event.inaxes) != str(self.ax2): return
        if event.button == 1:                    #if left click
            self.wheellocationx = event.xdata
            self.wheellocationz = event.ydata

        elif event.button == 3:                  #if right click
            designermenu = QMenu(self)
            cuppingAction = QAction(QApplication.translate("Contextual Menu", "Add to Cupping Notes",None),self)
            cuppingAction.triggered.connect(self.addTocuppingnotes)
            designermenu.addAction(cuppingAction)

            roastingAction = QAction(QApplication.translate("Contextual Menu", "Add to Roasting Notes",None),self)
            roastingAction.triggered.connect(self.addToroastingnotes)
            designermenu.addAction(roastingAction)

            cancelwheelAction = QAction(QApplication.translate("Contextual Menu", "Cancel selection",None),self)
            cancelwheelAction.triggered.connect(self.cancelwheelselection)
            designermenu.addAction(cancelwheelAction)

            editAction = QAction(QApplication.translate("Contextual Menu", "Edit Mode",None),self)
            editAction.triggered.connect(self.editmode)
            designermenu.addAction(editAction)

            exitAction = QAction(QApplication.translate("Contextual Menu", "Exit",None),self)
            exitAction.triggered.connect(self.exitviewmode)
            designermenu.addAction(exitAction)

            designermenu.exec_(QCursor.pos())

    def cancelwheelselection(self):
        self.segmentsalpha[self.wheelx][self.wheelz] -= .3   #restore alpha in mouse selection
        self.drawWheel()

    def editmode(self):
        self.segmentsalpha[self.wheelx][self.wheelz] -= .3   #restore alpha in mouse selection
        self.disconnectWheel(buttomvisibility=False)
        aw.graphwheel()

    def exitviewmode(self):
        self.segmentsalpha[self.wheelx][self.wheelz] -= .3   #restore alpha in mouse selection 
        self.disconnectWheel()
        self.redraw(recomputeAllDeltas=False)

    def connectWheel(self):
        self.wheelflag = True
        self.setCursor(Qt.PointingHandCursor)
        self.wheelconnections[0] = self.fig.canvas.mpl_connect('pick_event', self.wheel_pick)
        self.wheelconnections[1] = self.fig.canvas.mpl_connect('button_press_event', self.wheel_menu)           #right click menu context
        self.wheelconnections[2] = self.fig.canvas.mpl_connect('button_release_event', self.wheel_release)

    def disconnectWheel(self,buttomvisibility=True):
        self.wheelflag = False
        self.setCursor(Qt.ArrowCursor)
        self.fig.canvas.mpl_disconnect(self.wheelconnections[0])
        self.fig.canvas.mpl_disconnect(self.wheelconnections[1])
        self.fig.canvas.mpl_disconnect(self.wheelconnections[2])

    def drawWheel(self):
        try:
            ### var constants  #####
            pi = numpy.pi
            threesixty = 2.*pi
            div = threesixty/100.
            rad = 360./threesixty
            ########################
            # same as redraw but using different axes
            self.fig.clf()
            #create a new name ax1 instead of ax
            self.ax2 = self.fig.add_subplot(111, projection='polar', axisbg=self.backcolor)
            self.ax2.set_rmax(1.)
            self.ax2.set_aspect(self.wheelaspect)
            self.ax2.set_autoscale_on(True)
            self.ax2.grid(False)

            #delete degrees ticks to anotate flavor characteristics 
            for tick in self.ax2.xaxis.get_major_ticks():
                tick.label1On = False
            #delete yaxis 
            locs = self.ax2.get_yticks()
            labels = [""]*len(locs)
            self.ax2.set_yticklabels(labels)

            names = self.wheelnames[:]
            Wradii = self.wradii[:]
            startangle = self.startangle[:]
            projection = self.projection[:]

            #calculate text orientation
            wheels = len(names)

            if not wheels:
                self.fig.canvas.draw()
                return

            n,textangles,textloc = [],[],[]
            for i in range(wheels):
                l,tloc = [],[]
                count = self.startangle[i]
                #calculate text orientation
                for p in range(len(names[i])):
                    if projection[i] == 0:
                        l.append(0)
                    elif projection[i] == 1:
                        l.append(self.findCenterWheelTextAngle(3.6*self.segmentlengths[i][p]/2. + count))
                    elif projection[i] == 2:
                        l.append(self.findRadialWheelTextAngle(3.6*self.segmentlengths[i][p]/2. + count))
                    tloc.append((3.6*self.segmentlengths[i][p]/2. + count)/rad)
                    count += self.segmentlengths[i][p]*3.6

                textloc.append(tloc)
                textangles.append(l)
                Wradii[i] = float(Wradii[i])/100.                   #convert radii to float between 0-1 range
                startangle[i] = startangle[i]/rad                   #convert angles to radians
                n.append(len(names[i]))                             #store the number of names for each wheel


            #store the absolute len-radius origin of each circle
            lbottom = [0.]
            count = 0.
            for i in range(wheels-1):
                count += Wradii[i]
                lbottom.append(count)

            Wradiitext = [Wradii[0]/2.]
            for i in range(wheels-1):
                Wradiitext.append(lbottom[i+1] + Wradii[i+1]/2.)     #store absolute len-radius for text in each circle
                Wradii[i] += self.wheeledge                          #create extra color edge between wheels by overlaping wheels
            #Generate Wheel graph
            bar = []                                                 #holds bar-graphs (wheels)
            for z in range(len(n)):
                #create wheel
                theta,segmentwidth,radii = [],[],[]
                count = startangle[z]
                for i in range(n[z]):
                    #negative number affect eventpicker
                    if count > threesixty:
                        count %= threesixty
                    elif count < 0.:
                        count += threesixty
                    theta.append(count)
                    count += div*self.segmentlengths[z][i]
                    segmentwidth.append(div*self.segmentlengths[z][i])
                    radii.append(Wradii[z])
                bar.append(self.ax2.bar(theta, radii, width=segmentwidth, bottom=lbottom[z],edgecolor=self.wheellinecolor,
                                        linewidth=self.wheellinewidth,picker=1))
                count = 0
                #set color, alpha, and text
                for _,bar[z] in zip(radii, bar[z]):
                    bar[z].set_facecolor(self.wheelcolor[z][count])
                    bar[z].set_alpha(min(self.segmentsalpha[z][count],1))
                    bar[z].set_url(str(z) + "-" + str(count))
                    self.ax2.annotate(names[z][count],xy=(textloc[z][count],Wradiitext[z]),xytext=(textloc[z][count],Wradiitext[z]),
                        rotation=textangles[z][count],horizontalalignment="center",verticalalignment="center",fontsize=self.wheeltextsize[z],fontproperties=aw.mpl_fontproperties)
                    count += 1
            self.fig.canvas.draw()

        except ValueError as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Value Error:",None) + " drawWheel() {0}").format(str(e)),exc_tb.tb_lineno)
            return

        except Exception as e:
            _, _, exc_tb = sys.exc_info()
            aw.qmc.adderror((QApplication.translate("Error Message", "Exception:",None) + " drawWheel() {0}").format(str(e)),exc_tb.tb_lineno)
            return

    def makewheelcolorpattern(self):
        for x in range(len(self.wheelcolor)):
            wlen = len(self.wheelcolor[x])
            for i in range(wlen):
                color = QColor()
                color.setHsv((360/wlen)*i*self.wheelcolorpattern,255,255,255)
                self.wheelcolor[x][i] = str(color.name())

    # sets parent and corrects segment lengths so that child fits inside parent (multiple children can be set to same parent)
    # input: z = index of parent in previus wheel    # x = wheel number    # i = index of element in wheel x
    def setwheelchild(self,z,x,i):
        #set same start angle
        self.startangle[x] = self.startangle[x-1]
        self.wheellabelparent[x][i] = z

        #adjust lengths
        for x in range(1,len(self.segmentlengths)):
            nsegments = len(self.segmentlengths[x])
            parentanglecount = 0
            for i in range(nsegments):
                if  self.wheellabelparent[x][i]:                                                  #if parent selected (otherwise 0) 
                    parentindex = self.wheellabelparent[x][i]                                     #parent index
                    if self.wheellabelparent[x][i] == parentindex:                                #if match
                        parentangle = self.segmentlengths[x-1][self.wheellabelparent[x][i]-1]     #find parent angle (in %)
                        #find number of labels with same parent
                        count = self.wheellabelparent[x].count(parentindex)                       #count number of labels with same parent
                        self.segmentlengths[x][i] = parentangle/count                             #divide parent angle between children

                        #calculate last total angle
                        if i < nsegments-1:
                            parentanglecount += self.segmentlengths[x][i]

                        #adjust rest of angles to get 100 % coverage
                        for a in range(i+1,nsegments):
                            self.segmentlengths[x][a] = (100-parentanglecount)/(nsegments-(i+1))

    #adjusts size of all segements of the graph based on child parent relation
    #expects all segments to have a parent except in the first wheel
    def setWheelHierarchy(self):
        #check for not stablished relashionships (will cause graph plotting problems) and give warning
        for x in range(1,len(self.wheellabelparent)):
            for i in range(len(self.wheellabelparent[x])):
                if self.wheellabelparent[x][i] == 0:
                    QMessageBox.information(self,"Wheel Hierarchy Problem",
                    "Please assign a  a parent to wheel #%i element#%i: \n\n%s"%(x+1,i+1,self.wheelnames[x][i]))
                    return

        #adjust top wheel and make all segments equal
        for i in range(len(self.segmentlengths[-1])):
            self.segmentlengths[-1][i] = 100./len(self.segmentlengths[-1])

        #adjust lower wheels based on previous wheels
        for p in range(len(self.wheellabelparent)-1,0,-1):
            nsegments = len(self.wheellabelparent[p])
            nparentsegments = len(self.wheellabelparent[p-1])
            angles = [0]*nparentsegments
            for x in range(nparentsegments):
                for i in range(nsegments):
                    if self.wheellabelparent[p][i]-1 == x:
                        angles[x] += self.segmentlengths[p][i]

            #adjust angle length of parents proportionaly
            for i in range(nparentsegments):
                self.segmentlengths[p-1][i] = angles[i]

        self.drawWheel()

	#############################     MOUSE CROSS     #############################

    def togglecrosslines(self):
        if self.crossmarker == False and not self.designerflag and not self.flagstart:  #if not projection flag
            #turn ON
            self.l_horizontalcrossline = None
            self.l_verticalcrossline = None
            self.delayedUpdateBackground() # update bitlblit backgrounds
            self.crossmarker = True
            message = QApplication.translate("Message", "Mouse Cross ON: move mouse around",None)
            aw.sendmessage(message)
            self.crossmouseid = self.fig.canvas.mpl_connect('motion_notify_event', self.drawcross)
        else:
            #turn OFF
            self.crossmarker = False
            if (self.DeltaETflag or self.DeltaBTflag or (aw.qmc.background and (self.DeltaETBflag or self.DeltaBTBflag))) and not self.designerflag:
                self.resetdeltalines()
            else:
                self.resetlines()
            self.fig.canvas.draw()
            self.delayedUpdateBackground() # update bitlblit backgrounds
            message = QApplication.translate("Message", "Mouse cross OFF",None)
            aw.sendmessage(message)
            self.fig.canvas.mpl_disconnect(self.crossmouseid)
            self.l_horizontalcrossline = None
            self.l_verticalcrossline = None

    def drawcross(self,event):
        if event.inaxes == self.ax:
            x = event.xdata 
            y = event.ydata
            if x and y:
                if self.l_horizontalcrossline == None:
                    self.l_horizontalcrossline, = self.ax.plot([self.startofx,self.endofx*2], [y,y],color = self.palette["text"], linestyle = '-', linewidth= .5, alpha = 1.0,sketch_params=None,path_effects=[])
                else:
                    self.l_horizontalcrossline.set_data([self.startofx,self.endofx*2], [y,y])
                if self.l_verticalcrossline == None:
                    self.l_verticalcrossline, = self.ax.plot([x,x], [self.ylimit_min,self.ylimit],color = self.palette["text"], linestyle = '-', linewidth= .5, alpha = 1.0,sketch_params=None,path_effects=[])
                else:
                    self.l_verticalcrossline.set_data([x,x], [self.ylimit_min,self.ylimit])
                if self.ax_background:
                    self.fig.canvas.restore_region(self.ax_background)
                    aw.qmc.ax.draw_artist(self.l_horizontalcrossline)
                    aw.qmc.ax.draw_artist(self.l_verticalcrossline)
                    self.fig.canvas.blit(aw.qmc.ax.bbox)
                else:
                    self.fig.canvas.draw()
        elif event.inaxes == self.delta_ax:
            x = event.xdata 
            y = event.ydata
            if x and y:
                if self.l_horizontalcrossline == None:
                    self.l_horizontalcrossline, = self.delta_ax.plot([self.startofx,self.endofx*2], [y,y], color = self.palette["text"], linestyle = '-', linewidth = .5, alpha = 1.0,sketch_params=None,path_effects=[])
                else:
                    self.l_horizontalcrossline.set_data([self.startofx,self.endofx*2], [y,y])
                if self.l_verticalcrossline == None:
                    self.l_verticalcrossline, = self.delta_ax.plot([x,x], [self.zlimit_min,self.zlimit], color = self.palette["text"], linestyle = '-', linewidth = .5, alpha = 1.0,sketch_params=None,path_effects=[])
                else:
                    self.l_verticalcrossline.set_data([x,x], [self.zlimit_min,self.zlimit])
                if self.ax_background:
                    self.fig.canvas.restore_region(self.ax_background)
                    aw.qmc.delta_ax.draw_artist(self.l_horizontalcrossline)
                    aw.qmc.delta_ax.draw_artist(self.l_verticalcrossline)
                    self.fig.canvas.blit(aw.qmc.delta_ax.bbox)
                else:
                    self.fig.canvas.draw()

