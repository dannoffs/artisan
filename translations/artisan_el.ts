<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="el" sourcelanguage="">
<context>
    <name>About</name>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>About</source>
        <translation>Περι</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11007"/>
        <source>Version:</source>
        <translation type="obsolete">Εκδοση:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>Core developers:</source>
        <translation>Προγραμματιστες:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>Contributors:</source>
        <translation>Συντελεστες:</translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="../artisanlib/main.py" line="8265"/>
        <source>ON</source>
        <translation>ΕΝΕΡΓΟΠΟΙΗΣΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8278"/>
        <source>START</source>
        <translation>ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4157"/>
        <source>OFF</source>
        <translation>ΠΑΥΣΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8291"/>
        <source>FC
START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8298"/>
        <source>FC
END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8305"/>
        <source>SC
START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8312"/>
        <source>SC
END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8320"/>
        <source>RESET</source>
        <translation>ΑΡΧΙΚΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8333"/>
        <source>CHARGE</source>
        <translation>ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8341"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8349"/>
        <source>Control</source>
        <translation>Ελεγχος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8356"/>
        <source>EVENT</source>
        <translation>ΣΥΜΒΑΝ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8364"/>
        <source>SV +5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8372"/>
        <source>SV +10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8380"/>
        <source>SV +20</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8388"/>
        <source>SV -20</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8396"/>
        <source>SV -10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8404"/>
        <source>SV -5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8412"/>
        <source>HUD</source>
        <translation>ΠΡΟΒ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8427"/>
        <source>DRY
END</source>
        <translation>ΛΗΞΗ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8436"/>
        <source>COOL
END</source>
        <translation>ΛΗΞΗ ΨΥΞΗΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30475"/>
        <source>Update</source>
        <translation>Ενημέρωση</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14566"/>
        <source>PID Help</source>
        <translation type="obsolete">PID Βοήθεια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35151"/>
        <source>OK</source>
        <translation>Ενταξει</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32672"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16724"/>
        <source>Color</source>
        <translation>Χρωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16727"/>
        <source>Plot</source>
        <translation>Σχεδιο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29647"/>
        <source>Background</source>
        <translation>Φοντο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16707"/>
        <source>Virtual Device</source>
        <translation type="obsolete">Εικονικη Συσκευη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22624"/>
        <source>Save Image</source>
        <translation>Αποθηκευση Εικονας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31048"/>
        <source>Help</source>
        <translation>Βοηθεια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16792"/>
        <source>Info</source>
        <translation>Πληροφοριες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35003"/>
        <source>Set</source>
        <translation>Ορισμος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29602"/>
        <source>Defaults</source>
        <translation>Προκαθορισμενα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18154"/>
        <source>Order</source>
        <translation>Εντολη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30736"/>
        <source>Add</source>
        <translation>Προσθηκη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30745"/>
        <source>Delete</source>
        <translation>Καταργηση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18465"/>
        <source>in</source>
        <translation>Εισοδο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18470"/>
        <source>out</source>
        <translation>Εξοδο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19770"/>
        <source>Search</source>
        <translation>Αναζητηση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19923"/>
        <source>Path</source>
        <translation>Διαδρομη</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20245"/>
        <source>Transfer To</source>
        <translation type="obsolete">Μεταφορα προς</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20247"/>
        <source>Restore From</source>
        <translation type="obsolete">Επαναφορα απο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35131"/>
        <source>Save</source>
        <translation>Αποθηκευση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35128"/>
        <source>Load</source>
        <translation>Φορτωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22621"/>
        <source>Del</source>
        <translation>Ακυρωση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22861"/>
        <source>Align</source>
        <translation>Ευθυγραμμιση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22932"/>
        <source>Up</source>
        <translation>Πανω</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22934"/>
        <source>Down</source>
        <translation>Κατω</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22936"/>
        <source>Left</source>
        <translation>Απιστερα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22938"/>
        <source>Right</source>
        <translation>Δεξια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28050"/>
        <source>Reset</source>
        <translation>Επαναφορα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31356"/>
        <source>Close</source>
        <translation>Κλεισιμο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26765"/>
        <source>Create</source>
        <translation>Δημιουργια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19880"/>
        <source>Scan for Ports</source>
        <translation type="obsolete">Ανιχνευση Θυρων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30477"/>
        <source>Select</source>
        <translation>Επιλογη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29491"/>
        <source>Grid</source>
        <translation>Πλεγμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29498"/>
        <source>Title</source>
        <translation>Τιτλος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29505"/>
        <source>Y Label</source>
        <translation>Επιγραφη Αξονα Y</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29512"/>
        <source>X Label</source>
        <translation>Επιγραφη Αξονα Χ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29519"/>
        <source>Drying Phase</source>
        <translation>Φαση Ξηρανσης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29526"/>
        <source>Maillard Phase</source>
        <translation>Φαση Μaillard</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29533"/>
        <source>Development Phase</source>
        <translation>Φαση Αναπτυξης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29540"/>
        <source>Cooling Phase</source>
        <translation>Φαση Ψυξης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29547"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29554"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29561"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29568"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29575"/>
        <source>Markers</source>
        <translation>Δεικτες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29582"/>
        <source>Text</source>
        <translation>Κειμενο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29589"/>
        <source>Watermarks</source>
        <translation>Υδατογραφηματα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29596"/>
        <source>C Lines</source>
        <translation>Γραμμες C</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29605"/>
        <source>Grey</source>
        <translation>Γκριζο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29659"/>
        <source>LED</source>
        <translation>LED</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29691"/>
        <source>B/W</source>
        <translation>Ασπρομαυρο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30160"/>
        <source>Reset Parents</source>
        <translation>Επαναφορα Parents</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30164"/>
        <source>Reverse Hierarchy</source>
        <translation>Αντιστροφη Ιερα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30178"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30181"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30196"/>
        <source>Line Color</source>
        <translation>Χρωμα Γραμμης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30209"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30212"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30215"/>
        <source>Save File</source>
        <translation>Αποθυκευση Αρχειου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30218"/>
        <source>Save Img</source>
        <translation>Αποθυκευση Εικονας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30221"/>
        <source>View Mode</source>
        <translation>Λειτυργια Προβολης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30224"/>
        <source>Open</source>
        <translation>Ανοιγμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30500"/>
        <source>Set Color</source>
        <translation>Ορισμος Χρωματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30730"/>
        <source>All On</source>
        <translation>Λειτουργια Ολων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30733"/>
        <source>All Off</source>
        <translation>Παυση Ολων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31351"/>
        <source>Read Ra/So values</source>
        <translation>Αναγνωση Τιμων  Ra/So </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32413"/>
        <source>RampSoak ON</source>
        <translation>Εναρξη Γραφηματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32415"/>
        <source>RampSoak OFF</source>
        <translation>Παυση Γραφηματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32418"/>
        <source>PID OFF</source>
        <translation>Εναρξη PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32420"/>
        <source>PID ON</source>
        <translation>Παυση PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32535"/>
        <source>Write SV</source>
        <translation>Εγγραφη SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31367"/>
        <source>SV Buttons ON</source>
        <translation>Εναρξη Πληκτρα SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31369"/>
        <source>SV Buttons OFF</source>
        <translation>Παυση Πληκτρα SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31371"/>
        <source>Read SV</source>
        <translation>Αναγνωση SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31388"/>
        <source>Set p</source>
        <translation>Ορισμος p</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31389"/>
        <source>Set i</source>
        <translation>Ορισμος i</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31390"/>
        <source>Set d</source>
        <translation>Ορισμος d</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32668"/>
        <source>Autotune ON</source>
        <translation>Eναρξη αυτοματου συντονισμου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32670"/>
        <source>Autotune OFF</source>
        <translation>Παυση αυτοματου συντονισμου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31405"/>
        <source>Read PID Values</source>
        <translation>Αναγνωση Τιμων PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35282"/>
        <source>Read</source>
        <translation>Αναγνωση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32798"/>
        <source>Set ET PID to 1 decimal point</source>
        <translation>Ορισμος ΕΤ PID σε 1 δεκαδικο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32800"/>
        <source>Set BT PID to 1 decimal point</source>
        <translation>Ορισμος BT PID σε 1 δεκαδικο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32409"/>
        <source>Read RS values</source>
        <translation>Αναγνωση Τιμων RS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32471"/>
        <source>Write SV1</source>
        <translation>Εγγραφη SV1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32473"/>
        <source>Write SV2</source>
        <translation>Εγγραφη SV2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32475"/>
        <source>Write SV3</source>
        <translation>Εγγραφη SV3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32477"/>
        <source>Write SV4</source>
        <translation>Εγγραφη SV4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32479"/>
        <source>Write SV5</source>
        <translation>Εγγραφη SV5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32481"/>
        <source>Write SV6</source>
        <translation>Εγγραφη SV6</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32483"/>
        <source>Write SV7</source>
        <translation>Εγγραφη SV7</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32537"/>
        <source>ON SV buttons</source>
        <translation>Εναρξη Πληκτρα SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32540"/>
        <source>OFF SV buttons</source>
        <translation>Παυση Πληκτρα SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32543"/>
        <source>Read SV (7-0)</source>
        <translation>Αναγνωση SV(7-0)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32650"/>
        <source>pid 1</source>
        <translation>pid1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32652"/>
        <source>pid 2</source>
        <translation>pid2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32654"/>
        <source>pid 3</source>
        <translation>pid3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32656"/>
        <source>pid 4</source>
        <translation>pid4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32658"/>
        <source>pid 5</source>
        <translation>pid5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32660"/>
        <source>pid 6</source>
        <translation>pid6</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32662"/>
        <source>pid 7</source>
        <translation>pid7</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23923"/>
        <source>Read All</source>
        <translation type="obsolete">Αναγνωση Ολων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32802"/>
        <source>Set ET PID to MM:SS time units</source>
        <translation>Ορισμος ET PID σε χρονικη μορφη ΛΛ:ΔΔ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35283"/>
        <source>Write</source>
        <translation>Εγγραφη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18480"/>
        <source>scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21073"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30740"/>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30764"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32405"/>
        <source>Write All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32411"/>
        <source>Write RS values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32545"/>
        <source>Write SV (7-0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32664"/>
        <source>Read PIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32666"/>
        <source>Write PIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35153"/>
        <source>On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35155"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18490"/>
        <source>calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17692"/>
        <source>unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20861"/>
        <source>&lt;&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20863"/>
        <source>&gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18149"/>
        <source>Create Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16738"/>
        <source>Device</source>
        <translation type="obsolete">Συσκευη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16733"/>
        <source>BT/ET</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="18413"/>
        <source>Heavy FC</source>
        <translation>Eντονο FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18416"/>
        <source>Low FC</source>
        <translation>Αδυναμο FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18419"/>
        <source>Light Cut</source>
        <translation>Ανοικτοχρωμο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18422"/>
        <source>Dark Cut</source>
        <translation>Σκουροχρωμο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18425"/>
        <source>Drops</source>
        <translation>Στιγματα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18428"/>
        <source>Oily</source>
        <translation>Ελαιωδης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18431"/>
        <source>Uneven</source>
        <translation>Ανομοιο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18433"/>
        <source>Tipping</source>
        <translation>Tipping</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18435"/>
        <source>Scorching</source>
        <translation>scorching</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18437"/>
        <source>Divots</source>
        <translation>Καψιματα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22847"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22848"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16460"/>
        <source>Smooth Spikes</source>
        <translation>Λειανση Παρασιτων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16470"/>
        <source>Drop Spikes</source>
        <translation>Απορριψη Παρασιτων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16475"/>
        <source>Limits</source>
        <translation>Ορια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16504"/>
        <source>Projection</source>
        <translation>Προβολη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22844"/>
        <source>Show</source>
        <translation>Εμφανιση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16921"/>
        <source>Beep</source>
        <translation>Beep</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18140"/>
        <source>Delete roast properties on RESET</source>
        <translation>Διαγραφη Δεδομενων στην Επαναφορα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19821"/>
        <source>Serial Log ON/OFF</source>
        <translation>Serial Log ON/OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19913"/>
        <source>Autosave [a]</source>
        <translation>Αυτοματη Αποθυκευση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20123"/>
        <source>Lock Max</source>
        <translation>Κλειδωμα Τιμης Max</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20608"/>
        <source>Button</source>
        <translation>Πληκτρο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20617"/>
        <source>Mini Editor</source>
        <translation>Συντακτης</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15140"/>
        <source>Automatic CHARGE/DROP</source>
        <translation type="obsolete">Αυτοματο ΦΟΡΤΩΜΑ/ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21122"/>
        <source>CHARGE</source>
        <translation>ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26565"/>
        <source>DRY END</source>
        <translation>ΤΕΛΟΣ ΞΗΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26567"/>
        <source>FC START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26569"/>
        <source>FC END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26571"/>
        <source>SC START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26573"/>
        <source>SC END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21176"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21185"/>
        <source>COOL END</source>
        <translation>ΛΗΞΗ ΨΥΞΗΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22324"/>
        <source>Auto Adjusted</source>
        <translation>Αυτοματη Ρυθμιση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22327"/>
        <source>Watermarks</source>
        <translation>Υδατογραφηματα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22629"/>
        <source>Background</source>
        <translation>Φοντο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22845"/>
        <source>Text</source>
        <translation>Κειμενο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22846"/>
        <source>Events</source>
        <translation>Συμβαντα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22968"/>
        <source>Playback Aid</source>
        <translation>Βοηθημα Playback</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23358"/>
        <source>Time</source>
        <translation>Χρονος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23359"/>
        <source>Bar</source>
        <translation>Μπαρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20440"/>
        <source>d/m</source>
        <translation type="obsolete">d/m</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23361"/>
        <source>ETBTa</source>
        <translation>ETBTa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23362"/>
        <source>Evaluation</source>
        <translation>Αξιολογηση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23363"/>
        <source>Characteristics</source>
        <translation>Χαρακτηριστικα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27906"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27909"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16465"/>
        <source>Smooth2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16594"/>
        <source>Decimal Places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20806"/>
        <source>Auto CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20809"/>
        <source>Auto DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20812"/>
        <source>Mark TP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22329"/>
        <source>Phases LCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22331"/>
        <source>Auto DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22333"/>
        <source>Auto FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27979"/>
        <source>Modbus Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30768"/>
        <source>Load alarms from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35084"/>
        <source>Start PID on CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35139"/>
        <source>Load Ramp/Soak table from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28012"/>
        <source>Control Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28243"/>
        <source>Ratiometric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22849"/>
        <source>Align FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23360"/>
        <source>/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16948"/>
        <source>Alarm Popups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20126"/>
        <source>Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19987"/>
        <source>Batch Counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20611"/>
        <source>Annotations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComboBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="1015"/>
        <source>Speed</source>
        <translation>Ταχυτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1016"/>
        <source>Power</source>
        <translation>Δυναμη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1017"/>
        <source>Damper</source>
        <translation>Κλαπετο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1018"/>
        <source>Fan</source>
        <translation>Ανεμιστηρας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>linear</source>
        <translation>Γραμμικο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16506"/>
        <source>newton</source>
        <translation>newton</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17614"/>
        <source>metrics</source>
        <translation>μετρικα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17616"/>
        <source>thermal</source>
        <translation>θερμικο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>cubic</source>
        <translation>κυβικο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>nearest</source>
        <translation>κοντινοτερο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20399"/>
        <source>g</source>
        <translation>γραμ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20400"/>
        <source>Kg</source>
        <translation>Κιλα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19381"/>
        <source>ml</source>
        <translation>μιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18317"/>
        <source>l</source>
        <translation>λ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31120"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31121"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20101"/>
        <source>upper right</source>
        <translation>πανω δεξια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20102"/>
        <source>upper left</source>
        <translation>πανω αριστερα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20103"/>
        <source>lower left</source>
        <translation>κατω αριστερα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20104"/>
        <source>lower right</source>
        <translation>κατω δεξια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20105"/>
        <source>right</source>
        <translation>δεξια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20106"/>
        <source>center left</source>
        <translation>κεντρο αριστερα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20107"/>
        <source>center right</source>
        <translation>κεντρο δεξια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20108"/>
        <source>lower center</source>
        <translation>κατω κεντρικα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20109"/>
        <source>upper center</source>
        <translation>πανω κεντρικα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20110"/>
        <source>center</source>
        <translation>κεντρο</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18782"/>
        <source>30 seconds</source>
        <translation type="obsolete">30 δευτερολεπτα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20133"/>
        <source>1 minute</source>
        <translation>1 λεπτο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20134"/>
        <source>2 minute</source>
        <translation>2 λεπτα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20135"/>
        <source>3 minute</source>
        <translation>3 λεπτα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20136"/>
        <source>4 minute</source>
        <translation>4 λεπτα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20137"/>
        <source>5 minute</source>
        <translation>5 λεπτα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20163"/>
        <source>solid</source>
        <translation>Συμπαγες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20164"/>
        <source>dashed</source>
        <translation>διακεκομμενο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20165"/>
        <source>dashed-dot</source>
        <translation>διακεκομμενο-τελειες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20166"/>
        <source>dotted</source>
        <translation>με τελειες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>None</source>
        <translation>Κανενα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20379"/>
        <source>Event #0</source>
        <translation>Συμβαν#0</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20381"/>
        <source>Event #{0}</source>
        <translation type="unfinished">Συμβαν#{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20401"/>
        <source>lb</source>
        <translation>λιμπρες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20422"/>
        <source>liter</source>
        <translation>λιτρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20423"/>
        <source>gallon</source>
        <translation>γαλονια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20424"/>
        <source>quart</source>
        <translation>τεταρτο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20425"/>
        <source>pint</source>
        <translation>πιντα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20426"/>
        <source>cup</source>
        <translation>φλυτζανι</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20427"/>
        <source>cm^3</source>
        <translation>εκ.^3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20623"/>
        <source>Type</source>
        <translation>Τυπος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20624"/>
        <source>Value</source>
        <translation>Αξια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Serial Command</source>
        <translation>Σειριακη Εντολη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Modbus Command</source>
        <translation>Εντολη Modbus</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>DTA Command</source>
        <translation>Εντολη DTA</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Call Program</source>
        <translation>Ανακληση Προγραμματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>OFF</source>
        <translation>ΤΕΛΟΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>ON</source>
        <translation>ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Multiple Event</source>
        <translation>Πολλαπλα Συμβαντα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31119"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31118"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26747"/>
        <source>SV Commands</source>
        <translation>Εντολες SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26747"/>
        <source>Ramp Commands</source>
        <translation>Εντολες Γραφηματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27391"/>
        <source>little-endian</source>
        <translation>little-endian</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29612"/>
        <source>grey</source>
        <translation>γκριζο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29613"/>
        <source>Dark Grey</source>
        <translation>Σκουρο Γκριζο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29614"/>
        <source>Slate Grey</source>
        <translation>Σαγρε Γκριζο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29615"/>
        <source>Light Gray</source>
        <translation>Ανοικτο Γκριζο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29616"/>
        <source>Black</source>
        <translation>Μαυρο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29617"/>
        <source>White</source>
        <translation>Λευκο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29618"/>
        <source>Transparent</source>
        <translation>Διαφανο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Flat</source>
        <translation>Φλατ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Perpendicular</source>
        <translation>Καθετα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Radial</source>
        <translation>Ακτινωτα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>START</source>
        <translation>ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>CHARGE</source>
        <translation>ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>TP</source>
        <translation>ΤΡ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>DRY END</source>
        <translation>ΤΕΛΟΣ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>FC START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>FC END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>SC START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>SC END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>COOL</source>
        <translation>ΨΥΞΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31178"/>
        <source>below</source>
        <translation>αποκατω</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31178"/>
        <source>above</source>
        <translation>αποπανω</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Pop Up</source>
        <translation>Εμφανιση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Event Button</source>
        <translation>Πληκτρο Συμβαντος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Slider</source>
        <translation>Ολισθητης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16660"/>
        <source>classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16660"/>
        <source>xkcd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Default</source>
        <translation type="unfinished">Αρχικο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Humor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>FCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>SCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>SCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>COOL END</source>
        <translation type="unfinished">ΛΗΞΗ ΨΥΞΗΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>IO Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Heater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Fan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>RampSoak ON</source>
        <translation type="unfinished">Εναρξη Γραφηματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>RampSoak OFF</source>
        <translation type="unfinished">Παυση Γραφηματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>PID ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>PID OFF</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Contextual Menu</name>
    <message>
        <location filename="../artisanlib/main.py" line="5922"/>
        <source>Create</source>
        <translation>Δημιουργια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5926"/>
        <source>Config...</source>
        <translation>Διαμορφωση...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5936"/>
        <source>Add point</source>
        <translation>Προσθηκη σημειου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5940"/>
        <source>Remove point</source>
        <translation>Καταργηση σημειου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5946"/>
        <source>Reset Designer</source>
        <translation>Επαναφορα Σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5950"/>
        <source>Exit Designer</source>
        <translation>Εξοδος Σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6530"/>
        <source>Add to Cupping Notes</source>
        <translation>Προσθηκη Σημειωσεων cupping</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6534"/>
        <source>Add to Roasting Notes</source>
        <translation>Προσθηκη Σημειωσεων Ψησιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6538"/>
        <source>Cancel selection</source>
        <translation>Καταργηση επιλογης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6542"/>
        <source>Edit Mode</source>
        <translation>Επεξεργασια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6546"/>
        <source>Exit</source>
        <translation>Εξοδος</translation>
    </message>
</context>
<context>
    <name>Directory</name>
    <message>
        <location filename="artisanlib/main.py" line="804"/>
        <source>edit text</source>
        <translation type="obsolete">επεξεργασια  κειμενου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15803"/>
        <source>profiles</source>
        <translation>προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15793"/>
        <source>other</source>
        <translation>Αλλα</translation>
    </message>
</context>
<context>
    <name>Error Message</name>
    <message>
        <location filename="../artisanlib/main.py" line="35213"/>
        <source>Exception:</source>
        <translation>Εξαιρεση:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26453"/>
        <source>Value Error:</source>
        <translation>Σφαλμα Τιμης:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30654"/>
        <source>IO Error:</source>
        <translation>Σφαλμα ΙΟ:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="13930"/>
        <source>Error</source>
        <translation>Σφαλμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19414"/>
        <source>Unable to move CHARGE to a value that does not exist</source>
        <translation>Αδυνατη μετακινηση ΦΟΡΤΩΜΑ σε τιμη μη υπαρκτη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23814"/>
        <source>Modbus Error:</source>
        <translation>Σφαλμα Modbus:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26509"/>
        <source>Serial Exception:</source>
        <translation>Σειριακη Εξαιρεση:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24185"/>
        <source>F80h Error</source>
        <translation>Σφαλμα F80h</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24195"/>
        <source>CRC16 data corruption ERROR. TX does not match RX. Check wiring</source>
        <translation>Σφαλμα πληροφοριας CRC16.TX δεν αντιστοιχει RX.Ελεγξτε συνδεσμολογια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24198"/>
        <source>No RX data received</source>
        <translation>Δεν εγινε ληψη πληροφοριων RX</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24326"/>
        <source>DTAcommand(): {0} bytes received but 15 needed</source>
        <translation type="unfinished">ΕντοληDTA():{0}bytes εληφθη αλλα χρειζει15</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24650"/>
        <source>Unable to open serial port</source>
        <translation>Αδυνατη επικοινωνια με σειριακη θυρα</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18065"/>
        <source>HH806AUtemperature(): {0} bytes received but 14 needed</source>
        <translation type="obsolete">HH806AUθερμο():{0}bytes εληφθησαν αλλα χρειζει14</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24890"/>
        <source>HH806Wtemperature(): Unable to initiate device</source>
        <translation>HH806Wθερμο():Αδυνατη συνδεση συσκευης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25019"/>
        <source>HH506RAGetID: {0} bytes received but 5 needed</source>
        <translation type="unfinished">HH506RAGetID:{0}bytes εληφθη αλλα χρειζει5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25044"/>
        <source>HH506RAtemperature(): Unable to get id from HH506RA device </source>
        <translation>HH506RAθερμο():Αδυνατη ληψη id απο συσκευη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25064"/>
        <source>HH506RAtemperature(): {0} bytes received but 14 needed</source>
        <translation type="unfinished">HH506RAθερμο():{0}bytes εληφθη αλλα χρειζει 14</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25119"/>
        <source>CENTER302temperature(): {0} bytes received but 7 needed</source>
        <translation type="unfinished">CENTER302θερμο():{0}bytes εληφθη αλλα χρειζει7</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25188"/>
        <source>CENTER303temperature(): {0} bytes received but 8 needed</source>
        <translation type="unfinished">CENTER303θερμο():{0}bytes εληφθη αλλα χρειζει8</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25258"/>
        <source>CENTER306temperature(): {0} bytes received but 10 needed</source>
        <translation type="unfinished">CENTER306θερμο():{0}bytes εληφθη αλλα χρειζει10</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25344"/>
        <source>CENTER309temperature(): {0} bytes received but 45 needed</source>
        <translation type="unfinished">CENTER309θερμο():{0}bytes εληφθη αλλα χρειζει45</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26046"/>
        <source>Arduino could not set channels</source>
        <translation>Το arduino αδυνατει να διαμορφωση καναλια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26057"/>
        <source>Arduino could not set temperature unit</source>
        <translation>Το arduino αδυνατει να επικοινωνησει με θερμομετρο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27874"/>
        <source>Serial Exception: invalid comm port</source>
        <translation>Εξαιρεση σειριακη:ακυρη θυρα επικοινωνιας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27879"/>
        <source>Serial Exception: timeout</source>
        <translation>Εξαιρεση σειριακη:τελος χρονου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20915"/>
        <source>Device error</source>
        <translation type="obsolete">Σφαλμα συσκευης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34814"/>
        <source>Segment values could not be written into PID</source>
        <translation>Τιμη δεν εγγραφεται στο PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34552"/>
        <source>RampSoak could not be changed</source>
        <translation>Αδυνατη η αλλαγη του γραφηματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34870"/>
        <source>pid.readoneword(): {0} RX bytes received (7 needed) for unit ID={1}</source>
        <translation type="unfinished">pid.readoneword():{0} RX bytes εληφθη(χρειζει7)για συσκευη ID={1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17261"/>
        <source>Univariate: no profile data available</source>
        <translation>μονοπαραγοντική:Δεν ευρεθει προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17432"/>
        <source>Polyfit: no profile data available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24830"/>
        <source>HH806AUtemperature(): {0} bytes received but 16 needed</source>
        <translation type="unfinished">HH806AUθερμο():{0}bytes εληφθησαν αλλα χρειζει14 {806A?} {1 ?} {16 ?}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10419"/>
        <source>Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26069"/>
        <source>Arduino could not set filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24768"/>
        <source>MS6514temperature(): {0} bytes received but 16 needed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Flavor Scope Label</name>
    <message>
        <location filename="../artisanlib/main.py" line="14811"/>
        <source>OK</source>
        <translation>Ενταξει</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14812"/>
        <source>Grassy</source>
        <translation>Χορτοειδες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14813"/>
        <source>Leathery</source>
        <translation>Δερματοειδες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14814"/>
        <source>Toasty</source>
        <translation>Καμμενο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14815"/>
        <source>Bready</source>
        <translation>Ψωμοειδες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14816"/>
        <source>Acidic</source>
        <translation>Οξινο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14817"/>
        <source>Flat</source>
        <translation>Φλατ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14818"/>
        <source>Fracturing</source>
        <translation>Θρυμματοειδες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14819"/>
        <source>Sweet</source>
        <translation>Γλυκο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14820"/>
        <source>Less Sweet</source>
        <translation>Λιγοτερο γλυκο</translation>
    </message>
</context>
<context>
    <name>Form Caption</name>
    <message>
        <location filename="../artisanlib/main.py" line="16402"/>
        <source>Extras</source>
        <translation>Εξτρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18006"/>
        <source>Roast Properties</source>
        <translation>Ιδιοτητες ψησιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19721"/>
        <source>Artisan Platform</source>
        <translation>Πλατφορμα artisan</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19761"/>
        <source>Settings Viewer</source>
        <translation>Ρυθμισεις</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19820"/>
        <source>Serial Log</source>
        <translation>Σειριακο αρχειο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19864"/>
        <source>Error Log</source>
        <translation>Αρχειο σφαλματων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19889"/>
        <source>Message History</source>
        <translation>Ιστορικο μυνηματων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19910"/>
        <source>Keyboard Autosave [a]</source>
        <translation>Πληκτρολογιο αυτοματης αποθυκευσης[a]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19948"/>
        <source>AutoSave Path</source>
        <translation>Διαδρομη αυτοματης αποθυκευσης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20047"/>
        <source>Axes</source>
        <translation>Αξονες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20364"/>
        <source>Roast Calculator</source>
        <translation>Υπολογιστης Ψσιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20604"/>
        <source>Events</source>
        <translation>Συμβαντα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22273"/>
        <source>Roast Phases</source>
        <translation>Φασεις Ψησιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22602"/>
        <source>Cup Profile</source>
        <translation>Προφιλ cup</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22836"/>
        <source>Profile Background</source>
        <translation>Προφιλ Φοντου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23355"/>
        <source>Statistics</source>
        <translation>Στατιστικα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26559"/>
        <source>Designer Config</source>
        <translation>Διαμορφωση σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27098"/>
        <source>Manual Temperature Logger</source>
        <translation>Χειροκινητη καταγραφη δερμοκρασιας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27212"/>
        <source>Serial Ports Configuration</source>
        <translation>Διαμορφωση σειριακων θυρων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27896"/>
        <source>Device Assignment</source>
        <translation>Καταχωρηση συσκευης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29478"/>
        <source>Colors</source>
        <translation>Χρωματα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30151"/>
        <source>Wheel Graph Editor</source>
        <translation>Διαμορφωση Γραφηματος Ροδας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30725"/>
        <source>Alarms</source>
        <translation>Συναγερμοι</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31322"/>
        <source>Fuji PXR PID Control</source>
        <translation>Ελεγχος Fuji PXR PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32346"/>
        <source>Fuji PXG PID Control</source>
        <translation>Ελεγχος Fuji PXG PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35275"/>
        <source>Delta DTA PID Control</source>
        <translation>Ελεγχος Delta DTA PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34909"/>
        <source>Arduino Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17657"/>
        <source>Volume Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19581"/>
        <source>Tare Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19972"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="27918"/>
        <source>Curves</source>
        <translation>Καμπυλες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27927"/>
        <source>LCDs</source>
        <translation>LCDς</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16628"/>
        <source>HUD</source>
        <translation>ΠΡΟΒ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16642"/>
        <source>Input Filters</source>
        <translation>Φιλτρα Εισοδων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16844"/>
        <source>Interpolate</source>
        <translation>Παρεμβολη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16850"/>
        <source>Univariate</source>
        <translation>Μονοπαραγων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16908"/>
        <source>Appearance</source>
        <translation>Εμφανιση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16916"/>
        <source>Resolution</source>
        <translation>Αναλυση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16928"/>
        <source>Sound</source>
        <translation>Ηχος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18661"/>
        <source>Times</source>
        <translation>Χρονοι</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20227"/>
        <source>Time Axis</source>
        <translation>Αξονας Χρονου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20229"/>
        <source>Temperature Axis</source>
        <translation>Αξονας Θερμοκρασιας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20231"/>
        <source>DeltaBT/DeltaET Axis</source>
        <translation>Αξονες DeltaBT/DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20233"/>
        <source>Legend Location</source>
        <translation>Θεση Εμφανισης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20235"/>
        <source>Grid</source>
        <translation>Πλεγμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20477"/>
        <source>Rate of Change</source>
        <translation>Ρυθμος Μεταβολης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20479"/>
        <source>Temperature Conversion</source>
        <translation>Μετατροπη Θερμοκρασιας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20481"/>
        <source>Weight Conversion</source>
        <translation>Μετατροπη Βαρους</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20483"/>
        <source>Volume Conversion</source>
        <translation>Μετατροπη Ογκου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21111"/>
        <source>Event Types</source>
        <translation>Ειδη Συμβαντων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21262"/>
        <source>Default Buttons</source>
        <translation>Προεπιλεγμενα Πληκτρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21315"/>
        <source>Management</source>
        <translation>Διαχειρηση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23469"/>
        <source>Evaluation</source>
        <translation>Αξιολογηση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23471"/>
        <source>Display</source>
        <translation>Εμφανιση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26821"/>
        <source>Initial Settings</source>
        <translation>Αρχικες Ρυθμισεις</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27593"/>
        <source>Input 1</source>
        <translation>Εισοδος 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27607"/>
        <source>Input 2</source>
        <translation>Εισοδος 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27620"/>
        <source>Input 3</source>
        <translation>Εισοδος 3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27633"/>
        <source>Input 4</source>
        <translation>Εισοδος 4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28363"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28390"/>
        <source>Arduino TC4</source>
        <translation>Arduino TC4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28397"/>
        <source>External Program</source>
        <translation>Εξωτερικο Προγραμμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28403"/>
        <source>Symbolic Assignments</source>
        <translation>Συμβολικες αναθεσεις</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29773"/>
        <source>Timer LCD</source>
        <translation>Χρονικο LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29776"/>
        <source>ET LCD</source>
        <translation>ET LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29779"/>
        <source>BT LCD</source>
        <translation>BT LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29782"/>
        <source>DeltaET LCD</source>
        <translation>DeltaET LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29785"/>
        <source>DeltaBT LCD</source>
        <translation>DeltaBT LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29788"/>
        <source>Extra Devices / PID SV LCD</source>
        <translation>Εξτρα Συσκευες/PID SV LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30237"/>
        <source>Label Properties</source>
        <translation>Ιδιοτητες Επιγραφων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16877"/>
        <source>Polyfit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16687"/>
        <source>Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28337"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34912"/>
        <source>p-i-d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35075"/>
        <source>Set Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28310"/>
        <source>Phidget IO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28157"/>
        <source>Phidgets 1045</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28232"/>
        <source>Phidgets 1046 RTD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16970"/>
        <source>WebLCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20572"/>
        <source>Sampling Interval</source>
        <translation type="obsolete">Διαστημα Δειγματοληψειας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28116"/>
        <source>Phidgets 1048/1051</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21277"/>
        <source>Sampling</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTML Report Template</name>
    <message>
        <location filename="../artisanlib/main.py" line="14190"/>
        <source>Roasting Report</source>
        <translation>Λεπτομερειες Ψησιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14215"/>
        <source>Date:</source>
        <translation>Ημερομηνια:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14219"/>
        <source>Beans:</source>
        <translation>Κοκκοι:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14223"/>
        <source>Size:</source>
        <translation>Mεγεθος:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14227"/>
        <source>Weight:</source>
        <translation>Βαρος:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14231"/>
        <source>Degree:</source>
        <translation>Βαθμος:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14235"/>
        <source>Volume:</source>
        <translation>Ογκος:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14239"/>
        <source>Density:</source>
        <translation>Πυκνοτητα:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13386"/>
        <source>Humidity:</source>
        <translation type="obsolete">Υγρασια:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14251"/>
        <source>Roaster:</source>
        <translation>Ψηστηρι:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14255"/>
        <source>Operator:</source>
        <translation>Χειρηστης:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14259"/>
        <source>Cupping:</source>
        <translation>Cupping:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14263"/>
        <source>Color:</source>
        <translation>Χρωμα:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14271"/>
        <source>CHARGE:</source>
        <translation>ΦΟΡΤΩΜΑ:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14279"/>
        <source>DRY:</source>
        <translation>ΞΗΡΑΝΣΗ:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14283"/>
        <source>FCs:</source>
        <translation>FCε:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14287"/>
        <source>FCe:</source>
        <translation>FCλ:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14291"/>
        <source>SCs:</source>
        <translation>SCε:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14295"/>
        <source>SCe:</source>
        <translation>SCλ:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14299"/>
        <source>DROP:</source>
        <translation>ΞΕΦΟΡΤΩΜΑ:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14303"/>
        <source>COOL:</source>
        <translation>ΨΗΞΗ:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14311"/>
        <source>RoR:</source>
        <translation>RoR:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14315"/>
        <source>ETBTa:</source>
        <translation>ETBTa:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14327"/>
        <source>Drying:</source>
        <translation>Ξηρανση:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14331"/>
        <source>Maillard:</source>
        <translation>Maillard:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14335"/>
        <source>Development:</source>
        <translation>Αναπτυξη:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14339"/>
        <source>Cooling:</source>
        <translation>Ψυξη:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14365"/>
        <source>Roasting Notes</source>
        <translation>Σημειωσεις Ψησιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14375"/>
        <source>Cupping Notes</source>
        <translation>Σημειωσεις cupping</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14275"/>
        <source>TP:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14319"/>
        <source>CM:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14345"/>
        <source>Background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14345"/>
        <source>Events</source>
        <translation type="unfinished">Συμβαντα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14307"/>
        <source>MET:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14243"/>
        <source>Moisture:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14247"/>
        <source>Ambient:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="artisanlib/main.py" line="6104"/>
        <source>deg/min</source>
        <translation type="obsolete">βαθμ/λεπτ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26733"/>
        <source>ET</source>
        <translation>ΕΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27109"/>
        <source>BT</source>
        <translation>ΒΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5900"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5892"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18389"/>
        <source>at</source>
        <translation>στους</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26562"/>
        <source>CHARGE</source>
        <translation>ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18040"/>
        <source>DRY END</source>
        <translation>ΛΗΞΗ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18054"/>
        <source>FC START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18069"/>
        <source>FC END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18083"/>
        <source>SC START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18097"/>
        <source>SC END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26575"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1608"/>
        <source>EVENT</source>
        <translation>ΣΥΜΒΑΝ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3215"/>
        <source>BackgroundET</source>
        <translation>ΦοντοET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3219"/>
        <source>BackgroundBT</source>
        <translation>ΦοντοBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3259"/>
        <source>BackgroundDeltaET</source>
        <translation>ΦοντοDeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3263"/>
        <source>BackgroundDeltaBT</source>
        <translation>ΦοντοDeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13086"/>
        <source>d/m</source>
        <translation type="obsolete">d/m</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26590"/>
        <source>Time</source>
        <translation>Χρονος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5587"/>
        <source>BT {0} d/m for {1}</source>
        <translation type="obsolete">BT{0}d/mfor{1}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5603"/>
        <source>ET {0} d/m for {1}</source>
        <translation type="obsolete">ET{0}d/mfor{1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8521"/>
        <source>PID SV</source>
        <translation>PID SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8525"/>
        <source>PID %</source>
        <translation>PID %</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8574"/>
        <source>Event #&lt;b&gt;0 &lt;/b&gt;</source>
        <translation>Event#&lt;b&gt;0&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10451"/>
        <source>Event #&lt;b&gt;{0} &lt;/b&gt;</source>
        <translation type="unfinished">Event#&lt;b&gt;{0}&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15564"/>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15566"/>
        <source>City+</source>
        <translation>City+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15568"/>
        <source>Full City</source>
        <translation>Full City</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15570"/>
        <source>Full City+</source>
        <translation>Full City+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15572"/>
        <source>Light French</source>
        <translation>Light French</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15574"/>
        <source>French</source>
        <translation>French</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15125"/>
        <source>{0} to reach ET target {1}</source>
        <translation type="obsolete">{0} to reach ET target{1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15882"/>
        <source> at {0}</source>
        <translation type="unfinished">at{0}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15138"/>
        <source>{0} to reach BT target {1}</source>
        <translation type="obsolete">{0} to reach BT target{1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15894"/>
        <source>{0} after FCs</source>
        <translation type="unfinished">{0}afterFCS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15901"/>
        <source>{0} after FCe</source>
        <translation type="unfinished">{0}afterFCe</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15936"/>
        <source>ET - BT = {0}</source>
        <translation type="unfinished">ET-BT={0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15989"/>
        <source>ET - BT = {0}{1}</source>
        <translation type="unfinished">ET-BT={0}{1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16418"/>
        <source>ET Target 1</source>
        <translation>ET Στοχος1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16420"/>
        <source>BT Target 1</source>
        <translation>BT Στοχος1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16422"/>
        <source>ET Target 2</source>
        <translation>ET Στοχος2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16424"/>
        <source>BT Target 2</source>
        <translation>BT Στοχος2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35006"/>
        <source>Mode</source>
        <translation>Τροπος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16428"/>
        <source>ET p-i-d 1</source>
        <translation>ETp-i-d1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16435"/>
        <source>Smooth Deltas</source>
        <translation>Αμβλυνση των Delta</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16443"/>
        <source>Smooth Curves</source>
        <translation>Αμβλυνση Καμπυλων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35027"/>
        <source>min</source>
        <translation>Ελαχιστο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35035"/>
        <source>max</source>
        <translation>Μεγιστο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16702"/>
        <source>Y(x)</source>
        <translation>Y(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18126"/>
        <source>COOL</source>
        <translation>ΨΥΞΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18174"/>
        <source>Title</source>
        <translation>Τιτλος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18177"/>
        <source>Date</source>
        <translation>Ημερομηνια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18215"/>
        <source>Beans</source>
        <translation>Κοκκοι</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18225"/>
        <source>Weight</source>
        <translation>Βαρος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18261"/>
        <source> in</source>
        <translation>Εισοδος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18262"/>
        <source> out</source>
        <translation>Εξοδος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18275"/>
        <source> %</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18260"/>
        <source>Volume</source>
        <translation>Ογκος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18293"/>
        <source>Density</source>
        <translation>Πυκνοτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18308"/>
        <source>per</source>
        <translation>προς</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18337"/>
        <source>Bean Size</source>
        <translation>Μεγεθος Κοκκου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18343"/>
        <source>mm</source>
        <translation>μμ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18345"/>
        <source>Whole Color</source>
        <translation>Χρωμα Κοκκων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18351"/>
        <source>Ground Color</source>
        <translation>Χρωμα Αλεσμενου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18362"/>
        <source>Moisture Greens</source>
        <translation>Συνθηκες Αποθυκευσης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18383"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18381"/>
        <source>Ambient Conditions</source>
        <translation>Συνθηκες Περιβαλλοντος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18401"/>
        <source>Roaster</source>
        <translation>Ψηστηρι</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18403"/>
        <source>Operator</source>
        <translation>Χειρηστης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18404"/>
        <source>Roasting Notes</source>
        <translation>Σημειωσεις Ψησιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18408"/>
        <source>Cupping Notes</source>
        <translation>Σημειωσεις Cupping</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18489"/>
        <source>Ambient Source</source>
        <translation>Πηγη Περιβαλλοντος</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16790"/>
        <source>                 Density in: {0}  g/l   =&gt;   Density out: {1} g/l</source>
        <translation type="obsolete">Πυκνοτητα εισ:{0}g/l=&gt;Πυκνοτητα εξοδ:{1}g/l</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19385"/>
        <source>({0} g/l)</source>
        <translation type="unfinished">({0} g/l)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19871"/>
        <source>Number of errors found {0}</source>
        <translation type="unfinished">Αριθμος σφαλματων{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20991"/>
        <source>Max</source>
        <translation>Μεγιστο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20989"/>
        <source>Min</source>
        <translation>Ελαχιστο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20091"/>
        <source>Rotation</source>
        <translation>Περιστροφη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20114"/>
        <source>Initial Max</source>
        <translation>Αρχικο Μεγιστο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20153"/>
        <source>Step</source>
        <translation>Βημα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20161"/>
        <source>Style</source>
        <translation>Στυλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20171"/>
        <source>Width</source>
        <translation>Πλατος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22870"/>
        <source>Opaqueness</source>
        <translation>Διαφανεια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20366"/>
        <source>Enter two times along profile</source>
        <translation>Εισοδος δυο χρονων κατα μηκος προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20369"/>
        <source>Start (00:00)</source>
        <translation>Εναρξη(00:00)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20370"/>
        <source>End (00:00)</source>
        <translation>Ληξη(00:00)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20389"/>
        <source>Fahrenheit</source>
        <translation>Φαρεναιτ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20390"/>
        <source>Celsius</source>
        <translation>Κελσιου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20517"/>
        <source>Time syntax error. Time not valid</source>
        <translation>Σφαλμα χρονικης συνταξης.Χρονος μη συμβατος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20521"/>
        <source>Error: End time smaller than Start time</source>
        <translation>Σφαλμα:Χρονος ληξης μικροτερος εναρξης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20538"/>
        <source>Best approximation was made from {0} to {1}</source>
        <translation type="unfinished">Βελτιστη προσεγγιση υπηρξε απο {0} εως {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20543"/>
        <source>No profile found</source>
        <translation>Δεν ευρεθει προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20621"/>
        <source>Bars</source>
        <translation>Μπαρες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20706"/>
        <source>Color</source>
        <translation>Χρωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26588"/>
        <source>Marker</source>
        <translation>Σημανση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20710"/>
        <source>Thickness</source>
        <translation>Παχος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20712"/>
        <source>Opacity</source>
        <translation>Διαφανεια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20714"/>
        <source>Size</source>
        <translation>Μεγεθος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20825"/>
        <source>Max buttons per row</source>
        <translation>Μεγιστος αριθμος πληκτρων ανα σειρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20853"/>
        <source>Color Pattern</source>
        <translation>Προτυπο χρωματων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20865"/>
        <source>palette #</source>
        <translation>Προτυπο#</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20985"/>
        <source>Event</source>
        <translation>Συμβαν</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20889"/>
        <source>Action</source>
        <translation>Δραση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20891"/>
        <source>Command</source>
        <translation>Εντολη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20893"/>
        <source>Offset</source>
        <translation>Οφσετ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20895"/>
        <source>Factor</source>
        <translation>Παραγων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23404"/>
        <source>Drying</source>
        <translation>Ξυρανση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23405"/>
        <source>Maillard</source>
        <translation>Maillard</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23406"/>
        <source>Development</source>
        <translation>Αναπτυξη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22603"/>
        <source>Default</source>
        <translation>Αρχικο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22633"/>
        <source>Aspect Ratio</source>
        <translation>Αναλογια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22883"/>
        <source>ET Color</source>
        <translation>Χρωμα ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22890"/>
        <source>BT Color</source>
        <translation>Χρωμα ΒΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22918"/>
        <source>DeltaET Color</source>
        <translation>Χρωμα DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22925"/>
        <source>DeltaBT Color</source>
        <translation>Χρωμα DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22972"/>
        <source>Text Warning</source>
        <translation>Κειμενο Προειδοποιησης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22973"/>
        <source>sec</source>
        <translation>δευτ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23407"/>
        <source>Cooling</source>
        <translation>Ψυξη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26732"/>
        <source>Curviness</source>
        <translation>Καμπυλοτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26745"/>
        <source>Events Playback</source>
        <translation>Αναπαραγωγη Συμβαντων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27492"/>
        <source>Comm Port</source>
        <translation>Θυρα Επικοινωνιας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27496"/>
        <source>Baud Rate</source>
        <translation>Ρυθμος Baud</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27502"/>
        <source>Byte Size</source>
        <translation>Mεγεθος Byte</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27508"/>
        <source>Parity</source>
        <translation>Ισοτιμια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27515"/>
        <source>Stopbits</source>
        <translation>Stopbits</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27521"/>
        <source>Timeout</source>
        <translation>Xρονικη Ληξη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27247"/>
        <source>Settings for non-Modbus devices</source>
        <translation>Ρυθμισεις για μη Modbus συσκευες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27365"/>
        <source>Slave</source>
        <translation>Υποχειριο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27370"/>
        <source>Register</source>
        <translation>Καταχωρηση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27375"/>
        <source>Float</source>
        <translation>Πλευση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27376"/>
        <source>Function</source>
        <translation>Λειτουργια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27482"/>
        <source>Device</source>
        <translation>Συσκευη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27959"/>
        <source>Control ET</source>
        <translation>Ελεγχος ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27963"/>
        <source>Read BT</source>
        <translation>Αναγνωση ΒΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28103"/>
        <source>Type</source>
        <translation>Τυπος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27968"/>
        <source>RS485 Unit ID</source>
        <translation>RS485 Μοναδα ID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27984"/>
        <source>ET Channel</source>
        <translation>Καναλι ΕΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27987"/>
        <source>BT Channel</source>
        <translation>Καναλι ΒΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28007"/>
        <source>AT Channel</source>
        <translation>Καναλι ΑΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28031"/>
        <source>ET Y(x)</source>
        <translation>ΕΤ Υ(χ)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28032"/>
        <source>BT Y(x)</source>
        <translation>ΒΤ Υ(χ)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30170"/>
        <source>Ratio</source>
        <translation>Αναλογια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30177"/>
        <source>Text</source>
        <translation>Κειμενο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30184"/>
        <source>Edge</source>
        <translation>Ακρο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30190"/>
        <source>Line</source>
        <translation>Γραμμη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30199"/>
        <source>Color pattern</source>
        <translation>Προτυπο Χρωματων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30485"/>
        <source> dg</source>
        <translation>dg</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30900"/>
        <source>Enter description</source>
        <translation>Εισαγωγη περιγραφης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31327"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(1-4)</source>
        <translation>Γραφημα ΩΩ:ΔΔ&lt;br&gt;(1-4)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31332"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(5-8)</source>
        <translation>Γραφημα ΩΩ:ΔΔ&lt;br&gt;(5-8)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31334"/>
        <source>Ramp/Soak Pattern</source>
        <translation>Προτυπο Γραφηματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>WARNING</source>
        <translation>ΠΡΟΕΙΔΟΠΟΙΗΣΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>Writing eeprom memory</source>
        <translation>Εγγραφη Μνημης eeprom</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>&lt;u&gt;Max life&lt;/u&gt; 10,000 writes</source>
        <translation>&lt;u&gt;ΜεγΖωη&lt;/u&gt;10,000 εγγραφες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>Infinite read life.</source>
        <translation>Αρχικη αναγνωση ζωης.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>After &lt;u&gt;writing&lt;/u&gt; an adjustment,&lt;br&gt;never power down the pid&lt;br&gt;for the next 5 seconds &lt;br&gt;or the pid may never recover.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>Read operations manual</source>
        <translation>Διαβαστε οδηγιες λειτουργιας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32911"/>
        <source>ET Thermocouple type</source>
        <translation>Τυπος θερμοστοιχειου ΕΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32918"/>
        <source>BT Thermocouple type</source>
        <translation>Τυπος θερμοστοιχειου ΒΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32705"/>
        <source>NOTE: BT Thermocouple type is not stored in the Artisan settings</source>
        <translation>ΣΗΜΕΙΩΣΗ:Ο Τυπος Θερμοστοιχειου δεν περιεχεται στις ρυθμισεις του artisan</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32804"/>
        <source>Artisan uses 1 decimal point</source>
        <translation>Το artisan χρησιμοποιει 1 δεκαδικο στοιχειο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32354"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(1-7)</source>
        <translation>Γραφημα (ΩΩ:ΔΔ)&lt;br&gt;(1-7)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32360"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(8-16)</source>
        <translation>Γραφημα (ΩΩ:ΔΔ)&lt;br&gt;(8-16)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32407"/>
        <source>Pattern</source>
        <translation>Προτυπο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32462"/>
        <source>SV (7-0)</source>
        <translation>SV(7-0)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32581"/>
        <source>Write</source>
        <translation>Εγγραφη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32563"/>
        <source>P</source>
        <translation>Ρ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32569"/>
        <source>I</source>
        <translation>Ι</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32575"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32805"/>
        <source>Artisan Fuji PXG uses MINUTES:SECONDS units in Ramp/Soaks</source>
        <translation>Το artisan Fuji PXG χρησιμοποιει ΛΕΠΤΑ:ΔΕΥΤΕΡΑ στο γραφημα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35279"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6886"/>
        <source>Curves</source>
        <translation>Καμπυλες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6890"/>
        <source>Delta Curves</source>
        <translation>Delta Καμπυλες</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4639"/>
        <source>T</source>
        <translation type="obsolete">T</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>RoR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>ETBTa</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16813"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16814"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5252"/>
        <source>CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9392"/>
        <source>TP</source>
        <translation type="unfinished">ΤΡ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9431"/>
        <source>DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9480"/>
        <source>FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11390"/>
        <source>Start recording</source>
        <translation type="unfinished">Εναρξη καταγραφης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11404"/>
        <source>Charge the beans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16451"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16645"/>
        <source>Path Effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16665"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19996"/>
        <source>Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34939"/>
        <source>Source</source>
        <translation type="unfinished">Πηγη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34955"/>
        <source>Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35001"/>
        <source>Lookahead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35008"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35009"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35010"/>
        <source>Background</source>
        <translation type="unfinished">Φοντο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35015"/>
        <source>SV Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35018"/>
        <source>SV Slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20993"/>
        <source>Coarse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27407"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27412"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16414"/>
        <source>HUD Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28297"/>
        <source>Raw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28316"/>
        <source>ServerId:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28318"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>MET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9376"/>
        <source>DRY%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9413"/>
        <source>RAMP%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9462"/>
        <source>DEV%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14477"/>
        <source>greens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14482"/>
        <source>roasted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18371"/>
        <source>Moisture Roasted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6881"/>
        <source>/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14451"/>
        <source>/m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28296"/>
        <source>Async</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28299"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28217"/>
        <source>Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28298"/>
        <source>Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28218"/>
        <source>Wiring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16515"/>
        <source>Delta Span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17683"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17819"/>
        <source>ml</source>
        <translation type="unfinished">μιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17783"/>
        <source>Unit Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17802"/>
        <source>g</source>
        <translation type="unfinished">γραμ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17800"/>
        <source>Kg</source>
        <translation type="unfinished">Κιλα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17817"/>
        <source>l</source>
        <translation type="unfinished">λ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17775"/>
        <source>in</source>
        <translation type="unfinished">Εισοδο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17850"/>
        <source>out</source>
        <translation type="unfinished">Εξοδο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28015"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28142"/>
        <source>Emissivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21195"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21210"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3209"/>
        <source>BackgroundXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22897"/>
        <source>XT Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22904"/>
        <source>XT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20869"/>
        <source>current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18184"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19999"/>
        <source>Counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6364"/>
        <source>BT {0} {1}/min for {2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6380"/>
        <source>ET {0} {1}/min for {2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15871"/>
        <source>{0} to reach ET {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15884"/>
        <source>{0} to reach BT {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19327"/>
        <source>Density in: {0} g/l   =&gt;   Density out: {1} g/l</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19365"/>
        <source>Moisture loss: {0}%    Organic loss: {1}%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20539"/>
        <source>&lt;b&gt;{0}&lt;/b&gt; {1}/sec, &lt;b&gt;{2}&lt;/b&gt; {3}/min</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../const/UIconst.py" line="48"/>
        <source>Services</source>
        <translation>Υπηρεσιες</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="49"/>
        <source>Hide {0}</source>
        <translation type="unfinished">Αποκρυψη{0}</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="50"/>
        <source>Hide Others</source>
        <translation>Αποκρυψη Αλλων</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="51"/>
        <source>Show All</source>
        <translation>Εμφανιση Ολων</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="52"/>
        <source>Preferences...</source>
        <translation>Προτιμησεις...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="74"/>
        <source>Quit {0}</source>
        <translation type="unfinished">Κλεισιμο{0}</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="163"/>
        <source>About {0}</source>
        <translation type="unfinished">Περι{0}</translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="../artisanlib/main.py" line="20661"/>
        <source>Circle</source>
        <translation>Κυκλος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20662"/>
        <source>Square</source>
        <translation>Τετραγωνο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20663"/>
        <source>Pentagon</source>
        <translation>Πενταγωνο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20664"/>
        <source>Diamond</source>
        <translation>Διαμαντι</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20665"/>
        <source>Star</source>
        <translation>Αστερι</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20666"/>
        <source>Hexagon 1</source>
        <translation>Εξαγωνο1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20667"/>
        <source>Hexagon 2</source>
        <translation>Εξαγωνο2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20668"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20669"/>
        <source>x</source>
        <translation>χ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20670"/>
        <source>None</source>
        <translation>Κανενα</translation>
    </message>
</context>
<context>
    <name>Menu</name>
    <message>
        <location filename="../artisanlib/main.py" line="7766"/>
        <source>CSV...</source>
        <translation>CSV...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7770"/>
        <source>JSON...</source>
        <translation>JSON...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7774"/>
        <source>RoastLogger...</source>
        <translation>Καταγραφικο...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7739"/>
        <source>HH506RA...</source>
        <translation>HH506RA...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7743"/>
        <source>K202...</source>
        <translation>K202...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7747"/>
        <source>K204...</source>
        <translation>K204...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="57"/>
        <source>File</source>
        <translation>Αρχειο</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="60"/>
        <source>New</source>
        <translation>Νεο</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="61"/>
        <source>Open...</source>
        <translation>Ανοιγμα...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="62"/>
        <source>Open Recent</source>
        <translation>Ανοιγμα Προσφατου</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="63"/>
        <source>Import</source>
        <translation>Εισαγωγη</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="64"/>
        <source>Save</source>
        <translation>Αποθηκευση</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="65"/>
        <source>Save As...</source>
        <translation>Αποθηκευση ως...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="66"/>
        <source>Export</source>
        <translation>Εξαγωγη</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="67"/>
        <source>Save Graph</source>
        <translation>Αποθυκευση Γραφηματος</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="68"/>
        <source>Full Size...</source>
        <translation>Πληρες Μεγεθος...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="69"/>
        <source>Roasting Report</source>
        <translation>Εκθεση Ψησιματος</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="70"/>
        <source>Print...</source>
        <translation>Εκτυπωση...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="77"/>
        <source>Edit</source>
        <translation>Επεξεργασια</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="80"/>
        <source>Cut</source>
        <translation>Αποκοπη</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="81"/>
        <source>Copy</source>
        <translation>Αντιγραφη</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="82"/>
        <source>Paste</source>
        <translation>Επικολληση</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="85"/>
        <source>Roast</source>
        <translation>Ψησιμο</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="88"/>
        <source>Properties...</source>
        <translation>Ιδιοτητες...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="89"/>
        <source>Background...</source>
        <translation>Φοντο...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="90"/>
        <source>Cup Profile...</source>
        <translation>Προφιλ cup...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="91"/>
        <source>Temperature</source>
        <translation>Θερμοκρασια</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="92"/>
        <source>Convert to Fahrenheit</source>
        <translation>Μετατροπη σε Φαρεναιτ</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="93"/>
        <source>Convert to Celsius</source>
        <translation>Μετατροπη σε Κελσιου</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="94"/>
        <source>Fahrenheit Mode</source>
        <translation>Κατασταση Φαρεναιτ</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="95"/>
        <source>Celsius Mode</source>
        <translation>Κατασταση Κελσιου</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="99"/>
        <source>Config</source>
        <translation>Διαμορφωση</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="102"/>
        <source>Device...</source>
        <translation>Συσκευη...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="103"/>
        <source>Serial Port...</source>
        <translation>Σειριακη Θυρα...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="104"/>
        <source>Sampling Interval...</source>
        <translation>Διαστημα Δειγματοληψειας...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="107"/>
        <source>Colors...</source>
        <translation>Χρωματα...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="110"/>
        <source>Phases...</source>
        <translation>Φασεις...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="111"/>
        <source>Events...</source>
        <translation>Συμβαντα...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="112"/>
        <source>Statistics...</source>
        <translation>Στατιστικα...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="113"/>
        <source>Axes...</source>
        <translation>Αξονες...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="114"/>
        <source>Autosave...</source>
        <translation>Αυτοματη αποθυκευση...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="116"/>
        <source>Alarms...</source>
        <translation>Συναγερμοι...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="117"/>
        <source>Language</source>
        <translation>Γλωσσα</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="142"/>
        <source>Tools</source>
        <translation>Εργαλεια</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="145"/>
        <source>Designer</source>
        <translation>Σχεδιαστηριο</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="146"/>
        <source>Calculator</source>
        <translation>Υπολογιστης</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="147"/>
        <source>Wheel Graph</source>
        <translation>Γραφημα Ροδας</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="149"/>
        <source>Extras...</source>
        <translation>Εξτρα...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="159"/>
        <source>Help</source>
        <translation>Βοηθεια</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="165"/>
        <source>Documentation</source>
        <translation>Τεκμηριωση</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="167"/>
        <source>Keyboard Shortcuts</source>
        <translation>Συντομευσεις Πληκτρολογιου</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="168"/>
        <source>Errors</source>
        <translation>Σφαλματα</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="169"/>
        <source>Messages</source>
        <translation>Μυνηματα</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="170"/>
        <source>Serial</source>
        <translation>Σειριακο</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="174"/>
        <source>Settings</source>
        <translation>Ρυθμισεις</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="175"/>
        <source>Platform</source>
        <translation>Πλατφορμα</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="176"/>
        <source>Factory Reset</source>
        <translation>Εργοστασιακη επαναφορα</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="96"/>
        <source>Switch Profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="106"/>
        <source>Oversampling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="164"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="148"/>
        <source>LCDs</source>
        <translation type="unfinished">LCDς</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="115"/>
        <source>Batch...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="153"/>
        <source>Load Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="154"/>
        <source>Load Recent Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="155"/>
        <source>Save Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="108"/>
        <source>Buttons</source>
        <translation type="unfinished">Πληκτρα</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="109"/>
        <source>Sliders</source>
        <translation type="unfinished">Σλαιντερς</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../artisanlib/main.py" line="1864"/>
        <source>HUD OFF</source>
        <translation>ΠΡΟΒΟΛΕΑΣ ΑΝΕΝΕΡΓΟΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1878"/>
        <source>HUD ON</source>
        <translation>ΠΡΟΒΟΛΕΑΣ ΕΝΕΡΓΟΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1970"/>
        <source>Alarm notice</source>
        <translation>Σημειωμα συναγερμου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2006"/>
        <source>Alarm is calling: {0}</source>
        <translation type="unfinished">Καλει συναγερμος {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2015"/>
        <source>Alarm trigger button error, description &apos;{0}&apos; not a number</source>
        <translation type="unfinished">Σφαλμα πληκτρου συναγερμου,περιγραφη &apos;{0}&apos;δεν ειναι αριθμος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2043"/>
        <source>Alarm trigger slider error, description &apos;{0}&apos; not a valid number [0-100]</source>
        <translation type="unfinished">Σφαλμα ολισθητηρα συναγερμου,περιγραφη &apos;{0}&apos;δεν ειναι πρεπων αριθμος[0-100]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2463"/>
        <source>Save the profile, Discard the profile (Reset), or Cancel?</source>
        <translation>Αποθηκευση προφιλ,Επαναφορα η Απορριψη?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2464"/>
        <source>Profile unsaved</source>
        <translation>Προφιλ μη αποθηκευμενο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2473"/>
        <source>Action canceled</source>
        <translation>Ενεργεια καταργηθηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2547"/>
        <source>Scope has been reset</source>
        <translation>Επαναφορα καταγραφεα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3757"/>
        <source>Time format error encountered</source>
        <translation>Σφαλμα χρονικου τυπου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3863"/>
        <source>Convert profile data to Fahrenheit?</source>
        <translation>Μετατροπη προφιλ σε Φαρεναιτ?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3948"/>
        <source>Convert Profile Temperature</source>
        <translation>Μετατροπη Θερμοκρασιων Προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3904"/>
        <source>Profile changed to Fahrenheit</source>
        <translation>Το Προφιλ μετατραπηκε σε Φαρεναιτ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3907"/>
        <source>Unable to comply. You already are in Fahrenheit</source>
        <translation>Αδυνατη ενεργεια.Ειναι ηδη Φαρεναιτ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3950"/>
        <source>Profile not changed</source>
        <translation>Καμια αλλαγη στο Προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3913"/>
        <source>Convert profile data to Celsius?</source>
        <translation>Μετατροπη πληροφοριων προφιλ σε Κελσιου?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3948"/>
        <source>Unable to comply. You already are in Celsius</source>
        <translation>Αδυνατη ενεργεια.Ειναι ηδη Κελσιου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3954"/>
        <source>Profile changed to Celsius</source>
        <translation>Το Προφιλ μετατραπηκε σε Κελσιου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3959"/>
        <source>Convert Profile Scale</source>
        <translation>Μετατροπη κλιμακας Προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3959"/>
        <source>No profile data found</source>
        <translation>Δεν ευρεθησαν πληροφοριες του προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3976"/>
        <source>Colors set to defaults</source>
        <translation>Καθορισμος αρχικων χρωματων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3981"/>
        <source>Colors set to grey</source>
        <translation>Καθορισμος χρωματων σε κλιμακα του Γκρι</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4152"/>
        <source>Scope monitoring...</source>
        <translation>Καταγραφεας ενεργοςι...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4197"/>
        <source>Scope stopped</source>
        <translation>Καταγραφεας σταματησε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4311"/>
        <source>Scope recording...</source>
        <translation>Καταγραφεας λειτουργει...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4352"/>
        <source>Scope recording stopped</source>
        <translation>Λειτουργια καταγραφεα σταματησε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4405"/>
        <source>Not enough variables collected yet. Try again in a few seconds</source>
        <translation>Μη συλλογη ικανων πληροφοριων.Δοκιμαστε ξανα  σε μερικα δευτερα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4459"/>
        <source>Roast time starts now 00:00 BT = {0}</source>
        <translation type="unfinished">Εναρξη χρονου ψησιματος τωρα 00:00 ΒΤ{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4920"/>
        <source>Scope is OFF</source>
        <translation>Καταγραφεας ανενεργος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4540"/>
        <source>[DRY END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[ΛΗΞΗ ΞΥΡΑΝΣΗΣ]Καταγραφηκε στις {0} ΒΤ = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4603"/>
        <source>[FC START] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[ΕΝΡΞΗ FC]Καταγραφηκε στις {0} ΒΤ = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4658"/>
        <source>[FC END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[ΛΗΞΗ FC]Καταγραφηκε στις {0} ΒΤ = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4716"/>
        <source>[SC START] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[ΕΝΑΡΞΗ SC]Καταγραφηκε στις {0} ΒΤ = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4773"/>
        <source>[SC END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[ΛΗΞΗ SC]Καταγραφηκε στις {0} ΒΤ = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4855"/>
        <source>Roast ended at {0} BT = {1}</source>
        <translation type="unfinished">Τελος Ψησιματος στις {0}ΒΤ={1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4953"/>
        <source>[COOL END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[ΛΗΞΗ ΨΥΞΗΣ]Καταγραφηκε στις {0} ΒΤ = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5089"/>
        <source>Event # {0} recorded at BT = {1} Time = {2}</source>
        <translation type="unfinished">Συμβαν # {0} καταγραφηκε στις ΒΤ={1} Χρονος = {2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5104"/>
        <source>Timer is OFF</source>
        <translation>Χρονομετρο Ανενεργο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5136"/>
        <source>Computer Event # {0} recorded at BT = {1} Time = {2}</source>
        <translation type="unfinished">Συμβαν Υπολογιστη # {0} καταγραφηκε στις ΒΤ={1} Χρονος = {2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5298"/>
        <source>Statistics cancelled: need complete profile [CHARGE] + [DROP]</source>
        <translation>Ακυρωση στατιστικων:Χρειζει ολοκληρωμενου προφιλ [ΦΟΡΤΩΜΑ] + [ΞΕΦΟΡΤΩΜΑ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5483"/>
        <source>Unable to move background</source>
        <translation>Αδυνατη μετακινηση φοντου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5542"/>
        <source>No finished profile found</source>
        <translation>Δεν ευρεθησαν ολοκληρωμενα προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5560"/>
        <source>Polynomial coefficients (Horner form):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5563"/>
        <source>Knots:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5566"/>
        <source>Residual:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5569"/>
        <source>Roots:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5573"/>
        <source>Profile information</source>
        <translation>Πληροφοριες Προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5734"/>
        <source>Designer Start</source>
        <translation>Εναρξη Σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4161"/>
        <source>Importing a profile in to Designer will decimate
all data except the main [points].
Continue?</source>
        <translation type="obsolete">Εισαγωγη προφιλ στο Σχεδιαστηριο θα διαγραψει ολες πληροφοριες εκτος κυριων σημειων[points].Συνεχεια?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5778"/>
        <source>Designer Init</source>
        <translation>Προκαθορισμενο Σχεδιαστηριο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5778"/>
        <source>Unable to start designer.
Profile missing [CHARGE] or [DROP]</source>
        <translation>Αδυνατη εναρξη σχεδιαστηριου.Ελλειψη στο προφιλ[ΦΟΡΤΩΜΑ] η [ΞΕΦΟΡΤΩΜΑ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6024"/>
        <source>[ CHARGE ]</source>
        <translation>[ΦΟΡΤΩΜΑ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6027"/>
        <source>[ DRY END ]</source>
        <translation>[ΛΗΞΗ ΞΥΡΑΝΣΗΣ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6030"/>
        <source>[ FC START ]</source>
        <translation>[ΕΝΑΡΞΗ FC]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6033"/>
        <source>[ FC END ]</source>
        <translation>[ΛΗΞΗ FC]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6036"/>
        <source>[ SC START ]</source>
        <translation>[ENARJH ΣΨ]]]]

[ΕΝΑΡΞΗ SC]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6039"/>
        <source>[ SC END ]</source>
        <translation>[ΛΗΞΗ SC]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6042"/>
        <source>[ DROP ]</source>
        <translation>[ΞΕΦΟΡΤΩΜΑ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6305"/>
        <source>New profile created</source>
        <translation>Δημιουργηθηκε νεο προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30658"/>
        <source>Open Wheel Graph</source>
        <translation>Ανοιγμα γραφικου ροδας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6492"/>
        <source> added to cupping notes</source>
        <translation>Προστεθηκε σε σημειωσεις cupping</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6498"/>
        <source> added to roasting notes</source>
        <translation>Προστεθηκε σε σημειωσεις ψησιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6765"/>
        <source>Mouse Cross ON: move mouse around</source>
        <translation>Κερσορας ποντικου ΕΝΕΡΓΟΣ:μετακινηστε ποντικι</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6777"/>
        <source>Mouse cross OFF</source>
        <translation>Κερσορας ποντικου ΑΝΕΝΕΡΓΟΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9903"/>
        <source>Do you want to reset all settings?</source>
        <translation>Επιθυμειτε επαναφορα ολων των ρυθμισεων?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9904"/>
        <source>Factory Reset</source>
        <translation>Εργοστασιακη επαναφορα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10144"/>
        <source>Exit Designer?</source>
        <translation>Εξοδος Σχεδιαστηριου?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10145"/>
        <source>Designer Mode ON</source>
        <translation>Λειτουργια σχεδιαστηριου ΕΝΕΡΓΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8931"/>
        <source>Extra Event Button Palette</source>
        <translation type="obsolete">Εξτρα Πληκτρο Συμβαντος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10304"/>
        <source>Keyboard moves turned ON</source>
        <translation>Κινηση Πληκτρολογιου ΕΝΕΡΓΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10313"/>
        <source>Keyboard moves turned OFF</source>
        <translation>Κινηση Πληκτρολογιου ΑΝΕΝΕΡΓΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10410"/>
        <source>Profile {0} saved in: {1}</source>
        <translation type="unfinished">Προφιλ {0} αποθυκευτηκε σε {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10415"/>
        <source>Empty path or box unchecked in Autosave</source>
        <translation>Κενη Διαδρομη η κουτι αμαρκαριστο στην Αυτοματη Αποθυκευση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10422"/>
        <source>&lt;b&gt;[ENTER]&lt;/b&gt; = Turns ON/OFF Keyboard Shortcuts</source>
        <translation>&lt;b&gt;[ΕΙΣΑΓΩΓΗ]&lt;/b&gt; = ΑΝΟΙΓΕΙ/ΚΛΕΙΝΕΙ Συντομευσεις Πληκτρολογιου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10423"/>
        <source>&lt;b&gt;[SPACE]&lt;/b&gt; = Choses current button</source>
        <translation>&lt;b&gt;[ΔΙΑΣΤΗΜΑ]&lt;/b&gt; = Επιλογη τωρινου πληκτρου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10424"/>
        <source>&lt;b&gt;[LEFT]&lt;/b&gt; = Move to the left</source>
        <translation>&lt;b&gt;[ΑΡΙΣΤΕΡΑ]&lt;/b&gt; = Μετακινηση Αριστερα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10425"/>
        <source>&lt;b&gt;[RIGHT]&lt;/b&gt; = Move to the right</source>
        <translation>&lt;b&gt;[ΔΕΞΙΑ]&lt;/b&gt; = Μετακινηση Δεξια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10426"/>
        <source>&lt;b&gt;[a]&lt;/b&gt; = Autosave</source>
        <translation>&lt;b&gt;[a]&lt;/b&gt; = Aυτοματη Αποθυκευση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10427"/>
        <source>&lt;b&gt;[CRTL N]&lt;/b&gt; = Autosave + Reset + START</source>
        <translation>&lt;b&gt;[CRTL N]&lt;/b&gt; = Αυτοματη Αποθυκευση + Επαναφορα + ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10428"/>
        <source>&lt;b&gt;[t]&lt;/b&gt; = Mouse cross lines</source>
        <translation>&lt;b&gt;[t]&lt;/b&gt; = Μετακινηση Σταυρου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10430"/>
        <source>&lt;b&gt;[b]&lt;/b&gt; = Shows/Hides Extra Event Buttons</source>
        <translation>&lt;b&gt;[b]&lt;/b&gt; = Εμφανιση/Αποκρυψη Πληκτρων Εξτρα Συμβαντων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10431"/>
        <source>&lt;b&gt;[s]&lt;/b&gt; = Shows/Hides Event Sliders</source>
        <translation>&lt;b&gt;[s]&lt;/b&gt; = Εμφανιση/Αποκρυψη Σλαιντερ Συμβαντων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10432"/>
        <source>&lt;b&gt;[i]&lt;/b&gt; = Retrieve Weight In from Scale</source>
        <translation>&lt;b&gt;[i]&lt;/b&gt; = Ανακτηση Βαρους Εισοδου απο Ζυγο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10433"/>
        <source>&lt;b&gt;[o]&lt;/b&gt; = Retrieve Weight Out from Scale</source>
        <translation>&lt;b&gt;[o]&lt;/b&gt; = Ανακτηση Βαρους Εξοδου απο Ζυγο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10434"/>
        <source>&lt;b&gt;[0-9]&lt;/b&gt; = Changes Event Button Palettes</source>
        <translation>&lt;b&gt;[0-9]&lt;/b&gt; = Αλλαγη Προτυπων Πληκτρων Συμβαντων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10435"/>
        <source>&lt;b&gt;[;]&lt;/b&gt; = Application ScreenShot</source>
        <translation>&lt;b&gt;[;]&lt;/b&gt; = Φωτογραφια Εφαρμογης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10436"/>
        <source>&lt;b&gt;[:]&lt;/b&gt; = Desktop ScreenShot</source>
        <translation>&lt;b&gt;[:]&lt;/b&gt; = Φωτογραφια Επιφανειας Εργασιας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10440"/>
        <source>Keyboard Shotcuts</source>
        <translation>Συντομευσεις Πληκτρολογιου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10521"/>
        <source>Event #{0}:  {1} has been updated</source>
        <translation type="unfinished">Συμβαν #{0}: {1} εχει ενημερωθει</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10599"/>
        <source>Save</source>
        <translation>Αποθυκευση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10611"/>
        <source>Select Directory</source>
        <translation>Επιλεξτε Ευρετηριο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19230"/>
        <source>No profile found</source>
        <translation>Δεν ευρεθει Προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10660"/>
        <source>{0} has been saved. New roast has started</source>
        <translation type="unfinished">{0} εχει αποθυκευτει.Εναρξη νεου ψησιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10823"/>
        <source>Invalid artisan format</source>
        <translation>Μη αποδεκτη φορμα artisan</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10704"/>
        <source>{0}  loaded </source>
        <translation type="unfinished">{0} φορτωθηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10819"/>
        <source>Background {0} loaded successfully {1}</source>
        <translation type="unfinished">Το Φοντο {0}  φορτωθηκε  με επιτυχια {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10951"/>
        <source>Artisan CSV file loaded successfully</source>
        <translation>Αρχειο artisan csv φορτωθηκε με επιτυχια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11570"/>
        <source>To load this profile the extra devices configuration needs to be changed.
Continue?</source>
        <translation>Για επιτυχη φορτωση του προφιλ χρειαζεται αλλαγη διαμορφωσης των εξτρα συσκευων.Συνεχεια?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11571"/>
        <source>Found a different number of curves</source>
        <translation>Ευρεθει διαφορετικος αριθμος καμπυλων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12348"/>
        <source>Save Profile</source>
        <translation>Αποθυκευση Προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12355"/>
        <source>Profile saved</source>
        <translation>Προφιλ Αποθυκευτηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15387"/>
        <source>Cancelled</source>
        <translation>Ακυρωθηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12371"/>
        <source>Readings exported</source>
        <translation>Εγινε εξαγωγη δεδομενων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12379"/>
        <source>Export CSV</source>
        <translation>Εξαγωγη CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12382"/>
        <source>Export JSON</source>
        <translation>Εξαγωγη JSON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12385"/>
        <source>Export RoastLogger</source>
        <translation>Εξαγωγη Καταγραφεα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12394"/>
        <source>Readings imported</source>
        <translation>Δεδομενα εισηχθησαν</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12402"/>
        <source>Import CSV</source>
        <translation>Εισαγωγη CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12405"/>
        <source>Import JSON</source>
        <translation>Εισαγωγη JSON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12408"/>
        <source>Import RoastLogger</source>
        <translation>Εισαγωγη  Καταγραφεα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15113"/>
        <source>Sampling Interval</source>
        <translation>Διαστημα Δειγματοληψειας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15113"/>
        <source>Seconds</source>
        <translation>Δευτερολεπτα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15489"/>
        <source>Alarm Config</source>
        <translation>Διαμορφωση Συναγερμου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15489"/>
        <source>Alarms are not available for device None</source>
        <translation>Συναγερμοι δεν ειναι διαθεσιμοι για συσκευη Καμια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15546"/>
        <source>Switch Language</source>
        <translation>Αλλαγη Γλωσσας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15546"/>
        <source>Language successfully changed. Restart the application.</source>
        <translation>Επιτυχης αλλαγη γλωσσας.Επανεκκινηστε Εφαρμογη.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15578"/>
        <source>Import K202 CSV</source>
        <translation>Εισαγωγη Κ202 CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15630"/>
        <source>K202 file loaded successfully</source>
        <translation>Αρχειο Κ202 φορτωθηκε με επιτυχια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15644"/>
        <source>Import K204 CSV</source>
        <translation>Εισαγωγη K204 CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15710"/>
        <source>K204 file loaded successfully</source>
        <translation>Αρχειο Κ204 φορτωθηκε με επιτυχια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15724"/>
        <source>Import HH506RA CSV</source>
        <translation>Εισαγωγη HH506RA CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15775"/>
        <source>HH506RA file loaded successfully</source>
        <translation>Αρχειο HH506RA φορτωθηκε με επιτυχια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15820"/>
        <source>Save Graph as PNG</source>
        <translation>Αποθυκευση Γραφικου ως PNG</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15827"/>
        <source>{0}  size({1},{2}) saved</source>
        <translation type="unfinished">{0} μεγεθος({1},{2})αποθυκευτηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15836"/>
        <source>Save Graph as SVG</source>
        <translation>Αποθυκευση Γραφικου ως SVG</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15841"/>
        <source>{0} saved</source>
        <translation type="unfinished">{0} Αποθυκευτηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16046"/>
        <source>Invalid Wheel graph format</source>
        <translation>Μη αποδεκτο φορμα Γραφικου Ροδας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16049"/>
        <source>Wheel Graph succesfully open</source>
        <translation>Γραφικο Ροδας ανοιξε με επιτυχια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16068"/>
        <source>Return the absolute value of x.</source>
        <translation>Επιστροφη απολυτης τιμης χ.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16069"/>
        <source>Return the arc cosine (measured in radians) of x.</source>
        <translation>Επιστροφη arc cosine του χ.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16070"/>
        <source>Return the arc sine (measured in radians) of x.</source>
        <translation>Επιστροφη arc cine του χ.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16071"/>
        <source>Return the arc tangent (measured in radians) of x.</source>
        <translation>Επιστροφη arc tangent του χ.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16072"/>
        <source>Return the cosine of x (measured in radians).</source>
        <translation>Επιστροφη  cosine του χ.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16073"/>
        <source>Convert angle x from radians to degrees.</source>
        <translation>Μετατροπη γωνιας χ απο radian σε degrees.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16074"/>
        <source>Return e raised to the power of x.</source>
        <translation>Eπιστροφη e raised στην δυναμη του χ.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16075"/>
        <source>Return the logarithm of x to the given base. </source>
        <translation>Επιστροφη λογαριθμου χ στην δεδομενη βαση.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16076"/>
        <source>Return the base 10 logarithm of x.</source>
        <translation>Επιστροφη βασης 10 σε λογαριθμο του χ.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16079"/>
        <source>Return x**y (x to the power of y).</source>
        <translation>Επιστροφη x**y.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16080"/>
        <source>Convert angle x from degrees to radians.</source>
        <translation>Μετατροπη γωνιας χ απο degrees σε radians.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16081"/>
        <source>Return the sine of x (measured in radians).</source>
        <translation>Επιστροφη sine του χ.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16082"/>
        <source>Return the square root of x.</source>
        <translation>Επιστροφη τετραγωνικης ριζας του x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16083"/>
        <source>Return the tangent of x (measured in radians).</source>
        <translation>Eπιστροφη tangent του x.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15048"/>
        <source>previous ET value</source>
        <translation type="obsolete">προηγουμενη τιμη ΕΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15049"/>
        <source>previous BT value</source>
        <translation type="obsolete">προηγουμενη τιμη ΒΤ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15050"/>
        <source>previous Extra #1 T1 value</source>
        <translation type="obsolete">προηγουμενη τιμη Εξτρα #1 Τ1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15051"/>
        <source>previous Extra #1 T2 value</source>
        <translation type="obsolete">προηγουμενη τιμη Εξτρα #1 Τ2</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15052"/>
        <source>previous Extra #2 T1 value</source>
        <translation type="obsolete">προηγουμενη τιμη Εξτρα #2 Τ1</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15053"/>
        <source>previous Extra #2 T2 value</source>
        <translation type="obsolete">προηγουμενη τιμη Εξτρα #2 Τ2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16100"/>
        <source>MATHEMATICAL FUNCTIONS</source>
        <translation>MAΘΗΜΑΤΙΚΕΣ ΛΕΙΤΟΥΡΓΙΕΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16102"/>
        <source>SYMBOLIC VARIABLES</source>
        <translation>ΣΥΜΒΟΛΙΚΑ ΚΥΜΑΙΝΟΜΕΝΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16104"/>
        <source>Symbolic Functions</source>
        <translation>Συμβολικες Λειτουργιες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16234"/>
        <source>Save Palettes</source>
        <translation>Αποθυκευση Προτυπων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16238"/>
        <source>Palettes saved</source>
        <translation>Προτυπα Αποθυκευτηκαν</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16281"/>
        <source>Invalid palettes file format</source>
        <translation>Μη Αποδεκτη φορμα Προτυπων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16284"/>
        <source>Palettes loaded</source>
        <translation>Προτυπα Φορτωθηκαν</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16296"/>
        <source>Load Palettes</source>
        <translation>Φορτωση Προτυπων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16317"/>
        <source>Alarms loaded</source>
        <translation>Συναγερμοι Φορτωθηκαν</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17446"/>
        <source>Interpolation failed: no profile available</source>
        <translation>Σφαλμα:Δεν ευρεθει προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17455"/>
        <source>Sound turned ON</source>
        <translation>Ηχος ΕΝΕΡΓΟΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17459"/>
        <source>Sound turned OFF</source>
        <translation>Ηχος ΑΝΕΝΕΡΓΟΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19227"/>
        <source>Event #{0} added</source>
        <translation type="unfinished">Συμβαν #{0} προστεθηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19263"/>
        <source> Event #{0} deleted</source>
        <translation type="unfinished">Συμβαν #{0} διαγραφηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19269"/>
        <source>No events found</source>
        <translation>Δεν ευρεθησαν συμβαντα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19566"/>
        <source>Roast properties updated but profile not saved to disk</source>
        <translation>Ιδιοτητες Ψησιματος ενημερωθηκαν αλλα δεν εγινε αποθυκευση του Προφιλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19956"/>
        <source>Autosave ON. Prefix: {0}</source>
        <translation type="unfinished">Αυτοματη Αποθυκευση ΕΝΕΡΓΗ.Προκαθορισμενο: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19960"/>
        <source>Autosave OFF</source>
        <translation>Αυτοματη Αποθυκευση ΑΝΕΝΕΡΓΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20335"/>
        <source>xlimit = ({2},{3}) ylimit = ({0},{1}) zlimit = ({4},{5})</source>
        <translation type="unfinished">οριοχ=({2},{3}) οριοy = ({0},{1}) οριοz = ({4},{5})</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21457"/>
        <source>&lt;b&gt;Event&lt;/b&gt; hide or show the corresponding slider</source>
        <translation>&lt;b&gt;Συμβαν&lt;/b&gt;αποκρυψη η εμφανιση του αντιστοιχου σλαιντερ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21458"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action on slider release</source>
        <translation>&lt;b&gt;Ενεργεια&lt;/b&gt;Ενεργοποιηστε κατα την απελευθερωση του σλαιντερ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21459"/>
        <source>&lt;b&gt;Command&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by &lt;i&gt;value&lt;/i&gt;*&lt;i&gt;factor&lt;/i&gt; + &lt;i&gt;offset&lt;/i&gt;)</source>
        <translation>&lt;b&gt;Εντολη&lt;/b&gt;εξαρταται απο ειδος εντολης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22256"/>
        <source>Serial Command: ASCII serial command or binary a2b_uu(serial command)</source>
        <translation>Σειριακη Εντολη:ASCII σειριακη εντολη η δυαδικο a2b_uu</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20534"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) writes values to the registers in slaves specified by the given ids</source>
        <translation type="obsolete">Eγγραφη Εντολης Modbus</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22260"/>
        <source>DTA Command: Insert Data address : value, ex. 4701:1000 and sv is 100. always multiply with 10 if value Unit: 0.1 / ex. 4719:0 stops heating</source>
        <translation>Εντολη DTA:Χρειζει εισαγωγης τιμων,δεδομενων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21463"/>
        <source>&lt;b&gt;Offset&lt;/b&gt; added as offset to the slider value</source>
        <translation>&lt;b&gt;Οφσετ&lt;/b&gt;προστεθηκε ως οφσετ στην τιμη του σλαιντερ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21464"/>
        <source>&lt;b&gt;Factor&lt;/b&gt; multiplicator of the slider value</source>
        <translation>&lt;b&gt;Παραγων&lt;/b&gt;πολλαπλασιαστικο τιμης σλαιντερ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22264"/>
        <source>Event custom buttons</source>
        <translation>Πληκτρα Συμβαντων Χρηστη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22225"/>
        <source>Event configuration saved</source>
        <translation>Διαμορφωση Συμβαντων Αποθυκευτηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22228"/>
        <source>Found empty event type box</source>
        <translation>Ευρεθει κενο κουτι τυπου συμβαντων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22250"/>
        <source>&lt;b&gt;Button Label&lt;/b&gt; Enter \n to create labels with multiple lines.</source>
        <translation>&lt;b&gt;Επιγραφη Πληκτρου&lt;/b&gt;Εισαγετε \n για δημιουργια επιγραφων με πολλαπλες γραμμες.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22251"/>
        <source>&lt;b&gt;Event Description&lt;/b&gt; Description of the Event to be recorded.</source>
        <translation>&lt;b&gt;Περιγραφη Συμβαντων&lt;/b&gt;Περιγραφει το συμβαν προς καταγραφη.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22252"/>
        <source>&lt;b&gt;Event type&lt;/b&gt; Type of event to be recorded.</source>
        <translation>&lt;b&gt;Τυπος Συμβαν&lt;/b&gt;Τυπος του συμβαντος προς καταγραφη.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20781"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-10) to be recorded</source>
        <translation type="obsolete">&lt;b&gt;Τιμη Συμβαντος&lt;/b&gt;Τιμη του συμβαντος (1-10) προς καταγραφη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22254"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action at the time of the event</source>
        <translation>&lt;b&gt;Ενεργεια&lt;/b&gt;Επιλεξτε ενεργεια κατα το συμβαν</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22255"/>
        <source>&lt;b&gt;Documentation&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by the event value):</source>
        <translation>&lt;b&gt;Τεκμηριωση&lt;/b&gt;Εξαρταται απο ειδος ενεργειας:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22257"/>
        <source>Call Program: A program/script path (absolute or relative)</source>
        <translation>Aνακληση Προγραμματος:Διαδρομη προγραμματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22258"/>
        <source>Multiple Event: Adds events of other button numbers separated by a comma: 1,2,3, etc.</source>
        <translation>Πολλαπλα Συμβαντα:Προσθετει συμβαντα η αλλους αριθμους πληκτρων χωρισμενα με κομμα:1,2,3, κλπ.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22262"/>
        <source>&lt;b&gt;Button Visibility&lt;/b&gt; Hides/shows individual button</source>
        <translation>&lt;b&gt;Εμφανιση Πληκτρων&lt;/b&gt;Εμφανιση/Αποκρυψη μεμονομενων πληκτρων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22263"/>
        <source>&lt;b&gt;Keyboard Shorcut: &lt;/b&gt; [b] Hides/shows Extra Button Rows</source>
        <translation>&lt;b&gt;Συντομευση Πληκτρολογιου&lt;/b&gt;Εμφανιση/Αποκρυψη Εξτρα Σειρων Πληκτρων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22585"/>
        <source>Phases changed to {0} default: {1}</source>
        <translation type="unfinished">Αλλαγη Φασεων σε {0} προκαθορισμενο: {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22726"/>
        <source>Background profile not found</source>
        <translation>Προφιλ Φοντου δεν ευρεθει</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22731"/>
        <source>Background does not match number of labels</source>
        <translation>Το Φοντο Δεν ταιριαζει ως προς αριθμο επιγραφων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23082"/>
        <source>Playback Aid set ON at {0} secs</source>
        <translation type="unfinished">Βοηθημα αναπαραγωγης ΕΝΕΡΓΟ στα {0} δευτερολεπτα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23090"/>
        <source>No profile background found</source>
        <translation>Προφιλ Φοντου δεν ευρεθει</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23186"/>
        <source>Reading background profile...</source>
        <translation>Αναγνωση προφιλ φοντου...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26841"/>
        <source>Not enough time points for an ET curviness of {0}. Set curviness to {1}</source>
        <translation type="unfinished">Μη επαρκη χρονικα σημεια για καμπυλη ΕΤ του {0}.Επιλεξτε καμπυλη του {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27051"/>
        <source>Designer Config</source>
        <translation>Διαμορφωση Σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26848"/>
        <source>Not enough time points for an BT curviness of {0}. Set curviness to {1}</source>
        <translation type="unfinished">Μη επαρκη χρονικα σημεια για καμπυληΒΤ του {0}.Επιλεξτε καμπυλη του {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27043"/>
        <source>CHARGE</source>
        <translation>ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27044"/>
        <source>DRY END</source>
        <translation>ΛΗΞΗ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27045"/>
        <source>FC START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27046"/>
        <source>FC END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27047"/>
        <source>SC START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27048"/>
        <source>SC END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27049"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26863"/>
        <source>Incorrect time format. Please recheck {0} time</source>
        <translation type="unfinished">Μη αποδεκτο χρονικο φορμα.Παρακαλω ελεγξτε {0} χρονο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27050"/>
        <source>Times need to be in ascending order. Please recheck {0} time</source>
        <translation type="unfinished">Χρονοι χρειζουν ανοδικη κατευθυνση.Παρακαλω ελεγξτε {0} χρονο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27015"/>
        <source>Designer has been reset</source>
        <translation>Επαναφορα Σχεδιαστηριου</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23873"/>
        <source>These serial settings are used for all Modbus communication.</source>
        <translation type="obsolete">Ρυθμισεις σειριακες για χρηση ολων επικοινωνιων Modbus.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23874"/>
        <source>The MODBUS device corresponds to input channels 1 and 2.</source>
        <translation type="obsolete">Συσκευη MODBUS αντιστοιχει στα καναλια εισαγωγης 1 και 2.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23875"/>
        <source>The +MODBUS_34 extra device adds input channels 3 and 4.</source>
        <translation type="obsolete">Συσκευη +MODBUS_34 προσθετει καναλια καναλια εισαγωγης 3 και 4.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23876"/>
        <source>Inputs with slave id set to 0 are turned off.</source>
        <translation type="obsolete">Εισερχομενα με slave id καθορισμαενα στο 0 ειναι ανενεργα.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23877"/>
        <source>Modbus function 3 &apos;read holding register&apos; is the standard.</source>
        <translation type="obsolete">Λειτουργια 3 modbus &apos;read holding register&apos; ειναι στανταρτ.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23878"/>
        <source>Modbus function 4 triggers the use of &apos;read input register&apos;.</source>
        <translation type="obsolete">Λειτουργια 4 Modbus καθοριζει τη χρηση &apos;read input register&apos;.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23879"/>
        <source>Input registers (fct 4) usually are from the range 30000-39999.</source>
        <translation type="obsolete">Συνηθη δεδομενα εισαγωγης(fct 4) στη κλιμακα 30000-39999.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23880"/>
        <source>Most devices hold data in 2 byte integer registers.</source>
        <translation type="obsolete">Οι περισσοτερες συσκευες παρακρατουν δεδομενα σε μητρωα 2 bytes.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23882"/>
        <source>In that case you have to use the symbolic assignment &apos;x/10&apos;.</source>
        <translation type="obsolete">Σε αυτη την περιπτωση χρησιμοποιηστε symbolic assignment &apos;x/10&apos;.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23883"/>
        <source>Few devices hold data as 4 byte floats in two registers.</source>
        <translation type="obsolete">Λιγες συσκευες κρατουν δεδομενα σε διπλα μητρωα των 4 bytes.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27430"/>
        <source>Tick the Float flag in this case.</source>
        <translation>Κλικαρετε τη σημαια float σε αυτη την περιπτωση.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27872"/>
        <source>Serial Port Settings: {0}, {1}, {2}, {3}, {4}, {5}</source>
        <translation type="unfinished">Ρυθμισεις Σειριακων Θυρων: {0},{1},{2},{3},{4},{5}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20233"/>
        <source>Port scan on this platform not yet supported</source>
        <translation type="obsolete">Ανιχνευση θυρας σε αυτη την πλατφορμα δεν υποστηριζεται</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28654"/>
        <source>External program</source>
        <translation>Εξωτερικο Προγραμμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28863"/>
        <source>Device not set</source>
        <translation>Συσκευη δεν καθοριστηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28920"/>
        <source>PID to control ET set to {0} {1} ; PID to read BT set to {2} {3}</source>
        <translation type="unfinished">PID για ελεγχο ΕΤ καθοριστηκε σε {0} {1}; PID για αναγνωση ΒΤ καθοριστηκε σε {2} {3}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29137"/>
        <source>Device set to {0}. Now, check Serial Port settings</source>
        <translation type="unfinished">Συσκευη καθοριστηκε σε {0}.Τωρα ελεγξτε ρυθμισεις θυρων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29275"/>
        <source>Device set to {0}. Now, chose serial port</source>
        <translation type="unfinished">Συσκευη καθοριστηκε σε {0}.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28989"/>
        <source>Device set to CENTER 305, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation>Συσκευη καθοριστηκε στο CENTER 305, που ειναι ιδιο με CENTER 306.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29043"/>
        <source>Device set to {0}, which is equivalent to CENTER 309. Now, chose serial port</source>
        <translation type="unfinished">Συσκευη καθοριστηκε στο {0}, που ειναι ιδιο με CENTER 309.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29070"/>
        <source>Device set to {0}, which is equivalent to CENTER 303. Now, chose serial port</source>
        <translation type="unfinished">Συσκευη καθοριστηκε στο {0}, που ειναι ιδιο με CENTER 303.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29052"/>
        <source>Device set to {0}, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation type="unfinished">Συσκευη καθοριστηκε στο {0}, που ειναι ιδιο με CENTER 306.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29079"/>
        <source>Device set to {0}, which is equivalent to Omega HH506RA. Now, chose serial port</source>
        <translation type="unfinished">Συσκευη καθοριστηκε στο {0}, που ειναι ιδιο με Omega HH506RA.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29180"/>
        <source>Device set to {0}, which is equivalent to Omega HH806AU. Now, chose serial port</source>
        <translation type="unfinished">Συσκευη καθοριστηκε στο {0}, που ειναι ιδιο με Omega HH806AU.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29253"/>
        <source>Device set to {0}</source>
        <translation type="unfinished">Συσκευη καθοριστηκε στο {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29110"/>
        <source>Device set to {0}{1}</source>
        <translation type="unfinished">Συσκευη καθοριστηκε στο {0}{1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29171"/>
        <source>Device set to {0}, which is equivalent to CENTER 302. Now, chose serial port</source>
        <translation type="unfinished">Συσκευη καθοριστηκε στο {0}, που ειναι ιδιο με CENTER 302.Τωρα επιλεξτε θυρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30003"/>
        <source>Color of {0} set to {1}</source>
        <translation type="unfinished">Το χρωμα του {0} καθοριστηκε σε {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30648"/>
        <source>Save Wheel graph</source>
        <translation>Αποθυκευση Γραφηματος Ροδας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30652"/>
        <source>Wheel Graph saved</source>
        <translation>Γραφημα Ροδας Αποθυκευτηκε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30965"/>
        <source>Load Alarms</source>
        <translation>Φορτωση Συναγερμων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31005"/>
        <source>Save Alarms</source>
        <translation>Αποθυκευση Συναγερμων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31037"/>
        <source>&lt;b&gt;Status:&lt;/b&gt; activate or deactive alarm</source>
        <translation>&lt;b&gt;Θεσις&lt;/b&gt;Ενεργοποιηση η απενεργοποιηση συναγερμου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31038"/>
        <source>&lt;b&gt;If Alarm:&lt;/b&gt; alarm triggered only if the alarm with the given number was triggered before. Use 0 for no guard.</source>
        <translation>&lt;b&gt;Εαν Συναγερμος&lt;/b&gt;συναγερμος ανταποκρινεται μονο εαν ο συγκεκριμενος εχει ανταποκριθει πρωτυτερα.Χρηση 0 για μη προφυλαξη.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31040"/>
        <source>&lt;b&gt;From:&lt;/b&gt; alarm only triggered after the given event</source>
        <translation>&lt;b&gt;Απο&lt;/b&gt;συναγερμος καθοριζεται μετα απο δεδομενο συμβαν</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31041"/>
        <source>&lt;b&gt;Time:&lt;/b&gt; if not 00:00, alarm is triggered mm:ss after the event &apos;From&apos; happend</source>
        <translation>&lt;b&gt;Χρονος&lt;/b&gt;εαν οχι 00:00,συναγερμος ανταποκρινεταιι λλ:δδ μετα το συμβαν &apos;Απο&apos;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31042"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; the temperature source that is observed</source>
        <translation>&lt;b&gt;Πηγη&lt;/b&gt;Παρατηρηση πηγης θερμοκρασιας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31043"/>
        <source>&lt;b&gt;Condition:&lt;/b&gt; alarm is triggered if source rises above or below the specified temperature</source>
        <translation>&lt;b&gt;Κατασταση&lt;/b&gt;συναγερμος ανταποκριναταιι εαν η πηγη ειναι πανω η κατω απο ειδικη θερμοκρασια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31044"/>
        <source>&lt;b&gt;Temp:&lt;/b&gt; the speficied temperature limit</source>
        <translation>&lt;b&gt;Θερμοκρασια&lt;/b&gt;Προκαθορισμενο οριο θερμοκρασιας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31045"/>
        <source>&lt;b&gt;Action:&lt;/b&gt; if all conditions are fulfilled the alarm triggeres the corresponding action</source>
        <translation>&lt;b&gt;Ενεργεια&lt;/b&gt;Εαν εχουν επιτευχθει ολα τα ζητουμενα οι συναγερμοι ανταποκρινονται σε δεδομενη θερμοκρασια</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="22506"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button (if 0 the COOL event is triggered ) or the new value of the slider</source>
        <translation type="obsolete">&lt;b&gt;Περιγραφη&lt;/b&gt;κειμενο.ονομα προγραμματος,αριθμος πληκτρου συμβαντος η νεα τιμη σλαιντερ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31047"/>
        <source>&lt;b&gt;NOTE:&lt;/b&gt; each alarm is only triggered once</source>
        <translation>&lt;b&gt;ΣΗΜΕΙΩΣΗ&lt;/b&gt;καθε συναγερμος ανταποκρινεται απαξ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33957"/>
        <source>OFF</source>
        <translation>ΑΝΕΝΕΡΓΟ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33950"/>
        <source>CONTINUOUS CONTROL</source>
        <translation>ΣΥΝΕΧΗΣ ΕΛΕΓΧΟΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33963"/>
        <source>ON</source>
        <translation>ΕΝΕΡΓΟ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33962"/>
        <source>STANDBY MODE</source>
        <translation>ΚΑΤΑΣΤΑΣΗ STANDBY</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31999"/>
        <source>The rampsoak-mode tells how to start and end the ramp/soak</source>
        <translation>Θεση γραφηματος καθοριζει πως αρχιζει και τελειωνει το γραφημα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32000"/>
        <source>Your rampsoak mode in this pid is:</source>
        <translation>Η κατασταση γραφηματος σε αυτο το pid ειναι:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32001"/>
        <source>Mode = {0}</source>
        <translation type="unfinished">Κατασταση = {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32003"/>
        <source>Start to run from PV value: {0}</source>
        <translation type="unfinished">Εναρξη απο τιμη PV: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32004"/>
        <source>End output status at the end of ramp/soak: {0}</source>
        <translation type="unfinished">Τελος θεσης εξοδου στη ληξη ραμπας: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32005"/>
        <source>Output status while ramp/soak operation set to OFF: {0}</source>
        <translation type="unfinished">Θεση εξοδου οταν λειτουργια ραμπασ εχει καθοριστει ΑΝΕΝΕΡΓΗ: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32006"/>
        <source>
Repeat Operation at the end: {0}</source>
        <translation type="unfinished">Επαναληψη Λειτουργιας στη ληξη:{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32008"/>
        <source>Recomended Mode = 0</source>
        <translation>Συνιστωμενι θεση = 0</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32009"/>
        <source>If you need to change it, change it now and come back later</source>
        <translation>Εαν χριζει αλλαγης,αλλαξτε το τωρα και επανελθετε αργοτερα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32010"/>
        <source>Use the Parameter Loader Software by Fuji if you need to

</source>
        <translation>Χρησιμοποιηστε προγραμμα φορτωσης παραμετρων της Fuji</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32011"/>
        <source>Continue?</source>
        <translation>Συνεχεια?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31807"/>
        <source>RampSoak Mode</source>
        <translation type="obsolete">Θεση Ραμπας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33367"/>
        <source>Current sv = {0}. Change now to sv = {1}?</source>
        <translation type="unfinished">Τρεχων sv = {0}.Αλλαξτε τωρα σε sv = {1}?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33429"/>
        <source>Change svN</source>
        <translation>Αλλαξτε svN</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33428"/>
        <source>Current pid = {0}. Change now to pid ={1}?</source>
        <translation type="unfinished">Τρεχων pid = {0}.Αλλαξτε τωρα σε pid = {1}?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34183"/>
        <source>Ramp Soak start-end mode</source>
        <translation>Θεση εναρξης-Ληξης ραμπας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34089"/>
        <source>Pattern changed to {0}</source>
        <translation type="unfinished">Προτυπο αλλαχτηκε σε {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34091"/>
        <source>Pattern did not changed</source>
        <translation>Προτυπο δεν αλλαξε</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34094"/>
        <source>Ramp/Soak was found ON! Turn it off before changing the pattern</source>
        <translation>Ενεργη Ραμπα!Απενεργοποιηστε πριν την αλλαγη προτυπου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34096"/>
        <source>Ramp/Soak was found in Hold! Turn it off before changing the pattern</source>
        <translation> Ραμπα σε αναμονη!Απενεργοποιηστε πριν την αλλαγη προτυπου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34439"/>
        <source>Activate PID front buttons</source>
        <translation>Ενεργοποιηστε εμπροσθια πληκτρα PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34439"/>
        <source>Remember SV memory has a finite
life of ~10,000 writes.

Proceed?</source>
        <translation>Θυμηθειτε οτι η μνημη SV εχει διαρκεια ~10,000 εγγραφες.Συνεχεια?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34546"/>
        <source>RS ON</source>
        <translation>RS ΕΝΕΡΓΟ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34548"/>
        <source>RS OFF</source>
        <translation>RS ΑΝΕΝΕΡΓΟ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34550"/>
        <source>RS on HOLD</source>
        <translation>RS σε ΑΝΑΜΟΝΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34611"/>
        <source>PXG sv#{0} set to {1}</source>
        <translation type="unfinished">PXG sv#{0} καθοριστηκε σε {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34632"/>
        <source>PXR sv set to {0}</source>
        <translation type="unfinished">PXR sv καθοριστηκε σε {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34667"/>
        <source>SV{0} changed from {1} to {2})</source>
        <translation type="unfinished">SV{0} αλλαξε απο {1} σε {2})</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34675"/>
        <source>Unable to set sv{0}</source>
        <translation type="unfinished">Αδυνατος καθορισμος sv{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34686"/>
        <source>SV changed from {0} to {1}</source>
        <translation type="unfinished">SV αλλαξε απο {0} σε {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34694"/>
        <source>Unable to set sv</source>
        <translation>Aδυνατος καθορισμος sv</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34696"/>
        <source>Unable to set new sv</source>
        <translation>Aδυνατος καθορισμος νεου sv</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10438"/>
        <source>&lt;b&gt;[f]&lt;/b&gt; = Full Screen Mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15834"/>
        <source>Save Graph as PDF</source>
        <translation>Αποθυκευση Γραφικου ως PDF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23881"/>
        <source>A temperature of 145.2C is often sent as 1452.</source>
        <translation type="obsolete">Θερμοκρασια των 145.2 κ συχνακις εμφανιζεται ως 1452.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5734"/>
        <source>Importing a profile in to Designer will decimate all data except the main [points].
Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10429"/>
        <source>&lt;b&gt;[d]&lt;/b&gt; = Toggle xy scale (T/Delta)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15123"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15108"/>
        <source>Oversampling is only active with a sampling interval equal or larger than 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15123"/>
        <source>A tight sampling interval might lead to instability on some machines. We suggest a minimum of 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16093"/>
        <source>current background ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16094"/>
        <source>current background BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25514"/>
        <source>Phidget Temperature Sensor 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25716"/>
        <source>Phidget Bridge 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25829"/>
        <source>Phidget 1018 IO attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31039"/>
        <source>&lt;b&gt;But Not:&lt;/b&gt; alarm triggered only if the alarm with the given number was not triggered before. Use 0 for no guard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31046"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button or the new value of the slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32966"/>
        <source>Load PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33050"/>
        <source>Save PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35177"/>
        <source>Load Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35196"/>
        <source>Save Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35402"/>
        <source>PID turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35416"/>
        <source>PID turned off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10437"/>
        <source>&lt;b&gt;[q,w,e,r + &lt;i&gt;nn&lt;/i&gt;]&lt;/b&gt; = Quick Custom Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16077"/>
        <source>Return the minimum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16078"/>
        <source>Return the maximum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27418"/>
        <source>The MODBUS device corresponds to input channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27419"/>
        <source>1 and 2.. The MODBUS_34 extra device adds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27420"/>
        <source>input channels 3 and 4. Inputs with slave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27421"/>
        <source>id set to 0 are turned off. Modbus function 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27422"/>
        <source>&apos;read holding register&apos; is the standard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27423"/>
        <source>Modbus function 4 triggers the use of &apos;read </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27426"/>
        <source>2 byte integer registers. A temperature of 145.2C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27427"/>
        <source>is often sent as 1452. In that case you have to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27428"/>
        <source>use the symbolic assignment &apos;x/10&apos;. Few devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27429"/>
        <source>hold data as 4 byte floats in two registers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25962"/>
        <source>Yocto Thermocouple attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25964"/>
        <source>Yocto PT100 attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25419"/>
        <source>Phidget Temperature Sensor IR attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27424"/>
        <source>input register&apos;. Input registers (fct 4) usually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27425"/>
        <source> are from 30000-39999. Most devices hold data in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22261"/>
        <source>IO Command: set(n,0), set(n,1), toggle(n) to set Phidget IO digital output n</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16086"/>
        <source>ET value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16087"/>
        <source>BT value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16088"/>
        <source>Extra #1 T1 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16089"/>
        <source>Extra #1 T2 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16090"/>
        <source>Extra #2 T1 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16091"/>
        <source>Extra #2 T2 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16097"/>
        <source>Yn holds values sampled in the actual interval if refering to ET/BT or extra channels from devices listed before, otherwise Yn hold values sampled in the previous interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22259"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) or wcoils(slaveId,register,[&amp;lt;bool&amp;gt;,..,&amp;lt;bool&amp;gt;]) writes values to the registers in slaves specified by the given ids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22253"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-100) to be recorded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25423"/>
        <source>Phidget Temperature Sensor IR detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25518"/>
        <source>Phidget Temperature Sensor 4-input detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21461"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) or wcoils(slaveId,register,[&amp;lt;bool&amp;gt;,..,&amp;lt;bool&amp;gt;]) or wcoils(slaveId,register,&amp;lt;bool&amp;gt;) writes values to the registers in slaves specified by the given ids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25720"/>
        <source>Phidget Bridge 4-input detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25833"/>
        <source>Phidget 1018 IO detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15196"/>
        <source>Hottop control turned off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15207"/>
        <source>Hottop control turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15308"/>
        <source>Settings loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15368"/>
        <source>artisan-settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15369"/>
        <source>Save Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15372"/>
        <source>Settings saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1962"/>
        <source>Alarm {0} triggered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2008"/>
        <source>Calling alarm failed on {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4479"/>
        <source>[TP] recorded at {0} BT = {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17637"/>
        <source>[ET target 1 = {0}] [BT target 1 = {1}] [ET target 2 = {2}] [BT target 2 = {3}]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29162"/>
        <source>Device set to {0}. Now, chose Modbus serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15220"/>
        <source>PID Standby ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15225"/>
        <source>PID Standby OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19188"/>
        <source>Alarms from events #{0} created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19190"/>
        <source>No event selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19257"/>
        <source> Events #{0} deleted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Radio Button</name>
    <message>
        <location filename="../artisanlib/main.py" line="27932"/>
        <source>Meter</source>
        <translation>Μετρητης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27933"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20312"/>
        <source>Arduino TC4</source>
        <translation type="obsolete">Arduino TC4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27935"/>
        <source>Program</source>
        <translation>Προγραμμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27934"/>
        <source>TC4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Annotation</name>
    <message>
        <location filename="../artisanlib/main.py" line="876"/>
        <source>Speed</source>
        <translation>Ταχυτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="877"/>
        <source>Heater</source>
        <translation>Θερμαστρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="878"/>
        <source>Damper</source>
        <translation>Κλαπετο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="879"/>
        <source>Fan</source>
        <translation>Ανεμιστηρας</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3002"/>
        <source>START 00:00</source>
        <translation type="obsolete">ΕΝΑΡΞΗ 00:00</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4510"/>
        <source>DE {0}</source>
        <translation type="unfinished">DE{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4566"/>
        <source>FCs {0}</source>
        <translation type="unfinished">FCε{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4626"/>
        <source>FCe {0}</source>
        <translation type="unfinished">FCλ{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4679"/>
        <source>SCs {0}</source>
        <translation type="unfinished">SCε{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4737"/>
        <source>SCe {0}</source>
        <translation type="unfinished">SCλ{0}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3340"/>
        <source>END {0}</source>
        <translation type="obsolete">ΛΗΞΗ{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4912"/>
        <source>CE {0}</source>
        <translation type="unfinished">ΛΨ{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4410"/>
        <source>CHARGE 00:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2822"/>
        <source>CHARGE</source>
        <translation type="unfinished">ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4471"/>
        <source>TP {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4799"/>
        <source>DROP {0}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Title</name>
    <message>
        <location filename="../artisanlib/main.py" line="11685"/>
        <source>Roaster Scope</source>
        <translation>Καταγραφεας</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../artisanlib/main.py" line="23085"/>
        <source>Playback Aid set OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32349"/>
        <source>Ready</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33140"/>
        <source>Decimal position successfully set to 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33143"/>
        <source>Problem setting decimal position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33194"/>
        <source>Problem setting thermocouple type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34173"/>
        <source>setting autotune...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34200"/>
        <source>Autotune successfully turned OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34203"/>
        <source>Autotune successfully turned ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34101"/>
        <source>wait...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31844"/>
        <source>PID OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31847"/>
        <source>PID ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31882"/>
        <source>Empty SV box</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31891"/>
        <source>Unable to read SV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33996"/>
        <source>Ramp/Soak operation cancelled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33999"/>
        <source>No RX data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34037"/>
        <source>RS ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34011"/>
        <source>Need to change pattern mode...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34020"/>
        <source>Pattern has been changed. Wait 5 secs.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34023"/>
        <source>Pattern could not be changed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34054"/>
        <source>RampSoak could not be changed</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34057"/>
        <source>RS OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34067"/>
        <source>RS successfully turned OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32109"/>
        <source>setONOFFrampsoak(): Ramp Soak could not be set OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32135"/>
        <source>getsegment(): problem reading ramp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32148"/>
        <source>getsegment(): problem reading soak</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34169"/>
        <source>Finished reading Ramp/Soak val.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32204"/>
        <source>Finished reading pid values</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34280"/>
        <source>Ramp/Soak successfully written</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33103"/>
        <source>Time Units successfully set to MM:SS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33106"/>
        <source>Problem setting time units</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33191"/>
        <source>Thermocouple type successfully set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33388"/>
        <source>Problem setting SV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33390"/>
        <source>Cancelled svN change</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33411"/>
        <source>setNsv(): bad response</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33448"/>
        <source>setNpid(): bad confirmation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33452"/>
        <source>Cancelled pid change</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33558"/>
        <source>setsv(): Unable to set SV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33762"/>
        <source>getallpid(): Unable to read pid values</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33793"/>
        <source>getallpid(): Unable to read current sv</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34070"/>
        <source>Ramp Soak could not be set OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34111"/>
        <source>PID set to OFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34114"/>
        <source>PID set to ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34117"/>
        <source>Unable</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34121"/>
        <source>No data received</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34166"/>
        <source>problem reading Ramp/Soak</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34186"/>
        <source>Autotune cancelled</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34205"/>
        <source>UNABLE to set Autotune</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>SV</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>Ramp (MM:SS)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>Soak (MM:SS)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35278"/>
        <source>Work in Progress</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31871"/>
        <source>SV successfully set to {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34161"/>
        <source>Reading Ramp/Soak {0} ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32247"/>
        <source>{0} successfully sent to pid </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32256"/>
        <source>setpid(): There was a problem setting {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33384"/>
        <source>SV{0} set to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33408"/>
        <source>PID already using sv{0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33445"/>
        <source>pid changed to {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33470"/>
        <source>PID was already using pid {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33473"/>
        <source>setNpid(): Unable to set pid {0} </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33550"/>
        <source>SV{0} successfully set to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33676"/>
        <source>pid #{0} successfully set to ({1},{2},{3})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33684"/>
        <source>pid command failed. Bad data at pid{0} (8,8,8): ({1},{2},{3}) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33694"/>
        <source>sending commands for p{0} i{1} d{2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33790"/>
        <source>PID is using pid = {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33857"/>
        <source>PID is using SV = {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34182"/>
        <source>Current pid = {0}. Proceed with autotune command?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tab</name>
    <message>
        <location filename="../artisanlib/main.py" line="16986"/>
        <source>HUD</source>
        <translation>ΠΡΟΒΟΛΕΑΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16989"/>
        <source>Plotter</source>
        <translation>ΠΛΟΤΕΡ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16992"/>
        <source>Math</source>
        <translation>Μαθηματικα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16995"/>
        <source>UI</source>
        <translation>UI</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35295"/>
        <source>General</source>
        <translation>Γενικα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18716"/>
        <source>Notes</source>
        <translation>Σημειωσεις</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23055"/>
        <source>Events</source>
        <translation>Συμβαντα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23058"/>
        <source>Data</source>
        <translation>Πληροφορια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23052"/>
        <source>Config</source>
        <translation>Διαμορφωση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21433"/>
        <source>Buttons</source>
        <translation>Πληκτρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21436"/>
        <source>Sliders</source>
        <translation>Σλαιντερς</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21442"/>
        <source>Palettes</source>
        <translation>Προτυπα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21448"/>
        <source>Style</source>
        <translation>Στυλ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28457"/>
        <source>ET/BT</source>
        <translation>ΕΤ/ΒΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32957"/>
        <source>Extra</source>
        <translation>Εξτρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27721"/>
        <source>Modbus</source>
        <translation>Modbus</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27724"/>
        <source>Scale</source>
        <translation>Κλιμακα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28460"/>
        <source>Extra Devices</source>
        <translation>Εξτρα Συσκαυες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28463"/>
        <source>Symb ET/BT</source>
        <translation>Συμβ ΕΤ/ΒΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29809"/>
        <source>Graph</source>
        <translation>Γραφημα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29812"/>
        <source>LCDs</source>
        <translation>LCDς</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32945"/>
        <source>RS</source>
        <translation>RS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32948"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35165"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32954"/>
        <source>Set RS</source>
        <translation>Δημ RS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21439"/>
        <source>Quantifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27727"/>
        <source>Color</source>
        <translation type="unfinished">Χρωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35169"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28466"/>
        <source>Phidgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22430"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22434"/>
        <source>Espresso</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Table</name>
    <message>
        <location filename="artisanlib/main.py" line="17041"/>
        <source>Abs Time</source>
        <translation type="obsolete">Χρονος Abs</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17041"/>
        <source>Rel Time</source>
        <translation type="obsolete">Χρονος Rel</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17041"/>
        <source>DeltaET (d/m)</source>
        <translation type="obsolete">DeltaET(d/m)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17041"/>
        <source>DeltaBT (d/m)</source>
        <translation type="obsolete">DeltaBT(d/m)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17079"/>
        <source>{0} START</source>
        <translation type="obsolete">{0}ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17083"/>
        <source>{0} DRY END</source>
        <translation type="obsolete">{0}ΛΗΞΗ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17088"/>
        <source>{0} FC START</source>
        <translation type="obsolete">{0} ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17092"/>
        <source>{0} FC END</source>
        <translation type="obsolete">{0} ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17096"/>
        <source>{0} SC START</source>
        <translation type="obsolete">{0} ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17100"/>
        <source>{0} SC END</source>
        <translation type="obsolete">{0} ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17104"/>
        <source>{0} END</source>
        <translation type="obsolete">{0} ΛΗΞΗ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17109"/>
        <source>{0} EVENT #{1} {2}{3}</source>
        <translation type="obsolete">{0} ΣΥΜΒΑΝ #{1} {2}{3}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Time</source>
        <translation>Χρονος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Description</source>
        <translation>Περιγραφη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23212"/>
        <source>Type</source>
        <translation>Τυπος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23212"/>
        <source>Value</source>
        <translation>Τιμη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Label</source>
        <translation>Επιγραφη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Action</source>
        <translation>Ενεργεια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Documentation</source>
        <translation>Τεκμηριωση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Visibility</source>
        <translation>Ορατοτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Color</source>
        <translation>Χρωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Text Color</source>
        <translation>Χρωμα Κειμενου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Device</source>
        <translation>Συσκευη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Comm Port</source>
        <translation>Θυρα Επικοινωνιας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Baud Rate</source>
        <translation>Τιμη Baud</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Byte Size</source>
        <translation>Mεγεθος Byte</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Parity</source>
        <translation>Ισοτιμια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Stopbits</source>
        <translation>Stopbits</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Timeout</source>
        <translation>Χρονικη Ληξη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Color 1</source>
        <translation>Χρωμα1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Color 2</source>
        <translation>Χρωμα2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Label 1</source>
        <translation>Επιγραφη1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Label 2</source>
        <translation>Επιγραφη2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>y1(x)</source>
        <translation>y1(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>y2(x)</source>
        <translation>y2(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>LCD 1</source>
        <translation>LCD1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>LCD 2</source>
        <translation>LCD2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Curve 1</source>
        <translation>Καμπυλη1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Curve 2</source>
        <translation>Καμπυλη2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Parent</source>
        <translation>Γονικος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Width</source>
        <translation>Πλατος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Opaqueness</source>
        <translation>Διαφανεια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Delete Wheel</source>
        <translation>Διαγραφη Ροδας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Edit Labels</source>
        <translation>Επεξεργασια  Επιγραφων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Update Labels</source>
        <translation>Ενημερωση Επιγραφων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Properties</source>
        <translation>Ιδιοτητες</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Radius</source>
        <translation>Ακτινα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Starting angle</source>
        <translation>Γωνια Εναρξης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Projection</source>
        <translation>Προβολη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Text Size</source>
        <translation>Μεγεθος Κειμενου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Color Pattern</source>
        <translation>Προτυπο Χρωματων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Status</source>
        <translation>Θεση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>If Alarm</source>
        <translation>Εαν συναγερμος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>From</source>
        <translation>Απο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Source</source>
        <translation>Πηγη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Condition</source>
        <translation>Κατασταση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Temp</source>
        <translation>Θερμοκρασια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>Ramp HH:MM</source>
        <translation>Γραφημα Ramp ΩΩ:ΛΛ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>Soak HH:MM</source>
        <translation>Γραφημα Soak ΩΩ:ΛΛ</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13761"/>
        <source>START</source>
        <translation type="obsolete">ΕΝΑΡΞΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23301"/>
        <source>DRY END</source>
        <translation>ΛΗΞΗ ΞΥΡΑΝΣΗΣ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23304"/>
        <source>FC START</source>
        <translation>ΕΝΑΡΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23307"/>
        <source>FC END</source>
        <translation>ΛΗΞΗ FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23310"/>
        <source>SC START</source>
        <translation>ΕΝΑΡΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23313"/>
        <source>SC END</source>
        <translation>ΛΗΞΗ SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23319"/>
        <source>COOL</source>
        <translation>ΨΥΞΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19450"/>
        <source>EVENT #{1} {2}{3}</source>
        <translation type="obsolete">ΣΥΜΒΑΝ #{1} {2}{3}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23316"/>
        <source>DROP</source>
        <translation>ΞΕΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>DeltaET</source>
        <translation type="unfinished">DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>DeltaBT</source>
        <translation type="unfinished">DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23298"/>
        <source>CHARGE</source>
        <translation type="unfinished">ΦΟΡΤΩΜΑ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>But Not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Beep</source>
        <translation type="unfinished">Beep</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19680"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19680"/>
        <source>Weight</source>
        <translation type="unfinished">Βαρος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Nr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23323"/>
        <source>EVENT #{0} {1}{2}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Textbox</name>
    <message>
        <location filename="../artisanlib/main.py" line="560"/>
        <source>Acidity</source>
        <translation>Οξυτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="546"/>
        <source>Aftertaste</source>
        <translation>Μεταγευση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="518"/>
        <source>Clean Cup</source>
        <translation>Καθαριοτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="481"/>
        <source>Head</source>
        <translation>Κεφαλη</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="558"/>
        <source>Fragrance</source>
        <translation>Ευωδια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="550"/>
        <source>Sweetness</source>
        <translation>Γλυκητητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="542"/>
        <source>Aroma</source>
        <translation>Αρωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="565"/>
        <source>Balance</source>
        <translation>Ισορροπια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="559"/>
        <source>Body</source>
        <translation>Σωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="491"/>
        <source>Sour</source>
        <translation>Ξυνιλα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="536"/>
        <source>Flavor</source>
        <translation>Γευση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="493"/>
        <source>Critical
Stimulus</source>
        <translation>Κρισιμο κινητρο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="495"/>
        <source>Bitter</source>
        <translation>Πικρα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="496"/>
        <source>Astringency</source>
        <translation>Στυπτικοτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="497"/>
        <source>Solubles
Concentration</source>
        <translation>Περιεκτηκοτητα διαλυματων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="544"/>
        <source>Mouthfeel</source>
        <translation>Στο στομα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="499"/>
        <source>Other</source>
        <translation>Αλλα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="500"/>
        <source>Aromatic
Complexity</source>
        <translation>Αρωματικη Πολυπλοκοτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="501"/>
        <source>Roast
Color</source>
        <translation>Χρωμα Ψησιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="502"/>
        <source>Aromatic
Pungency</source>
        <translation>Αρωματικη Οξυτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="503"/>
        <source>Sweet</source>
        <translation>Γλυκα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="505"/>
        <source>pH</source>
        <translation>ΡΗ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="508"/>
        <source>Fragance</source>
        <translation>Ευωδια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="515"/>
        <source>Dry Fragrance</source>
        <translation>Ξηρα Ευωδια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="516"/>
        <source>Uniformity</source>
        <translation>Ομοιομορφια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="517"/>
        <source>Complexity</source>
        <translation>Πολυπλοκοτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="553"/>
        <source>Finish</source>
        <translation>Τελειωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="523"/>
        <source>Brightness</source>
        <translation>Λαμπεροτητα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="524"/>
        <source>Wet Aroma</source>
        <translation>Υγρο Αρωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="528"/>
        <source>Taste</source>
        <translation>Γευση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="529"/>
        <source>Nose</source>
        <translation>Οσφρηση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="534"/>
        <source>Fragrance-Aroma</source>
        <translation>Ευωδια-Αρωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="545"/>
        <source>Flavour</source>
        <translation>Γευση</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="555"/>
        <source>Roast Color</source>
        <translation>Χρωμα Ψησιματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="556"/>
        <source>Crema Texture</source>
        <translation>Υφη Κρεμας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="557"/>
        <source>Crema Volume</source>
        <translation>Ογκος Κρεμας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="561"/>
        <source>Bitterness</source>
        <translation>Πικριλα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="562"/>
        <source>Defects</source>
        <translation>Ελαττωματα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="563"/>
        <source>Aroma Intensity</source>
        <translation>Ενταση Αρωματος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="564"/>
        <source>Aroma Persistence</source>
        <translation>Πιστοτητα Αρωματος</translation>
    </message>
</context>
<context>
    <name>Tooltip</name>
    <message>
        <location filename="../artisanlib/main.py" line="4158"/>
        <source>Stop monitoring</source>
        <translation>Παυση παρακολουθησης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8267"/>
        <source>Start monitoring</source>
        <translation>Εναρξηπαρακολουθησης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4314"/>
        <source>Stop recording</source>
        <translation>Παυση καταγραφης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8280"/>
        <source>Start recording</source>
        <translation>Εναρξη καταγραφης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8214"/>
        <source>Marks the begining of First Crack (FCs)</source>
        <translation type="obsolete">Μαρκαρει εναρξη FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8221"/>
        <source>Marks the end of First Crack (FCs)</source>
        <translation type="obsolete">Μαρκαρει ληξη FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8228"/>
        <source>Marks the begining of Second Crack (SCs)</source>
        <translation type="obsolete">Μαρκαρει εναρξη SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8235"/>
        <source>Marks the end of Second Crack (SCe)</source>
        <translation type="obsolete">Μαρκαρει ληξη SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8329"/>
        <source>Reset</source>
        <translation>Επαναφορα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8256"/>
        <source>Marks the begining of the roast (beans in)</source>
        <translation type="obsolete">Μαρκαρει εναρξη ψησιματος/φορτωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8264"/>
        <source>Marks the end of the roast (drop beans)</source>
        <translation type="obsolete">Μαρκαρει ληξη ψησιματος.ξεφορτωμα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8279"/>
        <source>Marks an Event</source>
        <translation type="obsolete">Μαρκαρει συμβαν</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8369"/>
        <source>Increases the current SV value by 5</source>
        <translation>Αυξανει τιμη SV κατα 5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8377"/>
        <source>Increases the current SV value by 10</source>
        <translation>Αυξανει τιμη SV κατα 10</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8385"/>
        <source>Increases the current SV value by 20</source>
        <translation>Αυξανει τιμη SV κατα 20</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8393"/>
        <source>Decreases the current SV value by 20</source>
        <translation>Μειωνει τιμη SV κατα 20</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8401"/>
        <source>Decreases the current SV value by 10</source>
        <translation>Μειωνει τιμη SV κατα 10</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8409"/>
        <source>Decreases the current SV value by 5</source>
        <translation>Μειωνει τιμη SV κατα 5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8423"/>
        <source>Turns ON/OFF the HUD</source>
        <translation>Ανοιγει/Κλεινει Προβολεα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8351"/>
        <source>Marks the end of the Drying phase (DRYEND)</source>
        <translation type="obsolete">Μαρκαρει ληξη περιοδου ξηρανσης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8359"/>
        <source>Marks the end of the Cooling phase (COOLEND)</source>
        <translation type="obsolete">Μαρκαρει ληξη περιοδου ψυξης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8494"/>
        <source>Timer</source>
        <translation>Χρονομετρο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8495"/>
        <source>ET Temperature</source>
        <translation>Θερμοκρασια ΕΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8496"/>
        <source>BT Temperature</source>
        <translation>Θερμοκρασια ΒΤ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8497"/>
        <source>ET/time (degrees/min)</source>
        <translation>ΕΤ/Χρονος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8498"/>
        <source>BT/time (degrees/min)</source>
        <translation>ΒΤ/Χρονος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8499"/>
        <source>Value of SV in PID</source>
        <translation>Τιμη SV στο PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8500"/>
        <source>PID power %</source>
        <translation>Δυναμη PID%</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8579"/>
        <source>Number of events found</source>
        <translation>αΡΙΘΜΟς ΣΥΜΒΑΝΤΩΝ</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8589"/>
        <source>Type of event</source>
        <translation>Τυπος συμβαντος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8595"/>
        <source>Value of event</source>
        <translation>Τιμη συμβαντος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8607"/>
        <source>Updates the event</source>
        <translation>Ενημερωνει συμβαντα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16155"/>
        <source>&lt;b&gt;Label&lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16156"/>
        <source>&lt;b&gt;Description &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Περιγραφη &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16158"/>
        <source>&lt;b&gt;Type &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16159"/>
        <source>&lt;b&gt;Value &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16160"/>
        <source>&lt;b&gt;Documentation &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16161"/>
        <source>&lt;b&gt;Button# &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30219"/>
        <source>Save image using current graph size to a png format</source>
        <translation>Αποθυκευση φραφηματος σε μορφη png</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16783"/>
        <source>linear: linear interpolation
cubic: 3rd order spline interpolation
nearest: y value of the nearest point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19822"/>
        <source>ON/OFF logs serial communication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19912"/>
        <source>Automatic generated name = This text + date + time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19915"/>
        <source>ON/OFF of automatic saving when pressing keyboard letter [a]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19926"/>
        <source>Sets the directory to store batch profiles when using the letter [a]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20618"/>
        <source>Allows to enter a description of the last event</source>
        <translation>Επιτρεπει εισαγωγη περιγραφης τελευταιου συμβαντος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20837"/>
        <source>Add new extra Event button</source>
        <translation>Προσθηκη νεου πληκτρου συμβαντος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20842"/>
        <source>Delete the last extra Event button</source>
        <translation>Διαγραφη τελευταιου πληκτρου συμβαντος</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30760"/>
        <source>Show help</source>
        <translation>Εμφανιση βοηθειας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20880"/>
        <source>Backup all palettes to a text file</source>
        <translation>Backup ολων προτυπων σε μορφη κειμενου</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21212"/>
        <source>Action Type</source>
        <translation>Τυπος ενεργειας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21217"/>
        <source>Action String</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30172"/>
        <source>Aspect Ratio</source>
        <translation>Αναλογια</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28598"/>
        <source>Example: 100 + 2*x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28599"/>
        <source>Example: 100 + x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30161"/>
        <source>Erases wheel parent hierarchy</source>
        <translation>Διαγραφη ιεραρχιας ροδας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30165"/>
        <source>Sets graph hierarchy child-&gt;parent instead of parent-&gt;child</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30179"/>
        <source>Increase size of text in all the graph</source>
        <translation>Αυξανει μεγεθος κειμενων γραφηματων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30182"/>
        <source>Decrease size of text in all the graph</source>
        <translation>Μειωνει μεγεθος κειμενων γραφηματων</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30186"/>
        <source>Decorative edge beween wheels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30192"/>
        <source>Line thickness</source>
        <translation>Παχος γραμμης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30197"/>
        <source>Line color</source>
        <translation>Χρωμα γραμμης</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30201"/>
        <source>Apply color pattern to whole graph</source>
        <translation>Εφαρμογη προτυπυ χρωματων το γραφημα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30207"/>
        <source>Add new wheel</source>
        <translation>Προσθηκη ροδας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30210"/>
        <source>Rotate graph 1 degree counter clockwise</source>
        <translation>Περιστροφη γραφηματος δεξιοστροφα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30213"/>
        <source>Rotate graph 1 degree clockwise</source>
        <translation>Περιστροφη γραφηματος αριστεροστροφα</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30217"/>
        <source>Save graph to a text file.wg</source>
        <translation>Αποθυκευση γραφηματος σε μορφη κειμενου.wg</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30222"/>
        <source>Sets Wheel graph to view mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30225"/>
        <source>open graph file.wg</source>
        <translation>Ανοιγμα γραφηματος  απο wg</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30228"/>
        <source>Close wheel graph editor</source>
        <translation>Κλεισιμο διαμορφωσης ροδας</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20881"/>
        <source>Restore all palettes from a text file</source>
        <translation>Επαναφορα προτυπων απο κειμενο</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30765"/>
        <source>Clear alarms table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21219"/>
        <source>Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19974"/>
        <source>Batch prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19989"/>
        <source>ON/OFF batch counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8295"/>
        <source>First Crack Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8302"/>
        <source>First Crack End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8309"/>
        <source>Second Crack Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8316"/>
        <source>Second Crack End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8337"/>
        <source>Charge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8345"/>
        <source>Drop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8360"/>
        <source>Event</source>
        <translation type="unfinished">Συμβαν</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8432"/>
        <source>Dry End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8440"/>
        <source>Cool End</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
