<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="es" sourcelanguage="">
<context>
    <name>About</name>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="11059"/>
        <source>Version:</source>
        <translation type="obsolete">Version:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>Core developers:</source>
        <translation>Programadores principales:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>Contributors:</source>
        <translation>Contribuidores:</translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="../artisanlib/main.py" line="30475"/>
        <source>Update</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35151"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32672"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16792"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30736"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30745"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19923"/>
        <source>Path</source>
        <translation>Camino</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29602"/>
        <source>Defaults</source>
        <translation>Predeterminados</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30218"/>
        <source>Save Img</source>
        <translation>Guard. Imag</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35128"/>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22861"/>
        <source>Align</source>
        <translation>Alinear</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16727"/>
        <source>Plot</source>
        <translation>Trazar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31048"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28050"/>
        <source>Reset</source>
        <translation>Reiniciar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31356"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26765"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20700"/>
        <source>Scan for Ports</source>
        <translation type="obsolete">Buscar Puertos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29647"/>
        <source>Background</source>
        <translation>Fondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29491"/>
        <source>Grid</source>
        <translation>Cuadrícula</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29498"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29505"/>
        <source>Y Label</source>
        <translation>Etiqueta Y</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29512"/>
        <source>X Label</source>
        <translation>Etiqueta X</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29547"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29554"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29561"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29568"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29575"/>
        <source>Markers</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29582"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29589"/>
        <source>Watermarks</source>
        <translation>Filigranas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29596"/>
        <source>C Lines</source>
        <translation>Lineas C</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29605"/>
        <source>Grey</source>
        <translation>Gris</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29659"/>
        <source>LED</source>
        <translation>LED</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29691"/>
        <source>B/W</source>
        <translation>B/N</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30160"/>
        <source>Reset Parents</source>
        <translation>Reinicializar padres</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30164"/>
        <source>Reverse Hierarchy</source>
        <translation>Invertir Jerarquia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30178"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30181"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30196"/>
        <source>Line Color</source>
        <translation>Color de Linea</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30209"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30212"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30215"/>
        <source>Save File</source>
        <translation>Guardar Fichero</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30224"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30500"/>
        <source>Set Color</source>
        <translation>Ajustar Color</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31351"/>
        <source>Read Ra/So values</source>
        <translation>Leer valores Ra/So</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32413"/>
        <source>RampSoak ON</source>
        <translation>RampSoak ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32415"/>
        <source>RampSoak OFF</source>
        <translation>RampSoak OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32418"/>
        <source>PID OFF</source>
        <translation>PID OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32420"/>
        <source>PID ON</source>
        <translation>PID ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32535"/>
        <source>Write SV</source>
        <translation>Escribir SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31388"/>
        <source>Set p</source>
        <translation>Ajustar p</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31389"/>
        <source>Set i</source>
        <translation>Ajustar i</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31390"/>
        <source>Set d</source>
        <translation>Ajustar d</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32668"/>
        <source>Autotune ON</source>
        <translation>AutoAjuste ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32670"/>
        <source>Autotune OFF</source>
        <translation>AutoAjuste OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35003"/>
        <source>Set</source>
        <translation>Ajustar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32409"/>
        <source>Read RS values</source>
        <translation>Leer valores RS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32471"/>
        <source>Write SV1</source>
        <translation>Escribir SV1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32473"/>
        <source>Write SV2</source>
        <translation>Escribir SV2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32475"/>
        <source>Write SV3</source>
        <translation>Escribir SV3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32477"/>
        <source>Write SV4</source>
        <translation>Escribir SV4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32479"/>
        <source>Write SV5</source>
        <translation>Escribir SV5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32481"/>
        <source>Write SV6</source>
        <translation>Escribir SV6</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32483"/>
        <source>Write SV7</source>
        <translation>Escribir SV7</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32537"/>
        <source>ON SV buttons</source>
        <translation>Botones SV ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32540"/>
        <source>OFF SV buttons</source>
        <translation>Botones SV OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32543"/>
        <source>Read SV (7-0)</source>
        <translation>Leer SV(7-0)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32650"/>
        <source>pid 1</source>
        <translation>pid 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32652"/>
        <source>pid 2</source>
        <translation>pid 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32654"/>
        <source>pid 3</source>
        <translation>pid 3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32656"/>
        <source>pid 4</source>
        <translation>pid 4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32658"/>
        <source>pid 5</source>
        <translation>pid 5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32660"/>
        <source>pid 6</source>
        <translation>pid 6</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32662"/>
        <source>pid 7</source>
        <translation>pid 7</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24951"/>
        <source>Read All</source>
        <translation type="obsolete">Leer Todos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30730"/>
        <source>All On</source>
        <translation>Todos ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30733"/>
        <source>All Off</source>
        <translation>Todos OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35282"/>
        <source>Read</source>
        <translation>Leer</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32798"/>
        <source>Set ET PID to 1 decimal point</source>
        <translation>Ajustar ET PID a 1 décima</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32800"/>
        <source>Set BT PID to 1 decimal point</source>
        <translation>Ajustar BT PID a 1 décima</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32802"/>
        <source>Set ET PID to MM:SS time units</source>
        <translation>Ajustar ET PID a MM:SS unidades de tiempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22621"/>
        <source>Del</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22624"/>
        <source>Save Image</source>
        <translation>Guaradar Imagen</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22932"/>
        <source>Up</source>
        <translation>Arriba</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22934"/>
        <source>Down</source>
        <translation>Abajo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22936"/>
        <source>Left</source>
        <translation>Izquierda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22938"/>
        <source>Right</source>
        <translation>Derecha</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14566"/>
        <source>PID Help</source>
        <translation type="obsolete">PID Ayuda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30221"/>
        <source>View Mode</source>
        <translation>Ver Modo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30477"/>
        <source>Select</source>
        <translation>Seleccionar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19770"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16707"/>
        <source>Virtual Device</source>
        <translation type="obsolete">Dispositivo Virtual</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18154"/>
        <source>Order</source>
        <translation>Ordenar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18465"/>
        <source>in</source>
        <translation>dentro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18470"/>
        <source>out</source>
        <translation>fuera</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35131"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8265"/>
        <source>ON</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8278"/>
        <source>START</source>
        <translation>INICIAR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4157"/>
        <source>OFF</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8291"/>
        <source>FC
START</source>
        <translation>INICIO
FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8298"/>
        <source>FC
END</source>
        <translation>FIN
FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8305"/>
        <source>SC
START</source>
        <translation>INICIO
SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8312"/>
        <source>SC
END</source>
        <translation>FIN
SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8320"/>
        <source>RESET</source>
        <translation>REINICIAR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8333"/>
        <source>CHARGE</source>
        <translation>CARGAR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8341"/>
        <source>DROP</source>
        <translation>DESCENDER</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8349"/>
        <source>Control</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8356"/>
        <source>EVENT</source>
        <translation>EVENTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8364"/>
        <source>SV +5</source>
        <translation>SV +5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8372"/>
        <source>SV +10</source>
        <translation>SV +10</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8380"/>
        <source>SV +20</source>
        <translation>SV +20</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8388"/>
        <source>SV -20</source>
        <translation>SV -20</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8396"/>
        <source>SV -10</source>
        <translation>SV -10</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8404"/>
        <source>SV -5</source>
        <translation>SV -5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8412"/>
        <source>HUD</source>
        <translation>HUD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8427"/>
        <source>DRY
END</source>
        <translation>FIN
SECADO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8436"/>
        <source>COOL
END</source>
        <translation>FIN ENFRIAMIENTO</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20245"/>
        <source>Transfer To</source>
        <translation type="obsolete">Transferir a</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20247"/>
        <source>Restore From</source>
        <translation type="obsolete">Restablecer desde</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31367"/>
        <source>SV Buttons ON</source>
        <translation>Botones SV ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31369"/>
        <source>SV Buttons OFF</source>
        <translation>Botones SV OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31371"/>
        <source>Read SV</source>
        <translation>Leer SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31405"/>
        <source>Read PID Values</source>
        <translation>Leer valores PID </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35283"/>
        <source>Write</source>
        <translation>Escribir</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29519"/>
        <source>Drying Phase</source>
        <translation>Fase de Secado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29526"/>
        <source>Maillard Phase</source>
        <translation>Fase de Maillard</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29533"/>
        <source>Development Phase</source>
        <translation>Fase de Desarrollo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29540"/>
        <source>Cooling Phase</source>
        <translation>Fase de Enfriado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16724"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30740"/>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30764"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18480"/>
        <source>scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32405"/>
        <source>Write All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32411"/>
        <source>Write RS values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32545"/>
        <source>Write SV (7-0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32664"/>
        <source>Read PIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32666"/>
        <source>Write PIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21073"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35153"/>
        <source>On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35155"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18490"/>
        <source>calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17692"/>
        <source>unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20861"/>
        <source>&lt;&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20863"/>
        <source>&gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18149"/>
        <source>Create Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16738"/>
        <source>Device</source>
        <translation type="obsolete">Dispositivo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16733"/>
        <source>BT/ET</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="22847"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22848"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16504"/>
        <source>Projection</source>
        <translation>Proyeccion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16921"/>
        <source>Beep</source>
        <translation>Pitido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22324"/>
        <source>Auto Adjusted</source>
        <translation>Auto Ajustado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22844"/>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22845"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22846"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23358"/>
        <source>Time</source>
        <translation>Tiempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23359"/>
        <source>Bar</source>
        <translation>Barra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23362"/>
        <source>Evaluation</source>
        <translation>Evaluacion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23363"/>
        <source>Characteristics</source>
        <translation>Propiedades</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26565"/>
        <source>DRY END</source>
        <translation>FIN SECADO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26567"/>
        <source>FC START</source>
        <translation>INICIO FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26569"/>
        <source>FC END</source>
        <translation>FIN FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26571"/>
        <source>SC START</source>
        <translation>INICIO SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26573"/>
        <source>SC END</source>
        <translation>FIN SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27906"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20608"/>
        <source>Button</source>
        <translation>Boton</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20617"/>
        <source>Mini Editor</source>
        <translation>Editor Mini</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22629"/>
        <source>Background</source>
        <translation>Fondo</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15733"/>
        <source>Automatic CHARGE/DROP</source>
        <translation type="obsolete">Auto CARGA/DESCENSO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19821"/>
        <source>Serial Log ON/OFF</source>
        <translation>Registro Serie ON/OFF</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20440"/>
        <source>d/m</source>
        <translation type="obsolete">g/m</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18140"/>
        <source>Delete roast properties on RESET</source>
        <translation>Borrar propiedades de tueste en REINICIO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19913"/>
        <source>Autosave [a]</source>
        <translation>Autograbar [s]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21122"/>
        <source>CHARGE</source>
        <translation>CARGAR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21176"/>
        <source>DROP</source>
        <translation>DESCENDER</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21185"/>
        <source>COOL END</source>
        <translation>FIN ENFRIADO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23361"/>
        <source>ETBTa</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27909"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22968"/>
        <source>Playback Aid</source>
        <translation>Ayuda Reproduccion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18413"/>
        <source>Heavy FC</source>
        <translation>FC Fuerte</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18416"/>
        <source>Low FC</source>
        <translation>FC Débil</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18419"/>
        <source>Light Cut</source>
        <translation>Corte Ligero</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18422"/>
        <source>Dark Cut</source>
        <translation>Corte Oscuro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18425"/>
        <source>Drops</source>
        <translation>Descensos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18428"/>
        <source>Oily</source>
        <translation>Aceitoso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18431"/>
        <source>Uneven</source>
        <translation>Irregular</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18433"/>
        <source>Tipping</source>
        <translation>Crítico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18435"/>
        <source>Scorching</source>
        <translation>Abrasador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18437"/>
        <source>Divots</source>
        <translation>Chuletas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16470"/>
        <source>Drop Spikes</source>
        <translation>Picos Descenso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16460"/>
        <source>Smooth Spikes</source>
        <translation>Alisar picos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16475"/>
        <source>Limits</source>
        <translation>Límites</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22327"/>
        <source>Watermarks</source>
        <translation>Filigranas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20123"/>
        <source>Lock Max</source>
        <translation>Max Cierre</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30768"/>
        <source>Load alarms from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20806"/>
        <source>Auto CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20809"/>
        <source>Auto DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20812"/>
        <source>Mark TP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22329"/>
        <source>Phases LCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22331"/>
        <source>Auto DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22333"/>
        <source>Auto FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16594"/>
        <source>Decimal Places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27979"/>
        <source>Modbus Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16465"/>
        <source>Smooth2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35084"/>
        <source>Start PID on CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35139"/>
        <source>Load Ramp/Soak table from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28012"/>
        <source>Control Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28243"/>
        <source>Ratiometric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22849"/>
        <source>Align FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23360"/>
        <source>/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16948"/>
        <source>Alarm Popups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20126"/>
        <source>Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19987"/>
        <source>Batch Counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20611"/>
        <source>Annotations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComboBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1016"/>
        <source>Power</source>
        <translation>Potencia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1017"/>
        <source>Damper</source>
        <translation>Valvula</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1018"/>
        <source>Fan</source>
        <translation>Ventilador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>linear</source>
        <translation>Lineal</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16506"/>
        <source>newton</source>
        <translation>Newton</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17614"/>
        <source>metrics</source>
        <translation>Métrico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17616"/>
        <source>thermal</source>
        <translation>Térmico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>cubic</source>
        <translation>cúbico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>nearest</source>
        <translation>más cercano</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20399"/>
        <source>g</source>
        <translation>g</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20400"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19381"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18317"/>
        <source>l</source>
        <translation>l</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20101"/>
        <source>upper right</source>
        <translation>arriba derecha</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20102"/>
        <source>upper left</source>
        <translation>arriba izquierda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20103"/>
        <source>lower left</source>
        <translation>abajo izquierda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20104"/>
        <source>lower right</source>
        <translation>abajo derecha</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20105"/>
        <source>right</source>
        <translation>derecha</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20106"/>
        <source>center left</source>
        <translation>centro izquierda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20107"/>
        <source>center right</source>
        <translation>centro derecha</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20108"/>
        <source>lower center</source>
        <translation>abajo centro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20109"/>
        <source>upper center</source>
        <translation>arriba centro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20110"/>
        <source>center</source>
        <translation>centro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20379"/>
        <source>Event #0</source>
        <translation>Evento:#0</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20381"/>
        <source>Event #{0}</source>
        <translation type="unfinished">Evento #{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20401"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20422"/>
        <source>liter</source>
        <translation>litro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20423"/>
        <source>gallon</source>
        <translation>galón</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20424"/>
        <source>quart</source>
        <translation>cuarto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20425"/>
        <source>pint</source>
        <translation>pinta</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20426"/>
        <source>cup</source>
        <translation>taza</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20427"/>
        <source>cm^3</source>
        <translation>cm^3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Multiple Event</source>
        <translation>Evento multiple</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29612"/>
        <source>grey</source>
        <translation>gris</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29613"/>
        <source>Dark Grey</source>
        <translation>Gris Oscuro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29614"/>
        <source>Slate Grey</source>
        <translation>Gris Pizarra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29615"/>
        <source>Light Gray</source>
        <translation>Gris Claro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29616"/>
        <source>Black</source>
        <translation>Negro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29617"/>
        <source>White</source>
        <translation>Blanco</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29618"/>
        <source>Transparent</source>
        <translation>Trasparente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Flat</source>
        <translation>Llano</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Perpendicular</source>
        <translation>Perpendicular</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Radial</source>
        <translation>Radial</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>CHARGE</source>
        <translation>CARGA</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>DRY END</source>
        <translation>FIN SECADO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>FC START</source>
        <translation>INICIO FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>FC END</source>
        <translation>FIN FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>SC START</source>
        <translation>INICIO SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>DROP</source>
        <translation>DESCENDER</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>OFF</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>ON</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31120"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31121"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18782"/>
        <source>30 seconds</source>
        <translation type="obsolete">30 segundos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20133"/>
        <source>1 minute</source>
        <translation>1 minuto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20134"/>
        <source>2 minute</source>
        <translation>2 minutos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20135"/>
        <source>3 minute</source>
        <translation>3 minutos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20136"/>
        <source>4 minute</source>
        <translation>4 minutos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20137"/>
        <source>5 minute</source>
        <translation>5 minutos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20163"/>
        <source>solid</source>
        <translation>sólido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20164"/>
        <source>dashed</source>
        <translation>Rayado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20165"/>
        <source>dashed-dot</source>
        <translation>Rayado.punto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20166"/>
        <source>dotted</source>
        <translation>punteado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20623"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20624"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31118"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31119"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>SC END</source>
        <translation>FIN SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Serial Command</source>
        <translation>Comando Serie</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Modbus Command</source>
        <translation>Comando Modbus</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>DTA Command</source>
        <translation>Comando DTA</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Call Program</source>
        <translation>Llamar Programa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>START</source>
        <translation>INICIO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>TP</source>
        <translation>TP</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>COOL</source>
        <translation>ENFRIAR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Event Button</source>
        <translation>Botón Evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Slider</source>
        <translation>Deslizador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31178"/>
        <source>below</source>
        <translation>debajo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31178"/>
        <source>above</source>
        <translation>encima</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Pop Up</source>
        <translation>Salto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26747"/>
        <source>SV Commands</source>
        <translation>Comandos SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26747"/>
        <source>Ramp Commands</source>
        <translation>Comandos Rampa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1015"/>
        <source>Speed</source>
        <translation>Velocidad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27391"/>
        <source>little-endian</source>
        <translation>pequeño-endian</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16660"/>
        <source>classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16660"/>
        <source>xkcd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Default</source>
        <translation type="unfinished">Predeterminado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Humor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>FCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>SCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>SCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>COOL END</source>
        <translation type="unfinished">FIN ENFRIADO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>IO Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Heater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Fan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>RampSoak ON</source>
        <translation type="unfinished">RampSoak ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>RampSoak OFF</source>
        <translation type="unfinished">RampSoak OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>PID ON</source>
        <translation type="unfinished">PID ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>PID OFF</source>
        <translation type="unfinished">PID OFF</translation>
    </message>
</context>
<context>
    <name>Contextual Menu</name>
    <message>
        <location filename="../artisanlib/main.py" line="5936"/>
        <source>Add point</source>
        <translation>Añadir punto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5940"/>
        <source>Remove point</source>
        <translation>Quitar punto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5946"/>
        <source>Reset Designer</source>
        <translation>Reinicializar Diseñador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5950"/>
        <source>Exit Designer</source>
        <translation>Cerrar Diseñador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6530"/>
        <source>Add to Cupping Notes</source>
        <translation>Añadir a notas de Catacion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6534"/>
        <source>Add to Roasting Notes</source>
        <translation>Añadir a notas del Tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6538"/>
        <source>Cancel selection</source>
        <translation>Cancelar seleccion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6546"/>
        <source>Exit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6542"/>
        <source>Edit Mode</source>
        <translation>Modo Editar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5922"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5926"/>
        <source>Config...</source>
        <translation>Configuración...</translation>
    </message>
</context>
<context>
    <name>Directory</name>
    <message>
        <location filename="../artisanlib/main.py" line="15803"/>
        <source>profiles</source>
        <translation>perfiles</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15793"/>
        <source>other</source>
        <translation>otros</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="878"/>
        <source>edit text</source>
        <translation type="obsolete">editar texto</translation>
    </message>
</context>
<context>
    <name>Error Message</name>
    <message>
        <location filename="artisanlib/main.py" line="20841"/>
        <source>HH806AUtemperature(): {0} bytes received but 14 needed</source>
        <translation type="obsolete">HH806AUtemperature(): {0} bytes recibidos pero se necesitan 14</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25044"/>
        <source>HH506RAtemperature(): Unable to get id from HH506RA device </source>
        <translation>HH506RAtemperature():No se pudo conseguir la identificacion del dispositivo HH506RA</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25064"/>
        <source>HH506RAtemperature(): {0} bytes received but 14 needed</source>
        <translation type="unfinished">HH506RAtemperature(): {0} bytes recibidos pero se necesitan 14</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25019"/>
        <source>HH506RAGetID: {0} bytes received but 5 needed</source>
        <translation type="unfinished">HH506RAGetID: {0} recibidos pero se necesitan 5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34814"/>
        <source>Segment values could not be written into PID</source>
        <translation>Valores de segmentos no se pudieron grabar en la PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34552"/>
        <source>RampSoak could not be changed</source>
        <translation>RampSoak no se pudo cambiar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34870"/>
        <source>pid.readoneword(): {0} RX bytes received (7 needed) for unit ID={1}</source>
        <translation type="unfinished">pid.readoneword(): {0} RX bytesrecibidos pero se necesitan 7 en Unidad ID={1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19414"/>
        <source>Unable to move CHARGE to a value that does not exist</source>
        <translation>No se pudo mover CHARGE a un valor que no existe</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24890"/>
        <source>HH806Wtemperature(): Unable to initiate device</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="13930"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21184"/>
        <source>Device error</source>
        <translation type="obsolete">Error dispositivo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26453"/>
        <source>Value Error:</source>
        <translation>Exception Valor:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35213"/>
        <source>Exception:</source>
        <translation>Error:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30654"/>
        <source>IO Error:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23814"/>
        <source>Modbus Error:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26509"/>
        <source>Serial Exception:</source>
        <translation>Exception Serial:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24185"/>
        <source>F80h Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24195"/>
        <source>CRC16 data corruption ERROR. TX does not match RX. Check wiring</source>
        <translation>CRC16 data corrupcion ERROR. TX no coincide con RX. Comprueba los cables de conexion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24198"/>
        <source>No RX data received</source>
        <translation>No RX data recibida</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24650"/>
        <source>Unable to open serial port</source>
        <translation>Puerta serial no se pudo abrir</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25188"/>
        <source>CENTER303temperature(): {0} bytes received but 8 needed</source>
        <translation type="unfinished">CENTER303temperature(): {0} bytes recibidos pero se necesitan 8</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25258"/>
        <source>CENTER306temperature(): {0} bytes received but 10 needed</source>
        <translation type="unfinished">CENTER306temperature(): {0} bytes recibidos pero se necesitan 10</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24326"/>
        <source>DTAcommand(): {0} bytes received but 15 needed</source>
        <translation type="unfinished">DTAtemperature(): {0} bytes recibidos pero se necesitan 15</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25344"/>
        <source>CENTER309temperature(): {0} bytes received but 45 needed</source>
        <translation type="unfinished">CENTER309temperature(): {0} bytes recibidos pero se necesitan 45</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26046"/>
        <source>Arduino could not set channels</source>
        <translation>Ardunino no pudo cambiar canales</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26057"/>
        <source>Arduino could not set temperature unit</source>
        <translation>Arduino no pudo cambiar temperatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25119"/>
        <source>CENTER302temperature(): {0} bytes received but 7 needed</source>
        <translation type="unfinished">CENTER302temperature(): {0} bytes recibidos pero se necesitan 7</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27874"/>
        <source>Serial Exception: invalid comm port</source>
        <translation>Exception Serial: Comm nombre invalido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27879"/>
        <source>Serial Exception: timeout</source>
        <translation>Exception Serial: tiempo de espera</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17261"/>
        <source>Univariate: no profile data available</source>
        <translation>Univariate: no data de perfil disponible</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17432"/>
        <source>Polyfit: no profile data available</source>
        <translation>Polyfit: no data de perfil disponible</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24830"/>
        <source>HH806AUtemperature(): {0} bytes received but 16 needed</source>
        <translation type="unfinished">HH806AUtemperature(): {0} bytes recibidos pero se necesitan 14 {806A?} {1 ?} {16 ?}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10419"/>
        <source>Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26069"/>
        <source>Arduino could not set filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24768"/>
        <source>MS6514temperature(): {0} bytes received but 16 needed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Flavor Scope Label</name>
    <message>
        <location filename="../artisanlib/main.py" line="14811"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14812"/>
        <source>Grassy</source>
        <translation>Herbáceo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14813"/>
        <source>Leathery</source>
        <translation>Cueroso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14814"/>
        <source>Toasty</source>
        <translation>Tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14815"/>
        <source>Bready</source>
        <translation>Panificado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14816"/>
        <source>Acidic</source>
        <translation>Ácido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14817"/>
        <source>Flat</source>
        <translation>Llano</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14818"/>
        <source>Fracturing</source>
        <translation>Fracturación</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14819"/>
        <source>Sweet</source>
        <translation>Dulce</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14820"/>
        <source>Less Sweet</source>
        <translation>Menos Dulce</translation>
    </message>
</context>
<context>
    <name>Form Caption</name>
    <message>
        <location filename="../artisanlib/main.py" line="16402"/>
        <source>Extras</source>
        <translation>Extras</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18006"/>
        <source>Roast Properties</source>
        <translation>Propiedades del Tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19864"/>
        <source>Error Log</source>
        <translation>Historial de errores</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19889"/>
        <source>Message History</source>
        <translation>Historial de mensajes</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19948"/>
        <source>AutoSave Path</source>
        <translation>Camino Auto-Guardar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20364"/>
        <source>Roast Calculator</source>
        <translation>Calculadora del Tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20604"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22273"/>
        <source>Roast Phases</source>
        <translation>Fases del Tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22602"/>
        <source>Cup Profile</source>
        <translation>Perfil de la taza</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22836"/>
        <source>Profile Background</source>
        <translation>Perfil de fondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23355"/>
        <source>Statistics</source>
        <translation>Estadisticas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26559"/>
        <source>Designer Config</source>
        <translation>Config del Diseñador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27098"/>
        <source>Manual Temperature Logger</source>
        <translation>Grabadora manual de Temperaturas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27212"/>
        <source>Serial Ports Configuration</source>
        <translation>Configuracion Puertos Serie</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27896"/>
        <source>Device Assignment</source>
        <translation>Asignacion de dispositivos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29478"/>
        <source>Colors</source>
        <translation>Colores</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30151"/>
        <source>Wheel Graph Editor</source>
        <translation>Editor de Graficos de Rueda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30725"/>
        <source>Alarms</source>
        <translation>Alarmas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32346"/>
        <source>Fuji PXG PID Control</source>
        <translation>Fuji PXG PID Control</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20047"/>
        <source>Axes</source>
        <translation>Ejes</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35275"/>
        <source>Delta DTA PID Control</source>
        <translation>Control Delta DTA PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19820"/>
        <source>Serial Log</source>
        <translation>Historial Puertos Serie</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19721"/>
        <source>Artisan Platform</source>
        <translation>Plataforma Artisan</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19910"/>
        <source>Keyboard Autosave [a]</source>
        <translation>Auto-Guardar Teclado [a]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19761"/>
        <source>Settings Viewer</source>
        <translation>Visualizador de Configuración</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31322"/>
        <source>Fuji PXR PID Control</source>
        <translation>Fuji PXR PID Control</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34909"/>
        <source>Arduino Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17657"/>
        <source>Volume Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19581"/>
        <source>Tare Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19972"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="27918"/>
        <source>Curves</source>
        <translation>Curvas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16628"/>
        <source>HUD</source>
        <translation>HUD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16844"/>
        <source>Interpolate</source>
        <translation>Interpolar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16850"/>
        <source>Univariate</source>
        <translation>Univariable</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18661"/>
        <source>Times</source>
        <translation>Tiempos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20233"/>
        <source>Legend Location</source>
        <translation>Colocación Leyenda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20477"/>
        <source>Rate of Change</source>
        <translation>Tasa de Cambio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20479"/>
        <source>Temperature Conversion</source>
        <translation>Conversión Temperatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20481"/>
        <source>Weight Conversion</source>
        <translation>Conversión Peso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20483"/>
        <source>Volume Conversion</source>
        <translation>Conversión Volumen</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21111"/>
        <source>Event Types</source>
        <translation>Tipos de Eventos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23469"/>
        <source>Evaluation</source>
        <translation>Evaluación</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23471"/>
        <source>Display</source>
        <translation>Monitor</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28363"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29773"/>
        <source>Timer LCD</source>
        <translation>LCD Cronómetro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29776"/>
        <source>ET LCD</source>
        <translation>LCD ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29779"/>
        <source>BT LCD</source>
        <translation>LCD BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30237"/>
        <source>Label Properties</source>
        <translation>Propiedades etiquetas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20227"/>
        <source>Time Axis</source>
        <translation>Eje Tiempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20229"/>
        <source>Temperature Axis</source>
        <translation>Eje Temperatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20235"/>
        <source>Grid</source>
        <translation>Cuadrícula</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28390"/>
        <source>Arduino TC4</source>
        <translation>Arduino TC4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28403"/>
        <source>Symbolic Assignments</source>
        <translation>Asignaciones simbólicas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28397"/>
        <source>External Program</source>
        <translation>Programa externo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26821"/>
        <source>Initial Settings</source>
        <translation>Ajustes iniciales</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27927"/>
        <source>LCDs</source>
        <translation>LCDs</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16908"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16916"/>
        <source>Resolution</source>
        <translation>Resolución</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20231"/>
        <source>DeltaBT/DeltaET Axis</source>
        <translation>Eje DeltaBT/DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21262"/>
        <source>Default Buttons</source>
        <translation>Botones por Defecto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21315"/>
        <source>Management</source>
        <translation>Gestión</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27593"/>
        <source>Input 1</source>
        <translation>Entrada 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27607"/>
        <source>Input 2</source>
        <translation>Entrada 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27620"/>
        <source>Input 3</source>
        <translation>Entrada 3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27633"/>
        <source>Input 4</source>
        <translation>Entrada 4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16928"/>
        <source>Sound</source>
        <translation>Sonido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29782"/>
        <source>DeltaET LCD</source>
        <translation>LCD ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29785"/>
        <source>DeltaBT LCD</source>
        <translation>LCD DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16642"/>
        <source>Input Filters</source>
        <translation>Filtros de Entrada</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29788"/>
        <source>Extra Devices / PID SV LCD</source>
        <translation>Dispositivos extras / PID SV LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16877"/>
        <source>Polyfit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16687"/>
        <source>Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28337"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34912"/>
        <source>p-i-d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35075"/>
        <source>Set Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28310"/>
        <source>Phidget IO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28157"/>
        <source>Phidgets 1045</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28232"/>
        <source>Phidgets 1046 RTD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16970"/>
        <source>WebLCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20572"/>
        <source>Sampling Interval</source>
        <translation type="obsolete">Intervalo de Muestreo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28116"/>
        <source>Phidgets 1048/1051</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21277"/>
        <source>Sampling</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTML Report Template</name>
    <message>
        <location filename="../artisanlib/main.py" line="14190"/>
        <source>Roasting Report</source>
        <translation>Reporte Tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14215"/>
        <source>Date:</source>
        <translation>Fecha:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14219"/>
        <source>Beans:</source>
        <translation>Granos:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14227"/>
        <source>Weight:</source>
        <translation>Peso:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14235"/>
        <source>Volume:</source>
        <translation>Volumen:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14251"/>
        <source>Roaster:</source>
        <translation>Tostador:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14255"/>
        <source>Operator:</source>
        <translation>Operador:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14259"/>
        <source>Cupping:</source>
        <translation>Catación:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14279"/>
        <source>DRY:</source>
        <translation>SECAR:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14283"/>
        <source>FCs:</source>
        <translation>FCi:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14287"/>
        <source>FCe:</source>
        <translation>FCf:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14291"/>
        <source>SCs:</source>
        <translation>SCi:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14295"/>
        <source>SCe:</source>
        <translation>SCf:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14365"/>
        <source>Roasting Notes</source>
        <translation>Notas del Tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14375"/>
        <source>Cupping Notes</source>
        <translation>Notas de catación</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14223"/>
        <source>Size:</source>
        <translation>Tamaño:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14231"/>
        <source>Degree:</source>
        <translation>Grado:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14239"/>
        <source>Density:</source>
        <translation>Densidad:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13386"/>
        <source>Humidity:</source>
        <translation type="obsolete">Humedad:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14299"/>
        <source>DROP:</source>
        <translation>DESCENDER:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14303"/>
        <source>COOL:</source>
        <translation>ENFRIAR:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14311"/>
        <source>RoR:</source>
        <translation>RoR:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14315"/>
        <source>ETBTa:</source>
        <translation>ETBTa:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14263"/>
        <source>Color:</source>
        <translation>Color:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14331"/>
        <source>Maillard:</source>
        <translation>Maillard:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14335"/>
        <source>Development:</source>
        <translation>Desarrollo:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14339"/>
        <source>Cooling:</source>
        <translation>Enfriamiento:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14327"/>
        <source>Drying:</source>
        <translation>Secando:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14271"/>
        <source>CHARGE:</source>
        <translation>CARGAR:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14275"/>
        <source>TP:</source>
        <translation>TP:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14345"/>
        <source>Events</source>
        <translation type="unfinished">Eventos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14319"/>
        <source>CM:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14345"/>
        <source>Background:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14307"/>
        <source>MET:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14243"/>
        <source>Moisture:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14247"/>
        <source>Ambient:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="../artisanlib/main.py" line="26733"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27109"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5900"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5892"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8521"/>
        <source>PID SV</source>
        <translation>PID SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8574"/>
        <source>Event #&lt;b&gt;0 &lt;/b&gt;</source>
        <translation>Evento #&lt;b&gt;0 &lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15564"/>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15566"/>
        <source>City+</source>
        <translation>City+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15568"/>
        <source>Full City</source>
        <translation>Full City</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15570"/>
        <source>Full City+</source>
        <translation>Full City+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15572"/>
        <source>Light French</source>
        <translation>Light French</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15574"/>
        <source>French</source>
        <translation>French</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35006"/>
        <source>Mode</source>
        <translation>Modo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26562"/>
        <source>CHARGE</source>
        <translation>CARGA</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18040"/>
        <source>DRY END</source>
        <translation>FIN SECADO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18054"/>
        <source>FC START</source>
        <translation>INICIO FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18069"/>
        <source>FC END</source>
        <translation>FIN FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18083"/>
        <source>SC START</source>
        <translation>INICIO SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18097"/>
        <source>SC END</source>
        <translation>FIN SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26575"/>
        <source>DROP</source>
        <translation>DESCENDER</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18174"/>
        <source>Title</source>
        <translation>Titulo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18177"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18215"/>
        <source>Beans</source>
        <translation>Granos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18225"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18261"/>
        <source> in</source>
        <translation>dentro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18262"/>
        <source> out</source>
        <translation>fuera</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18275"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18260"/>
        <source>Volume</source>
        <translation>Volumen</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18293"/>
        <source>Density</source>
        <translation>Densidad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18308"/>
        <source>per</source>
        <translation>per</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18383"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18389"/>
        <source>at</source>
        <translation>a</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18401"/>
        <source>Roaster</source>
        <translation>Tostador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18403"/>
        <source>Operator</source>
        <translation>Operador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18404"/>
        <source>Roasting Notes</source>
        <translation>Notas del tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18408"/>
        <source>Cupping Notes</source>
        <translation>Notas de Catación</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16790"/>
        <source>                 Density in: {0}  g/l   =&gt;   Density out: {1} g/l</source>
        <translation type="obsolete">                 Densidad entrante: {0}  g/l   =&gt;   Densidad saliente: {1} g/l</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19385"/>
        <source>({0} g/l)</source>
        <translation type="unfinished">({0} g/l)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20991"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20989"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20366"/>
        <source>Enter two times along profile</source>
        <translation>Pon dos tiempos en el perfil</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20369"/>
        <source>Start (00:00)</source>
        <translation>Inicio (00:00)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20370"/>
        <source>End (00:00)</source>
        <translation>Fin (00:00)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20389"/>
        <source>Fahrenheit</source>
        <translation>Fahrenheit</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20390"/>
        <source>Celsius</source>
        <translation>Celsius</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22870"/>
        <source>Opaqueness</source>
        <translation>Opacidad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22890"/>
        <source>BT Color</source>
        <translation>Color BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26732"/>
        <source>Curviness</source>
        <translation>Curvedad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26745"/>
        <source>Events Playback</source>
        <translation>Reproduccion Eventos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27492"/>
        <source>Comm Port</source>
        <translation>Puerto Comm</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27496"/>
        <source>Baud Rate</source>
        <translation>Flujo Baudios</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27502"/>
        <source>Byte Size</source>
        <translation>Tamaño Byte</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27508"/>
        <source>Parity</source>
        <translation>Paridad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27515"/>
        <source>Stopbits</source>
        <translation>Stopbits</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27521"/>
        <source>Timeout</source>
        <translation>Tiempo muerto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27959"/>
        <source>Control ET</source>
        <translation>Control ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28103"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27963"/>
        <source>Read BT</source>
        <translation>Leer BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35279"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31334"/>
        <source>Ramp/Soak Pattern</source>
        <translation>Ramp/Soak modelo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32407"/>
        <source>Pattern</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32462"/>
        <source>SV (7-0)</source>
        <translation>SV (7-0)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32581"/>
        <source>Write</source>
        <translation>Escribir</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32563"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32569"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32575"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10451"/>
        <source>Event #&lt;b&gt;{0} &lt;/b&gt;</source>
        <translation type="unfinished">Evento #&lt;b&gt;{0} &lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19871"/>
        <source>Number of errors found {0}</source>
        <translation type="unfinished">Numero de errores {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>WARNING</source>
        <translation>ALERTA</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>Writing eeprom memory</source>
        <translation>Grabando en memoria EEPROM</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>&lt;u&gt;Max life&lt;/u&gt; 10,000 writes</source>
        <translation>&lt;u&gt;Vida Max &lt;/u&gt; 10,000 Grabaciones</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>Infinite read life.</source>
        <translation>Vida de lectura infinita.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>After &lt;u&gt;writing&lt;/u&gt; an adjustment,&lt;br&gt;never power down the pid&lt;br&gt;for the next 5 seconds &lt;br&gt;or the pid may never recover.</source>
        <translation>Despues de &lt;u&gt;hacer&lt;/u&gt; un ajuste,&lt;br&gt;nunca desconectes el pid&lt;br&gt;durante los siguientes 5 segundos &lt;br&gt;o la pid no funcionara.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>Read operations manual</source>
        <translation>Leer manual de operacion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20517"/>
        <source>Time syntax error. Time not valid</source>
        <translation>Error de tiempo. Tiempo inválido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20521"/>
        <source>Error: End time smaller than Start time</source>
        <translation>Error: Tiempo final inferior a inicial</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20538"/>
        <source>Best approximation was made from {0} to {1}</source>
        <translation type="unfinished">Mejor aproximacion hecha desde {0} a {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20543"/>
        <source>No profile found</source>
        <translation>Perfil no encontrado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8525"/>
        <source>PID %</source>
        <translation>PID %</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28031"/>
        <source>ET Y(x)</source>
        <translation>ET Y(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28032"/>
        <source>BT Y(x)</source>
        <translation>BT Y(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30177"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30184"/>
        <source>Edge</source>
        <translation>Borde</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30190"/>
        <source>Line</source>
        <translation>Linea</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30199"/>
        <source>Color pattern</source>
        <translation>Muestra de Color</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31327"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(1-4)</source>
        <translation>Ramp Soak HH:MM&lt;br&gt;(1-4)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31332"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(5-8)</source>
        <translation>Ramp Soak HH:MM&lt;br&gt;(5-8)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32354"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(1-7)</source>
        <translation>Ramp Soak (MM:SS)&lt;br&gt;(1-7)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32360"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(8-16)</source>
        <translation>Ramp Soak (MM:SS)&lt;br&gt;(8-16)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20091"/>
        <source>Rotation</source>
        <translation>Rotación</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20153"/>
        <source>Step</source>
        <translation>Paso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20161"/>
        <source>Style</source>
        <translation>Estilo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20171"/>
        <source>Width</source>
        <translation>Grosor</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32911"/>
        <source>ET Thermocouple type</source>
        <translation>ET tipo de Termopar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32918"/>
        <source>BT Thermocouple type</source>
        <translation>BT tipo de Termopar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32804"/>
        <source>Artisan uses 1 decimal point</source>
        <translation>Artisan usa 1 punto decimal</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32805"/>
        <source>Artisan Fuji PXG uses MINUTES:SECONDS units in Ramp/Soaks</source>
        <translation>Artisan Fuji PXG usa unidades MINUTOS:SEGUNDOS en Ramp/Soaks</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16702"/>
        <source>Y(x)</source>
        <translation>Y(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22603"/>
        <source>Default</source>
        <translation>Predeterminado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22883"/>
        <source>ET Color</source>
        <translation>Color ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27984"/>
        <source>ET Channel</source>
        <translation>Canal ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27987"/>
        <source>BT Channel</source>
        <translation>Canal BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27968"/>
        <source>RS485 Unit ID</source>
        <translation>ID Unidad RS485</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22633"/>
        <source>Aspect Ratio</source>
        <translation>Relación de Aspecto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30170"/>
        <source>Ratio</source>
        <translation>Proporción</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20825"/>
        <source>Max buttons per row</source>
        <translation>Botones max por fila</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20853"/>
        <source>Color Pattern</source>
        <translation>Muestra de Color</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20865"/>
        <source>palette #</source>
        <translation>paleta #</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26588"/>
        <source>Marker</source>
        <translation>Marcador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26590"/>
        <source>Time</source>
        <translation>Tiempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27482"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18126"/>
        <source>COOL</source>
        <translation>ENFRIAR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18337"/>
        <source>Bean Size</source>
        <translation>Tamaño del grano</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18343"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20985"/>
        <source>Event</source>
        <translation>Evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20889"/>
        <source>Action</source>
        <translation>Accion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20891"/>
        <source>Command</source>
        <translation>Comando</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20893"/>
        <source>Offset</source>
        <translation>Offset</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20895"/>
        <source>Factor</source>
        <translation>Factor</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27365"/>
        <source>Slave</source>
        <translation>Esclavo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27370"/>
        <source>Register</source>
        <translation>Registrar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28007"/>
        <source>AT Channel</source>
        <translation>Canal AT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22918"/>
        <source>DeltaET Color</source>
        <translation>Color DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22925"/>
        <source>DeltaBT Color</source>
        <translation>Color DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22972"/>
        <source>Text Warning</source>
        <translation>Alerta de Texto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22973"/>
        <source>sec</source>
        <translation>sec</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18362"/>
        <source>Moisture Greens</source>
        <translation>Condiciones de almacenaje</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18381"/>
        <source>Ambient Conditions</source>
        <translation>Condiciones Ambientales</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18489"/>
        <source>Ambient Source</source>
        <translation>Fuente Ambiental</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20706"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20710"/>
        <source>Thickness</source>
        <translation>Grosor</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20712"/>
        <source>Opacity</source>
        <translation>Opacidad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20714"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20621"/>
        <source>Bars</source>
        <translation>Barras</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16435"/>
        <source>Smooth Deltas</source>
        <translation>Alisar Deltas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16443"/>
        <source>Smooth Curves</source>
        <translation>Alisar Curvas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18345"/>
        <source>Whole Color</source>
        <translation>Color Entero</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18351"/>
        <source>Ground Color</source>
        <translation>Color del poso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27375"/>
        <source>Float</source>
        <translation>Flotar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27376"/>
        <source>Function</source>
        <translation>Función</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="6104"/>
        <source>deg/min</source>
        <translation type="obsolete">grad/min</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3215"/>
        <source>BackgroundET</source>
        <translation>FondoET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3219"/>
        <source>BackgroundBT</source>
        <translation>FondoBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3259"/>
        <source>BackgroundDeltaET</source>
        <translation>FondoDeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3263"/>
        <source>BackgroundDeltaBT</source>
        <translation>FondoDeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13086"/>
        <source>d/m</source>
        <translation type="obsolete">g/m</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5587"/>
        <source>BT {0} d/m for {1}</source>
        <translation type="obsolete">BT {0} g/m para {1}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5603"/>
        <source>ET {0} d/m for {1}</source>
        <translation type="obsolete">ET {0} g/m para {1}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15125"/>
        <source>{0} to reach ET target {1}</source>
        <translation type="obsolete">{0} para alcanzar objetivo ET {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15882"/>
        <source> at {0}</source>
        <translation type="unfinished"> a {0}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15138"/>
        <source>{0} to reach BT target {1}</source>
        <translation type="obsolete">{0} para alcanzar objetivo BT {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15936"/>
        <source>ET - BT = {0}</source>
        <translation type="unfinished">ET - BT = {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15989"/>
        <source>ET - BT = {0}{1}</source>
        <translation type="unfinished">ET - BT = {0}{1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30485"/>
        <source> dg</source>
        <translation> gd</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30900"/>
        <source>Enter description</source>
        <translation>Entrar descripcion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32705"/>
        <source>NOTE: BT Thermocouple type is not stored in the Artisan settings</source>
        <translation>NOTA: EL tipo de termopar BT no se encuentra en la configuracion de Artisan</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15894"/>
        <source>{0} after FCs</source>
        <translation type="unfinished">{0} tras FCi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15901"/>
        <source>{0} after FCe</source>
        <translation type="unfinished">{0} tras FCf</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16418"/>
        <source>ET Target 1</source>
        <translation>ET meta 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16420"/>
        <source>BT Target 1</source>
        <translation>BT meta 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16422"/>
        <source>ET Target 2</source>
        <translation>ET meta 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16424"/>
        <source>BT Target 2</source>
        <translation>BT meta 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16428"/>
        <source>ET p-i-d 1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35027"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35035"/>
        <source>max</source>
        <translation>max</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23404"/>
        <source>Drying</source>
        <translation>Secando</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23405"/>
        <source>Maillard</source>
        <translation>Maillard</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23406"/>
        <source>Development</source>
        <translation>Desarrollo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23407"/>
        <source>Cooling</source>
        <translation>Enfriamiento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1608"/>
        <source>EVENT</source>
        <translation>EVENTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20114"/>
        <source>Initial Max</source>
        <translation>Max Inicial</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27247"/>
        <source>Settings for non-Modbus devices</source>
        <translation>Ajustes para dispositivos no-Modbus</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6886"/>
        <source>Curves</source>
        <translation>Curvas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6890"/>
        <source>Delta Curves</source>
        <translation>Delta Curvas</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4639"/>
        <source>T</source>
        <translation type="obsolete">T</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>RoR</source>
        <translation>RoR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>ETBTa</source>
        <translation>ETBTa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16813"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16814"/>
        <source>End</source>
        <translation>Fin</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16645"/>
        <source>Path Effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16665"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9392"/>
        <source>TP</source>
        <translation type="unfinished">TP</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9431"/>
        <source>DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9480"/>
        <source>FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11404"/>
        <source>Charge the beans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11390"/>
        <source>Start recording</source>
        <translation type="unfinished">Comienza a grabar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19996"/>
        <source>Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34939"/>
        <source>Source</source>
        <translation type="unfinished">Fuente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5252"/>
        <source>CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16451"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34955"/>
        <source>Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35001"/>
        <source>Lookahead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35008"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35009"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35010"/>
        <source>Background</source>
        <translation type="unfinished">Fondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35015"/>
        <source>SV Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35018"/>
        <source>SV Slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20993"/>
        <source>Coarse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27407"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27412"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16414"/>
        <source>HUD Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28297"/>
        <source>Raw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28316"/>
        <source>ServerId:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28318"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>MET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9376"/>
        <source>DRY%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9413"/>
        <source>RAMP%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9462"/>
        <source>DEV%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14477"/>
        <source>greens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14482"/>
        <source>roasted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18371"/>
        <source>Moisture Roasted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6881"/>
        <source>/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14451"/>
        <source>/m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28296"/>
        <source>Async</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28299"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28217"/>
        <source>Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28298"/>
        <source>Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28218"/>
        <source>Wiring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16515"/>
        <source>Delta Span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17683"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17819"/>
        <source>ml</source>
        <translation type="unfinished">ml</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17783"/>
        <source>Unit Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17802"/>
        <source>g</source>
        <translation type="unfinished">g</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17800"/>
        <source>Kg</source>
        <translation type="unfinished">Kg</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17817"/>
        <source>l</source>
        <translation type="unfinished">l</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17775"/>
        <source>in</source>
        <translation type="unfinished">dentro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17850"/>
        <source>out</source>
        <translation type="unfinished">fuera</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28015"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28142"/>
        <source>Emissivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21195"/>
        <source>ON</source>
        <translation type="unfinished">ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21210"/>
        <source>OFF</source>
        <translation type="unfinished">OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3209"/>
        <source>BackgroundXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22897"/>
        <source>XT Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22904"/>
        <source>XT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20869"/>
        <source>current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18184"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19999"/>
        <source>Counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6364"/>
        <source>BT {0} {1}/min for {2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6380"/>
        <source>ET {0} {1}/min for {2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15871"/>
        <source>{0} to reach ET {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15884"/>
        <source>{0} to reach BT {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19327"/>
        <source>Density in: {0} g/l   =&gt;   Density out: {1} g/l</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19365"/>
        <source>Moisture loss: {0}%    Organic loss: {1}%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20539"/>
        <source>&lt;b&gt;{0}&lt;/b&gt; {1}/sec, &lt;b&gt;{2}&lt;/b&gt; {3}/min</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../const/UIconst.py" line="48"/>
        <source>Services</source>
        <translation>Servicios</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="49"/>
        <source>Hide {0}</source>
        <translation type="unfinished">Ocultar {0}</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="50"/>
        <source>Hide Others</source>
        <translation>Ocultar otros</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="51"/>
        <source>Show All</source>
        <translation>Mostrar todo</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="52"/>
        <source>Preferences...</source>
        <translation>Preferencias…</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="74"/>
        <source>Quit {0}</source>
        <translation type="unfinished">Salir de {0}</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="163"/>
        <source>About {0}</source>
        <translation type="unfinished">Acerca de {0}</translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="../artisanlib/main.py" line="20661"/>
        <source>Circle</source>
        <translation>Circulo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20662"/>
        <source>Square</source>
        <translation>Cuadrado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20663"/>
        <source>Pentagon</source>
        <translation>Pentagono</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20664"/>
        <source>Diamond</source>
        <translation>Diamante</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20665"/>
        <source>Star</source>
        <translation>Estrella</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20666"/>
        <source>Hexagon 1</source>
        <translation>Hexagono 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20667"/>
        <source>Hexagon 2</source>
        <translation>Hexagono 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20668"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20669"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20670"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
</context>
<context>
    <name>Menu</name>
    <message>
        <location filename="../const/UIconst.py" line="60"/>
        <source>New</source>
        <translation>Nuevo</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="61"/>
        <source>Open...</source>
        <translation>Abrir...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="62"/>
        <source>Open Recent</source>
        <translation>Abiertos Recientemente</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="63"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="64"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="65"/>
        <source>Save As...</source>
        <translation>Guardar Como...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="70"/>
        <source>Print...</source>
        <translation>Imprimir...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="85"/>
        <source>Roast</source>
        <translation>Tostado</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="88"/>
        <source>Properties...</source>
        <translation>Propiedades...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="89"/>
        <source>Background...</source>
        <translation>Fondo...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="90"/>
        <source>Cup Profile...</source>
        <translation>Perfil en Taza...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="91"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="146"/>
        <source>Calculator</source>
        <translation>Calculadora</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="102"/>
        <source>Device...</source>
        <translation>Dispositivo...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="103"/>
        <source>Serial Port...</source>
        <translation>Puerto Serie...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="104"/>
        <source>Sampling Interval...</source>
        <translation>Intervalo de Muestreo...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="107"/>
        <source>Colors...</source>
        <translation>Colores...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="110"/>
        <source>Phases...</source>
        <translation>Etapas...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="111"/>
        <source>Events...</source>
        <translation>Eventos...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="112"/>
        <source>Statistics...</source>
        <translation>Estadísticas...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="114"/>
        <source>Autosave...</source>
        <translation>Auto-guardar...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="149"/>
        <source>Extras...</source>
        <translation>Extras...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="159"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="165"/>
        <source>Documentation</source>
        <translation>Documentación</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="57"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="67"/>
        <source>Save Graph</source>
        <translation>Guardar Gráfico</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="99"/>
        <source>Config</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="113"/>
        <source>Axes...</source>
        <translation>Ejes...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="168"/>
        <source>Errors</source>
        <translation>Errores</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="167"/>
        <source>Keyboard Shortcuts</source>
        <translation>Atajos de Teclado</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="169"/>
        <source>Messages</source>
        <translation>Mensajes</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="116"/>
        <source>Alarms...</source>
        <translation>Alarmas...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="77"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="80"/>
        <source>Cut</source>
        <translation>Cortar</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="81"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="82"/>
        <source>Paste</source>
        <translation>Pegar</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="68"/>
        <source>Full Size...</source>
        <translation>Tamaño grande...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="145"/>
        <source>Designer</source>
        <translation>Diseñador</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="92"/>
        <source>Convert to Fahrenheit</source>
        <translation>Convertir a  Fahrenheit</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="93"/>
        <source>Convert to Celsius</source>
        <translation>Convertir a Celsius</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="94"/>
        <source>Fahrenheit Mode</source>
        <translation>Modo Fahrenheit</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="95"/>
        <source>Celsius Mode</source>
        <translation>Modo Celsius</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7739"/>
        <source>HH506RA...</source>
        <translation>HH506RA...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7743"/>
        <source>K202...</source>
        <translation>K202...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="142"/>
        <source>Tools</source>
        <translation>Herramientas</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="147"/>
        <source>Wheel Graph</source>
        <translation>Grafico de Rueda</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="176"/>
        <source>Factory Reset</source>
        <translation>Reinicializacion Total</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="117"/>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="170"/>
        <source>Serial</source>
        <translation>Registro del Puerto Serie</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7747"/>
        <source>K204...</source>
        <translation>K204...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="174"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="175"/>
        <source>Platform</source>
        <translation>Plataforma</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="69"/>
        <source>Roasting Report</source>
        <translation>Reporte de Tueste</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7766"/>
        <source>CSV...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7770"/>
        <source>JSON...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7774"/>
        <source>RoastLogger...</source>
        <translation>Registro de Tueste...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="66"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="164"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="96"/>
        <source>Switch Profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="106"/>
        <source>Oversampling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="148"/>
        <source>LCDs</source>
        <translation type="unfinished">LCDs</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="115"/>
        <source>Batch...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="153"/>
        <source>Load Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="154"/>
        <source>Load Recent Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="155"/>
        <source>Save Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="108"/>
        <source>Buttons</source>
        <translation type="unfinished">Botones</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="109"/>
        <source>Sliders</source>
        <translation type="unfinished">Deslizadores</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../artisanlib/main.py" line="6765"/>
        <source>Mouse Cross ON: move mouse around</source>
        <translation>Cruz de Raton ON: mover el raton para ver</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6777"/>
        <source>Mouse cross OFF</source>
        <translation>Cruz de Raton OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1864"/>
        <source>HUD OFF</source>
        <translation>HUD OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1878"/>
        <source>HUD ON</source>
        <translation>HUD ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1970"/>
        <source>Alarm notice</source>
        <translation>Avido de Alarma</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2006"/>
        <source>Alarm is calling: {0}</source>
        <translation type="unfinished">Alarma esta llamando: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2463"/>
        <source>Save the profile, Discard the profile (Reset), or Cancel?</source>
        <translation>Guardar perfil, Desechar perfil (reset), o Cancelar?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2464"/>
        <source>Profile unsaved</source>
        <translation>Perfil sin guardar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2547"/>
        <source>Scope has been reset</source>
        <translation>Grabador reinicializado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3757"/>
        <source>Time format error encountered</source>
        <translation>Error en el formato del tiempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3863"/>
        <source>Convert profile data to Fahrenheit?</source>
        <translation>Quieres convertir el perfil a Fahrenheit?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3948"/>
        <source>Convert Profile Temperature</source>
        <translation>Cambiar Temperatura del perfil</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3904"/>
        <source>Profile changed to Fahrenheit</source>
        <translation>Perfil cambiado a modo Fahrenheit</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3907"/>
        <source>Unable to comply. You already are in Fahrenheit</source>
        <translation>Ya estas en modo Fahrenheit</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3950"/>
        <source>Profile not changed</source>
        <translation>Perfil sin cambiar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3913"/>
        <source>Convert profile data to Celsius?</source>
        <translation>Quieres convertir el perfil a Celsius?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3948"/>
        <source>Unable to comply. You already are in Celsius</source>
        <translation>Ya estas en modo Celsius</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3954"/>
        <source>Profile changed to Celsius</source>
        <translation>Perfil cambiado a modo Celsius</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3959"/>
        <source>Convert Profile Scale</source>
        <translation>Cambiar Escala del perfil</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3959"/>
        <source>No profile data found</source>
        <translation>No data de perfil disponible</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3976"/>
        <source>Colors set to defaults</source>
        <translation>Colores predeterminados</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3981"/>
        <source>Colors set to grey</source>
        <translation>Colores gris</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4152"/>
        <source>Scope monitoring...</source>
        <translation>Monitoreo...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4197"/>
        <source>Scope stopped</source>
        <translation>Grabador parado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4311"/>
        <source>Scope recording...</source>
        <translation>Grabando...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4352"/>
        <source>Scope recording stopped</source>
        <translation>Grabador parado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4405"/>
        <source>Not enough variables collected yet. Try again in a few seconds</source>
        <translation>No se tienen suficientes variales. Intenta de nuevo en algunos segundos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4459"/>
        <source>Roast time starts now 00:00 BT = {0}</source>
        <translation type="unfinished">Tiempo de tostado empieza ahora 00:00 BT= {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4920"/>
        <source>Scope is OFF</source>
        <translation>Grabador OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4540"/>
        <source>[DRY END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[SECO FIN] grabado a  {0} BT = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4603"/>
        <source>[FC START] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[FC START] grabado a {0} BT = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4658"/>
        <source>[FC END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[FC FIN] grabado a {0} BT = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4716"/>
        <source>[SC START] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[SC START] grabado a {0} BT = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4773"/>
        <source>[SC END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[SC END] grabado a {0} BT = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4855"/>
        <source>Roast ended at {0} BT = {1}</source>
        <translation type="unfinished">Tostado termino a {0} BT = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4953"/>
        <source>[COOL END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[COOL END] grabado a {0} BT = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5089"/>
        <source>Event # {0} recorded at BT = {1} Time = {2}</source>
        <translation type="unfinished">Evento # {0} grabado a BT = {1} Tiempo = {2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5104"/>
        <source>Timer is OFF</source>
        <translation>Cornometro Apagado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5136"/>
        <source>Computer Event # {0} recorded at BT = {1} Time = {2}</source>
        <translation type="unfinished">Evento de computadora # {0} grabado a BT = {1} Tiempo = {2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5298"/>
        <source>Statistics cancelled: need complete profile [CHARGE] + [DROP]</source>
        <translation>Estadisticas canceladas. Se necesita un perfil completo  [CARGA] + [DESCAR]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5483"/>
        <source>Unable to move background</source>
        <translation>No se pudo mover el fondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5542"/>
        <source>No finished profile found</source>
        <translation>No se pudo encontrar perfil terminado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5560"/>
        <source>Polynomial coefficients (Horner form):</source>
        <translation>Coeficientes del polinomio (forma Horner):</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5563"/>
        <source>Knots:</source>
        <translation>Nudos:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5566"/>
        <source>Residual:</source>
        <translation>Residuo:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5569"/>
        <source>Roots:</source>
        <translation>Raices:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5573"/>
        <source>Profile information</source>
        <translation>Informacion del perfil</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5734"/>
        <source>Designer Start</source>
        <translation>Comenzar el Diseñador</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4341"/>
        <source>Importing a profile in to Designer will decimate
all data except the main [points].
Continue?</source>
        <translation type="obsolete">Importar un perfil al diseñador borrara 
los puntos excepto lor principales.
 Continuar?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5778"/>
        <source>Designer Init</source>
        <translation>Iniciar Diseñador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5778"/>
        <source>Unable to start designer.
Profile missing [CHARGE] or [DROP]</source>
        <translation>No se puedo iniciar el diseñador.
No existe [CARGA] or [DESCAR] en el perfil</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6024"/>
        <source>[ CHARGE ]</source>
        <translation>[ CARGA ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6027"/>
        <source>[ DRY END ]</source>
        <translation>[ SECO ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6030"/>
        <source>[ FC START ]</source>
        <translation>[ FC START ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6033"/>
        <source>[ FC END ]</source>
        <translation>[ FC FIN ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6036"/>
        <source>[ SC START ]</source>
        <translation>[ SC START ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6039"/>
        <source>[ SC END ]</source>
        <translation>[ SC FIN ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6042"/>
        <source>[ DROP ]</source>
        <translation>[ DESCAR ]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6305"/>
        <source>New profile created</source>
        <translation>Nuevo perfil creado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30658"/>
        <source>Open Wheel Graph</source>
        <translation>Abrir Grafica de Rueda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6492"/>
        <source> added to cupping notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6498"/>
        <source> added to roasting notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9903"/>
        <source>Do you want to reset all settings?</source>
        <translation>Quieres reiniciar todas las variables?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9904"/>
        <source>Factory Reset</source>
        <translation>Reinicializacion Total</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10304"/>
        <source>Keyboard moves turned ON</source>
        <translation>Atajos de Teclado ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10313"/>
        <source>Keyboard moves turned OFF</source>
        <translation>Atajos de Teclado OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10410"/>
        <source>Profile {0} saved in: {1}</source>
        <translation type="unfinished">Perfil {0} guardado en: {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10415"/>
        <source>Empty path or box unchecked in Autosave</source>
        <translation>Camino vacio o Autosave no seleccionado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10422"/>
        <source>&lt;b&gt;[ENTER]&lt;/b&gt; = Turns ON/OFF Keyboard Shortcuts</source>
        <translation>&lt;b&gt;[ENTER]&lt;/b&gt; = Enciende/Apaga Atajos de teclado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10423"/>
        <source>&lt;b&gt;[SPACE]&lt;/b&gt; = Choses current button</source>
        <translation>&lt;b&gt;[SPACE]&lt;/b&gt; = Elije el boton seleccionado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10424"/>
        <source>&lt;b&gt;[LEFT]&lt;/b&gt; = Move to the left</source>
        <translation>&lt;b&gt;[LEFT]&lt;/b&gt; =Mover a la izquierda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10425"/>
        <source>&lt;b&gt;[RIGHT]&lt;/b&gt; = Move to the right</source>
        <translation>&lt;b&gt;[RIGHT]&lt;/b&gt; =Mover a la derecha</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10426"/>
        <source>&lt;b&gt;[a]&lt;/b&gt; = Autosave</source>
        <translation>&lt;b&gt;[a]&lt;/b&gt; =Autoguardar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10428"/>
        <source>&lt;b&gt;[t]&lt;/b&gt; = Mouse cross lines</source>
        <translation>&lt;b&gt;[t]&lt;/b&gt; = lineas de la Cruz de Raton</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10430"/>
        <source>&lt;b&gt;[b]&lt;/b&gt; = Shows/Hides Extra Event Buttons</source>
        <translation>&lt;b&gt;[b]&lt;/b&gt; = Mostrar/Esconder botones de eventos extra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10432"/>
        <source>&lt;b&gt;[i]&lt;/b&gt; = Retrieve Weight In from Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10433"/>
        <source>&lt;b&gt;[o]&lt;/b&gt; = Retrieve Weight Out from Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10434"/>
        <source>&lt;b&gt;[0-9]&lt;/b&gt; = Changes Event Button Palettes</source>
        <translation>&lt;b&gt;[0-9]&lt;/b&gt; = Cambia paletas de botones</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10435"/>
        <source>&lt;b&gt;[;]&lt;/b&gt; = Application ScreenShot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10436"/>
        <source>&lt;b&gt;[:]&lt;/b&gt; = Desktop ScreenShot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10440"/>
        <source>Keyboard Shotcuts</source>
        <translation>Atajos del Teclado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10521"/>
        <source>Event #{0}:  {1} has been updated</source>
        <translation type="unfinished">Evento #{0}:  {1} ha sido actualizado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10599"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10611"/>
        <source>Select Directory</source>
        <translation>Seleccionar Directorio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19230"/>
        <source>No profile found</source>
        <translation>Perfil no encontrado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10660"/>
        <source>{0} has been saved. New roast has started</source>
        <translation type="unfinished">{0} ha sido guardado. Comenzado un nuevo tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10823"/>
        <source>Invalid artisan format</source>
        <translation>Formato invalido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10704"/>
        <source>{0}  loaded </source>
        <translation type="unfinished">{0} abierto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10819"/>
        <source>Background {0} loaded successfully {1}</source>
        <translation type="unfinished">Fondo {0} abierto {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10951"/>
        <source>Artisan CSV file loaded successfully</source>
        <translation>Artisan CSV ficha abierta correctamente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12348"/>
        <source>Save Profile</source>
        <translation>Guardar Perfil</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12355"/>
        <source>Profile saved</source>
        <translation>Perfil guardado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15387"/>
        <source>Cancelled</source>
        <translation>Cancelado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12371"/>
        <source>Readings exported</source>
        <translation>Propiedades exportadas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12379"/>
        <source>Export CSV</source>
        <translation>Exportar a CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12382"/>
        <source>Export JSON</source>
        <translation>Exportar a JSON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12385"/>
        <source>Export RoastLogger</source>
        <translation>Exportar a RoastLogger</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12394"/>
        <source>Readings imported</source>
        <translation>Propiedades importadas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12402"/>
        <source>Import CSV</source>
        <translation>Importar de CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12405"/>
        <source>Import JSON</source>
        <translation>Importar de JSON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12408"/>
        <source>Import RoastLogger</source>
        <translation>Importar de RoastLogger</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15113"/>
        <source>Sampling Interval</source>
        <translation>Intervalo de Muestreo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15113"/>
        <source>Seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15489"/>
        <source>Alarm Config</source>
        <translation>Config Alarmas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15489"/>
        <source>Alarms are not available for device None</source>
        <translation>Las alarmas no se pueden utilizar con dispositivo Ninguno</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15546"/>
        <source>Switch Language</source>
        <translation>Cambiar Languaje</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15546"/>
        <source>Language successfully changed. Restart the application.</source>
        <translation>Leguaje cambiado. Reinicia el programa para que tome efecto. </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15578"/>
        <source>Import K202 CSV</source>
        <translation>Importar K202 CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15630"/>
        <source>K202 file loaded successfully</source>
        <translation>K202 ficha abierta correctamente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15644"/>
        <source>Import K204 CSV</source>
        <translation>Importar K204 CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15710"/>
        <source>K204 file loaded successfully</source>
        <translation>ficha K204 subida</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15724"/>
        <source>Import HH506RA CSV</source>
        <translation>Importar HH506RA CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15775"/>
        <source>HH506RA file loaded successfully</source>
        <translation>HH506RA ficha abierta correctamente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15820"/>
        <source>Save Graph as PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15827"/>
        <source>{0}  size({1},{2}) saved</source>
        <translation type="unfinished">{0}  tamaño({1},{2}) guardado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15836"/>
        <source>Save Graph as SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16046"/>
        <source>Invalid Wheel graph format</source>
        <translation>Formato de Grafica de Rueda invalido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16049"/>
        <source>Wheel Graph succesfully open</source>
        <translation>Grafica de Rueda abierto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16068"/>
        <source>Return the absolute value of x.</source>
        <translation>Valor absoluto de x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16069"/>
        <source>Return the arc cosine (measured in radians) of x.</source>
        <translation>Arco coseno de x (en radianes).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16070"/>
        <source>Return the arc sine (measured in radians) of x.</source>
        <translation>Arco seno de x (en radianes).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16071"/>
        <source>Return the arc tangent (measured in radians) of x.</source>
        <translation>Arco tangente de x (en radianes).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16072"/>
        <source>Return the cosine of x (measured in radians).</source>
        <translation>Coseno de x (en radianes).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16073"/>
        <source>Convert angle x from radians to degrees.</source>
        <translation>Convierte angulo x de radianes a grados.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16074"/>
        <source>Return e raised to the power of x.</source>
        <translation>e al exponente x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16075"/>
        <source>Return the logarithm of x to the given base. </source>
        <translation>Logaritmo de x a la base dada.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16076"/>
        <source>Return the base 10 logarithm of x.</source>
        <translation>logaritmo de x a la base 10.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16079"/>
        <source>Return x**y (x to the power of y).</source>
        <translation>x**y (x al exponente y).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16080"/>
        <source>Convert angle x from degrees to radians.</source>
        <translation>Convertir angulo x de grados a radianes.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16081"/>
        <source>Return the sine of x (measured in radians).</source>
        <translation>seno de x (en radianes).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16082"/>
        <source>Return the square root of x.</source>
        <translation>raiz quadrada de x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16083"/>
        <source>Return the tangent of x (measured in radians).</source>
        <translation>Tangente de x (en radianes).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16100"/>
        <source>MATHEMATICAL FUNCTIONS</source>
        <translation>FUNCIONES MATEMATICAS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16102"/>
        <source>SYMBOLIC VARIABLES</source>
        <translation>VARIABLES SIMBOLICAS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16104"/>
        <source>Symbolic Functions</source>
        <translation>Funciones simbolicas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16234"/>
        <source>Save Palettes</source>
        <translation>Guardar Paleta</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16238"/>
        <source>Palettes saved</source>
        <translation>Paleta guardado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16281"/>
        <source>Invalid palettes file format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16284"/>
        <source>Palettes loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16296"/>
        <source>Load Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16317"/>
        <source>Alarms loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17455"/>
        <source>Sound turned ON</source>
        <translation>Sonido Encendido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17459"/>
        <source>Sound turned OFF</source>
        <translation>Sonido Apagado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19227"/>
        <source>Event #{0} added</source>
        <translation type="unfinished">Evento #{0} añadido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19263"/>
        <source> Event #{0} deleted</source>
        <translation type="unfinished"> Evento #{0} borrado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19269"/>
        <source>No events found</source>
        <translation>Eventos no encontrados</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19566"/>
        <source>Roast properties updated but profile not saved to disk</source>
        <translation>Propiedades del tostado actualizadas pero perfil no ha sido grabado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19956"/>
        <source>Autosave ON. Prefix: {0}</source>
        <translation type="unfinished">Autoguardar ON. Prefijo: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19960"/>
        <source>Autosave OFF</source>
        <translation>Autograbar apagado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20335"/>
        <source>xlimit = ({2},{3}) ylimit = ({0},{1}) zlimit = ({4},{5})</source>
        <translation type="unfinished">xlimit = ({2},{3}) ylimit = ({0},{1}) zlimit = ({4},{5})</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21457"/>
        <source>&lt;b&gt;Event&lt;/b&gt; hide or show the corresponding slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21458"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action on slider release</source>
        <translation>&lt;b&gt;Accion&lt;/b&gt; Ejecuta una accion al mismo tiempo que el evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21459"/>
        <source>&lt;b&gt;Command&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by &lt;i&gt;value&lt;/i&gt;*&lt;i&gt;factor&lt;/i&gt; + &lt;i&gt;offset&lt;/i&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22256"/>
        <source>Serial Command: ASCII serial command or binary a2b_uu(serial command)</source>
        <translation>Comando serial: ASCII comando serial o binario a2b_uu(comando serial)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22260"/>
        <source>DTA Command: Insert Data address : value, ex. 4701:1000 and sv is 100. always multiply with 10 if value Unit: 0.1 / ex. 4719:0 stops heating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21463"/>
        <source>&lt;b&gt;Offset&lt;/b&gt; added as offset to the slider value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21464"/>
        <source>&lt;b&gt;Factor&lt;/b&gt; multiplicator of the slider value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22264"/>
        <source>Event custom buttons</source>
        <translation>Botones Eventos configurables</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22225"/>
        <source>Event configuration saved</source>
        <translation>Configuracion de Eventos guardada</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22228"/>
        <source>Found empty event type box</source>
        <translation>Caja de tipos de Eventos vacia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22250"/>
        <source>&lt;b&gt;Button Label&lt;/b&gt; Enter \n to create labels with multiple lines.</source>
        <translation>&lt;b&gt;Etiqueta Boton &lt;/b&gt; Escribe \n para crear lineas multiples.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22251"/>
        <source>&lt;b&gt;Event Description&lt;/b&gt; Description of the Event to be recorded.</source>
        <translation>&lt;b&gt;Descripcion Evento &lt;/b&gt; La descripcion del evento.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22252"/>
        <source>&lt;b&gt;Event type&lt;/b&gt; Type of event to be recorded.</source>
        <translation>&lt;b&gt; Tipo de Evento &lt;/b&gt; Tipo de Evento.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20781"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-10) to be recorded</source>
        <translation type="obsolete">&lt;b&gt;Valor de Evento &lt;/b&gt; Valor del Evento (1-10)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22254"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action at the time of the event</source>
        <translation>&lt;b&gt;Accion&lt;/b&gt; Ejecuta una accion al mismo tiempo que el evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22255"/>
        <source>&lt;b&gt;Documentation&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by the event value):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22257"/>
        <source>Call Program: A program/script path (absolute or relative)</source>
        <translation>Llamar programa: Un programa o camino de script (absoluto o relativo) </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22258"/>
        <source>Multiple Event: Adds events of other button numbers separated by a comma: 1,2,3, etc.</source>
        <translation>Evento multiple: Añade eventos con el numero de otros botones separados por coma: 1,2,3, etc.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22262"/>
        <source>&lt;b&gt;Button Visibility&lt;/b&gt; Hides/shows individual button</source>
        <translation>&lt;b&gt;Visibilidad de botones&lt;/b&gt; Muestra/esconde botones</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22263"/>
        <source>&lt;b&gt;Keyboard Shorcut: &lt;/b&gt; [b] Hides/shows Extra Button Rows</source>
        <translation>&lt;b&gt;Atajo teclado: &lt;/b&gt; [b] Muestra/Esconde filas de botones</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22731"/>
        <source>Background does not match number of labels</source>
        <translation>Fondo no coincide con numero de etiquetas existentes</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26841"/>
        <source>Not enough time points for an ET curviness of {0}. Set curviness to {1}</source>
        <translation type="unfinished">Insuficientes puntos de tiempo para curva ET de {0}. Cambiado a {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27051"/>
        <source>Designer Config</source>
        <translation>Config Diseñador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26848"/>
        <source>Not enough time points for an BT curviness of {0}. Set curviness to {1}</source>
        <translation type="unfinished">Insuficientes puntos de tiempo para curva BT de {0}. Cambiado a {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27043"/>
        <source>CHARGE</source>
        <translation>CARGAR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27044"/>
        <source>DRY END</source>
        <translation>SECO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27045"/>
        <source>FC START</source>
        <translation>FC START</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27046"/>
        <source>FC END</source>
        <translation>FC FIN</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27047"/>
        <source>SC START</source>
        <translation>SC START</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27048"/>
        <source>SC END</source>
        <translation>SC FIN</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27049"/>
        <source>DROP</source>
        <translation>DESCAR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26863"/>
        <source>Incorrect time format. Please recheck {0} time</source>
        <translation type="unfinished">Formato de tiempo incorrecto. Corrije {0} </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27050"/>
        <source>Times need to be in ascending order. Please recheck {0} time</source>
        <translation type="unfinished">Tiempos necesitan estar en orden ascendente. Corrije {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27015"/>
        <source>Designer has been reset</source>
        <translation>Diseñador ha siso reiniciado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27872"/>
        <source>Serial Port Settings: {0}, {1}, {2}, {3}, {4}, {5}</source>
        <translation type="unfinished">Configuarcion Puero Serial : {0}, {1}, {2}, {3}, {4}, {5}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="21105"/>
        <source>Port scan on this platform not yet supported</source>
        <translation type="obsolete">Escaneo de Puertos en esta plataforma no existe</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28654"/>
        <source>External program</source>
        <translation>Programa externo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28920"/>
        <source>PID to control ET set to {0} {1} ; PID to read BT set to {2} {3}</source>
        <translation type="unfinished">PID control ET puesto a {0} {1} ; PID leer BT puesto a {2} {3}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29137"/>
        <source>Device set to {0}. Now, check Serial Port settings</source>
        <translation type="unfinished">Dispositivo seleccionado {0}. Ahora elije puerto serial </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29275"/>
        <source>Device set to {0}. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo puesto a {0}. Ahora elije puerto serial</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28989"/>
        <source>Device set to CENTER 305, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation>Dispositivo seleccionado CENTER 305, que es equivalente a CENTER 306. Ahora elije puerto serial </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29043"/>
        <source>Device set to {0}, which is equivalent to CENTER 309. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo seleccionado {0}, que es equivalente a CENTER 309. Ahora elije puerto serial </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29070"/>
        <source>Device set to {0}, which is equivalent to CENTER 303. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo seleccionado {0}, que es equivalente a CENTER 303. Ahora elije puerto serial </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29052"/>
        <source>Device set to {0}, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo seleccionado {0}, que es equivalente a CENTER 306. Ahora elije puerto serial </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29079"/>
        <source>Device set to {0}, which is equivalent to Omega HH506RA. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo seleccionado {0}, que es equivalente a Omega HH505RA. Ahora elije puerto serial </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29180"/>
        <source>Device set to {0}, which is equivalent to Omega HH806AU. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo seleccionado {0}, que es equivalente a Omega HH806AU. Ahora elije puerto serial </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29253"/>
        <source>Device set to {0}</source>
        <translation type="unfinished">Dispositivo seleccionado {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29110"/>
        <source>Device set to {0}{1}</source>
        <translation type="unfinished">Dispositivo seleccionado {0}{1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29171"/>
        <source>Device set to {0}, which is equivalent to CENTER 302. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo seleccionado {0}, que es equivalente a CENTER 306. Ahora elije puerto serial  {1,?} {302.?}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30003"/>
        <source>Color of {0} set to {1}</source>
        <translation type="unfinished">Color de {0} cambiado a {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30648"/>
        <source>Save Wheel graph</source>
        <translation>Guardar Grafica de Rueda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30652"/>
        <source>Wheel Graph saved</source>
        <translation>Grafica de Rueda guardada</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30965"/>
        <source>Load Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31005"/>
        <source>Save Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31037"/>
        <source>&lt;b&gt;Status:&lt;/b&gt; activate or deactive alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31038"/>
        <source>&lt;b&gt;If Alarm:&lt;/b&gt; alarm triggered only if the alarm with the given number was triggered before. Use 0 for no guard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31040"/>
        <source>&lt;b&gt;From:&lt;/b&gt; alarm only triggered after the given event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31041"/>
        <source>&lt;b&gt;Time:&lt;/b&gt; if not 00:00, alarm is triggered mm:ss after the event &apos;From&apos; happend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31042"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; the temperature source that is observed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31043"/>
        <source>&lt;b&gt;Condition:&lt;/b&gt; alarm is triggered if source rises above or below the specified temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31045"/>
        <source>&lt;b&gt;Action:&lt;/b&gt; if all conditions are fulfilled the alarm triggeres the corresponding action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31047"/>
        <source>&lt;b&gt;NOTE:&lt;/b&gt; each alarm is only triggered once</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33957"/>
        <source>OFF</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33950"/>
        <source>CONTINUOUS CONTROL</source>
        <translation>CONTROL CONTINUADO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33963"/>
        <source>ON</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33962"/>
        <source>STANDBY MODE</source>
        <translation>MODE DESCANSO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31999"/>
        <source>The rampsoak-mode tells how to start and end the ramp/soak</source>
        <translation>EL modo RampSoak describe como encender y apagar el RampSoak</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32000"/>
        <source>Your rampsoak mode in this pid is:</source>
        <translation>Tu modo RampSoak en esta pid es:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32001"/>
        <source>Mode = {0}</source>
        <translation type="unfinished">Modo= {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32003"/>
        <source>Start to run from PV value: {0}</source>
        <translation type="unfinished">Comienza desde valor de PV: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32004"/>
        <source>End output status at the end of ramp/soak: {0}</source>
        <translation type="unfinished">Estado de final en RampSoak: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32006"/>
        <source>
Repeat Operation at the end: {0}</source>
        <translation type="unfinished">
Repite operacion al final: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32008"/>
        <source>Recomended Mode = 0</source>
        <translation>Modo recomendad0 = 0</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32009"/>
        <source>If you need to change it, change it now and come back later</source>
        <translation>Si ncesitas cambiarlo, cambialo ahora y regresa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32010"/>
        <source>Use the Parameter Loader Software by Fuji if you need to

</source>
        <translation>Usa el programa que se llama Loader Software de Fuji si necesitas

</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32011"/>
        <source>Continue?</source>
        <translation>Continuar?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31807"/>
        <source>RampSoak Mode</source>
        <translation type="obsolete">Ramp/Soak Modo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33367"/>
        <source>Current sv = {0}. Change now to sv = {1}?</source>
        <translation type="unfinished">SV actual = {0}. Cambiar a SV={1}?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33429"/>
        <source>Change svN</source>
        <translation>Cambiar svN</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33428"/>
        <source>Current pid = {0}. Change now to pid ={1}?</source>
        <translation type="unfinished">PID actual = {0}. Cambiar a PID = {1}?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34183"/>
        <source>Ramp Soak start-end mode</source>
        <translation>Ramp Soak modo de inicio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34089"/>
        <source>Pattern changed to {0}</source>
        <translation type="unfinished">Modo cambiado a {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34091"/>
        <source>Pattern did not changed</source>
        <translation>Modo no cambio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34094"/>
        <source>Ramp/Soak was found ON! Turn it off before changing the pattern</source>
        <translation>Ramp/Soak encontrado Encendido. Se necesita apagar para cambiar el modo </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34096"/>
        <source>Ramp/Soak was found in Hold! Turn it off before changing the pattern</source>
        <translation>Ramp/Soak encontrado en espera. Apagalo antes de cambiar el modo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34439"/>
        <source>Activate PID front buttons</source>
        <translation>Activar botones PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34439"/>
        <source>Remember SV memory has a finite
life of ~10,000 writes.

Proceed?</source>
        <translation>Recuerda que PID memoria  tiene 
vida limitada de 10,000 escrituras

Continuar?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34548"/>
        <source>RS OFF</source>
        <translation>RS Apagado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34550"/>
        <source>RS on HOLD</source>
        <translation>RS en espera</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34611"/>
        <source>PXG sv#{0} set to {1}</source>
        <translation type="unfinished">PXG sv#{0} cambiado a {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34632"/>
        <source>PXR sv set to {0}</source>
        <translation type="unfinished">PXR sv cambiado a {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34667"/>
        <source>SV{0} changed from {1} to {2})</source>
        <translation type="unfinished">SV{0} cambiado de {1} a {2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34675"/>
        <source>Unable to set sv{0}</source>
        <translation type="unfinished">No se pudo cambiar sv {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34694"/>
        <source>Unable to set sv</source>
        <translation>No se pudo cambiar SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34696"/>
        <source>Unable to set new sv</source>
        <translation>No se pudo escribir SV nuevo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10144"/>
        <source>Exit Designer?</source>
        <translation>Salir del Diseñador?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10145"/>
        <source>Designer Mode ON</source>
        <translation>Diseñador ON</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="8931"/>
        <source>Extra Event Button Palette</source>
        <translation type="obsolete">Paleta de botones de eventos extra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31044"/>
        <source>&lt;b&gt;Temp:&lt;/b&gt; the speficied temperature limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2473"/>
        <source>Action canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17446"/>
        <source>Interpolation failed: no profile available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23082"/>
        <source>Playback Aid set ON at {0} secs</source>
        <translation type="unfinished">Ayuda de reproduccion ON a {0} segundos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23090"/>
        <source>No profile background found</source>
        <translation>No se pudo encontrar perfil de fondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23186"/>
        <source>Reading background profile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27430"/>
        <source>Tick the Float flag in this case.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28863"/>
        <source>Device not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11570"/>
        <source>To load this profile the extra devices configuration needs to be changed.
Continue?</source>
        <translation>Para cargar y ver este perfil,
 la configuracion de los dispositivos extra debe cambiar.
Continuar?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11571"/>
        <source>Found a different number of curves</source>
        <translation>El numero de curvas encontrado es differente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22726"/>
        <source>Background profile not found</source>
        <translation>No se pudo encontrar fondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10427"/>
        <source>&lt;b&gt;[CRTL N]&lt;/b&gt; = Autosave + Reset + START</source>
        <translation>&lt;b&gt;[CRTL N]&lt;/b&gt; =Autoguardar + Reinicio + START</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34546"/>
        <source>RS ON</source>
        <translation>RS ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34686"/>
        <source>SV changed from {0} to {1}</source>
        <translation type="unfinished">SV cambiado de {0} a {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32005"/>
        <source>Output status while ramp/soak operation set to OFF: {0}</source>
        <translation type="unfinished">Estado final mientras RampSoak cambiado a OFF: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22585"/>
        <source>Phases changed to {0} default: {1}</source>
        <translation type="unfinished">Fases cambiadas a {0} predeterminado: {1})</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10438"/>
        <source>&lt;b&gt;[f]&lt;/b&gt; = Full Screen Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15834"/>
        <source>Save Graph as PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25514"/>
        <source>Phidget Temperature Sensor 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25716"/>
        <source>Phidget Bridge 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31039"/>
        <source>&lt;b&gt;But Not:&lt;/b&gt; alarm triggered only if the alarm with the given number was not triggered before. Use 0 for no guard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5734"/>
        <source>Importing a profile in to Designer will decimate all data except the main [points].
Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31046"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button or the new value of the slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32966"/>
        <source>Load PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33050"/>
        <source>Save PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15123"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15123"/>
        <source>A tight sampling interval might lead to instability on some machines. We suggest a minimum of 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15108"/>
        <source>Oversampling is only active with a sampling interval equal or larger than 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16093"/>
        <source>current background ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16094"/>
        <source>current background BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10429"/>
        <source>&lt;b&gt;[d]&lt;/b&gt; = Toggle xy scale (T/Delta)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10431"/>
        <source>&lt;b&gt;[s]&lt;/b&gt; = Shows/Hides Event Sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25829"/>
        <source>Phidget 1018 IO attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35177"/>
        <source>Load Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35196"/>
        <source>Save Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35402"/>
        <source>PID turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35416"/>
        <source>PID turned off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10437"/>
        <source>&lt;b&gt;[q,w,e,r + &lt;i&gt;nn&lt;/i&gt;]&lt;/b&gt; = Quick Custom Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16077"/>
        <source>Return the minimum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16078"/>
        <source>Return the maximum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27418"/>
        <source>The MODBUS device corresponds to input channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27419"/>
        <source>1 and 2.. The MODBUS_34 extra device adds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27420"/>
        <source>input channels 3 and 4. Inputs with slave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27421"/>
        <source>id set to 0 are turned off. Modbus function 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27422"/>
        <source>&apos;read holding register&apos; is the standard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27423"/>
        <source>Modbus function 4 triggers the use of &apos;read </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27426"/>
        <source>2 byte integer registers. A temperature of 145.2C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27427"/>
        <source>is often sent as 1452. In that case you have to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27428"/>
        <source>use the symbolic assignment &apos;x/10&apos;. Few devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27429"/>
        <source>hold data as 4 byte floats in two registers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25962"/>
        <source>Yocto Thermocouple attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25964"/>
        <source>Yocto PT100 attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25419"/>
        <source>Phidget Temperature Sensor IR attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27424"/>
        <source>input register&apos;. Input registers (fct 4) usually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27425"/>
        <source> are from 30000-39999. Most devices hold data in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22261"/>
        <source>IO Command: set(n,0), set(n,1), toggle(n) to set Phidget IO digital output n</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16086"/>
        <source>ET value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16087"/>
        <source>BT value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16088"/>
        <source>Extra #1 T1 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16089"/>
        <source>Extra #1 T2 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16090"/>
        <source>Extra #2 T1 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16091"/>
        <source>Extra #2 T2 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16097"/>
        <source>Yn holds values sampled in the actual interval if refering to ET/BT or extra channels from devices listed before, otherwise Yn hold values sampled in the previous interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22259"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) or wcoils(slaveId,register,[&amp;lt;bool&amp;gt;,..,&amp;lt;bool&amp;gt;]) writes values to the registers in slaves specified by the given ids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22253"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-100) to be recorded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25423"/>
        <source>Phidget Temperature Sensor IR detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25518"/>
        <source>Phidget Temperature Sensor 4-input detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21461"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) or wcoils(slaveId,register,[&amp;lt;bool&amp;gt;,..,&amp;lt;bool&amp;gt;]) or wcoils(slaveId,register,&amp;lt;bool&amp;gt;) writes values to the registers in slaves specified by the given ids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25720"/>
        <source>Phidget Bridge 4-input detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25833"/>
        <source>Phidget 1018 IO detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15196"/>
        <source>Hottop control turned off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15207"/>
        <source>Hottop control turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15308"/>
        <source>Settings loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15368"/>
        <source>artisan-settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15369"/>
        <source>Save Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15372"/>
        <source>Settings saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1962"/>
        <source>Alarm {0} triggered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2008"/>
        <source>Calling alarm failed on {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2015"/>
        <source>Alarm trigger button error, description &apos;{0}&apos; not a number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2043"/>
        <source>Alarm trigger slider error, description &apos;{0}&apos; not a valid number [0-100]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4479"/>
        <source>[TP] recorded at {0} BT = {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15841"/>
        <source>{0} saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17637"/>
        <source>[ET target 1 = {0}] [BT target 1 = {1}] [ET target 2 = {2}] [BT target 2 = {3}]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29162"/>
        <source>Device set to {0}. Now, chose Modbus serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15220"/>
        <source>PID Standby ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15225"/>
        <source>PID Standby OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19188"/>
        <source>Alarms from events #{0} created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19190"/>
        <source>No event selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19257"/>
        <source> Events #{0} deleted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Radio Button</name>
    <message>
        <location filename="../artisanlib/main.py" line="27933"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="23500"/>
        <source>Arduino TC4</source>
        <translation type="obsolete">Arduino TC4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27935"/>
        <source>Program</source>
        <translation>Programa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27932"/>
        <source>Meter</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27934"/>
        <source>TC4</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Annotation</name>
    <message>
        <location filename="../artisanlib/main.py" line="878"/>
        <source>Damper</source>
        <translation>Regulador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="879"/>
        <source>Fan</source>
        <translation>Ventilador</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3031"/>
        <source>START 00:00</source>
        <translation type="obsolete">START 00:00</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4510"/>
        <source>DE {0}</source>
        <translation type="unfinished">SF {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4566"/>
        <source>FCs {0}</source>
        <translation type="unfinished">FCi {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4626"/>
        <source>FCe {0}</source>
        <translation type="unfinished">FCf {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4679"/>
        <source>SCs {0}</source>
        <translation type="unfinished">SCi {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4737"/>
        <source>SCe {0}</source>
        <translation type="unfinished">SCf {0}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="3369"/>
        <source>END {0}</source>
        <translation type="obsolete">FIN {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="877"/>
        <source>Heater</source>
        <translation>Calentador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="876"/>
        <source>Speed</source>
        <translation>Velocidad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4410"/>
        <source>CHARGE 00:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2822"/>
        <source>CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4471"/>
        <source>TP {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4799"/>
        <source>DROP {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4912"/>
        <source>CE {0}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Title</name>
    <message>
        <location filename="../artisanlib/main.py" line="11685"/>
        <source>Roaster Scope</source>
        <translation>Perfil de tueste</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../artisanlib/main.py" line="32349"/>
        <source>Ready</source>
        <translation>Listo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23085"/>
        <source>Playback Aid set OFF</source>
        <translation>Ayuda de reproduccion OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34173"/>
        <source>setting autotune...</source>
        <translation>AutoAfinamiento...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34200"/>
        <source>Autotune successfully turned OFF</source>
        <translation>AutoAfinamiento Apagado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34203"/>
        <source>Autotune successfully turned ON</source>
        <translation>AutoAfinamiento Encendido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34101"/>
        <source>wait...</source>
        <translation>Espera...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31844"/>
        <source>PID OFF</source>
        <translation>PID OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31847"/>
        <source>PID ON</source>
        <translation>PID ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31882"/>
        <source>Empty SV box</source>
        <translation>Caja SV vacia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31891"/>
        <source>Unable to read SV</source>
        <translation>No se pudo leer SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33996"/>
        <source>Ramp/Soak operation cancelled</source>
        <translation>Operacion RampSoak cancelada</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33999"/>
        <source>No RX data</source>
        <translation>No RX data</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34011"/>
        <source>Need to change pattern mode...</source>
        <translation>Se necesita cambiar el modo...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34020"/>
        <source>Pattern has been changed. Wait 5 secs.</source>
        <translation>Modo ha sido cambiado. Espera 5 segundos.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34023"/>
        <source>Pattern could not be changed</source>
        <translation>No se pudo cambiar modo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34054"/>
        <source>RampSoak could not be changed</source>
        <translation>No se pudo cambiar RampSoak </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34067"/>
        <source>RS successfully turned OFF</source>
        <translation>RS Apagado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32109"/>
        <source>setONOFFrampsoak(): Ramp Soak could not be set OFF</source>
        <translation>RampSoak no se pudo apagar en setONOFFrampsoak()</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="28671"/>
        <source>getallsegments(): problem reading R/S </source>
        <translation type="obsolete">problema leyendo R/S en getallsegments()</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34169"/>
        <source>Finished reading Ramp/Soak val.</source>
        <translation>Terminado leyendo valores RampSoak</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32204"/>
        <source>Finished reading pid values</source>
        <translation type="unfinished">Terminado leyendo valores pid</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32256"/>
        <source>setpid(): There was a problem setting {0}</source>
        <translation type="unfinished">problema cambiando pid setpid() {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33384"/>
        <source>SV{0} set to {1}</source>
        <translation type="unfinished">SV{0} cambiado a {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33388"/>
        <source>Problem setting SV</source>
        <translation>Problema cambiando SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33390"/>
        <source>Cancelled svN change</source>
        <translation>Cambio de svN cancelado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33408"/>
        <source>PID already using sv{0}</source>
        <translation type="unfinished">PId ya estaba usando sv{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33411"/>
        <source>setNsv(): bad response</source>
        <translation>respuesta mala en setNsv()</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33448"/>
        <source>setNpid(): bad confirmation</source>
        <translation>mala confirmacion ensetNpid()</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33452"/>
        <source>Cancelled pid change</source>
        <translation>Cambio de pid cancelado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33470"/>
        <source>PID was already using pid {0}</source>
        <translation type="unfinished">PID yas estaba usando pid{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33473"/>
        <source>setNpid(): Unable to set pid {0} </source>
        <translation type="unfinished">No se pudo cambiar pid{0} en setNpid()</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33550"/>
        <source>SV{0} successfully set to {1}</source>
        <translation type="unfinished">SV{0} cambiado a {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33676"/>
        <source>pid #{0} successfully set to ({1},{2},{3})</source>
        <translation type="unfinished">pid #{0} cambiado a ({1},{2},{3})</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33684"/>
        <source>pid command failed. Bad data at pid{0} (8,8,8): ({1},{2},{3}) </source>
        <translation type="unfinished">pid commando error. Data mala en  pid{0} (8,8,8): ({1},{2},{3}) </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33694"/>
        <source>sending commands for p{0} i{1} d{2}</source>
        <translation type="unfinished">Enviando comado para  p{0} i{1} d{2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33790"/>
        <source>PID is using pid = {0}</source>
        <translation type="unfinished">PID esta usando pid = {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33857"/>
        <source>PID is using SV = {0}</source>
        <translation type="unfinished">PID esta usando SV = {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34070"/>
        <source>Ramp Soak could not be set OFF</source>
        <translation>Ramp Soak no se pudo apagar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34111"/>
        <source>PID set to OFF</source>
        <translation>PID Apagado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34114"/>
        <source>PID set to ON</source>
        <translation>PID Encendido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34117"/>
        <source>Unable</source>
        <translation>No se puede hacer</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34121"/>
        <source>No data received</source>
        <translation>No data recibida</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34161"/>
        <source>Reading Ramp/Soak {0} ...</source>
        <translation type="unfinished">Leyendo Ramp/Soak {0} ...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34166"/>
        <source>problem reading Ramp/Soak</source>
        <translation>problema leyendo Ramp/Soak</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34182"/>
        <source>Current pid = {0}. Proceed with autotune command?</source>
        <translation type="unfinished">PID actual = {0}. Proceder con AutoAjuste?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34186"/>
        <source>Autotune cancelled</source>
        <translation>AutoAjuste cancelado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34205"/>
        <source>UNABLE to set Autotune</source>
        <translation>No se pudo cambiar AutoAjuste</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33140"/>
        <source>Decimal position successfully set to 1</source>
        <translation>Posicion decimal cambiada a 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33143"/>
        <source>Problem setting decimal position</source>
        <translation>Problema cambiando posicion decimal</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33191"/>
        <source>Thermocouple type successfully set</source>
        <translation>Tipo de Thermocouple cambiado a </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33194"/>
        <source>Problem setting thermocouple type</source>
        <translation>Problema cambiando tipo de thermocouple </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34280"/>
        <source>Ramp/Soak successfully written</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33103"/>
        <source>Time Units successfully set to MM:SS</source>
        <translation>Unidades de tiempo cambiadas a MM:SS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33106"/>
        <source>Problem setting time units</source>
        <translation>Problema cambiando las unidades de tiempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>Ramp (MM:SS)</source>
        <translation>Ramp (MM:SS)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>Soak (MM:SS)</source>
        <translation>Soak (MM:SS)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32135"/>
        <source>getsegment(): problem reading ramp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32148"/>
        <source>getsegment(): problem reading soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33558"/>
        <source>setsv(): Unable to set SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33762"/>
        <source>getallpid(): Unable to read pid values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33793"/>
        <source>getallpid(): Unable to read current sv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35278"/>
        <source>Work in Progress</source>
        <translation>Trabajo en progreso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31871"/>
        <source>SV successfully set to {0}</source>
        <translation type="unfinished">SV enviado satisfactoriamente a {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34037"/>
        <source>RS ON</source>
        <translation>RS ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34057"/>
        <source>RS OFF</source>
        <translation>RS Apagado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32247"/>
        <source>{0} successfully sent to pid </source>
        <translation type="unfinished">{0} enviado satisfactoriamente al pid</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33445"/>
        <source>pid changed to {0}</source>
        <translation type="unfinished">pid cambiado a {0}</translation>
    </message>
</context>
<context>
    <name>Tab</name>
    <message>
        <location filename="../artisanlib/main.py" line="16986"/>
        <source>HUD</source>
        <translation>HUD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16992"/>
        <source>Math</source>
        <translation>Matemáticas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21448"/>
        <source>Style</source>
        <translation>Estilo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35295"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18716"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23055"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23058"/>
        <source>Data</source>
        <translation>Datos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23052"/>
        <source>Config</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16989"/>
        <source>Plotter</source>
        <translation>Graficador</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29809"/>
        <source>Graph</source>
        <translation>Gráfico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29812"/>
        <source>LCDs</source>
        <translation>LCDs</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32945"/>
        <source>RS</source>
        <translation>RS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32948"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32954"/>
        <source>Set RS</source>
        <translation>Ajustar RS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35165"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32957"/>
        <source>Extra</source>
        <translation>Extra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21433"/>
        <source>Buttons</source>
        <translation>Botones</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28457"/>
        <source>ET/BT</source>
        <translation>ET/BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28460"/>
        <source>Extra Devices</source>
        <translation>Dispositivos Extra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28463"/>
        <source>Symb ET/BT</source>
        <translation>Simb ET/BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21436"/>
        <source>Sliders</source>
        <translation>Deslizadores</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21442"/>
        <source>Palettes</source>
        <translation>Paletas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27721"/>
        <source>Modbus</source>
        <translation>Modbus</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27724"/>
        <source>Scale</source>
        <translation>Escala</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16995"/>
        <source>UI</source>
        <translation>UI</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27727"/>
        <source>Color</source>
        <translation type="unfinished">Color</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21439"/>
        <source>Quantifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35169"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28466"/>
        <source>Phidgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22430"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22434"/>
        <source>Espresso</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Table</name>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>Abs Time</source>
        <translation type="obsolete">Tiempo absoluto</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>Rel Time</source>
        <translation type="obsolete">Tiempo relativo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>DeltaBT (d/m)</source>
        <translation type="obsolete">DeltaBT (g/m)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18675"/>
        <source>DeltaET (d/m)</source>
        <translation type="obsolete">DeltaET (g/m)</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18713"/>
        <source>{0} START</source>
        <translation type="obsolete">{0} INICIO</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18717"/>
        <source>{0} DRY END</source>
        <translation type="obsolete">{0} FIN SECADO</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18722"/>
        <source>{0} FC START</source>
        <translation type="obsolete">{0} INICIO FC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18726"/>
        <source>{0} FC END</source>
        <translation type="obsolete">{0} FIN SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18730"/>
        <source>{0} SC START</source>
        <translation type="obsolete">{0} INICIO SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18734"/>
        <source>{0} SC END</source>
        <translation type="obsolete">{0} FIN SC</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18738"/>
        <source>{0} END</source>
        <translation type="obsolete">{0} FIN</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18743"/>
        <source>{0} EVENT #{1} {2}{3}</source>
        <translation type="obsolete">{0} EVENTO #{1} {2}{3}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Time</source>
        <translation>Tiempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23212"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23212"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Action</source>
        <translation>Accion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Comm Port</source>
        <translation>Puerto Serial</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Baud Rate</source>
        <translation>Flujo Baudios</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Byte Size</source>
        <translation>Tamaño de Byte</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Parity</source>
        <translation>Paridad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Stopbits</source>
        <translation>Stopbits</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Timeout</source>
        <translation>Tiempo muerto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Label 1</source>
        <translation>Etiqueta 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Label 2</source>
        <translation>Etiqueta 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Label</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Parent</source>
        <translation>Padre</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Width</source>
        <translation>Grosor</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Opaqueness</source>
        <translation>Opacidad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Delete Wheel</source>
        <translation>Borrar Rueda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Edit Labels</source>
        <translation>Editar Etiquetas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Update Labels</source>
        <translation>Actualizar Etiquetas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Properties</source>
        <translation>Propiedades</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Radius</source>
        <translation>Radio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Starting angle</source>
        <translation>Angulo inicial</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Color Pattern</source>
        <translation>Paleta de Color</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>From</source>
        <translation>De</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Source</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>Ramp HH:MM</source>
        <translation>Rampa HH:MM</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>Soak HH:MM</source>
        <translation>Soak HH:MM</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Documentation</source>
        <translation>Documentación</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Visibility</source>
        <translation>Visibilidad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Color 1</source>
        <translation>Color 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Color 2</source>
        <translation>Color 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>y1(x)</source>
        <translation>y1(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>y2(x)</source>
        <translation>y2(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Text Color</source>
        <translation>Color de Texto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>LCD 1</source>
        <translation>LCD 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>LCD 2</source>
        <translation>LCD 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Curve 1</source>
        <translation>Curva 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Curve 2</source>
        <translation>Curva 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>If Alarm</source>
        <translation>Si Alamrma</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Condition</source>
        <translation>Condición</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Projection</source>
        <translation>Proyección</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Text Size</source>
        <translation>Tamaño de Texto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Temp</source>
        <translation>Temp</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="13822"/>
        <source>START</source>
        <translation type="obsolete">INICIO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23301"/>
        <source>DRY END</source>
        <translation>SECO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23304"/>
        <source>FC START</source>
        <translation>FC START</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23307"/>
        <source>FC END</source>
        <translation>FC FIN</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23310"/>
        <source>SC START</source>
        <translation>SC START</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23313"/>
        <source>SC END</source>
        <translation>SC FIN</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23319"/>
        <source>COOL</source>
        <translation>ENFRIAR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19450"/>
        <source>EVENT #{1} {2}{3}</source>
        <translation type="obsolete">EVENTO #{1} {2}{3}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23316"/>
        <source>DROP</source>
        <translation>DESCAR</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23298"/>
        <source>CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>But Not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>DeltaET</source>
        <translation type="unfinished">DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>DeltaBT</source>
        <translation type="unfinished">DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23323"/>
        <source>EVENT #{0} {1}{2}</source>
        <translation type="unfinished">EVENTO #{0} {1}{2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Beep</source>
        <translation type="unfinished">Pitido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19680"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19680"/>
        <source>Weight</source>
        <translation type="unfinished">Peso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Nr</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Textbox</name>
    <message>
        <location filename="../artisanlib/main.py" line="560"/>
        <source>Acidity</source>
        <translation>Acidez</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="518"/>
        <source>Clean Cup</source>
        <translation>Limpieza de la Taza</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="481"/>
        <source>Head</source>
        <translation>Cabeza</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="508"/>
        <source>Fragance</source>
        <translation>Fragancia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="550"/>
        <source>Sweetness</source>
        <translation>Dulzura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="542"/>
        <source>Aroma</source>
        <translation>Aroma</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="565"/>
        <source>Balance</source>
        <translation>Equilibrio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="559"/>
        <source>Body</source>
        <translation>Cuerpo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="491"/>
        <source>Sour</source>
        <translation>Agrio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="536"/>
        <source>Flavor</source>
        <translation>Sabor</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="493"/>
        <source>Critical
Stimulus</source>
        <translation>Estímulo
Crítico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="546"/>
        <source>Aftertaste</source>
        <translation>Regusto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="495"/>
        <source>Bitter</source>
        <translation>Amargo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="496"/>
        <source>Astringency</source>
        <translation>Astringencia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="497"/>
        <source>Solubles
Concentration</source>
        <translation>Concentration
Solubles</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="544"/>
        <source>Mouthfeel</source>
        <translation>Sensación en boca</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="499"/>
        <source>Other</source>
        <translation>Otro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="500"/>
        <source>Aromatic
Complexity</source>
        <translation>Complejidad
Aromática</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="501"/>
        <source>Roast
Color</source>
        <translation>Color
Tueste</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="502"/>
        <source>Aromatic
Pungency</source>
        <translation>Acritud
Aromática</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="503"/>
        <source>Sweet</source>
        <translation>Dulce</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="505"/>
        <source>pH</source>
        <translation>pH</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="515"/>
        <source>Dry Fragrance</source>
        <translation>Fragancia Seca</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="516"/>
        <source>Uniformity</source>
        <translation>Uniformidad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="517"/>
        <source>Complexity</source>
        <translation>Complejidad</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="523"/>
        <source>Brightness</source>
        <translation>Brillo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="524"/>
        <source>Wet Aroma</source>
        <translation>Aroma Húmedo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="558"/>
        <source>Fragrance</source>
        <translation>Fragancia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="528"/>
        <source>Taste</source>
        <translation>Gusto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="529"/>
        <source>Nose</source>
        <translation>Nariz</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="534"/>
        <source>Fragrance-Aroma</source>
        <translation>Fragancia-Aroma</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="545"/>
        <source>Flavour</source>
        <translation>Sabor</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="553"/>
        <source>Finish</source>
        <translation>Finalización</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="555"/>
        <source>Roast Color</source>
        <translation>Color del Tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="556"/>
        <source>Crema Texture</source>
        <translation>Textura de la Crema</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="557"/>
        <source>Crema Volume</source>
        <translation>Volumen de la Crema</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="561"/>
        <source>Bitterness</source>
        <translation>Amargura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="562"/>
        <source>Defects</source>
        <translation>Defectos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="563"/>
        <source>Aroma Intensity</source>
        <translation>Intensidad del Aroma</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="564"/>
        <source>Aroma Persistence</source>
        <translation>Persistencia del Aroma</translation>
    </message>
</context>
<context>
    <name>Tooltip</name>
    <message>
        <location filename="../artisanlib/main.py" line="8256"/>
        <source>Marks the begining of the roast (beans in)</source>
        <translation type="obsolete">Marca el inicio del tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8264"/>
        <source>Marks the end of the roast (drop beans)</source>
        <translation type="obsolete">Marca el final del tostado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8279"/>
        <source>Marks an Event</source>
        <translation type="obsolete">Marca un evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8369"/>
        <source>Increases the current SV value by 5</source>
        <translation>Incrementa SV por 5 grados</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8377"/>
        <source>Increases the current SV value by 10</source>
        <translation>Incrementa SV por 10 grados</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8385"/>
        <source>Increases the current SV value by 20</source>
        <translation>Incrementa SV por 20 grados</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8393"/>
        <source>Decreases the current SV value by 20</source>
        <translation>Dismunuye SV por 20 grados</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8401"/>
        <source>Decreases the current SV value by 10</source>
        <translation>Dismunuye SV por 10 grados</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8409"/>
        <source>Decreases the current SV value by 5</source>
        <translation>Dismunuye SV por 5 grados</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8423"/>
        <source>Turns ON/OFF the HUD</source>
        <translation>Enciende Apaga HUD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8494"/>
        <source>Timer</source>
        <translation>Cronometro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8495"/>
        <source>ET Temperature</source>
        <translation>Temperatura ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8496"/>
        <source>BT Temperature</source>
        <translation>Temperatura BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8497"/>
        <source>ET/time (degrees/min)</source>
        <translation>ET/Tiempo (grados/minuto)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8498"/>
        <source>BT/time (degrees/min)</source>
        <translation>BT/Tiempo (grados/minuto)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8499"/>
        <source>Value of SV in PID</source>
        <translation>Valores de SV en PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8579"/>
        <source>Number of events found</source>
        <translation>Numero de eventos encontrados</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8589"/>
        <source>Type of event</source>
        <translation>Tipos de eventos</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8595"/>
        <source>Value of event</source>
        <translation>Valor de Evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8607"/>
        <source>Updates the event</source>
        <translation>Actualiza el evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16783"/>
        <source>linear: linear interpolation
cubic: 3rd order spline interpolation
nearest: y value of the nearest point</source>
        <translation>Lineal: Interpolacion lineal
Cubica: Interpolacion cubica de tercer grado
Cercana: valor y del punto cercano</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19912"/>
        <source>Automatic generated name = This text + date + time</source>
        <translation>Nombre generado automaticamente = Texto + fecha + tiempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20618"/>
        <source>Allows to enter a description of the last event</source>
        <translation>Permite escribir una descripcion en el ultimo evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8500"/>
        <source>PID power %</source>
        <translation>PID potencia %</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30219"/>
        <source>Save image using current graph size to a png format</source>
        <translation>Guarda Imagen usando el tamaño de la grafica en fomato png</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20837"/>
        <source>Add new extra Event button</source>
        <translation>Añade boton de Evento extra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20842"/>
        <source>Delete the last extra Event button</source>
        <translation>Borra el ultimo boton de Evento extra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30760"/>
        <source>Show help</source>
        <translation>Mostrar Ayuda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28598"/>
        <source>Example: 100 + 2*x</source>
        <translation>Ejemplo: 100 + 2*x</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28599"/>
        <source>Example: 100 + x</source>
        <translation>Ejemplo: 100 + x</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30161"/>
        <source>Erases wheel parent hierarchy</source>
        <translation>Borra herarquia padre</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30165"/>
        <source>Sets graph hierarchy child-&gt;parent instead of parent-&gt;child</source>
        <translation>Cambia herarquia hijo&gt;padre a padre&gt;hijo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30179"/>
        <source>Increase size of text in all the graph</source>
        <translation>Incrementa el tamaño del texto en toda la grafica</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30182"/>
        <source>Decrease size of text in all the graph</source>
        <translation>Disminuye el tamaño del texto en todo la grafica</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30186"/>
        <source>Decorative edge beween wheels</source>
        <translation>Borde decorativo entre ruedas</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30192"/>
        <source>Line thickness</source>
        <translation>Grosor de linea</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30197"/>
        <source>Line color</source>
        <translation>Color de linea</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30201"/>
        <source>Apply color pattern to whole graph</source>
        <translation>Aplica modo de color en toda la grafica</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30207"/>
        <source>Add new wheel</source>
        <translation>Añade nueva rueda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30210"/>
        <source>Rotate graph 1 degree counter clockwise</source>
        <translation>Gira la grafica 1 grado contra reloj</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30213"/>
        <source>Rotate graph 1 degree clockwise</source>
        <translation>Gira la grafica 1 grado </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30217"/>
        <source>Save graph to a text file.wg</source>
        <translation>Guarda la grafica a ficha de texto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30225"/>
        <source>open graph file.wg</source>
        <translation>Abre grafica ficha.wg</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30228"/>
        <source>Close wheel graph editor</source>
        <translation>Cierra editor de grafica de ruedas </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30222"/>
        <source>Sets Wheel graph to view mode</source>
        <translation>pone la Grafica de Rueda en modo normal</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19822"/>
        <source>ON/OFF logs serial communication</source>
        <translation>ON/OFF grabador communicaion serial</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30172"/>
        <source>Aspect Ratio</source>
        <translation>Proporción</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20880"/>
        <source>Backup all palettes to a text file</source>
        <translation>Guarda todas las paletas a una ficha de texto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19915"/>
        <source>ON/OFF of automatic saving when pressing keyboard letter [a]</source>
        <translation>Enciende Apaga AutoGuardar con la tecla [a]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19926"/>
        <source>Sets the directory to store batch profiles when using the letter [a]</source>
        <translation>Guarda el directorio para guardar cuando se usa la tecla [a]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21212"/>
        <source>Action Type</source>
        <translation>Tipos de Accion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21217"/>
        <source>Action String</source>
        <translation>Comando de Accion</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16155"/>
        <source>&lt;b&gt;Label&lt;/b&gt;= </source>
        <translation>&lt;b&gt;Etiqueta&lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16156"/>
        <source>&lt;b&gt;Description &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Descripcion &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16158"/>
        <source>&lt;b&gt;Type &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Tipo &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16159"/>
        <source>&lt;b&gt;Value &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Valor &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16160"/>
        <source>&lt;b&gt;Documentation &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Documentacion &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16161"/>
        <source>&lt;b&gt;Button# &lt;/b&gt;= </source>
        <translation>&lt;b&gt;Boton# &lt;/b&gt;= </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8214"/>
        <source>Marks the begining of First Crack (FCs)</source>
        <translation type="obsolete">Marca el inicio del primer chasquido FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8221"/>
        <source>Marks the end of First Crack (FCs)</source>
        <translation type="obsolete">Marca el final del primer chasquido FC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8228"/>
        <source>Marks the begining of Second Crack (SCs)</source>
        <translation type="obsolete">Marca el inicio del segundo chasquido SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8235"/>
        <source>Marks the end of Second Crack (SCe)</source>
        <translation type="obsolete">Marca el final del segundo chasquido SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8351"/>
        <source>Marks the end of the Drying phase (DRYEND)</source>
        <translation type="obsolete">Marca el final de la fase SECO (SECO-FIN)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8359"/>
        <source>Marks the end of the Cooling phase (COOLEND)</source>
        <translation type="obsolete">Marca el final del enfriado</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4158"/>
        <source>Stop monitoring</source>
        <translation>Parar el monitoreo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8267"/>
        <source>Start monitoring</source>
        <translation>Iniciar el monitoreo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4314"/>
        <source>Stop recording</source>
        <translation>Parar de grabar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8280"/>
        <source>Start recording</source>
        <translation>Comienza a grabar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8329"/>
        <source>Reset</source>
        <translation>Reinicializar</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20881"/>
        <source>Restore all palettes from a text file</source>
        <translation>Restaurar todas las paletas desde fichero de texto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30765"/>
        <source>Clear alarms table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21219"/>
        <source>Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19974"/>
        <source>Batch prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19989"/>
        <source>ON/OFF batch counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8295"/>
        <source>First Crack Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8302"/>
        <source>First Crack End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8309"/>
        <source>Second Crack Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8316"/>
        <source>Second Crack End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8337"/>
        <source>Charge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8345"/>
        <source>Drop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8360"/>
        <source>Event</source>
        <translation type="unfinished">Evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8432"/>
        <source>Dry End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8440"/>
        <source>Cool End</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
