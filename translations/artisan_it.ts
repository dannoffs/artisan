<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="it" sourcelanguage="">
<context>
    <name>About</name>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>About</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>Core developers:</source>
        <translation>Sviluppatori:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>Contributors:</source>
        <translation>Collaboratori:</translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="../artisanlib/main.py" line="31048"/>
        <source>Help</source>
        <translation>Aiuto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30475"/>
        <source>Update</source>
        <translation>Aggiornamento</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="14499"/>
        <source>PID Help</source>
        <translation type="obsolete">Aiuto PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35151"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32672"/>
        <source>Cancel</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16727"/>
        <source>Plot</source>
        <translation>Disegna</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29647"/>
        <source>Background</source>
        <translation>Sfondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16707"/>
        <source>Virtual Device</source>
        <translation type="obsolete">Dispositivo virtuale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22624"/>
        <source>Save Image</source>
        <translation>Salva immagine</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16792"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35003"/>
        <source>Set</source>
        <translation>Imposta</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29602"/>
        <source>Defaults</source>
        <translation>Predefiniti</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18154"/>
        <source>Order</source>
        <translation>Ordine</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30736"/>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30745"/>
        <source>Delete</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18465"/>
        <source>in</source>
        <translation>Entrata</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18470"/>
        <source>out</source>
        <translation>Uscita</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19770"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19923"/>
        <source>Path</source>
        <translation>Percorso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22621"/>
        <source>Del</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35128"/>
        <source>Load</source>
        <translation>Carica</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22861"/>
        <source>Align</source>
        <translation>Allinea</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22932"/>
        <source>Up</source>
        <translation>Su</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22934"/>
        <source>Down</source>
        <translation>Giù</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22936"/>
        <source>Left</source>
        <translation>Sinistra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22938"/>
        <source>Right</source>
        <translation>Destra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28050"/>
        <source>Reset</source>
        <translation>Azzera</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31356"/>
        <source>Close</source>
        <translation>Chiude</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26765"/>
        <source>Create</source>
        <translation>Crea</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30477"/>
        <source>Select</source>
        <translation>Seleziona</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29491"/>
        <source>Grid</source>
        <translation>Griglia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29498"/>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29505"/>
        <source>Y Label</source>
        <translation>Etichetta Y</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29512"/>
        <source>X Label</source>
        <translation>Etichetta X</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29547"/>
        <source>ET</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29554"/>
        <source>BT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29561"/>
        <source>DeltaET</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29568"/>
        <source>DeltaBT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29575"/>
        <source>Markers</source>
        <translation>Segnali</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29582"/>
        <source>Text</source>
        <translation>Testo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29589"/>
        <source>Watermarks</source>
        <translation>Indicatoridi livello</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29596"/>
        <source>C Lines</source>
        <translation>Linee C</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29605"/>
        <source>Grey</source>
        <translation>Grigio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29659"/>
        <source>LED</source>
        <translation>LED</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29691"/>
        <source>B/W</source>
        <translation type="unfinished">B/W</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30160"/>
        <source>Reset Parents</source>
        <translation type="unfinished">Azzera fonti</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30164"/>
        <source>Reverse Hierarchy</source>
        <translation type="unfinished">Ordine di inversione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30178"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30181"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30196"/>
        <source>Line Color</source>
        <translation>Colore linea</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30209"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30212"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30215"/>
        <source>Save File</source>
        <translation>Salva file</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30218"/>
        <source>Save Img</source>
        <translation>Salva immagine</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30221"/>
        <source>View Mode</source>
        <translation>Modo Visualizzazione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30224"/>
        <source>Open</source>
        <translation>Apre</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30500"/>
        <source>Set Color</source>
        <translation>Impostazione colore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30730"/>
        <source>All On</source>
        <translation>Tutto acceso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30733"/>
        <source>All Off</source>
        <translation>Tutto spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31351"/>
        <source>Read Ra/So values</source>
        <translation type="unfinished">Valori lettura Ra/So</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32413"/>
        <source>RampSoak ON</source>
        <translation type="unfinished">Temperatura a rampa ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32415"/>
        <source>RampSoak OFF</source>
        <translation type="unfinished">Temperatura a rampa OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32418"/>
        <source>PID OFF</source>
        <translation type="unfinished">PID spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32420"/>
        <source>PID ON</source>
        <translation type="unfinished">PID acceso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32535"/>
        <source>Write SV</source>
        <translation type="unfinished">Scrittura valori di salvataggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31388"/>
        <source>Set p</source>
        <translation>Imposta p</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31389"/>
        <source>Set i</source>
        <translation>Imposta i</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31390"/>
        <source>Set d</source>
        <translation>Imposta d</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32668"/>
        <source>Autotune ON</source>
        <translation>Autoadattamento ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32670"/>
        <source>Autotune OFF</source>
        <translation>Autoadattamento OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35282"/>
        <source>Read</source>
        <translation>Legge</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32798"/>
        <source>Set ET PID to 1 decimal point</source>
        <translation>Imposta ET PID in decimali</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32800"/>
        <source>Set BT PID to 1 decimal point</source>
        <translation>Imposta BT PID in decimali</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32409"/>
        <source>Read RS values</source>
        <translation type="unfinished">Lettura valori temperatura a rampa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32471"/>
        <source>Write SV1</source>
        <translation type="unfinished">Scrittura valori settaggio 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32473"/>
        <source>Write SV2</source>
        <translation type="unfinished">Scrittura valori settaggio 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32475"/>
        <source>Write SV3</source>
        <translation type="unfinished">Scrittura valori settaggio 3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32477"/>
        <source>Write SV4</source>
        <translation type="unfinished">Scrittura valori settaggio 4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32479"/>
        <source>Write SV5</source>
        <translation type="unfinished">Scrittura valori settaggio 5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32481"/>
        <source>Write SV6</source>
        <translation type="unfinished">Scrittura valori settaggio 6</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32483"/>
        <source>Write SV7</source>
        <translation type="unfinished">Scrittura valori settaggio 7</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32537"/>
        <source>ON SV buttons</source>
        <translation type="unfinished">Tasti ON valori settaggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32540"/>
        <source>OFF SV buttons</source>
        <translation type="unfinished">Tasti OFF valori settaggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32543"/>
        <source>Read SV (7-0)</source>
        <translation type="unfinished">Legge valori settaggio (7-0)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32650"/>
        <source>pid 1</source>
        <translation>PID 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32652"/>
        <source>pid 2</source>
        <translation>PID 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32654"/>
        <source>pid 3</source>
        <translation>PID 3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32656"/>
        <source>pid 4</source>
        <translation>PID 4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32658"/>
        <source>pid 5</source>
        <translation>PID 5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32660"/>
        <source>pid 6</source>
        <translation>PID 6</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32662"/>
        <source>pid 7</source>
        <translation>PID 7</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32802"/>
        <source>Set ET PID to MM:SS time units</source>
        <translation>Imposta ET PID in MM:SS</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35131"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8265"/>
        <source>ON</source>
        <translation>ACCESO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8278"/>
        <source>START</source>
        <translation>INIZIO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4157"/>
        <source>OFF</source>
        <translation>SPENTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8291"/>
        <source>FC
START</source>
        <translation>INIZIO
PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8298"/>
        <source>FC
END</source>
        <translation>FINE
PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8305"/>
        <source>SC
START</source>
        <translation>INIZIO
SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8312"/>
        <source>SC
END</source>
        <translation>FINE
SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8320"/>
        <source>RESET</source>
        <translation>AZZERA</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8333"/>
        <source>CHARGE</source>
        <translation>CARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8341"/>
        <source>DROP</source>
        <translation>SCARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8349"/>
        <source>Control</source>
        <translation>Controllo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8356"/>
        <source>EVENT</source>
        <translation>EVENTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8364"/>
        <source>SV +5</source>
        <translation>IV +5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8372"/>
        <source>SV +10</source>
        <translation>IV +10</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8380"/>
        <source>SV +20</source>
        <translation>IV +20</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8388"/>
        <source>SV -20</source>
        <translation>IV -20</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8396"/>
        <source>SV -10</source>
        <translation>IV -10</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8404"/>
        <source>SV -5</source>
        <translation>IV -5</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8412"/>
        <source>HUD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8427"/>
        <source>DRY
END</source>
        <translation>ASCIUTTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8436"/>
        <source>COOL
END</source>
        <translation>RAFFREDDATO</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19948"/>
        <source>Transfer To</source>
        <translation type="obsolete">Trasferimento a </translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="19950"/>
        <source>Restore From</source>
        <translation type="obsolete">Ripristino da</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31367"/>
        <source>SV Buttons ON</source>
        <translation>Inserimento tasti VI</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31369"/>
        <source>SV Buttons OFF</source>
        <translation type="unfinished">Disinserimento tasti VI</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31371"/>
        <source>Read SV</source>
        <translation type="unfinished">Legge IV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31405"/>
        <source>Read PID Values</source>
        <translation type="unfinished">Legge valori PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35283"/>
        <source>Write</source>
        <translation type="unfinished">Scrive</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29519"/>
        <source>Drying Phase</source>
        <translation>Fase asciugatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29526"/>
        <source>Maillard Phase</source>
        <translation>Fase di Maillard</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29533"/>
        <source>Development Phase</source>
        <translation>Fase sviluppo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29540"/>
        <source>Cooling Phase</source>
        <translation>Fase raffreddamento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16724"/>
        <source>Color</source>
        <translation>Colore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18480"/>
        <source>scan</source>
        <translation>Scansione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21073"/>
        <source>Apply</source>
        <translation>Applica</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30740"/>
        <source>Insert</source>
        <translation type="unfinished">Inserimento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30764"/>
        <source>Clear</source>
        <translation type="unfinished">Pulisce</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32405"/>
        <source>Write All</source>
        <translation>Scrive tutto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32411"/>
        <source>Write RS values</source>
        <translation>Scrittura valori temperatura a rampa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32545"/>
        <source>Write SV (7-0)</source>
        <translation>Scrittura IV (7-0)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32664"/>
        <source>Read PIDs</source>
        <translation>Lettura PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32666"/>
        <source>Write PIDs</source>
        <translation>Scrittura PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35153"/>
        <source>On</source>
        <translation>Acceso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35155"/>
        <source>Off</source>
        <translation>Spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18490"/>
        <source>calc</source>
        <translation>Calcola</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17692"/>
        <source>unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20861"/>
        <source>&lt;&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20863"/>
        <source>&gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18149"/>
        <source>Create Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16738"/>
        <source>Device</source>
        <translation type="obsolete">Dispositivo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16733"/>
        <source>BT/ET</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="22847"/>
        <source>DeltaET</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22848"/>
        <source>DeltaBT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16504"/>
        <source>Projection</source>
        <translation>Proiezione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22844"/>
        <source>Show</source>
        <translation>Mostra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16921"/>
        <source>Beep</source>
        <translation type="unfinished">Trillo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18140"/>
        <source>Delete roast properties on RESET</source>
        <translation type="unfinished">Cancella proprietà tostatura su RESET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19821"/>
        <source>Serial Log ON/OFF</source>
        <translation>Registro seriale ON/OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19913"/>
        <source>Autosave [a]</source>
        <translation>Salva automaticamente [a]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20608"/>
        <source>Button</source>
        <translation>Tasto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20617"/>
        <source>Mini Editor</source>
        <translation type="unfinished">Mini annotatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21122"/>
        <source>CHARGE</source>
        <translation>CARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26565"/>
        <source>DRY END</source>
        <translation>ASCIUTTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26567"/>
        <source>FC START</source>
        <translation>INIZIO PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26569"/>
        <source>FC END</source>
        <translation>FINE PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26571"/>
        <source>SC START</source>
        <translation>INIZIO SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26573"/>
        <source>SC END</source>
        <translation>FINE SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21176"/>
        <source>DROP</source>
        <translation>SCARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21185"/>
        <source>COOL END</source>
        <translation>RAFFREDDATO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22324"/>
        <source>Auto Adjusted</source>
        <translation type="unfinished">Auto aggiustamento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22629"/>
        <source>Background</source>
        <translation type="unfinished">Sfondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22845"/>
        <source>Text</source>
        <translation>Testo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22846"/>
        <source>Events</source>
        <translation>Eventi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23358"/>
        <source>Time</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23359"/>
        <source>Bar</source>
        <translation type="unfinished">Barra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23361"/>
        <source>ETBTa</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23362"/>
        <source>Evaluation</source>
        <translation type="unfinished">Valutazione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23363"/>
        <source>Characteristics</source>
        <translation>Caratteristiche</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27906"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27909"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22968"/>
        <source>Playback Aid</source>
        <translation>Aiuto riproduzione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18413"/>
        <source>Heavy FC</source>
        <translation>PC forte</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18416"/>
        <source>Low FC</source>
        <translation>PC leggero</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18419"/>
        <source>Light Cut</source>
        <translation type="unfinished">Taglio chiaro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18422"/>
        <source>Dark Cut</source>
        <translation type="unfinished">Taglio scuro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18425"/>
        <source>Drops</source>
        <translation type="unfinished">Gocce</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18428"/>
        <source>Oily</source>
        <translation>Oleoso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18431"/>
        <source>Uneven</source>
        <translation>Non omogeneo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18433"/>
        <source>Tipping</source>
        <translation>Tipping</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18435"/>
        <source>Scorching</source>
        <translation>Scorching</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18437"/>
        <source>Divots</source>
        <translation type="unfinished">Vuoti</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16470"/>
        <source>Drop Spikes</source>
        <translation type="unfinished">Picco acuto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16460"/>
        <source>Smooth Spikes</source>
        <translation type="unfinished">Picco morbido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16475"/>
        <source>Limits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22327"/>
        <source>Watermarks</source>
        <translation type="unfinished">Indicatori di livello</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20123"/>
        <source>Lock Max</source>
        <translation type="unfinished">chiusa massima</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16465"/>
        <source>Smooth2</source>
        <translation type="unfinished">Morbido2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16594"/>
        <source>Decimal Places</source>
        <translation type="unfinished">Punti decimali</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20806"/>
        <source>Auto CHARGE</source>
        <translation>Auto CARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20809"/>
        <source>Auto DROP</source>
        <translation>Auto SCARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20812"/>
        <source>Mark TP</source>
        <translation>Segnale punto di minimo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22329"/>
        <source>Phases LCDs</source>
        <translation>Fasi LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22331"/>
        <source>Auto DRY</source>
        <translation>Auto ASCIUTTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22333"/>
        <source>Auto FCs</source>
        <translation type="unfinished">Auto iPC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27979"/>
        <source>Modbus Port</source>
        <translation>Porta Modbus</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30768"/>
        <source>Load alarms from profile</source>
        <translation>Carica allarmi da profilo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35084"/>
        <source>Start PID on CHARGE</source>
        <translation type="unfinished">Inizio PID su carico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35139"/>
        <source>Load Ramp/Soak table from profile</source>
        <translation type="unfinished">Carica temperatura a rampa da profilo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16948"/>
        <source>Alarm Popups</source>
        <translation type="unfinished">Popup allarmi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20126"/>
        <source>Lock</source>
        <translation type="unfinished">Blocco</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22849"/>
        <source>Align FCs</source>
        <translation type="unfinished">Allinea iPC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23360"/>
        <source>/min</source>
        <translation>/min</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28012"/>
        <source>Control Button</source>
        <translation>Tasto controllo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28243"/>
        <source>Ratiometric</source>
        <translation>Raziometrico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19987"/>
        <source>Batch Counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20611"/>
        <source>Annotations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComboBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>None</source>
        <translation>Nulla</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1016"/>
        <source>Power</source>
        <translation>Potenza</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1017"/>
        <source>Damper</source>
        <translation type="unfinished">Smorzatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1018"/>
        <source>Fan</source>
        <translation type="unfinished">Ventilatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>linear</source>
        <translation>Lineare</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16506"/>
        <source>newton</source>
        <translation>Newton</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17614"/>
        <source>metrics</source>
        <translation type="unfinished">Metrico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17616"/>
        <source>thermal</source>
        <translation>Termico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>cubic</source>
        <translation>Cubico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>nearest</source>
        <translation type="unfinished">Più vicino</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20399"/>
        <source>g</source>
        <translation>g</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20400"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19381"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18317"/>
        <source>l</source>
        <translation>l</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20101"/>
        <source>upper right</source>
        <translation>Superiore destro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20102"/>
        <source>upper left</source>
        <translation>Superiore sinistro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20103"/>
        <source>lower left</source>
        <translation>Inferiore sinistro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20104"/>
        <source>lower right</source>
        <translation>Inferiore destro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20105"/>
        <source>right</source>
        <translation>Destra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20106"/>
        <source>center left</source>
        <translation>Centro sinistra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20107"/>
        <source>center right</source>
        <translation>Centro destra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20108"/>
        <source>lower center</source>
        <translation type="unfinished">Centro in basso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20109"/>
        <source>upper center</source>
        <translation type="unfinished">Centro sopra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20110"/>
        <source>center</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="18769"/>
        <source>30 seconds</source>
        <translation type="obsolete">30 secondi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20133"/>
        <source>1 minute</source>
        <translation>1 minuto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20134"/>
        <source>2 minute</source>
        <translation>2 minuti</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20135"/>
        <source>3 minute</source>
        <translation>3 minuti</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20136"/>
        <source>4 minute</source>
        <translation>4 minuti</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20137"/>
        <source>5 minute</source>
        <translation>5 minuti</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20163"/>
        <source>solid</source>
        <translation type="unfinished">Solido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20164"/>
        <source>dashed</source>
        <translation type="unfinished">Tratteggiato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20165"/>
        <source>dashed-dot</source>
        <translation type="unfinished">Dot tratteggiato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20166"/>
        <source>dotted</source>
        <translation type="unfinished">Punteggiato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20379"/>
        <source>Event #0</source>
        <translation>Evento#0</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20381"/>
        <source>Event #{0}</source>
        <translation type="unfinished">Evento #{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20401"/>
        <source>lb</source>
        <translation>Libbre</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20422"/>
        <source>liter</source>
        <translation>Litri</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20423"/>
        <source>gallon</source>
        <translation>Galloni</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20424"/>
        <source>quart</source>
        <translation type="unfinished">Quart</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20425"/>
        <source>pint</source>
        <translation>Pinta</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20426"/>
        <source>cup</source>
        <translation type="unfinished">Coppa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20427"/>
        <source>cm^3</source>
        <translation type="unfinished">Cm^3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20623"/>
        <source>Type</source>
        <translation type="unfinished">Tipo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20624"/>
        <source>Value</source>
        <translation>Valore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Serial Command</source>
        <translation>Comando seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Modbus Command</source>
        <translation type="unfinished">Comando Modbus</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>DTA Command</source>
        <translation type="unfinished">Comando DTA</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Call Program</source>
        <translation type="unfinished">Programma chiama</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>OFF</source>
        <translation>Spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>ON</source>
        <translation>Acceso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Multiple Event</source>
        <translation>Evento multiplo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29612"/>
        <source>grey</source>
        <translation>Grigio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29613"/>
        <source>Dark Grey</source>
        <translation>Grigio scuro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29614"/>
        <source>Slate Grey</source>
        <translation>Grigio ardesia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29615"/>
        <source>Light Gray</source>
        <translation>Grigio chiaro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29616"/>
        <source>Black</source>
        <translation>Nero</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29617"/>
        <source>White</source>
        <translation>Bianco</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29618"/>
        <source>Transparent</source>
        <translation>Trasparente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Flat</source>
        <translation>Piatto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Perpendicular</source>
        <translation>Perpendicolare</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Radial</source>
        <translation>Radiale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31118"/>
        <source>DeltaET</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31119"/>
        <source>DeltaBT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31120"/>
        <source>ET</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31121"/>
        <source>BT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>START</source>
        <translation>INIZIO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>CHARGE</source>
        <translation>CARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>TP</source>
        <translation>PM</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>DRY END</source>
        <translation>ASCIUTTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>FC START</source>
        <translation>INIZIO PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>FC END</source>
        <translation>FINE PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>SC START</source>
        <translation>INIZIO SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>SC END</source>
        <translation>FINE SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>DROP</source>
        <translation>SCARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>COOL</source>
        <translation>RAFFREDDATO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Event Button</source>
        <translation>Tasto evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Slider</source>
        <translation>Cursore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31178"/>
        <source>below</source>
        <translation>Sotto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31178"/>
        <source>above</source>
        <translation>Sopra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Pop Up</source>
        <translation type="unfinished">Spuntare</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26747"/>
        <source>SV Commands</source>
        <translation>Comandi valore settaggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26747"/>
        <source>Ramp Commands</source>
        <translation type="unfinished">Comandi incremento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1015"/>
        <source>Speed</source>
        <translation>Velocità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27391"/>
        <source>little-endian</source>
        <translation>Formato little-endian</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16660"/>
        <source>classic</source>
        <translation>Classico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16660"/>
        <source>xkcd</source>
        <translation>xkcd</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Default</source>
        <translation>Predefinito</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Humor</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Comic</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>DRY</source>
        <translation>ASCIUTTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>FCs</source>
        <translation>iPC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>FCe</source>
        <translation>fPC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>SCs</source>
        <translation>iSC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>SCe</source>
        <translation>fSC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>COOL END</source>
        <translation>RAFFREDDATO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>IO Command</source>
        <translation>Comando IO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Heater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Fan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>RampSoak ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>RampSoak OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>PID ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>PID OFF</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Contextual Menu</name>
    <message>
        <location filename="../artisanlib/main.py" line="5922"/>
        <source>Create</source>
        <translation>Crea</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5936"/>
        <source>Add point</source>
        <translation>Aggiungi punto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5940"/>
        <source>Remove point</source>
        <translation>Rimuovi punto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5946"/>
        <source>Reset Designer</source>
        <translation>Ripristino disegnatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5950"/>
        <source>Exit Designer</source>
        <translation>Uscita disegnatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6530"/>
        <source>Add to Cupping Notes</source>
        <translation>Aggiungi note assaggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6534"/>
        <source>Add to Roasting Notes</source>
        <translation>Aggiungi note tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6538"/>
        <source>Cancel selection</source>
        <translation>Cancella selezione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6542"/>
        <source>Edit Mode</source>
        <translation>Compone modalità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6546"/>
        <source>Exit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5926"/>
        <source>Config...</source>
        <translation>Configurazione...</translation>
    </message>
</context>
<context>
    <name>Directory</name>
    <message>
        <location filename="../artisanlib/main.py" line="15803"/>
        <source>profiles</source>
        <translation>Profili</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15793"/>
        <source>other</source>
        <translation>Altro</translation>
    </message>
</context>
<context>
    <name>Error Message</name>
    <message>
        <location filename="../artisanlib/main.py" line="19414"/>
        <source>Unable to move CHARGE to a value that does not exist</source>
        <translation>Non idoneo a spostare Charge a valore non esistente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24890"/>
        <source>HH806Wtemperature(): Unable to initiate device</source>
        <translation type="unfinished">HH806WTemperatura():Non idoneo ad avviare un dispositivo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25019"/>
        <source>HH506RAGetID: {0} bytes received but 5 needed</source>
        <translation type="unfinished">HH506RAGetID: {0} di bytes ricevuti ma 5 necessari</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25044"/>
        <source>HH506RAtemperature(): Unable to get id from HH506RA device </source>
        <translation type="unfinished">HH506RAtemperatura():non identificabile da dispositivo HH506RA</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25064"/>
        <source>HH506RAtemperature(): {0} bytes received but 14 needed</source>
        <translation type="unfinished">HH506RAtemperatura():{0} di bytes ricevuti ma 14 necessario</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34814"/>
        <source>Segment values could not be written into PID</source>
        <translation type="unfinished">Valori di segmento non possono essere scritti nel PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34552"/>
        <source>RampSoak could not be changed</source>
        <translation type="unfinished">Temperatura a rampa non può essere cambiato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34870"/>
        <source>pid.readoneword(): {0} RX bytes received (7 needed) for unit ID={1}</source>
        <translation type="unfinished">pid.lettura():{0}RXbytes ricevuti (7richiesti) per unità ID={1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="13930"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26453"/>
        <source>Value Error:</source>
        <translation>Errore valore:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35213"/>
        <source>Exception:</source>
        <translation>Eccezione:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30654"/>
        <source>IO Error:</source>
        <translation>Errore IO:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23814"/>
        <source>Modbus Error:</source>
        <translation>Errore Modbus:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26509"/>
        <source>Serial Exception:</source>
        <translation>Eccezione seriale:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24185"/>
        <source>F80h Error</source>
        <translation>Errore F80h</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24195"/>
        <source>CRC16 data corruption ERROR. TX does not match RX. Check wiring</source>
        <translation>CRC16 Errore corruzione dati. TX non combina con RX. Controlla collegamento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24198"/>
        <source>No RX data received</source>
        <translation>No ricevimento dati RX</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24650"/>
        <source>Unable to open serial port</source>
        <translation>Non in grado ad aprire porta seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25188"/>
        <source>CENTER303temperature(): {0} bytes received but 8 needed</source>
        <translation type="unfinished">Centro303temperatura():{0}bytes ricevuti ma 8 necessari</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25258"/>
        <source>CENTER306temperature(): {0} bytes received but 10 needed</source>
        <translation type="unfinished">Center306temperatura(): {0} bytes ricevuti ma 10 necesari</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24326"/>
        <source>DTAcommand(): {0} bytes received but 15 needed</source>
        <translation type="unfinished">ComandoDTA():{0}bytes ricevuti ma 15 necessari</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25344"/>
        <source>CENTER309temperature(): {0} bytes received but 45 needed</source>
        <translation type="unfinished">Center309temperatura(): {0} bytes ricevuti ma 45 necesari</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26046"/>
        <source>Arduino could not set channels</source>
        <translation>Arduino non può impostare i canali</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26057"/>
        <source>Arduino could not set temperature unit</source>
        <translation>Arduino non può impostare unità temperatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25119"/>
        <source>CENTER302temperature(): {0} bytes received but 7 needed</source>
        <translation type="unfinished">Centro302temperatura():{0}bytes ricevuti ma 7 necessari</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27874"/>
        <source>Serial Exception: invalid comm port</source>
        <translation>Eccezione seriale: porta comunicazione non valida</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27879"/>
        <source>Serial Exception: timeout</source>
        <translation>Eccezione seriale: fuori tempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17261"/>
        <source>Univariate: no profile data available</source>
        <translation>Univariabile: dati profilo non disponibili</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17432"/>
        <source>Polyfit: no profile data available</source>
        <translation>Polyfit: dati profilo non disponibili</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24768"/>
        <source>MS6514temperature(): {0} bytes received but 16 needed</source>
        <translation type="unfinished">MS6514temperatura(): {0} bytes ricevuti ma 16 necessari</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24830"/>
        <source>HH806AUtemperature(): {0} bytes received but 16 needed</source>
        <translation type="unfinished">HH806Atemperatura(): {0} bytes ricevuti ma 16 richiesti</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10419"/>
        <source>Error:</source>
        <translation>Errore:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26069"/>
        <source>Arduino could not set filters</source>
        <translation>Arduino non può impostare i filtri</translation>
    </message>
</context>
<context>
    <name>Flavor Scope Label</name>
    <message>
        <location filename="../artisanlib/main.py" line="14811"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14812"/>
        <source>Grassy</source>
        <translation>Erbaceo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14813"/>
        <source>Leathery</source>
        <translation>Cuoio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14814"/>
        <source>Toasty</source>
        <translation>Tostato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14815"/>
        <source>Bready</source>
        <translation>Pane</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14816"/>
        <source>Acidic</source>
        <translation>Acido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14817"/>
        <source>Flat</source>
        <translation>Piatto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14818"/>
        <source>Fracturing</source>
        <translation>Rottura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14819"/>
        <source>Sweet</source>
        <translation>Dolcezza</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14820"/>
        <source>Less Sweet</source>
        <translation>Minor dolcezza</translation>
    </message>
</context>
<context>
    <name>Form Caption</name>
    <message>
        <location filename="../artisanlib/main.py" line="16402"/>
        <source>Extras</source>
        <translation>Extra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18006"/>
        <source>Roast Properties</source>
        <translation>Proprietà tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19721"/>
        <source>Artisan Platform</source>
        <translation>Piattaforma Artisan</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19820"/>
        <source>Serial Log</source>
        <translation>Registro seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19864"/>
        <source>Error Log</source>
        <translation>Registro errori</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19889"/>
        <source>Message History</source>
        <translation>Storico messaggi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19910"/>
        <source>Keyboard Autosave [a]</source>
        <translation type="unfinished">Tasti autosalvataggio [a]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19948"/>
        <source>AutoSave Path</source>
        <translation type="unfinished">Percorso autosalvataggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20047"/>
        <source>Axes</source>
        <translation>Assi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20364"/>
        <source>Roast Calculator</source>
        <translation>Calcolatore tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20604"/>
        <source>Events</source>
        <translation>Eventi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22273"/>
        <source>Roast Phases</source>
        <translation>Fasi tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22602"/>
        <source>Cup Profile</source>
        <translation>Profilo tazza</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22836"/>
        <source>Profile Background</source>
        <translation type="unfinished">Sfondo profilo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23355"/>
        <source>Statistics</source>
        <translation>Statistiche</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26559"/>
        <source>Designer Config</source>
        <translation>Configurazioni disegnatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27098"/>
        <source>Manual Temperature Logger</source>
        <translation>Registrazione temperatura manuale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27212"/>
        <source>Serial Ports Configuration</source>
        <translation>Configurazione porte seriali</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27896"/>
        <source>Device Assignment</source>
        <translation>Assegnazione dispositivo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29478"/>
        <source>Colors</source>
        <translation>Colori</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30151"/>
        <source>Wheel Graph Editor</source>
        <translation>Editor grafico ruota</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30725"/>
        <source>Alarms</source>
        <translation>Allarmi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32346"/>
        <source>Fuji PXG PID Control</source>
        <translation>Controllo Fuji PXG PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35275"/>
        <source>Delta DTA PID Control</source>
        <translation>Controllo Delta DTA PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19761"/>
        <source>Settings Viewer</source>
        <translation>Visualizzatore settaggi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31322"/>
        <source>Fuji PXR PID Control</source>
        <translation>Controllo Fuji PXR PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34909"/>
        <source>Arduino Control</source>
        <translation>Controllo Arduino</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17657"/>
        <source>Volume Calculator</source>
        <translation>Calcolatore Volume</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19581"/>
        <source>Tare Setup</source>
        <translation>Impostare tara</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19972"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="27918"/>
        <source>Curves</source>
        <translation>Curve</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27927"/>
        <source>LCDs</source>
        <translation>LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16628"/>
        <source>HUD</source>
        <translation>HUD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16844"/>
        <source>Interpolate</source>
        <translation>Interpolato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16850"/>
        <source>Univariate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16908"/>
        <source>Appearance</source>
        <translation>Aspetto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16916"/>
        <source>Resolution</source>
        <translation>Risoluzione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18661"/>
        <source>Times</source>
        <translation>Tempi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20227"/>
        <source>Time Axis</source>
        <translation>Asse tempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20229"/>
        <source>Temperature Axis</source>
        <translation>Asse temperatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20231"/>
        <source>DeltaBT/DeltaET Axis</source>
        <translation>Asse DeltaBT/DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20233"/>
        <source>Legend Location</source>
        <translation>Posizione legenda</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20235"/>
        <source>Grid</source>
        <translation>Griglia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20477"/>
        <source>Rate of Change</source>
        <translation>Tasso di variazione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20479"/>
        <source>Temperature Conversion</source>
        <translation>Conversione temperatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20481"/>
        <source>Weight Conversion</source>
        <translation>Conversione peso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20483"/>
        <source>Volume Conversion</source>
        <translation>Conversione volume</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21111"/>
        <source>Event Types</source>
        <translation type="unfinished">Tipi evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21262"/>
        <source>Default Buttons</source>
        <translation>Tasti impostazione predefinita</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21315"/>
        <source>Management</source>
        <translation>Gestione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23469"/>
        <source>Evaluation</source>
        <translation>Valutazione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23471"/>
        <source>Display</source>
        <translation>Schermo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26821"/>
        <source>Initial Settings</source>
        <translation>Impostazioni iniziali</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27593"/>
        <source>Input 1</source>
        <translation>Input 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27607"/>
        <source>Input 2</source>
        <translation>Input 2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27620"/>
        <source>Input 3</source>
        <translation>Input 3</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27633"/>
        <source>Input 4</source>
        <translation>Input 4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28363"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28390"/>
        <source>Arduino TC4</source>
        <translation>Arduino TC4</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28397"/>
        <source>External Program</source>
        <translation>Programma esterno</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28403"/>
        <source>Symbolic Assignments</source>
        <translation>Assegnazione simbolica</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29773"/>
        <source>Timer LCD</source>
        <translation>LCD temporizzatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29776"/>
        <source>ET LCD</source>
        <translation>LCD ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29779"/>
        <source>BT LCD</source>
        <translation>LCD BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30237"/>
        <source>Label Properties</source>
        <translation>Proprietà etichetta</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16928"/>
        <source>Sound</source>
        <translation>Suono</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29782"/>
        <source>DeltaET LCD</source>
        <translation>LCD DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29785"/>
        <source>DeltaBT LCD</source>
        <translation>LCD DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16642"/>
        <source>Input Filters</source>
        <translation>Filtri ingresso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29788"/>
        <source>Extra Devices / PID SV LCD</source>
        <translation>Dispositivi extra / PID SV LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16877"/>
        <source>Polyfit</source>
        <translation>Polyfit</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16687"/>
        <source>Look</source>
        <translation type="unfinished">Aspetto</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24236"/>
        <source>1048 Probe Types</source>
        <translation type="obsolete">Tipi sonda 1048</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28337"/>
        <source>Network</source>
        <translation>Rete</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24268"/>
        <source>Phidgets</source>
        <translation type="obsolete">Phidgets</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34912"/>
        <source>p-i-d</source>
        <translation>p-i-d</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35075"/>
        <source>Set Value</source>
        <translation>Valore impostato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16970"/>
        <source>WebLCDs</source>
        <translation>WebLCDs</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28157"/>
        <source>Phidgets 1045</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28232"/>
        <source>Phidgets 1046 RTD</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28310"/>
        <source>Phidget IO</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20344"/>
        <source>Sampling Interval</source>
        <translation type="obsolete">Intervallo di Tempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21277"/>
        <source>Sampling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28116"/>
        <source>Phidgets 1048/1051</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTML Report Template</name>
    <message>
        <location filename="../artisanlib/main.py" line="14190"/>
        <source>Roasting Report</source>
        <translation>Report tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14215"/>
        <source>Date:</source>
        <translation>Data:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14219"/>
        <source>Beans:</source>
        <translation>Chicchi:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14223"/>
        <source>Size:</source>
        <translation>Misura:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14227"/>
        <source>Weight:</source>
        <translation>Peso:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14231"/>
        <source>Degree:</source>
        <translation>Gradi:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14235"/>
        <source>Volume:</source>
        <translation>Volume:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14239"/>
        <source>Density:</source>
        <translation>Densità:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12609"/>
        <source>Humidity:</source>
        <translation type="obsolete">Umidità:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14251"/>
        <source>Roaster:</source>
        <translation>Tostatrice:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14255"/>
        <source>Operator:</source>
        <translation>Operatore:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14259"/>
        <source>Cupping:</source>
        <translation>Assaggio:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14279"/>
        <source>DRY:</source>
        <translation>Asciugatura:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14283"/>
        <source>FCs:</source>
        <translation>iPC:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14287"/>
        <source>FCe:</source>
        <translation>fPC:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14291"/>
        <source>SCs:</source>
        <translation>iSC:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14295"/>
        <source>SCe:</source>
        <translation>fSC:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14299"/>
        <source>DROP:</source>
        <translation>SCARICO:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14303"/>
        <source>COOL:</source>
        <translation>RAFFREDDATO:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14311"/>
        <source>RoR:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14315"/>
        <source>ETBTa:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14365"/>
        <source>Roasting Notes</source>
        <translation>Note sulla tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14375"/>
        <source>Cupping Notes</source>
        <translation>Note sull&apos;assaggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14263"/>
        <source>Color:</source>
        <translation>Colore:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14331"/>
        <source>Maillard:</source>
        <translation>Maillard:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14335"/>
        <source>Development:</source>
        <translation>Sviluppo:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14339"/>
        <source>Cooling:</source>
        <translation>Raffreddamento:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14327"/>
        <source>Drying:</source>
        <translation>Asciugatura:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14271"/>
        <source>CHARGE:</source>
        <translation>Carico:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14275"/>
        <source>TP:</source>
        <translation>PM:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14319"/>
        <source>CM:</source>
        <translation>CM:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14345"/>
        <source>Background:</source>
        <translation>Sfondo:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14345"/>
        <source>Events</source>
        <translation>Eventi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14243"/>
        <source>Moisture:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14247"/>
        <source>Ambient:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14307"/>
        <source>MET:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="../artisanlib/main.py" line="26733"/>
        <source>ET</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27109"/>
        <source>BT</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5900"/>
        <source>DeltaET</source>
        <translation>DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5892"/>
        <source>DeltaBT</source>
        <translation>DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8521"/>
        <source>PID SV</source>
        <translation>Valore settaggio del PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8525"/>
        <source>PID %</source>
        <translation>Valore % del PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8574"/>
        <source>Event #&lt;b&gt;0 &lt;/b&gt;</source>
        <translation>Evento #&lt;b&gt;0&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10451"/>
        <source>Event #&lt;b&gt;{0} &lt;/b&gt;</source>
        <translation type="unfinished">Evento #&lt;b&gt;{0}&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15564"/>
        <source>City</source>
        <translation>City</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15566"/>
        <source>City+</source>
        <translation>City+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15568"/>
        <source>Full City</source>
        <translation>Full City</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15570"/>
        <source>Full City+</source>
        <translation>Full City+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15572"/>
        <source>Light French</source>
        <translation>Light French</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15574"/>
        <source>French</source>
        <translation>French</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35006"/>
        <source>Mode</source>
        <translation>Modalità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16702"/>
        <source>Y(x)</source>
        <translation>Y(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26562"/>
        <source>CHARGE</source>
        <translation>CARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18040"/>
        <source>DRY END</source>
        <translation>ASCIUTTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18054"/>
        <source>FC START</source>
        <translation>INIZIO PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18069"/>
        <source>FC END</source>
        <translation>FINE PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18083"/>
        <source>SC START</source>
        <translation>INIZIO SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18097"/>
        <source>SC END</source>
        <translation>FINE SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26575"/>
        <source>DROP</source>
        <translation>SCARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18126"/>
        <source>COOL</source>
        <translation>RAFFREDDATO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18174"/>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18177"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18215"/>
        <source>Beans</source>
        <translation>Chicchi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18225"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18261"/>
        <source> in</source>
        <translation>Entrata</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18262"/>
        <source> out</source>
        <translation>Uscita</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18275"/>
        <source> %</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18260"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18293"/>
        <source>Density</source>
        <translation>Densità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18308"/>
        <source>per</source>
        <translation>per</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18337"/>
        <source>Bean Size</source>
        <translation>Dimensione chicco</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18343"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18383"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18389"/>
        <source>at</source>
        <translation>a</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18401"/>
        <source>Roaster</source>
        <translation>Tostatrice</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18403"/>
        <source>Operator</source>
        <translation>Operatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18404"/>
        <source>Roasting Notes</source>
        <translation>Note sullai tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18408"/>
        <source>Cupping Notes</source>
        <translation>Note sull&apos;assaggio</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="16645"/>
        <source>                 Density in: {0}  g/l   =&gt;   Density out: {1} g/l</source>
        <translation type="obsolete">Densità in: {0} g/l =&gt; Densità out: {1} g/l</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19385"/>
        <source>({0} g/l)</source>
        <translation type="unfinished">({0} g/l)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19871"/>
        <source>Number of errors found {0}</source>
        <translation type="unfinished">Numero di errori trovati {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20991"/>
        <source>Max</source>
        <translation>Massimo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20989"/>
        <source>Min</source>
        <translation>Minimo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20091"/>
        <source>Rotation</source>
        <translation>Rotazione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20153"/>
        <source>Step</source>
        <translation type="unfinished">Step</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20161"/>
        <source>Style</source>
        <translation>Stile</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20171"/>
        <source>Width</source>
        <translation>Ampiezza</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22870"/>
        <source>Opaqueness</source>
        <translation>Opacità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20366"/>
        <source>Enter two times along profile</source>
        <translation type="unfinished">Inserire due volte durante il profilo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20369"/>
        <source>Start (00:00)</source>
        <translation>Inizio (00:00)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20370"/>
        <source>End (00:00)</source>
        <translation>Fine (00:00)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20389"/>
        <source>Fahrenheit</source>
        <translation>Fahrenheit</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20390"/>
        <source>Celsius</source>
        <translation>Celsius</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20517"/>
        <source>Time syntax error. Time not valid</source>
        <translation>Errore di sintassi tempo.Tempo non valido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20521"/>
        <source>Error: End time smaller than Start time</source>
        <translation>Errore: tempo finale inferiore al tempo iniziale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20538"/>
        <source>Best approximation was made from {0} to {1}</source>
        <translation type="unfinished">Migliore approssimazione fatta dall&apos; 1% al 2%</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20543"/>
        <source>No profile found</source>
        <translation>Profilo mom trovato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20825"/>
        <source>Max buttons per row</source>
        <translation>N° max di tasti per fila</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20853"/>
        <source>Color Pattern</source>
        <translation>Colore schema</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20865"/>
        <source>palette #</source>
        <translation>Gamma colori #</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20985"/>
        <source>Event</source>
        <translation>Evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20889"/>
        <source>Action</source>
        <translation>Azione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20891"/>
        <source>Command</source>
        <translation>Comando</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20893"/>
        <source>Offset</source>
        <translation type="unfinished">Compensazione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20895"/>
        <source>Factor</source>
        <translation>Fattore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22603"/>
        <source>Default</source>
        <translation>Predefinito</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22633"/>
        <source>Aspect Ratio</source>
        <translation>Rapprto di aspetto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22890"/>
        <source>BT Color</source>
        <translation type="unfinished">Colore BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22883"/>
        <source>ET Color</source>
        <translation type="unfinished">Colore ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26588"/>
        <source>Marker</source>
        <translation>Segnale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26590"/>
        <source>Time</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26732"/>
        <source>Curviness</source>
        <translation>Curvatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26745"/>
        <source>Events Playback</source>
        <translation>Riproduzione eventi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27492"/>
        <source>Comm Port</source>
        <translation>Porta comune</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27496"/>
        <source>Baud Rate</source>
        <translation>Velocità trasmissione dati</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27502"/>
        <source>Byte Size</source>
        <translation>Dimensione dati</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27508"/>
        <source>Parity</source>
        <translation>Parità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27515"/>
        <source>Stopbits</source>
        <translation>Bit di stop</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27521"/>
        <source>Timeout</source>
        <translation>Timeout</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27365"/>
        <source>Slave</source>
        <translation>Dispositivo slave</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27370"/>
        <source>Register</source>
        <translation>Registro</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27482"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27959"/>
        <source>Control ET</source>
        <translation type="unfinished">Controllo ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27963"/>
        <source>Read BT</source>
        <translation type="unfinished">Controllo BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28103"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27968"/>
        <source>RS485 Unit ID</source>
        <translation>RS485 unità ID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27984"/>
        <source>ET Channel</source>
        <translation type="unfinished">Canale ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27987"/>
        <source>BT Channel</source>
        <translation type="unfinished">Canale BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28007"/>
        <source>AT Channel</source>
        <translation type="unfinished">Canale AT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28031"/>
        <source>ET Y(x)</source>
        <translation type="unfinished">ET Y(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28032"/>
        <source>BT Y(x)</source>
        <translation type="unfinished">BT Y(x)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30170"/>
        <source>Ratio</source>
        <translation>Rapporto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30177"/>
        <source>Text</source>
        <translation>Testo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30184"/>
        <source>Edge</source>
        <translation>Bordo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30190"/>
        <source>Line</source>
        <translation>Linea</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30199"/>
        <source>Color pattern</source>
        <translation>Colore disegno</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31327"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(1-4)</source>
        <translation type="unfinished">Incremento e assorbimento HH:MM&lt;br&gt; (1-4)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31332"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(5-8)</source>
        <translation type="unfinished">Temperatura a rampa HH:MM&lt;br&gt;(5-8)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31334"/>
        <source>Ramp/Soak Pattern</source>
        <translation type="unfinished">Schema temperatura a rampa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>WARNING</source>
        <translation>Avvertimento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>Writing eeprom memory</source>
        <translation type="unfinished">Scrittura memoria eeprom</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>&lt;u&gt;Max life&lt;/u&gt; 10,000 writes</source>
        <translation type="unfinished">&lt;u&gt;periodo massimo&lt;u&gt;scrivi 10.000</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>Infinite read life.</source>
        <translation>Durata di lettura infinita.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>After &lt;u&gt;writing&lt;/u&gt; an adjustment,&lt;br&gt;never power down the pid&lt;br&gt;for the next 5 seconds &lt;br&gt;or the pid may never recover.</source>
        <translation>Dopo &lt;u&gt;scrittura&lt;/u&gt; adattamento,&lt;br&gt;non disinserire mai il PID&lt;br&gt;nei successivi 5 secondi&lt;br&gt;altrimenti il PID non potrà recuperare</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>Read operations manual</source>
        <translation>Leggi manuale operativo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32911"/>
        <source>ET Thermocouple type</source>
        <translation type="unfinished">Tipo termocoppia ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32918"/>
        <source>BT Thermocouple type</source>
        <translation type="unfinished">Tipo termocoppia BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32804"/>
        <source>Artisan uses 1 decimal point</source>
        <translation>Artisan utilizza 1 punto decimale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32354"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(1-7)</source>
        <translation>Temperatura a rampa (MM:SS)&lt;br&gt;(1-7)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32360"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(8-16)</source>
        <translation type="unfinished">Temperatura a rampa (MM:SS)&lt;br&gt;(8-16)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32407"/>
        <source>Pattern</source>
        <translation type="unfinished">Modello</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32462"/>
        <source>SV (7-0)</source>
        <translation>SV (7-0)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32581"/>
        <source>Write</source>
        <translation type="unfinished">Scrivi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32563"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32569"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32575"/>
        <source>D</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32805"/>
        <source>Artisan Fuji PXG uses MINUTES:SECONDS units in Ramp/Soaks</source>
        <translation type="unfinished">Fuji PXG con Artisan utilizza temperatura a rampa in MINUTI:SECONDI</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22918"/>
        <source>DeltaET Color</source>
        <translation type="unfinished">Colore DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22925"/>
        <source>DeltaBT Color</source>
        <translation type="unfinished">Colore DeltaBT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22972"/>
        <source>Text Warning</source>
        <translation>Avvertimento testo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22973"/>
        <source>sec</source>
        <translation>secondi</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15924"/>
        <source>Storage Conditions</source>
        <translation type="obsolete">Condizioni stoccaggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18381"/>
        <source>Ambient Conditions</source>
        <translation>Condizioni ambiente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18489"/>
        <source>Ambient Source</source>
        <translation type="unfinished">Fonte ambiente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20706"/>
        <source>Color</source>
        <translation>Colore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20710"/>
        <source>Thickness</source>
        <translation>Spessore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20712"/>
        <source>Opacity</source>
        <translation>Opacità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20714"/>
        <source>Size</source>
        <translation>Misura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35279"/>
        <source>SV</source>
        <translation>SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20621"/>
        <source>Bars</source>
        <translation>Barre</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16435"/>
        <source>Smooth Deltas</source>
        <translation type="unfinished">Delta graduali</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16443"/>
        <source>Smooth Curves</source>
        <translation type="unfinished">Curve graduali</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18345"/>
        <source>Whole Color</source>
        <translation>Spettro cromatico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18351"/>
        <source>Ground Color</source>
        <translation>Colore di fondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27375"/>
        <source>Float</source>
        <translation type="unfinished">Fluttuazione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27376"/>
        <source>Function</source>
        <translation>Funzione</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5996"/>
        <source>deg/min</source>
        <translation type="obsolete">Gradi decimali/min</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3215"/>
        <source>BackgroundET</source>
        <translation type="unfinished">Sfondo ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3219"/>
        <source>BackgroundBT</source>
        <translation type="unfinished">Sfondo BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3259"/>
        <source>BackgroundDeltaET</source>
        <translation type="unfinished">Sfondo DeltaET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3263"/>
        <source>BackgroundDeltaBT</source>
        <translation type="unfinished">Sfondo DeltaBT</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="12809"/>
        <source>d/m</source>
        <translation type="obsolete">d/m</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5479"/>
        <source>BT {0} d/m for {1}</source>
        <translation type="obsolete">Temperatura chicchi {0} d/m per{1}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="5495"/>
        <source>ET {0} d/m for {1}</source>
        <translation type="obsolete">Temperatura fumi {0} d/m per{1}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15125"/>
        <source>{0} to reach ET target {1}</source>
        <translation type="obsolete">{0} per ottenere Temperatura fumi {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15882"/>
        <source> at {0}</source>
        <translation type="unfinished">a {0}</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="15138"/>
        <source>{0} to reach BT target {1}</source>
        <translation type="obsolete">{0} per ottenere temperatura chicchi {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15936"/>
        <source>ET - BT = {0}</source>
        <translation type="unfinished">ET - BT= {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15989"/>
        <source>ET - BT = {0}{1}</source>
        <translation type="unfinished">ET - BT = {0}{1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30485"/>
        <source> dg</source>
        <translation>dg</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30900"/>
        <source>Enter description</source>
        <translation>Invio descrizione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32705"/>
        <source>NOTE: BT Thermocouple type is not stored in the Artisan settings</source>
        <translation>NOTA: tipo termocoppia per temperatura chicchi non è registrata nelle impostazioni Artisan</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15894"/>
        <source>{0} after FCs</source>
        <translation type="unfinished">{0} dopo iPC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15901"/>
        <source>{0} after FCe</source>
        <translation type="unfinished">{0} dopo fPC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16418"/>
        <source>ET Target 1</source>
        <translation type="unfinished">Obiettivo 1 ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16420"/>
        <source>BT Target 1</source>
        <translation type="unfinished">Obiettivo 1 BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16422"/>
        <source>ET Target 2</source>
        <translation type="unfinished">Obiettivo 2 ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16424"/>
        <source>BT Target 2</source>
        <translation type="unfinished">Obiettivo 2 BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16428"/>
        <source>ET p-i-d 1</source>
        <translation type="unfinished">ET p-i-d 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35027"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35035"/>
        <source>max</source>
        <translation>max</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23404"/>
        <source>Drying</source>
        <translation>Asciugatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23405"/>
        <source>Maillard</source>
        <translation>Maillard</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23406"/>
        <source>Development</source>
        <translation>Sviluppo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23407"/>
        <source>Cooling</source>
        <translation>Raffreddamento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1608"/>
        <source>EVENT</source>
        <translation type="unfinished">EVENTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20114"/>
        <source>Initial Max</source>
        <translation>Massimo iniziale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27247"/>
        <source>Settings for non-Modbus devices</source>
        <translation>Impostazioni per dispositivo non-Modbus</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6886"/>
        <source>Curves</source>
        <translation>Curve</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6890"/>
        <source>Delta Curves</source>
        <translation>Delta curve</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="4576"/>
        <source>T</source>
        <translation type="obsolete">T</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>RoR</source>
        <translation>Percentuale di crescita</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>ETBTa</source>
        <translation></translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="17579"/>
        <source>&lt;b&gt;{0}&lt;/b&gt; deg/sec, &lt;b&gt;{1}&lt;/b&gt; deg/min</source>
        <translation type="obsolete">&lt;b&gt;{0}&lt;/b&gt; deg/sec, &lt;b&gt;{1}&lt;b&gt; deg/min</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16813"/>
        <source>Start</source>
        <translation>Inizio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16814"/>
        <source>End</source>
        <translation>Fine</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5252"/>
        <source>CM</source>
        <translation>CM</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9392"/>
        <source>TP</source>
        <translation>PM</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9431"/>
        <source>DRY</source>
        <translation>ASCIUTTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9480"/>
        <source>FCs</source>
        <translation>iPC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11390"/>
        <source>Start recording</source>
        <translation>Inizio registrazione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11404"/>
        <source>Charge the beans</source>
        <translation>Carico dei chicchi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16451"/>
        <source>Window</source>
        <translation type="unfinished">Finestra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16645"/>
        <source>Path Effects</source>
        <translation>Effettipercorso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16665"/>
        <source>Font</source>
        <translation>Carattere</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19996"/>
        <source>Prefix</source>
        <translation>Prefisso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34939"/>
        <source>Source</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24211"/>
        <source>1:</source>
        <translation type="obsolete">1:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24215"/>
        <source>2:</source>
        <translation type="obsolete">2:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24219"/>
        <source>3:</source>
        <translation type="obsolete">3:</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24223"/>
        <source>4:</source>
        <translation type="obsolete">4:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34955"/>
        <source>Cycle</source>
        <translation>Ciclo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35001"/>
        <source>Lookahead</source>
        <translation>Previsione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35008"/>
        <source>Manual</source>
        <translation>Manuale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35009"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished">Controllore ramp/soak</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35010"/>
        <source>Background</source>
        <translation>sfondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35015"/>
        <source>SV Buttons</source>
        <translation type="unfinished">Tasti SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35018"/>
        <source>SV Slider</source>
        <translation type="unfinished">Cursore SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6881"/>
        <source>/min</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>MET</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6364"/>
        <source>BT {0} {1}/min for {2}</source>
        <translation type="unfinished">BT {0} {1}/min per {2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6380"/>
        <source>ET {0} {1}/min for {2}</source>
        <translation type="unfinished">ET {0} {1}/min per {2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9376"/>
        <source>DRY%</source>
        <translation type="unfinished">ASCIUTTO%</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9413"/>
        <source>RAMP%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9462"/>
        <source>DEV%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14451"/>
        <source>/m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14477"/>
        <source>greens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14482"/>
        <source>roasted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16414"/>
        <source>HUD Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16515"/>
        <source>Delta Span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27412"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17683"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17819"/>
        <source>ml</source>
        <translation type="unfinished">ml</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17783"/>
        <source>Unit Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17802"/>
        <source>g</source>
        <translation type="unfinished">g</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17800"/>
        <source>Kg</source>
        <translation type="unfinished">Kg</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17817"/>
        <source>l</source>
        <translation type="unfinished">l</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17775"/>
        <source>in</source>
        <translation type="unfinished">Entrata</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17850"/>
        <source>out</source>
        <translation type="unfinished">Uscita</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18362"/>
        <source>Moisture Greens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18371"/>
        <source>Moisture Roasted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20993"/>
        <source>Coarse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27407"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28015"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28296"/>
        <source>Async</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28299"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28142"/>
        <source>Emissivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28217"/>
        <source>Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28218"/>
        <source>Wiring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28298"/>
        <source>Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28297"/>
        <source>Raw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28316"/>
        <source>ServerId:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28318"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21195"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21210"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3209"/>
        <source>BackgroundXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18184"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19999"/>
        <source>Counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20869"/>
        <source>current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22897"/>
        <source>XT Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22904"/>
        <source>XT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15871"/>
        <source>{0} to reach ET {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15884"/>
        <source>{0} to reach BT {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19327"/>
        <source>Density in: {0} g/l   =&gt;   Density out: {1} g/l</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19365"/>
        <source>Moisture loss: {0}%    Organic loss: {1}%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20539"/>
        <source>&lt;b&gt;{0}&lt;/b&gt; {1}/sec, &lt;b&gt;{2}&lt;/b&gt; {3}/min</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../const/UIconst.py" line="48"/>
        <source>Services</source>
        <translation>Servizi (notifiche)</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="49"/>
        <source>Hide {0}</source>
        <translation type="unfinished">Nascosto {0}</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="50"/>
        <source>Hide Others</source>
        <translation>Altri nascosti</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="51"/>
        <source>Show All</source>
        <translation>Mostra tutti</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="52"/>
        <source>Preferences...</source>
        <translation>Preferenze...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="74"/>
        <source>Quit {0}</source>
        <translation type="unfinished">Esci {0}</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="163"/>
        <source>About {0}</source>
        <translation type="unfinished">Circa {0}</translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="../artisanlib/main.py" line="20661"/>
        <source>Circle</source>
        <translation>Cerchio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20662"/>
        <source>Square</source>
        <translation>Quadrato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20663"/>
        <source>Pentagon</source>
        <translation>Pentagono</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20664"/>
        <source>Diamond</source>
        <translation>Diamante</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20665"/>
        <source>Star</source>
        <translation>Stella</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20666"/>
        <source>Hexagon 1</source>
        <translation>Esagono 1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20667"/>
        <source>Hexagon 2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20668"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20669"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20670"/>
        <source>None</source>
        <translation>Nulla</translation>
    </message>
</context>
<context>
    <name>Menu</name>
    <message>
        <location filename="../const/UIconst.py" line="57"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="60"/>
        <source>New</source>
        <translation>Nuova</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="61"/>
        <source>Open...</source>
        <translation>Apri...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="62"/>
        <source>Open Recent</source>
        <translation>Apri Recente</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="63"/>
        <source>Import</source>
        <translation>Importa</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="64"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="65"/>
        <source>Save As...</source>
        <translation>Salva Come...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="67"/>
        <source>Save Graph</source>
        <translation>Salva Grafico</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="70"/>
        <source>Print...</source>
        <translation>Stampa...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="85"/>
        <source>Roast</source>
        <translation>Torrefazione</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="88"/>
        <source>Properties...</source>
        <translation>Proprietà...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="89"/>
        <source>Background...</source>
        <translation>Sfondo...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="90"/>
        <source>Cup Profile...</source>
        <translation>Profilo Tazza...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="91"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="146"/>
        <source>Calculator</source>
        <translation>Calcolatore</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="99"/>
        <source>Config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="102"/>
        <source>Device...</source>
        <translation>Dispositivo...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="103"/>
        <source>Serial Port...</source>
        <translation>Interfaccia Seriale...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="104"/>
        <source>Sampling Interval...</source>
        <translation>Intervallo di Tempo...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="107"/>
        <source>Colors...</source>
        <translation>Colori...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="110"/>
        <source>Phases...</source>
        <translation>Fasi...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="111"/>
        <source>Events...</source>
        <translation>Eventi...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="112"/>
        <source>Statistics...</source>
        <translation>Statistiche...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="113"/>
        <source>Axes...</source>
        <translation>Assi...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="114"/>
        <source>Autosave...</source>
        <translation>Salva Automaticamente...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="149"/>
        <source>Extras...</source>
        <translation>Extra...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="159"/>
        <source>Help</source>
        <translation>Aiuto</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="165"/>
        <source>Documentation</source>
        <translation>Documentazione</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="168"/>
        <source>Errors</source>
        <translation>Errori</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="167"/>
        <source>Keyboard Shortcuts</source>
        <translation>Tasti di Scelta Rapida</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="169"/>
        <source>Messages</source>
        <translation>Messaggi</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="116"/>
        <source>Alarms...</source>
        <translation>Allarmista...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="77"/>
        <source>Edit</source>
        <translation>Composizione</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="80"/>
        <source>Cut</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="81"/>
        <source>Copy</source>
        <translation>Copia</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="82"/>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="68"/>
        <source>Full Size...</source>
        <translation>Grandezza Naturale...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="145"/>
        <source>Designer</source>
        <translation>Disegnatore</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="92"/>
        <source>Convert to Fahrenheit</source>
        <translation>Conversione a Fahrenheit</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="93"/>
        <source>Convert to Celsius</source>
        <translation>Conversione da Gradi Centigradi</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="94"/>
        <source>Fahrenheit Mode</source>
        <translation>Modalità Fahrenheit</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="95"/>
        <source>Celsius Mode</source>
        <translation>Modalità Gradi Centigradi</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="142"/>
        <source>Tools</source>
        <translation>Strumenti</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="147"/>
        <source>Wheel Graph</source>
        <translation>Grafico ruota</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="176"/>
        <source>Factory Reset</source>
        <translation>Ripristino impostazioni</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="117"/>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="170"/>
        <source>Serial</source>
        <translation>Seriale</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="174"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="175"/>
        <source>Platform</source>
        <translation>Piattaforma</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="69"/>
        <source>Roasting Report</source>
        <translation>Rapporto di tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7766"/>
        <source>CSV...</source>
        <translation>CSV...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7770"/>
        <source>JSON...</source>
        <translation>JSON...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7774"/>
        <source>RoastLogger...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7739"/>
        <source>HH506RA...</source>
        <translation>HH506RA...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7743"/>
        <source>K202...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7747"/>
        <source>K204...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="66"/>
        <source>Export</source>
        <translation>Esporta</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="96"/>
        <source>Switch Profiles</source>
        <translation>Cambia profili</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="106"/>
        <source>Oversampling</source>
        <translation>Sovracampionamento</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="164"/>
        <source>About Qt</source>
        <translation>Circa &quot;Qt&quot;</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="148"/>
        <source>LCDs</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="115"/>
        <source>Batch...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="153"/>
        <source>Load Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="154"/>
        <source>Load Recent Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="155"/>
        <source>Save Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="108"/>
        <source>Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="109"/>
        <source>Sliders</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../artisanlib/main.py" line="6765"/>
        <source>Mouse Cross ON: move mouse around</source>
        <translation>Mouse Cross ON:muovi il mouse</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6777"/>
        <source>Mouse cross OFF</source>
        <translation>Mouse Cross OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1864"/>
        <source>HUD OFF</source>
        <translation type="unfinished">HUD spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1878"/>
        <source>HUD ON</source>
        <translation type="unfinished">HUD in funzione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1970"/>
        <source>Alarm notice</source>
        <translation>Notizia allarme</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2006"/>
        <source>Alarm is calling: {0}</source>
        <translation type="unfinished">Chiamata Allarme: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2015"/>
        <source>Alarm trigger button error, description &apos;{0}&apos; not a number</source>
        <translation type="unfinished">Allarme innesco tasto, descrizione &apos;{0}&apos; no un numero</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2043"/>
        <source>Alarm trigger slider error, description &apos;{0}&apos; not a valid number [0-100]</source>
        <translation type="unfinished">Allarme errore innesco cursore, descrizione &apos;{0}&apos; non un  numero valido [0-100]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2463"/>
        <source>Save the profile, Discard the profile (Reset), or Cancel?</source>
        <translation>Salva il profilo, azzera il profilo, o cancella?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2464"/>
        <source>Profile unsaved</source>
        <translation>Profilo non salvato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2547"/>
        <source>Scope has been reset</source>
        <translation>Scope è stato azzerato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3757"/>
        <source>Time format error encountered</source>
        <translation>Errore rilevato sul format tempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3863"/>
        <source>Convert profile data to Fahrenheit?</source>
        <translation>Converte profilo dati in Fahrenheit?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3948"/>
        <source>Convert Profile Temperature</source>
        <translation>Converte profilo temperatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3904"/>
        <source>Profile changed to Fahrenheit</source>
        <translation>Profilo cambiato in Fahrenheit</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3907"/>
        <source>Unable to comply. You already are in Fahrenheit</source>
        <translation>Non idoneo a seguire. Sei già in Fahrenheit</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3950"/>
        <source>Profile not changed</source>
        <translation>Profilo non cambiato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3913"/>
        <source>Convert profile data to Celsius?</source>
        <translation>Converti i dati del profilo in Celsius?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3948"/>
        <source>Unable to comply. You already are in Celsius</source>
        <translation type="unfinished">Impossibilitato a rispettare. Sei già in Celsius</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3954"/>
        <source>Profile changed to Celsius</source>
        <translation>Profilo cambiato in Celsius</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3959"/>
        <source>Convert Profile Scale</source>
        <translation>Converte scala profilo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3959"/>
        <source>No profile data found</source>
        <translation>Dati profilo non trovati</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3976"/>
        <source>Colors set to defaults</source>
        <translation>Imposta colori di default</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3981"/>
        <source>Colors set to grey</source>
        <translation type="unfinished">Imposta colori grigio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4152"/>
        <source>Scope monitoring...</source>
        <translation>Monitoraggio Scope...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4197"/>
        <source>Scope stopped</source>
        <translation>Scope fermato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4311"/>
        <source>Scope recording...</source>
        <translation>Registrazione Scope...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4352"/>
        <source>Scope recording stopped</source>
        <translation type="unfinished">Registrazione Scope fermata</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4405"/>
        <source>Not enough variables collected yet. Try again in a few seconds</source>
        <translation>Variabili raccolte non ancora sufficienti. Riprova tra pochi secondi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4459"/>
        <source>Roast time starts now 00:00 BT = {0}</source>
        <translation type="unfinished">Tempo tostatura inizia  ora 00:00 Temperatura chicchi = {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4920"/>
        <source>Scope is OFF</source>
        <translation>Scope è spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4540"/>
        <source>[DRY END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[ASCIUTTO] registrato a {0} temperatura chicchi = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4603"/>
        <source>[FC START] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[INIZIO PC] registrato a {0} temperatura chicchi = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4658"/>
        <source>[FC END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[FINE PC] registrato a {0} temperatura chicchi = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4716"/>
        <source>[SC START] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[INIZIO SC] registrato a {0} temperatura chicchi = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4773"/>
        <source>[SC END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[FINE SC] registrato a {0} temperatura chicchi = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4855"/>
        <source>Roast ended at {0} BT = {1}</source>
        <translation type="unfinished">Tostatura terminata a {0} temperatura chicchi = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4953"/>
        <source>[COOL END] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[RAFFREDDATO] registrato a {0} temperatura chicchi = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5089"/>
        <source>Event # {0} recorded at BT = {1} Time = {2}</source>
        <translation type="unfinished">Evento # {0} registrato a temperatura chicchi = {1} tempo = {2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5104"/>
        <source>Timer is OFF</source>
        <translation>Timer spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5136"/>
        <source>Computer Event # {0} recorded at BT = {1} Time = {2}</source>
        <translation type="unfinished">Evento computer # {0} registrato a temperatura chicchi = {1} tempo = {2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5298"/>
        <source>Statistics cancelled: need complete profile [CHARGE] + [DROP]</source>
        <translation>Statistiche cancellate: necessario profilo completo [carico] + [scarico]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5483"/>
        <source>Unable to move background</source>
        <translation>Non in grado di muovere in sottofondo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5542"/>
        <source>No finished profile found</source>
        <translation>Profilo completato non trovato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5560"/>
        <source>Polynomial coefficients (Horner form):</source>
        <translation>Coefficienti polinomiali (Horner):</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5563"/>
        <source>Knots:</source>
        <translation>Nodi:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5566"/>
        <source>Residual:</source>
        <translation>Residuo:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5569"/>
        <source>Roots:</source>
        <translation>Origini:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5573"/>
        <source>Profile information</source>
        <translation>Informazione profilo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5734"/>
        <source>Designer Start</source>
        <translation>Inizio disegnatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5778"/>
        <source>Designer Init</source>
        <translation type="unfinished">Inizializzazione disegnatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5778"/>
        <source>Unable to start designer.
Profile missing [CHARGE] or [DROP]</source>
        <translation type="unfinished">Non in grado di far partire designer. Profilo mancante [CARICO] o [SCARICO]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6024"/>
        <source>[ CHARGE ]</source>
        <translation>[CARICO]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6027"/>
        <source>[ DRY END ]</source>
        <translation>[ASCIUTTO]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6030"/>
        <source>[ FC START ]</source>
        <translation>[INIZIO PC]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6033"/>
        <source>[ FC END ]</source>
        <translation>[FINE PC]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6036"/>
        <source>[ SC START ]</source>
        <translation>[INIZIO SC]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6039"/>
        <source>[ SC END ]</source>
        <translation>[FINE SC]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6042"/>
        <source>[ DROP ]</source>
        <translation>[SCARICO]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6305"/>
        <source>New profile created</source>
        <translation>Creato nuovo profilo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30658"/>
        <source>Open Wheel Graph</source>
        <translation>Apre grafico a ruota</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6492"/>
        <source> added to cupping notes</source>
        <translation>Aggiunto alle note d&apos;assaggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6498"/>
        <source> added to roasting notes</source>
        <translation>Aggiunto alle note di tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9903"/>
        <source>Do you want to reset all settings?</source>
        <translation>Vuoi azzerare tutte le impostazioni?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9904"/>
        <source>Factory Reset</source>
        <translation>Ripristino impostazioni di fabbrica</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10304"/>
        <source>Keyboard moves turned ON</source>
        <translation type="unfinished">Tasto girato in ON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10313"/>
        <source>Keyboard moves turned OFF</source>
        <translation type="unfinished">Tasto girato in OFF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10410"/>
        <source>Profile {0} saved in: {1}</source>
        <translation type="unfinished">Profilo {0} salvato in : {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10415"/>
        <source>Empty path or box unchecked in Autosave</source>
        <translation type="unfinished">Percorso vuoto o box non verificatp in autosalvataggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10422"/>
        <source>&lt;b&gt;[ENTER]&lt;/b&gt; = Turns ON/OFF Keyboard Shortcuts</source>
        <translation type="unfinished">&lt;b&gt; [inserisce] &lt;/b&gt; = cambia ON/OFF tasti veloci</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10423"/>
        <source>&lt;b&gt;[SPACE]&lt;/b&gt; = Choses current button</source>
        <translation>&lt;b&gt;[spazio]&lt;/b&gt; =  sceglie tasto attuale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10424"/>
        <source>&lt;b&gt;[LEFT]&lt;/b&gt; = Move to the left</source>
        <translation>&lt;b&gt;[sinistra]&lt;/b&gt; = muove a sinistra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10425"/>
        <source>&lt;b&gt;[RIGHT]&lt;/b&gt; = Move to the right</source>
        <translation>&lt;b&gt;[destra]&lt;/b&gt; = muove a destra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10426"/>
        <source>&lt;b&gt;[a]&lt;/b&gt; = Autosave</source>
        <translation>&lt;b&gt;[a]&lt;/b&gt; = Autosalvataggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10428"/>
        <source>&lt;b&gt;[t]&lt;/b&gt; = Mouse cross lines</source>
        <translation type="unfinished">&lt;b&gt;[t]&lt;/b&gt; = mouse attraversa le linee</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10430"/>
        <source>&lt;b&gt;[b]&lt;/b&gt; = Shows/Hides Extra Event Buttons</source>
        <translation type="unfinished">&lt;b&gt;[b]&lt;/b&gt; = mostra/nasconde tasti evento extra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10431"/>
        <source>&lt;b&gt;[s]&lt;/b&gt; = Shows/Hides Event Sliders</source>
        <translation type="unfinished">&lt;b&gt;[s]&lt;/b&gt; = mostra/nasconde cursori evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10432"/>
        <source>&lt;b&gt;[i]&lt;/b&gt; = Retrieve Weight In from Scale</source>
        <translation>&lt;b&gt;[i]&lt;/b&gt; = richiama peso In da bilancia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10433"/>
        <source>&lt;b&gt;[o]&lt;/b&gt; = Retrieve Weight Out from Scale</source>
        <translation>&lt;b&gt;[o]&lt;/b&gt; = richiama peso Out da bilancia</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10434"/>
        <source>&lt;b&gt;[0-9]&lt;/b&gt; = Changes Event Button Palettes</source>
        <translation type="unfinished">&lt;b&gt;[0-9]&lt;/b&gt; = cambia evento tasto tavolozze</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10435"/>
        <source>&lt;b&gt;[;]&lt;/b&gt; = Application ScreenShot</source>
        <translation>&lt;b&gt;[;]&lt;/b&gt; = applicazione schermata</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10436"/>
        <source>&lt;b&gt;[:]&lt;/b&gt; = Desktop ScreenShot</source>
        <translation>&lt;b&gt;[;]&lt;/b&gt; = computer schermata</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10440"/>
        <source>Keyboard Shotcuts</source>
        <translation>Scorciatoie della tastiera</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10521"/>
        <source>Event #{0}:  {1} has been updated</source>
        <translation type="unfinished">Evento #{0}: {1} è stato aggiornato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10599"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10611"/>
        <source>Select Directory</source>
        <translation>Seleziona elenco</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19230"/>
        <source>No profile found</source>
        <translation>Profilo non trovato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10660"/>
        <source>{0} has been saved. New roast has started</source>
        <translation type="unfinished">{0} è stato salvato. Ininiziata nuova tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10823"/>
        <source>Invalid artisan format</source>
        <translation>Formato non valido di Artisan</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10704"/>
        <source>{0}  loaded </source>
        <translation type="unfinished">{0} caricato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10819"/>
        <source>Background {0} loaded successfully {1}</source>
        <translation type="unfinished">Sfondo {0} caricato correttamente {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10951"/>
        <source>Artisan CSV file loaded successfully</source>
        <translation>File formato CSV di Artisan caricato correttamente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12348"/>
        <source>Save Profile</source>
        <translation>Salva profilo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12355"/>
        <source>Profile saved</source>
        <translation>Profilo salvato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15387"/>
        <source>Cancelled</source>
        <translation>Annullato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12371"/>
        <source>Readings exported</source>
        <translation>Letture esportate</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12379"/>
        <source>Export CSV</source>
        <translation>Esporta formato CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12382"/>
        <source>Export JSON</source>
        <translation>Esporta formato JSON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12385"/>
        <source>Export RoastLogger</source>
        <translation>Esporta formato RoastLogger</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12394"/>
        <source>Readings imported</source>
        <translation>Letture importate</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12402"/>
        <source>Import CSV</source>
        <translation>Importa formato CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12405"/>
        <source>Import JSON</source>
        <translation>Importa formato JSON</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12408"/>
        <source>Import RoastLogger</source>
        <translation>Importa registratore tostatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15113"/>
        <source>Sampling Interval</source>
        <translation>Intervallo di Tempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15113"/>
        <source>Seconds</source>
        <translation>Secondi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15489"/>
        <source>Alarm Config</source>
        <translation>Configurazione allarme</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15489"/>
        <source>Alarms are not available for device None</source>
        <translation type="unfinished">Allarmi non disponibiliper dispositivo None</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15546"/>
        <source>Switch Language</source>
        <translation>Cambio linguaggio</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15546"/>
        <source>Language successfully changed. Restart the application.</source>
        <translation>Linguaggio cambiato correttamente. Riavvio applicazione.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15578"/>
        <source>Import K202 CSV</source>
        <translation>Importa K202 formato CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15630"/>
        <source>K202 file loaded successfully</source>
        <translation>File K202 caricato correttamente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15644"/>
        <source>Import K204 CSV</source>
        <translation>Importa K204 formato CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15710"/>
        <source>K204 file loaded successfully</source>
        <translation>File K204 caricato correttamente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15724"/>
        <source>Import HH506RA CSV</source>
        <translation>Importa  HH506RA formato CSV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15775"/>
        <source>HH506RA file loaded successfully</source>
        <translation>File HH506RA caricato correttamente</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15820"/>
        <source>Save Graph as PNG</source>
        <translation>Salva grafico in formato PNG</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15827"/>
        <source>{0}  size({1},{2}) saved</source>
        <translation type="unfinished">{0} misura({1}, {2}) salvato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15836"/>
        <source>Save Graph as SVG</source>
        <translation>Salva grafico come SVG</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15841"/>
        <source>{0} saved</source>
        <translation type="unfinished">Salvato {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16046"/>
        <source>Invalid Wheel graph format</source>
        <translation>Formato non valido grafico ruota</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16049"/>
        <source>Wheel Graph succesfully open</source>
        <translation type="unfinished">Apertura riuscita grafico ruota</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16068"/>
        <source>Return the absolute value of x.</source>
        <translation>Inviol valore assoluto di x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16069"/>
        <source>Return the arc cosine (measured in radians) of x.</source>
        <translation>Invio arco coseno (misura in radianti) di x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16070"/>
        <source>Return the arc sine (measured in radians) of x.</source>
        <translation>Invio arco seno (misura in radianti) di x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16071"/>
        <source>Return the arc tangent (measured in radians) of x.</source>
        <translation>Invio arco tangente (misura in radianti) di x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16072"/>
        <source>Return the cosine of x (measured in radians).</source>
        <translation>Invio coseno di x (misura in radianti).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16073"/>
        <source>Convert angle x from radians to degrees.</source>
        <translation>Converte angolo x da radianti a gradi.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16074"/>
        <source>Return e raised to the power of x.</source>
        <translation>Invio e elevato alla potenza di x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16075"/>
        <source>Return the logarithm of x to the given base. </source>
        <translation>Invio logaritmo di x a logaritmo in base e.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16076"/>
        <source>Return the base 10 logarithm of x.</source>
        <translation>Invio logaritmo in base 10 di x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16079"/>
        <source>Return x**y (x to the power of y).</source>
        <translation>Invio x**v (x alla potenza di y).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16080"/>
        <source>Convert angle x from degrees to radians.</source>
        <translation>Converte angolo x da gradi a radianti.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16081"/>
        <source>Return the sine of x (measured in radians).</source>
        <translation>Invio il seno di x (misurato in radianti).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16082"/>
        <source>Return the square root of x.</source>
        <translation>Invio radice quadrata di x.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16083"/>
        <source>Return the tangent of x (measured in radians).</source>
        <translation>Invio tangente di x (misura in radianti).</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16100"/>
        <source>MATHEMATICAL FUNCTIONS</source>
        <translation>FUNZIONI MATEMATICHE</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16102"/>
        <source>SYMBOLIC VARIABLES</source>
        <translation>VARIABILI SIMBOLICHE</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16104"/>
        <source>Symbolic Functions</source>
        <translation>Funzioni simboliche</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16234"/>
        <source>Save Palettes</source>
        <translation>Salva tavolozze</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16238"/>
        <source>Palettes saved</source>
        <translation>Tavolozze salvate</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16281"/>
        <source>Invalid palettes file format</source>
        <translation>Formato file tavolozze non valido</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16284"/>
        <source>Palettes loaded</source>
        <translation>Tavolozze caricate</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16296"/>
        <source>Load Palettes</source>
        <translation>Carica tavolozze</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16317"/>
        <source>Alarms loaded</source>
        <translation>Avvisi caricati</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17455"/>
        <source>Sound turned ON</source>
        <translation>Audio acceso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17459"/>
        <source>Sound turned OFF</source>
        <translation>Audio spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19227"/>
        <source>Event #{0} added</source>
        <translation type="unfinished">Evento#{0} aggiunto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19263"/>
        <source> Event #{0} deleted</source>
        <translation type="unfinished">Evento#{0} eliminato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19269"/>
        <source>No events found</source>
        <translation type="unfinished">Non sono stati trovati eventi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19566"/>
        <source>Roast properties updated but profile not saved to disk</source>
        <translation>Proprietà tostatura aggiornate ma profilo non salvato su disco</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19956"/>
        <source>Autosave ON. Prefix: {0}</source>
        <translation type="unfinished">Autosalvataggio inserito. Prefisso: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19960"/>
        <source>Autosave OFF</source>
        <translation>Autosalvataggio disinserito</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20335"/>
        <source>xlimit = ({2},{3}) ylimit = ({0},{1}) zlimit = ({4},{5})</source>
        <translation type="unfinished">limitex = ({2},{3}) limitey = ({0},{1}) limitez = ({4},{5})</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21457"/>
        <source>&lt;b&gt;Event&lt;/b&gt; hide or show the corresponding slider</source>
        <translation>&lt;b&gt;Evento&lt;/b&gt;nasconde o visualizza il corrispondente dispositivo di scorrimento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21458"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action on slider release</source>
        <translation>&lt;b&gt;Azione&lt;/b&gt;performa un&apos;azione sul dispositivo di scorrimento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21459"/>
        <source>&lt;b&gt;Command&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by &lt;i&gt;value&lt;/i&gt;*&lt;i&gt;factor&lt;/i&gt; + &lt;i&gt;offset&lt;/i&gt;)</source>
        <translation type="unfinished">&lt;b&gt;comando&lt;/b&gt;dipende dal tipo di azione (&apos;[ ] è sostituito con &lt;i&gt; valore&lt;/i&gt;*&lt;i&gt;fattore&lt;/i&gt; + &lt;i&gt;offset&lt;i&gt;)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22256"/>
        <source>Serial Command: ASCII serial command or binary a2b_uu(serial command)</source>
        <translation>Comando seriale: comando seriale ASCII o binario a2b_uu(comando seriale)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22260"/>
        <source>DTA Command: Insert Data address : value, ex. 4701:1000 and sv is 100. always multiply with 10 if value Unit: 0.1 / ex. 4719:0 stops heating</source>
        <translation type="unfinished">Comando DTA: inserisce indirizzo dati : valore, ex. 4701:1000 e sv è 100, sempre multiplo con 10 se unità di valore: 0.1/ ex. 4719:0 ferma riscaldamento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21463"/>
        <source>&lt;b&gt;Offset&lt;/b&gt; added as offset to the slider value</source>
        <translation type="unfinished">&lt;b&gt;Offset&lt;/b&gt;aggiunge come sostituto al dispositivo di scorrimento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21464"/>
        <source>&lt;b&gt;Factor&lt;/b&gt; multiplicator of the slider value</source>
        <translation>&lt;b&gt;Fattore&lt;/b&gt; moltiplicatore del dispositivo di scorrimento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22264"/>
        <source>Event custom buttons</source>
        <translation>Tasti evento personalizzato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22225"/>
        <source>Event configuration saved</source>
        <translation>Configurazione evento salvato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22228"/>
        <source>Found empty event type box</source>
        <translation>Trova evento vuoto type box</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22250"/>
        <source>&lt;b&gt;Button Label&lt;/b&gt; Enter \n to create labels with multiple lines.</source>
        <translation>&lt;b&gt;Tasto etichetta&lt;/b&gt; Invio\n per creare etichette con linee multiple</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22251"/>
        <source>&lt;b&gt;Event Description&lt;/b&gt; Description of the Event to be recorded.</source>
        <translation>&lt;b&gt;Descrizione evento&lt;/b&gt; descrizione dell&apos;evento da registrare.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22252"/>
        <source>&lt;b&gt;Event type&lt;/b&gt; Type of event to be recorded.</source>
        <translation>&lt;b&gt;Tipo evento&lt;/b&gt; tipo dell&apos;evento da registrare.</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="20768"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-10) to be recorded</source>
        <translation type="obsolete">&lt;b&gt;Tipo evento&lt;/b&gt; valore dell&apos;evento (1-10) da registrare</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22254"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action at the time of the event</source>
        <translation>&lt;b&gt;Azione&lt;/b&gt;performa un&apos;azione al momento dell&apos;evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22255"/>
        <source>&lt;b&gt;Documentation&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by the event value):</source>
        <translation>&lt;b&gt;Documentazione&lt;/b&gt; dipende dal tipo di azione ([]&apos; è sostituito dalvalore dell&apos; evento):</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22257"/>
        <source>Call Program: A program/script path (absolute or relative)</source>
        <translation type="unfinished">Chiama programma: percorso programma/documento (assoluto o relativo)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22258"/>
        <source>Multiple Event: Adds events of other button numbers separated by a comma: 1,2,3, etc.</source>
        <translation>Evento multiplo: aggiunge eventi di altri numeri di tasti separati da una virgola:1,2,3, etc.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22262"/>
        <source>&lt;b&gt;Button Visibility&lt;/b&gt; Hides/shows individual button</source>
        <translation>&lt;b&gt;Visibilità tasto&lt;/b&gt;nasconde/visualizza tasto individuale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22263"/>
        <source>&lt;b&gt;Keyboard Shorcut: &lt;/b&gt; [b] Hides/shows Extra Button Rows</source>
        <translation>&lt;b&gt;Tasti veloci: &lt;/b&gt; [b]nasconde/visualizza frecce tasti extra</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22731"/>
        <source>Background does not match number of labels</source>
        <translation>Sfondo non corrisponde al numero degli scritti</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26841"/>
        <source>Not enough time points for an ET curviness of {0}. Set curviness to {1}</source>
        <translation type="unfinished">Punti temporali non sufficienti per una curvatura della ET del {0}. Posiziona curvatura al {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27051"/>
        <source>Designer Config</source>
        <translation type="unfinished">Configurazioni disegnatore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26848"/>
        <source>Not enough time points for an BT curviness of {0}. Set curviness to {1}</source>
        <translation type="unfinished">Punti temporali non sufficienti per una curvatura della BT del {0}. Posiziona curvatura al {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27043"/>
        <source>CHARGE</source>
        <translation>CARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27044"/>
        <source>DRY END</source>
        <translation>ASCIUTTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27045"/>
        <source>FC START</source>
        <translation>INIZIO PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27046"/>
        <source>FC END</source>
        <translation>FINE PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27047"/>
        <source>SC START</source>
        <translation>INIZIO SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27048"/>
        <source>SC END</source>
        <translation>FINE SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27049"/>
        <source>DROP</source>
        <translation>SCARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26863"/>
        <source>Incorrect time format. Please recheck {0} time</source>
        <translation type="unfinished">Formato tempo non corretto.Prego controllo {0} tempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27050"/>
        <source>Times need to be in ascending order. Please recheck {0} time</source>
        <translation type="unfinished">Tempi devono essere in ordine crescente. Prego ricontrollo tempo {0} </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27015"/>
        <source>Designer has been reset</source>
        <translation type="unfinished">Il disegnatore è stato ripristinato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27872"/>
        <source>Serial Port Settings: {0}, {1}, {2}, {3}, {4}, {5}</source>
        <translation type="unfinished">Impostazoni porta seriale: {0}, {1}, {2}, {3}, {4}, {5}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28654"/>
        <source>External program</source>
        <translation>Programma esterno</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28920"/>
        <source>PID to control ET set to {0} {1} ; PID to read BT set to {2} {3}</source>
        <translation type="unfinished">PID al controllo ET impostato a {0} {1} ; PID a lettura BT impostato a {2} {3}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29137"/>
        <source>Device set to {0}. Now, check Serial Port settings</source>
        <translation type="unfinished">Dispositivo impostato a {0}. Ora, controllo impostazioni porta seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29275"/>
        <source>Device set to {0}. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo impostato a {0}. Ora, scelta porta seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28989"/>
        <source>Device set to CENTER 305, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation>Dispositivo impostato a CENTRO 305, che equivale a CENTRO 306. Ora, scelta porta seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29043"/>
        <source>Device set to {0}, which is equivalent to CENTER 309. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo impostato a {0}, che equivale a CENTRO 309. Ora, scelta porta seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29070"/>
        <source>Device set to {0}, which is equivalent to CENTER 303. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo impostato a {0}, che equivale a CENTRO 303. Ora, scelta porta seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29052"/>
        <source>Device set to {0}, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo impostato a {0}, che equivale a CENTRO 306. Ora, scelta porta seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29079"/>
        <source>Device set to {0}, which is equivalent to Omega HH506RA. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo impostato a {0}, che equivale a Omega HH506RA. Ora, scelta porta seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29180"/>
        <source>Device set to {0}, which is equivalent to Omega HH806AU. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo impostato a {0}, che equivale a Omega HH806AU. Ora, scelta porta seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29253"/>
        <source>Device set to {0}</source>
        <translation type="unfinished">Dispositivo impostato a {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29110"/>
        <source>Device set to {0}{1}</source>
        <translation type="unfinished">Dispositivo impostato a {0}{1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29171"/>
        <source>Device set to {0}, which is equivalent to CENTER 302. Now, chose serial port</source>
        <translation type="unfinished">Dispositivo impostato a {0}, che equivale a CENTRO 302. Ora, scelta porta seriale</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30003"/>
        <source>Color of {0} set to {1}</source>
        <translation type="unfinished">Colore di {0} imposta a {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30648"/>
        <source>Save Wheel graph</source>
        <translation>Salva grafico a ruota</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30652"/>
        <source>Wheel Graph saved</source>
        <translation>Grafico a ruota salvato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30965"/>
        <source>Load Alarms</source>
        <translation>Carica avvisi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31005"/>
        <source>Save Alarms</source>
        <translation>Salva avvisi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31037"/>
        <source>&lt;b&gt;Status:&lt;/b&gt; activate or deactive alarm</source>
        <translation>&lt;b&gt;Stato&lt;/b&gt; attiva o disattiva avviso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31038"/>
        <source>&lt;b&gt;If Alarm:&lt;/b&gt; alarm triggered only if the alarm with the given number was triggered before. Use 0 for no guard.</source>
        <translation>&lt;b&gt;Se avviso:&lt;/b&gt;avviso innescato solo se avviso con relativo numero è stato innescato precedentemente. Utilizza 0 per non controllare.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31040"/>
        <source>&lt;b&gt;From:&lt;/b&gt; alarm only triggered after the given event</source>
        <translation>&lt;b&gt;Da&lt;/b&gt;avviso innescato dopo manifestazione evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31041"/>
        <source>&lt;b&gt;Time:&lt;/b&gt; if not 00:00, alarm is triggered mm:ss after the event &apos;From&apos; happend</source>
        <translation>&lt;b&gt;Tempo&lt;/b&gt; se no 00:00, avviso innescato  mm:ss dopo evento &apos;Da&apos; avvenuto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31042"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; the temperature source that is observed</source>
        <translation>&lt;b&gt;Fonte&lt;/b&gt; la fonte temperatura che viene osservata</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31043"/>
        <source>&lt;b&gt;Condition:&lt;/b&gt; alarm is triggered if source rises above or below the specified temperature</source>
        <translation>&lt;b&gt;Condizione:&lt;/b&gt; avviso è innescato se la fonte cresce sopra o sotto la specifica temperatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31045"/>
        <source>&lt;b&gt;Action:&lt;/b&gt; if all conditions are fulfilled the alarm triggeres the corresponding action</source>
        <translation>&lt;b&gt;Azione:&lt;/b&gt; se tutte le condizioni sono soddisfatte l&apos;avviso innesca la corrispondente azione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31047"/>
        <source>&lt;b&gt;NOTE:&lt;/b&gt; each alarm is only triggered once</source>
        <translation>&lt;b&gt;NOTA:&lt;/b&gt; ogni avviso è innescato solo una volta</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33957"/>
        <source>OFF</source>
        <translation>SPENTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33950"/>
        <source>CONTINUOUS CONTROL</source>
        <translation>CONTROLLO CONTINUO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33963"/>
        <source>ON</source>
        <translation>ACCESO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33962"/>
        <source>STANDBY MODE</source>
        <translation>MODALITA&apos; STANDBY</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31999"/>
        <source>The rampsoak-mode tells how to start and end the ramp/soak</source>
        <translation>La modalità temperatura a rampe indica come iniziare e terminare la temperatura a rampe</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32000"/>
        <source>Your rampsoak mode in this pid is:</source>
        <translation>La tua modalità a rampe in questo PID è:</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32001"/>
        <source>Mode = {0}</source>
        <translation type="unfinished">Modalità = {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32003"/>
        <source>Start to run from PV value: {0}</source>
        <translation type="unfinished">Inizia a scorrere da valore PV: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32004"/>
        <source>End output status at the end of ramp/soak: {0}</source>
        <translation type="unfinished">Termina stato uscita al termine della temperatura a rampe: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32006"/>
        <source>
Repeat Operation at the end: {0}</source>
        <translation type="unfinished">Ripetizione operazione al termine: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32008"/>
        <source>Recomended Mode = 0</source>
        <translation>Modalità raccomandata = 0</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32009"/>
        <source>If you need to change it, change it now and come back later</source>
        <translation>Se hai necessità di cambiarlo, cambialo ora e ritorna in seguito</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32010"/>
        <source>Use the Parameter Loader Software by Fuji if you need to

</source>
        <translation type="unfinished">Utilizza il Parametro Caricatore Software da Fuji se necessario</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32011"/>
        <source>Continue?</source>
        <translation>Continua?</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="31159"/>
        <source>RampSoak Mode</source>
        <translation type="obsolete">Modalità temperatura a rampe</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33367"/>
        <source>Current sv = {0}. Change now to sv = {1}?</source>
        <translation type="unfinished">Attuale sv = {0}. Cambia ora in sv = {1}?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33429"/>
        <source>Change svN</source>
        <translation>Cambia svN</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33428"/>
        <source>Current pid = {0}. Change now to pid ={1}?</source>
        <translation type="unfinished">Attuale pid = {0}. Cambia ora in pid = {1}?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34183"/>
        <source>Ramp Soak start-end mode</source>
        <translation>Temperatura a rampe modalità inizio-fine</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34089"/>
        <source>Pattern changed to {0}</source>
        <translation type="unfinished">Modello cambiato in {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34091"/>
        <source>Pattern did not changed</source>
        <translation>Modello non modificato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34094"/>
        <source>Ramp/Soak was found ON! Turn it off before changing the pattern</source>
        <translation>Temperatura a rampe è in funzione! Spegni prima di cambiare modalità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34096"/>
        <source>Ramp/Soak was found in Hold! Turn it off before changing the pattern</source>
        <translation>Temperatura a rampe è in attesa! Spegni prima di cambiare modalità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34439"/>
        <source>Activate PID front buttons</source>
        <translation>Attiva tasti frontali PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34439"/>
        <source>Remember SV memory has a finite
life of ~10,000 writes.

Proceed?</source>
        <translation>Ricorda la memoria SV ha una vita utile di circa 10.000 scritte. Procedere?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34548"/>
        <source>RS OFF</source>
        <translation>RS spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34550"/>
        <source>RS on HOLD</source>
        <translation>RS in attesa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34611"/>
        <source>PXG sv#{0} set to {1}</source>
        <translation type="unfinished">PXG sc{0} imposta in {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34632"/>
        <source>PXR sv set to {0}</source>
        <translation type="unfinished">PXR sv imposta in {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34667"/>
        <source>SV{0} changed from {1} to {2})</source>
        <translation type="unfinished">SV{0} cambiata da {1} in {2})</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34675"/>
        <source>Unable to set sv{0}</source>
        <translation type="unfinished">Impossibilitato ad impostare sv{0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34694"/>
        <source>Unable to set sv</source>
        <translation>Impossibilitato ad impostare sv</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34696"/>
        <source>Unable to set new sv</source>
        <translation>Impossibilitato ad impostare nuovo sv</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10144"/>
        <source>Exit Designer?</source>
        <translation>Uscita disegnatore?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10145"/>
        <source>Designer Mode ON</source>
        <translation>Modalitàdisegnatore attiva</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31044"/>
        <source>&lt;b&gt;Temp:&lt;/b&gt; the speficied temperature limit</source>
        <translation>&lt;b&gt;Temperatura:&lt;/b&gt; limite specifico di temperatura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2473"/>
        <source>Action canceled</source>
        <translation>Azione annllata</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17446"/>
        <source>Interpolation failed: no profile available</source>
        <translation>Interpolazione non riuscita: profilo non disponibile</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23082"/>
        <source>Playback Aid set ON at {0} secs</source>
        <translation type="unfinished">Aiuto riproduzione acceso a secondi {0} </translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23090"/>
        <source>No profile background found</source>
        <translation>Profilo in sottofondo non trovato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23186"/>
        <source>Reading background profile...</source>
        <translation>Lettura profilo sottofondo...</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27430"/>
        <source>Tick the Float flag in this case.</source>
        <translation type="unfinished">Spunta la bandierina fluttuante in questo caso.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28863"/>
        <source>Device not set</source>
        <translation>Dispositivo non impostato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11570"/>
        <source>To load this profile the extra devices configuration needs to be changed.
Continue?</source>
        <translation>Per caricare questo profilo la configurazione di altri dispositivi deve essere modificata. Continua?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11571"/>
        <source>Found a different number of curves</source>
        <translation>Trova un numero diverso di curve</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17637"/>
        <source>[ET target 1 = {0}] [BT target 1 = {1}] [ET target 2 = {2}] [BT target 2 = {3}]</source>
        <translation type="unfinished">[ET obiettivo 1 = {0}] [BT obiettivo 1 = {1}] [ET obiettivo 2 = {2}] [BT obiettivo 2 = {3}]</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22726"/>
        <source>Background profile not found</source>
        <translation>Profilo sottofondo non trovato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10427"/>
        <source>&lt;b&gt;[CRTL N]&lt;/b&gt; = Autosave + Reset + START</source>
        <translation>&lt;b&gt;[CRTL N]&lt;/b&gt; = Autosalvataggio + Resetta + START</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34546"/>
        <source>RS ON</source>
        <translation>RS acceso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34686"/>
        <source>SV changed from {0} to {1}</source>
        <translation type="unfinished">SV cambiato da {0} a {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32005"/>
        <source>Output status while ramp/soak operation set to OFF: {0}</source>
        <translation type="unfinished">Stato produzione mentre le operazioni temperatura a rampe sono impostate su OFF: {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22585"/>
        <source>Phases changed to {0} default: {1}</source>
        <translation type="unfinished">Fasi cambiate a {0} impostato: {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10438"/>
        <source>&lt;b&gt;[f]&lt;/b&gt; = Full Screen Mode</source>
        <translation>&lt;b&gt;[f]&lt;/b&gt; = modalità schermo intero</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15834"/>
        <source>Save Graph as PDF</source>
        <translation>Salva il grafico come PDF</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1962"/>
        <source>Alarm {0} triggered</source>
        <translation type="unfinished">Allarme {0} innescato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4479"/>
        <source>[TP] recorded at {0} BT = {1}</source>
        <translation type="unfinished">[Turning Point] registrato a {0} BT = {1}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5734"/>
        <source>Importing a profile in to Designer will decimate all data except the main [points].
Continue?</source>
        <translation>Impotare un profilo nel Disegnatore decima tutti i dati esclusi il principale [punti] Continua?</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10429"/>
        <source>&lt;b&gt;[d]&lt;/b&gt; = Toggle xy scale (T/Delta)</source>
        <translation>&lt;b&gt;[d]&lt;/b&gt; = pulsante scala xy (T/Delta)</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15123"/>
        <source>Warning</source>
        <translation>Avviso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15108"/>
        <source>Oversampling is only active with a sampling interval equal or larger than 3s.</source>
        <translation>Sovracampionamento è attivo solo con intervallo campionamento uguale o maggiore di 3 sec.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15123"/>
        <source>A tight sampling interval might lead to instability on some machines. We suggest a minimum of 3s.</source>
        <translation>Un campionamento con intervallo stretto può portare ad instabilità su alcune macchine. Suggeriamo un minimo di 3 s.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16093"/>
        <source>current background ET</source>
        <translation type="unfinished">Sfondo corrente ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16094"/>
        <source>current background BT</source>
        <translation type="unfinished">Sfondo corrente BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25514"/>
        <source>Phidget Temperature Sensor 4-input attached</source>
        <translation>Sensore temperatura Phidget 4entrate collegato</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24480"/>
        <source>Phidget Temperature Sensor 4-input not attached</source>
        <translation type="obsolete">Sensore temperatura Phidget 4 entrate non collegato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25716"/>
        <source>Phidget Bridge 4-input attached</source>
        <translation>Ponte Phidget 4entrate collegato</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24650"/>
        <source>Phidget Bridge 4-input not attached</source>
        <translation type="obsolete">Ponte Phidget 4entrate non collegato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29162"/>
        <source>Device set to {0}. Now, chose Modbus serial port</source>
        <translation type="unfinished">Dispositivo impostato a {0}. Ora, scegli porta seriale Modbus</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31039"/>
        <source>&lt;b&gt;But Not:&lt;/b&gt; alarm triggered only if the alarm with the given number was not triggered before. Use 0 for no guard.</source>
        <translation>&lt;b&gt;Se no:&lt;/b&gt;avviso innescato solo se avviso con relativo numero non è stato innescato precedentemente. Utilizza 0 per non controllare.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31046"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button or the new value of the slider</source>
        <translation>&lt;b&gt;Descrizione:&lt;/b&gt;testo del popup, nome del programma, numero del tasto dell&apos;evento o nuovo valore del cursore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32966"/>
        <source>Load PID Settings</source>
        <translation>Carica impostazioni PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33050"/>
        <source>Save PID Settings</source>
        <translation>Salva impostazioni PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25829"/>
        <source>Phidget 1018 IO attached</source>
        <translation>Phidget 1018 IO collegato</translation>
    </message>
    <message>
        <location filename="artisanlib/main.py" line="24931"/>
        <source>Phidget 1018 IO not attached</source>
        <translation type="obsolete">Phidget 1018 IO non collegato</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35177"/>
        <source>Load Ramp/Soak Table</source>
        <translation>Carica tabella temperatura a rampa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35196"/>
        <source>Save Ramp/Soak Table</source>
        <translation>Salva tabella temperatura a rampa</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35402"/>
        <source>PID turned on</source>
        <translation>PID acceso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35416"/>
        <source>PID turned off</source>
        <translation>PID spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10437"/>
        <source>&lt;b&gt;[q,w,e,r + &lt;i&gt;nn&lt;/i&gt;]&lt;/b&gt; = Quick Custom Event</source>
        <translation>&lt;b&gt;[q,w,e,r + &lt;i&gt;nn&lt;/i&gt;]&lt;b&gt; = evento personalizzato veloce</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2008"/>
        <source>Calling alarm failed on {0}</source>
        <translation type="unfinished">Chiama allarme mancato su {0}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16077"/>
        <source>Return the minimum of x and y.</source>
        <translation>Ritorno al minimo di x ed y.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16078"/>
        <source>Return the maximum of x and y.</source>
        <translation>Ritorno al massimo di x ed y.</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16086"/>
        <source>ET value</source>
        <translation type="unfinished">Valore ET</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16087"/>
        <source>BT value</source>
        <translation type="unfinished">Valore BT</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16088"/>
        <source>Extra #1 T1 value</source>
        <translation>Extra #1 Valore T1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16089"/>
        <source>Extra #1 T2 value</source>
        <translation>Extra #1 Valore T2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16090"/>
        <source>Extra #2 T1 value</source>
        <translation>Extra #2 Valore T1</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16091"/>
        <source>Extra #2 T2 value</source>
        <translation>Extra #2 Valore T2</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16097"/>
        <source>Yn holds values sampled in the actual interval if refering to ET/BT or extra channels from devices listed before, otherwise Yn hold values sampled in the previous interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22259"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) or wcoils(slaveId,register,[&amp;lt;bool&amp;gt;,..,&amp;lt;bool&amp;gt;]) writes values to the registers in slaves specified by the given ids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22261"/>
        <source>IO Command: set(n,0), set(n,1), toggle(n) to set Phidget IO digital output n</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25419"/>
        <source>Phidget Temperature Sensor IR attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25962"/>
        <source>Yocto Thermocouple attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25964"/>
        <source>Yocto PT100 attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27418"/>
        <source>The MODBUS device corresponds to input channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27419"/>
        <source>1 and 2.. The MODBUS_34 extra device adds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27420"/>
        <source>input channels 3 and 4. Inputs with slave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27421"/>
        <source>id set to 0 are turned off. Modbus function 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27422"/>
        <source>&apos;read holding register&apos; is the standard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27423"/>
        <source>Modbus function 4 triggers the use of &apos;read </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27424"/>
        <source>input register&apos;. Input registers (fct 4) usually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27425"/>
        <source> are from 30000-39999. Most devices hold data in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27426"/>
        <source>2 byte integer registers. A temperature of 145.2C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27427"/>
        <source>is often sent as 1452. In that case you have to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27428"/>
        <source>use the symbolic assignment &apos;x/10&apos;. Few devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27429"/>
        <source>hold data as 4 byte floats in two registers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22253"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-100) to be recorded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25423"/>
        <source>Phidget Temperature Sensor IR detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25518"/>
        <source>Phidget Temperature Sensor 4-input detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21461"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) or wcoils(slaveId,register,[&amp;lt;bool&amp;gt;,..,&amp;lt;bool&amp;gt;]) or wcoils(slaveId,register,&amp;lt;bool&amp;gt;) writes values to the registers in slaves specified by the given ids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25720"/>
        <source>Phidget Bridge 4-input detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25833"/>
        <source>Phidget 1018 IO detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15196"/>
        <source>Hottop control turned off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15207"/>
        <source>Hottop control turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15308"/>
        <source>Settings loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15368"/>
        <source>artisan-settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15369"/>
        <source>Save Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15372"/>
        <source>Settings saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15220"/>
        <source>PID Standby ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15225"/>
        <source>PID Standby OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19188"/>
        <source>Alarms from events #{0} created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19190"/>
        <source>No event selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19257"/>
        <source> Events #{0} deleted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Radio Button</name>
    <message>
        <location filename="../artisanlib/main.py" line="27933"/>
        <source>PID</source>
        <translation>PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27935"/>
        <source>Program</source>
        <translation>Programma</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27932"/>
        <source>Meter</source>
        <translation>Unità misura</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27934"/>
        <source>TC4</source>
        <translation>TC4</translation>
    </message>
</context>
<context>
    <name>Scope Annotation</name>
    <message>
        <location filename="../artisanlib/main.py" line="878"/>
        <source>Damper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="879"/>
        <source>Fan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="877"/>
        <source>Heater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="876"/>
        <source>Speed</source>
        <translation type="unfinished">Velocità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4410"/>
        <source>CHARGE 00:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2822"/>
        <source>CHARGE</source>
        <translation type="unfinished">Carico</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4471"/>
        <source>TP {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4510"/>
        <source>DE {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4566"/>
        <source>FCs {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4626"/>
        <source>FCe {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4679"/>
        <source>SCs {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4737"/>
        <source>SCe {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4799"/>
        <source>DROP {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4912"/>
        <source>CE {0}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Title</name>
    <message>
        <location filename="../artisanlib/main.py" line="11685"/>
        <source>Roaster Scope</source>
        <translation>Prospettiva tostatura</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../artisanlib/main.py" line="32349"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23085"/>
        <source>Playback Aid set OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33140"/>
        <source>Decimal position successfully set to 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33143"/>
        <source>Problem setting decimal position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33194"/>
        <source>Problem setting thermocouple type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34173"/>
        <source>setting autotune...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34200"/>
        <source>Autotune successfully turned OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34203"/>
        <source>Autotune successfully turned ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34101"/>
        <source>wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31844"/>
        <source>PID OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31847"/>
        <source>PID ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31882"/>
        <source>Empty SV box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31891"/>
        <source>Unable to read SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33996"/>
        <source>Ramp/Soak operation cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33999"/>
        <source>No RX data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34011"/>
        <source>Need to change pattern mode...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34020"/>
        <source>Pattern has been changed. Wait 5 secs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34023"/>
        <source>Pattern could not be changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34054"/>
        <source>RampSoak could not be changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34067"/>
        <source>RS successfully turned OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32109"/>
        <source>setONOFFrampsoak(): Ramp Soak could not be set OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34169"/>
        <source>Finished reading Ramp/Soak val.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32204"/>
        <source>Finished reading pid values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34280"/>
        <source>Ramp/Soak successfully written</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33103"/>
        <source>Time Units successfully set to MM:SS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33106"/>
        <source>Problem setting time units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33191"/>
        <source>Thermocouple type successfully set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33388"/>
        <source>Problem setting SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33390"/>
        <source>Cancelled svN change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33411"/>
        <source>setNsv(): bad response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33448"/>
        <source>setNpid(): bad confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33452"/>
        <source>Cancelled pid change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34070"/>
        <source>Ramp Soak could not be set OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34111"/>
        <source>PID set to OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34114"/>
        <source>PID set to ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34117"/>
        <source>Unable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34121"/>
        <source>No data received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34166"/>
        <source>problem reading Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34186"/>
        <source>Autotune cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34205"/>
        <source>UNABLE to set Autotune</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>SV</source>
        <translation type="unfinished">SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>Ramp (MM:SS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>Soak (MM:SS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32135"/>
        <source>getsegment(): problem reading ramp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32148"/>
        <source>getsegment(): problem reading soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33558"/>
        <source>setsv(): Unable to set SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33762"/>
        <source>getallpid(): Unable to read pid values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33793"/>
        <source>getallpid(): Unable to read current sv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35278"/>
        <source>Work in Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34037"/>
        <source>RS ON</source>
        <translation type="unfinished">RS acceso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34057"/>
        <source>RS OFF</source>
        <translation type="unfinished">RS spento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31871"/>
        <source>SV successfully set to {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34161"/>
        <source>Reading Ramp/Soak {0} ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32247"/>
        <source>{0} successfully sent to pid </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32256"/>
        <source>setpid(): There was a problem setting {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33384"/>
        <source>SV{0} set to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33408"/>
        <source>PID already using sv{0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33445"/>
        <source>pid changed to {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33470"/>
        <source>PID was already using pid {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33473"/>
        <source>setNpid(): Unable to set pid {0} </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33550"/>
        <source>SV{0} successfully set to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33676"/>
        <source>pid #{0} successfully set to ({1},{2},{3})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33684"/>
        <source>pid command failed. Bad data at pid{0} (8,8,8): ({1},{2},{3}) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33694"/>
        <source>sending commands for p{0} i{1} d{2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33790"/>
        <source>PID is using pid = {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33857"/>
        <source>PID is using SV = {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34182"/>
        <source>Current pid = {0}. Proceed with autotune command?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tab</name>
    <message>
        <location filename="../artisanlib/main.py" line="16986"/>
        <source>HUD</source>
        <translation type="unfinished">Display HUD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16989"/>
        <source>Plotter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16992"/>
        <source>Math</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21448"/>
        <source>Style</source>
        <translation type="unfinished">Stile</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35295"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18716"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23055"/>
        <source>Events</source>
        <translation type="unfinished">Eventi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23058"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23052"/>
        <source>Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21433"/>
        <source>Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21436"/>
        <source>Sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21442"/>
        <source>Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28457"/>
        <source>ET/BT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32957"/>
        <source>Extra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27721"/>
        <source>Modbus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27724"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28460"/>
        <source>Extra Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28463"/>
        <source>Symb ET/BT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29809"/>
        <source>Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29812"/>
        <source>LCDs</source>
        <translation type="unfinished">LCD</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32945"/>
        <source>RS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32948"/>
        <source>SV</source>
        <translation type="unfinished">SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32954"/>
        <source>Set RS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35165"/>
        <source>PID</source>
        <translation type="unfinished">PID</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16995"/>
        <source>UI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21439"/>
        <source>Quantifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27727"/>
        <source>Color</source>
        <translation type="unfinished">Colore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35169"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22430"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22434"/>
        <source>Espresso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28466"/>
        <source>Phidgets</source>
        <translation type="unfinished">Phidgets</translation>
    </message>
</context>
<context>
    <name>Table</name>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Documentation</source>
        <translation type="unfinished">Documentazione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>ET</source>
        <translation>Temperatura fumi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>BT</source>
        <translation>Temperatura chicchi</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Time</source>
        <translation type="unfinished">Tempo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23212"/>
        <source>Type</source>
        <translation type="unfinished">Tipo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23212"/>
        <source>Value</source>
        <translation type="unfinished">Valore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Action</source>
        <translation type="unfinished">Azione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Color</source>
        <translation type="unfinished">Colore</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Device</source>
        <translation type="unfinished">Dispositivo</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Comm Port</source>
        <translation type="unfinished">Porta comune</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Baud Rate</source>
        <translation type="unfinished">Velocità trasmissione dati</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Byte Size</source>
        <translation type="unfinished">Dimensione dati</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Parity</source>
        <translation type="unfinished">Parità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Stopbits</source>
        <translation type="unfinished">Bit di stop</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Timeout</source>
        <translation type="unfinished">Timeout</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Color 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Color 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Label 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Label 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>y1(x)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>y2(x)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>LCD 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>LCD 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Curve 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Curve 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Parent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Width</source>
        <translation type="unfinished">Ampiezza</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Opaqueness</source>
        <translation type="unfinished">Opacità</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Delete Wheel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Edit Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Update Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Starting angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Color Pattern</source>
        <translation type="unfinished">Colore schema</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>If Alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Source</source>
        <translation type="unfinished">Fonte</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Condition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>SV</source>
        <translation type="unfinished">SV</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>Ramp HH:MM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>Soak HH:MM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Projection</source>
        <translation type="unfinished">Proiezione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Text Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23301"/>
        <source>DRY END</source>
        <translation>ASCIUTTO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23304"/>
        <source>FC START</source>
        <translation>INIZIO PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23307"/>
        <source>FC END</source>
        <translation>FINE PC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23310"/>
        <source>SC START</source>
        <translation>INIZIO SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23313"/>
        <source>SC END</source>
        <translation>FINE SC</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23319"/>
        <source>COOL</source>
        <translation>RAFFREDDATO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19450"/>
        <source>EVENT #{1} {2}{3}</source>
        <translation type="obsolete">EVENTO #{1} {2}{3}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23316"/>
        <source>DROP</source>
        <translation>SCARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>DeltaET</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>DeltaBT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23298"/>
        <source>CHARGE</source>
        <translation>CARICO</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23323"/>
        <source>EVENT #{0} {1}{2}</source>
        <translation type="unfinished">EVENTO #{0} {1}{2}</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>But Not</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Beep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19680"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19680"/>
        <source>Weight</source>
        <translation type="unfinished">Peso</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Nr</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Textbox</name>
    <message>
        <location filename="../artisanlib/main.py" line="560"/>
        <source>Acidity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="518"/>
        <source>Clean Cup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="481"/>
        <source>Head</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="508"/>
        <source>Fragance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="550"/>
        <source>Sweetness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="542"/>
        <source>Aroma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="565"/>
        <source>Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="559"/>
        <source>Body</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="491"/>
        <source>Sour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="536"/>
        <source>Flavor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="493"/>
        <source>Critical
Stimulus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="546"/>
        <source>Aftertaste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="495"/>
        <source>Bitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="496"/>
        <source>Astringency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="497"/>
        <source>Solubles
Concentration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="544"/>
        <source>Mouthfeel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="499"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="500"/>
        <source>Aromatic
Complexity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="501"/>
        <source>Roast
Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="502"/>
        <source>Aromatic
Pungency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="503"/>
        <source>Sweet</source>
        <translation type="unfinished">Dolcezza</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="505"/>
        <source>pH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="515"/>
        <source>Dry Fragrance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="516"/>
        <source>Uniformity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="517"/>
        <source>Complexity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="523"/>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="524"/>
        <source>Wet Aroma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="558"/>
        <source>Fragrance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="528"/>
        <source>Taste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="529"/>
        <source>Nose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="534"/>
        <source>Fragrance-Aroma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="545"/>
        <source>Flavour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="553"/>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="555"/>
        <source>Roast Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="556"/>
        <source>Crema Texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="557"/>
        <source>Crema Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="561"/>
        <source>Bitterness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="562"/>
        <source>Defects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="563"/>
        <source>Aroma Intensity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="564"/>
        <source>Aroma Persistence</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tooltip</name>
    <message>
        <location filename="../artisanlib/main.py" line="8369"/>
        <source>Increases the current SV value by 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8377"/>
        <source>Increases the current SV value by 10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8385"/>
        <source>Increases the current SV value by 20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8393"/>
        <source>Decreases the current SV value by 20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8401"/>
        <source>Decreases the current SV value by 10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8409"/>
        <source>Decreases the current SV value by 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8423"/>
        <source>Turns ON/OFF the HUD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8494"/>
        <source>Timer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8495"/>
        <source>ET Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8496"/>
        <source>BT Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8497"/>
        <source>ET/time (degrees/min)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8498"/>
        <source>BT/time (degrees/min)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8499"/>
        <source>Value of SV in PID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8500"/>
        <source>PID power %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8579"/>
        <source>Number of events found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8589"/>
        <source>Type of event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8595"/>
        <source>Value of event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8607"/>
        <source>Updates the event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30219"/>
        <source>Save image using current graph size to a png format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16783"/>
        <source>linear: linear interpolation
cubic: 3rd order spline interpolation
nearest: y value of the nearest point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19822"/>
        <source>ON/OFF logs serial communication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19912"/>
        <source>Automatic generated name = This text + date + time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19915"/>
        <source>ON/OFF of automatic saving when pressing keyboard letter [a]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19926"/>
        <source>Sets the directory to store batch profiles when using the letter [a]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20618"/>
        <source>Allows to enter a description of the last event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20837"/>
        <source>Add new extra Event button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20842"/>
        <source>Delete the last extra Event button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30760"/>
        <source>Show help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20880"/>
        <source>Backup all palettes to a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21212"/>
        <source>Action Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21217"/>
        <source>Action String</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30172"/>
        <source>Aspect Ratio</source>
        <translation type="unfinished">Rapprto di aspetto</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28598"/>
        <source>Example: 100 + 2*x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28599"/>
        <source>Example: 100 + x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30161"/>
        <source>Erases wheel parent hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30165"/>
        <source>Sets graph hierarchy child-&gt;parent instead of parent-&gt;child</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30179"/>
        <source>Increase size of text in all the graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30182"/>
        <source>Decrease size of text in all the graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30186"/>
        <source>Decorative edge beween wheels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30192"/>
        <source>Line thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30197"/>
        <source>Line color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30201"/>
        <source>Apply color pattern to whole graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30207"/>
        <source>Add new wheel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30210"/>
        <source>Rotate graph 1 degree counter clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30213"/>
        <source>Rotate graph 1 degree clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30217"/>
        <source>Save graph to a text file.wg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30222"/>
        <source>Sets Wheel graph to view mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30225"/>
        <source>open graph file.wg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30228"/>
        <source>Close wheel graph editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16155"/>
        <source>&lt;b&gt;Label&lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16156"/>
        <source>&lt;b&gt;Description &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16158"/>
        <source>&lt;b&gt;Type &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16159"/>
        <source>&lt;b&gt;Value &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16160"/>
        <source>&lt;b&gt;Documentation &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16161"/>
        <source>&lt;b&gt;Button# &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4158"/>
        <source>Stop monitoring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8267"/>
        <source>Start monitoring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4314"/>
        <source>Stop recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8280"/>
        <source>Start recording</source>
        <translation type="unfinished">Inizio registrazione</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8329"/>
        <source>Reset</source>
        <translation type="unfinished">Azzera</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20881"/>
        <source>Restore all palettes from a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30765"/>
        <source>Clear alarms table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19974"/>
        <source>Batch prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19989"/>
        <source>ON/OFF batch counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21219"/>
        <source>Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8295"/>
        <source>First Crack Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8302"/>
        <source>First Crack End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8309"/>
        <source>Second Crack Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8316"/>
        <source>Second Crack End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8337"/>
        <source>Charge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8345"/>
        <source>Drop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8360"/>
        <source>Event</source>
        <translation type="unfinished">Evento</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8432"/>
        <source>Dry End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8440"/>
        <source>Cool End</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
