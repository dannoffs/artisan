<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="sv" sourcelanguage="">
<context>
    <name>About</name>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>Core developers:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15019"/>
        <source>Contributors:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="../artisanlib/main.py" line="30475"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35151"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32672"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16792"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30736"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30745"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19923"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29602"/>
        <source>Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30218"/>
        <source>Save Img</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35128"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22861"/>
        <source>Align</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16727"/>
        <source>Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31048"/>
        <source>Help</source>
        <translation type="unfinished">Hjälp</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28050"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31356"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26765"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29647"/>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29491"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29498"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29505"/>
        <source>Y Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29512"/>
        <source>X Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29547"/>
        <source>ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29554"/>
        <source>BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29561"/>
        <source>DeltaET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29568"/>
        <source>DeltaBT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29575"/>
        <source>Markers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29582"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29589"/>
        <source>Watermarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29596"/>
        <source>C Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29605"/>
        <source>Grey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29659"/>
        <source>LED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29691"/>
        <source>B/W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30160"/>
        <source>Reset Parents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30164"/>
        <source>Reverse Hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30178"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30181"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30196"/>
        <source>Line Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30209"/>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30212"/>
        <source>&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30215"/>
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30224"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30500"/>
        <source>Set Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31351"/>
        <source>Read Ra/So values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32413"/>
        <source>RampSoak ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32415"/>
        <source>RampSoak OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32418"/>
        <source>PID OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32420"/>
        <source>PID ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32535"/>
        <source>Write SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31388"/>
        <source>Set p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31389"/>
        <source>Set i</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31390"/>
        <source>Set d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32668"/>
        <source>Autotune ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32670"/>
        <source>Autotune OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35003"/>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32409"/>
        <source>Read RS values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32471"/>
        <source>Write SV1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32473"/>
        <source>Write SV2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32475"/>
        <source>Write SV3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32477"/>
        <source>Write SV4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32479"/>
        <source>Write SV5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32481"/>
        <source>Write SV6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32483"/>
        <source>Write SV7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32537"/>
        <source>ON SV buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32540"/>
        <source>OFF SV buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32543"/>
        <source>Read SV (7-0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32650"/>
        <source>pid 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32652"/>
        <source>pid 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32654"/>
        <source>pid 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32656"/>
        <source>pid 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32658"/>
        <source>pid 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32660"/>
        <source>pid 6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32662"/>
        <source>pid 7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30730"/>
        <source>All On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30733"/>
        <source>All Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35282"/>
        <source>Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32798"/>
        <source>Set ET PID to 1 decimal point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32800"/>
        <source>Set BT PID to 1 decimal point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32802"/>
        <source>Set ET PID to MM:SS time units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22621"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22624"/>
        <source>Save Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22932"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22934"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22936"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22938"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8265"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8278"/>
        <source>START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4157"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8291"/>
        <source>FC
START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8298"/>
        <source>FC
END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8305"/>
        <source>SC
START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8312"/>
        <source>SC
END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8320"/>
        <source>RESET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8333"/>
        <source>CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8341"/>
        <source>DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8349"/>
        <source>Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8356"/>
        <source>EVENT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8364"/>
        <source>SV +5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8372"/>
        <source>SV +10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8380"/>
        <source>SV +20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8388"/>
        <source>SV -20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8396"/>
        <source>SV -10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8404"/>
        <source>SV -5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8412"/>
        <source>HUD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8427"/>
        <source>DRY
END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8436"/>
        <source>COOL
END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16724"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17692"/>
        <source>unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18465"/>
        <source>in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18470"/>
        <source>out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18154"/>
        <source>Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18490"/>
        <source>calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18480"/>
        <source>scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19770"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20861"/>
        <source>&lt;&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20863"/>
        <source>&gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35131"/>
        <source>Save</source>
        <translation type="unfinished">Spara</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21073"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30477"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29519"/>
        <source>Drying Phase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29526"/>
        <source>Maillard Phase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29533"/>
        <source>Development Phase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29540"/>
        <source>Cooling Phase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30221"/>
        <source>View Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30740"/>
        <source>Insert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30764"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31367"/>
        <source>SV Buttons ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31369"/>
        <source>SV Buttons OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31371"/>
        <source>Read SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31405"/>
        <source>Read PID Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32405"/>
        <source>Write All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32411"/>
        <source>Write RS values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32545"/>
        <source>Write SV (7-0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32664"/>
        <source>Read PIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32666"/>
        <source>Write PIDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35153"/>
        <source>On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35155"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35283"/>
        <source>Write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18149"/>
        <source>Create Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16733"/>
        <source>BT/ET</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="22847"/>
        <source>DeltaET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22848"/>
        <source>DeltaBT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16504"/>
        <source>Projection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16921"/>
        <source>Beep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22324"/>
        <source>Auto Adjusted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22844"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22845"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22846"/>
        <source>Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23358"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23359"/>
        <source>Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23362"/>
        <source>Evaluation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23363"/>
        <source>Characteristics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26565"/>
        <source>DRY END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26567"/>
        <source>FC START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26569"/>
        <source>FC END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26571"/>
        <source>SC START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26573"/>
        <source>SC END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27906"/>
        <source>ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20608"/>
        <source>Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20617"/>
        <source>Mini Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22629"/>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18413"/>
        <source>Heavy FC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18416"/>
        <source>Low FC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18419"/>
        <source>Light Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18422"/>
        <source>Dark Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18425"/>
        <source>Drops</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18428"/>
        <source>Oily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18431"/>
        <source>Uneven</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18433"/>
        <source>Tipping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18435"/>
        <source>Scorching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18437"/>
        <source>Divots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16460"/>
        <source>Smooth Spikes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16465"/>
        <source>Smooth2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16470"/>
        <source>Drop Spikes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16475"/>
        <source>Limits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16594"/>
        <source>Decimal Places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16948"/>
        <source>Alarm Popups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18140"/>
        <source>Delete roast properties on RESET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19821"/>
        <source>Serial Log ON/OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19913"/>
        <source>Autosave [a]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19987"/>
        <source>Batch Counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20123"/>
        <source>Lock Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20126"/>
        <source>Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20806"/>
        <source>Auto CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20809"/>
        <source>Auto DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20812"/>
        <source>Mark TP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21122"/>
        <source>CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21176"/>
        <source>DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21185"/>
        <source>COOL END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22327"/>
        <source>Watermarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22329"/>
        <source>Phases LCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22331"/>
        <source>Auto DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22333"/>
        <source>Auto FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22849"/>
        <source>Align FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22968"/>
        <source>Playback Aid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23360"/>
        <source>/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23361"/>
        <source>ETBTa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27909"/>
        <source>BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27979"/>
        <source>Modbus Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28012"/>
        <source>Control Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28243"/>
        <source>Ratiometric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30768"/>
        <source>Load alarms from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35084"/>
        <source>Start PID on CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35139"/>
        <source>Load Ramp/Soak table from profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20611"/>
        <source>Annotations</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComboBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1016"/>
        <source>Power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1017"/>
        <source>Damper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1018"/>
        <source>Fan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16506"/>
        <source>newton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17614"/>
        <source>metrics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17616"/>
        <source>thermal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>cubic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16780"/>
        <source>nearest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20399"/>
        <source>g</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20400"/>
        <source>Kg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19381"/>
        <source>ml</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18317"/>
        <source>l</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20101"/>
        <source>upper right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20102"/>
        <source>upper left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20103"/>
        <source>lower left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20104"/>
        <source>lower right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20105"/>
        <source>right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20106"/>
        <source>center left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20107"/>
        <source>center right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20108"/>
        <source>lower center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20109"/>
        <source>upper center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20110"/>
        <source>center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20379"/>
        <source>Event #0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20401"/>
        <source>lb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20422"/>
        <source>liter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20423"/>
        <source>gallon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20424"/>
        <source>quart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20425"/>
        <source>pint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20426"/>
        <source>cup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20427"/>
        <source>cm^3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Multiple Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29612"/>
        <source>grey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29613"/>
        <source>Dark Grey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29614"/>
        <source>Slate Grey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29615"/>
        <source>Light Gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29616"/>
        <source>Black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29617"/>
        <source>White</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29618"/>
        <source>Transparent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Flat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Perpendicular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30491"/>
        <source>Radial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>DRY END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>FC START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>FC END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>SC START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31120"/>
        <source>ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31121"/>
        <source>BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20133"/>
        <source>1 minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20134"/>
        <source>2 minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20135"/>
        <source>3 minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20136"/>
        <source>4 minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20137"/>
        <source>5 minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20163"/>
        <source>solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20164"/>
        <source>dashed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20165"/>
        <source>dashed-dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20166"/>
        <source>dotted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1015"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16660"/>
        <source>classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16660"/>
        <source>xkcd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Humor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16672"/>
        <source>Comic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31118"/>
        <source>DeltaET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31119"/>
        <source>DeltaBT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20623"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20624"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Serial Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Modbus Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>DTA Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Call Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Heater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Fan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>Hottop Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21783"/>
        <source>IO Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26747"/>
        <source>SV Commands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26747"/>
        <source>Ramp Commands</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27391"/>
        <source>little-endian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>TP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>SC END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31151"/>
        <source>COOL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31178"/>
        <source>below</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31178"/>
        <source>above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Pop Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Event Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>Slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>FCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>SCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>SCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>COOL END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20381"/>
        <source>Event #{0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>RampSoak ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>RampSoak OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>PID ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31188"/>
        <source>PID OFF</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Contextual Menu</name>
    <message>
        <location filename="../artisanlib/main.py" line="5936"/>
        <source>Add point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5940"/>
        <source>Remove point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5946"/>
        <source>Reset Designer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5950"/>
        <source>Exit Designer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5922"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5926"/>
        <source>Config...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6530"/>
        <source>Add to Cupping Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6534"/>
        <source>Add to Roasting Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6538"/>
        <source>Cancel selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6542"/>
        <source>Edit Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6546"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Directory</name>
    <message>
        <location filename="../artisanlib/main.py" line="15803"/>
        <source>profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15793"/>
        <source>other</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Error Message</name>
    <message>
        <location filename="../artisanlib/main.py" line="25044"/>
        <source>HH506RAtemperature(): Unable to get id from HH506RA device </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34814"/>
        <source>Segment values could not be written into PID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34552"/>
        <source>RampSoak could not be changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35213"/>
        <source>Exception:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26453"/>
        <source>Value Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10419"/>
        <source>Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30654"/>
        <source>IO Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="13930"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17261"/>
        <source>Univariate: no profile data available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17432"/>
        <source>Polyfit: no profile data available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19414"/>
        <source>Unable to move CHARGE to a value that does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23814"/>
        <source>Modbus Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26509"/>
        <source>Serial Exception:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24185"/>
        <source>F80h Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24195"/>
        <source>CRC16 data corruption ERROR. TX does not match RX. Check wiring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24198"/>
        <source>No RX data received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24650"/>
        <source>Unable to open serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24890"/>
        <source>HH806Wtemperature(): Unable to initiate device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26046"/>
        <source>Arduino could not set channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26057"/>
        <source>Arduino could not set temperature unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26069"/>
        <source>Arduino could not set filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27874"/>
        <source>Serial Exception: invalid comm port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27879"/>
        <source>Serial Exception: timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24326"/>
        <source>DTAcommand(): {0} bytes received but 15 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24768"/>
        <source>MS6514temperature(): {0} bytes received but 16 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="24830"/>
        <source>HH806AUtemperature(): {0} bytes received but 16 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25019"/>
        <source>HH506RAGetID: {0} bytes received but 5 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25064"/>
        <source>HH506RAtemperature(): {0} bytes received but 14 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25119"/>
        <source>CENTER302temperature(): {0} bytes received but 7 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25188"/>
        <source>CENTER303temperature(): {0} bytes received but 8 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25258"/>
        <source>CENTER306temperature(): {0} bytes received but 10 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25344"/>
        <source>CENTER309temperature(): {0} bytes received but 45 needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34870"/>
        <source>pid.readoneword(): {0} RX bytes received (7 needed) for unit ID={1}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Flavor Scope Label</name>
    <message>
        <location filename="../artisanlib/main.py" line="14811"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14812"/>
        <source>Grassy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14813"/>
        <source>Leathery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14814"/>
        <source>Toasty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14815"/>
        <source>Bready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14816"/>
        <source>Acidic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14817"/>
        <source>Flat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14818"/>
        <source>Fracturing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14819"/>
        <source>Sweet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14820"/>
        <source>Less Sweet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form Caption</name>
    <message>
        <location filename="../artisanlib/main.py" line="16402"/>
        <source>Extras</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18006"/>
        <source>Roast Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19864"/>
        <source>Error Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19889"/>
        <source>Message History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19948"/>
        <source>AutoSave Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20364"/>
        <source>Roast Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20604"/>
        <source>Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22273"/>
        <source>Roast Phases</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22602"/>
        <source>Cup Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22836"/>
        <source>Profile Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23355"/>
        <source>Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26559"/>
        <source>Designer Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27098"/>
        <source>Manual Temperature Logger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27212"/>
        <source>Serial Ports Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27896"/>
        <source>Device Assignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29478"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30151"/>
        <source>Wheel Graph Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30725"/>
        <source>Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32346"/>
        <source>Fuji PXG PID Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20047"/>
        <source>Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17657"/>
        <source>Volume Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19581"/>
        <source>Tare Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19721"/>
        <source>Artisan Platform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19761"/>
        <source>Settings Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19820"/>
        <source>Serial Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19910"/>
        <source>Keyboard Autosave [a]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19972"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31322"/>
        <source>Fuji PXR PID Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34909"/>
        <source>Arduino Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35275"/>
        <source>Delta DTA PID Control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupBox</name>
    <message>
        <location filename="../artisanlib/main.py" line="27918"/>
        <source>Curves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16628"/>
        <source>HUD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16844"/>
        <source>Interpolate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16850"/>
        <source>Univariate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18661"/>
        <source>Times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisan.py" line="8670"/>
        <source>Temperature</source>
        <translation type="obsolete">Temperatur</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20233"/>
        <source>Legend Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20477"/>
        <source>Rate of Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20479"/>
        <source>Temperature Conversion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20481"/>
        <source>Weight Conversion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20483"/>
        <source>Volume Conversion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21111"/>
        <source>Event Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23469"/>
        <source>Evaluation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23471"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28363"/>
        <source>PID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29773"/>
        <source>Timer LCD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29776"/>
        <source>ET LCD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29779"/>
        <source>BT LCD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30237"/>
        <source>Label Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20227"/>
        <source>Time Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20229"/>
        <source>Temperature Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20235"/>
        <source>Grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27927"/>
        <source>LCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16642"/>
        <source>Input Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16687"/>
        <source>Look</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16877"/>
        <source>Polyfit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16908"/>
        <source>Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16916"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16928"/>
        <source>Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16970"/>
        <source>WebLCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20231"/>
        <source>DeltaBT/DeltaET Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21262"/>
        <source>Default Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21277"/>
        <source>Sampling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21315"/>
        <source>Management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26821"/>
        <source>Initial Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27593"/>
        <source>Input 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27607"/>
        <source>Input 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27620"/>
        <source>Input 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27633"/>
        <source>Input 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28116"/>
        <source>Phidgets 1048/1051</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28157"/>
        <source>Phidgets 1045</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28232"/>
        <source>Phidgets 1046 RTD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28310"/>
        <source>Phidget IO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28337"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28390"/>
        <source>Arduino TC4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28397"/>
        <source>External Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28403"/>
        <source>Symbolic Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29782"/>
        <source>DeltaET LCD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29785"/>
        <source>DeltaBT LCD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29788"/>
        <source>Extra Devices / PID SV LCD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34912"/>
        <source>p-i-d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35075"/>
        <source>Set Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTML Report Template</name>
    <message>
        <location filename="../artisanlib/main.py" line="14190"/>
        <source>Roasting Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14215"/>
        <source>Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14219"/>
        <source>Beans:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14227"/>
        <source>Weight:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14235"/>
        <source>Volume:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14251"/>
        <source>Roaster:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14255"/>
        <source>Operator:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14259"/>
        <source>Cupping:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14279"/>
        <source>DRY:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14283"/>
        <source>FCs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14287"/>
        <source>FCe:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14291"/>
        <source>SCs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14295"/>
        <source>SCe:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14365"/>
        <source>Roasting Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14345"/>
        <source>Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14375"/>
        <source>Cupping Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14223"/>
        <source>Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14231"/>
        <source>Degree:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14239"/>
        <source>Density:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14243"/>
        <source>Moisture:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14247"/>
        <source>Ambient:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14263"/>
        <source>Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14271"/>
        <source>CHARGE:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14275"/>
        <source>TP:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14299"/>
        <source>DROP:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14303"/>
        <source>COOL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14307"/>
        <source>MET:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14311"/>
        <source>RoR:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14315"/>
        <source>ETBTa:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14319"/>
        <source>CM:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14327"/>
        <source>Drying:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14331"/>
        <source>Maillard:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14335"/>
        <source>Development:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14339"/>
        <source>Cooling:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14345"/>
        <source>Background:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Label</name>
    <message>
        <location filename="../artisanlib/main.py" line="26733"/>
        <source>ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27109"/>
        <source>BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5900"/>
        <source>DeltaET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5892"/>
        <source>DeltaBT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8521"/>
        <source>PID SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8574"/>
        <source>Event #&lt;b&gt;0 &lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15564"/>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15566"/>
        <source>City+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15568"/>
        <source>Full City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15570"/>
        <source>Full City+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15572"/>
        <source>Light French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15574"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35006"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28015"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26562"/>
        <source>CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18040"/>
        <source>DRY END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18054"/>
        <source>FC START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18069"/>
        <source>FC END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18083"/>
        <source>SC START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18097"/>
        <source>SC END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26575"/>
        <source>DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18174"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18177"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18215"/>
        <source>Beans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18225"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18261"/>
        <source> in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18262"/>
        <source> out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18275"/>
        <source> %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18260"/>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18293"/>
        <source>Density</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18308"/>
        <source>per</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18383"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18389"/>
        <source>at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18401"/>
        <source>Roaster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18403"/>
        <source>Operator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18404"/>
        <source>Roasting Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18408"/>
        <source>Cupping Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20991"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20989"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20366"/>
        <source>Enter two times along profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20369"/>
        <source>Start (00:00)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20370"/>
        <source>End (00:00)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20389"/>
        <source>Fahrenheit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20390"/>
        <source>Celsius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22870"/>
        <source>Opaqueness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22890"/>
        <source>BT Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26732"/>
        <source>Curviness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26745"/>
        <source>Events Playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27492"/>
        <source>Comm Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27496"/>
        <source>Baud Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27502"/>
        <source>Byte Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27508"/>
        <source>Parity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27515"/>
        <source>Stopbits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27521"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27959"/>
        <source>Control ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28103"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27963"/>
        <source>Read BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35279"/>
        <source>SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31334"/>
        <source>Ramp/Soak Pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32407"/>
        <source>Pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32462"/>
        <source>SV (7-0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32581"/>
        <source>Write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32563"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32569"/>
        <source>I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32575"/>
        <source>D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>WARNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>Writing eeprom memory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>&lt;u&gt;Max life&lt;/u&gt; 10,000 writes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31378"/>
        <source>Infinite read life.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>After &lt;u&gt;writing&lt;/u&gt; an adjustment,&lt;br&gt;never power down the pid&lt;br&gt;for the next 5 seconds &lt;br&gt;or the pid may never recover.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31382"/>
        <source>Read operations manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20517"/>
        <source>Time syntax error. Time not valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20521"/>
        <source>Error: End time smaller than Start time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20543"/>
        <source>No profile found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8525"/>
        <source>PID %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28031"/>
        <source>ET Y(x)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28032"/>
        <source>BT Y(x)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30177"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30184"/>
        <source>Edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30190"/>
        <source>Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30199"/>
        <source>Color pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31327"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(1-4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31332"/>
        <source>Ramp Soak HH:MM&lt;br&gt;(5-8)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32354"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(1-7)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32360"/>
        <source>Ramp Soak (MM:SS)&lt;br&gt;(8-16)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20091"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20153"/>
        <source>Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20161"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20171"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32911"/>
        <source>ET Thermocouple type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32918"/>
        <source>BT Thermocouple type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32804"/>
        <source>Artisan uses 1 decimal point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32805"/>
        <source>Artisan Fuji PXG uses MINUTES:SECONDS units in Ramp/Soaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16702"/>
        <source>Y(x)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22603"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27984"/>
        <source>ET Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27987"/>
        <source>BT Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22883"/>
        <source>ET Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18126"/>
        <source>COOL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1608"/>
        <source>EVENT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35027"/>
        <source>min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6881"/>
        <source>/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3209"/>
        <source>BackgroundXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3215"/>
        <source>BackgroundET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3219"/>
        <source>BackgroundBT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3259"/>
        <source>BackgroundDeltaET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3263"/>
        <source>BackgroundDeltaBT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>ETBTa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>RoR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5242"/>
        <source>MET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5252"/>
        <source>CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26590"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6886"/>
        <source>Curves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6890"/>
        <source>Delta Curves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9392"/>
        <source>TP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9431"/>
        <source>DRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9480"/>
        <source>FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9376"/>
        <source>DRY%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9413"/>
        <source>RAMP%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9462"/>
        <source>DEV%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11390"/>
        <source>Start recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11404"/>
        <source>Charge the beans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14451"/>
        <source>/m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14477"/>
        <source>greens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="14482"/>
        <source>roasted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16414"/>
        <source>HUD Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16418"/>
        <source>ET Target 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16420"/>
        <source>BT Target 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16422"/>
        <source>ET Target 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16424"/>
        <source>BT Target 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16428"/>
        <source>ET p-i-d 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16435"/>
        <source>Smooth Deltas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16443"/>
        <source>Smooth Curves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16451"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35035"/>
        <source>max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16515"/>
        <source>Delta Span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16645"/>
        <source>Path Effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16665"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16813"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16814"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27412"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17683"/>
        <source>Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17819"/>
        <source>ml</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17783"/>
        <source>Unit Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17802"/>
        <source>g</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17800"/>
        <source>Kg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17817"/>
        <source>l</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17775"/>
        <source>in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17850"/>
        <source>out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18184"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18337"/>
        <source>Bean Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18343"/>
        <source>mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18345"/>
        <source>Whole Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18351"/>
        <source>Ground Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18362"/>
        <source>Moisture Greens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18371"/>
        <source>Moisture Roasted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18381"/>
        <source>Ambient Conditions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18489"/>
        <source>Ambient Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19996"/>
        <source>Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19999"/>
        <source>Counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20114"/>
        <source>Initial Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20621"/>
        <source>Bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20706"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26588"/>
        <source>Marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20710"/>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20712"/>
        <source>Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20714"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20825"/>
        <source>Max buttons per row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20853"/>
        <source>Color Pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20865"/>
        <source>palette #</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20869"/>
        <source>current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20985"/>
        <source>Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20889"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20891"/>
        <source>Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20893"/>
        <source>Offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20895"/>
        <source>Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34939"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20993"/>
        <source>Coarse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21195"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21210"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23404"/>
        <source>Drying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23405"/>
        <source>Maillard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23406"/>
        <source>Development</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22633"/>
        <source>Aspect Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22897"/>
        <source>XT Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22904"/>
        <source>XT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22918"/>
        <source>DeltaET Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22925"/>
        <source>DeltaBT Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22972"/>
        <source>Text Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22973"/>
        <source>sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23407"/>
        <source>Cooling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27247"/>
        <source>Settings for non-Modbus devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27365"/>
        <source>Slave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27370"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27375"/>
        <source>Float</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27376"/>
        <source>Function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27407"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27482"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27968"/>
        <source>RS485 Unit ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28007"/>
        <source>AT Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28296"/>
        <source>Async</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28299"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28142"/>
        <source>Emissivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28217"/>
        <source>Gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28218"/>
        <source>Wiring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28298"/>
        <source>Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28297"/>
        <source>Raw</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28316"/>
        <source>ServerId:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28318"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30170"/>
        <source>Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30485"/>
        <source> dg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30900"/>
        <source>Enter description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32705"/>
        <source>NOTE: BT Thermocouple type is not stored in the Artisan settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34955"/>
        <source>Cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35001"/>
        <source>Lookahead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35008"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35009"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35010"/>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35015"/>
        <source>SV Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35018"/>
        <source>SV Slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10451"/>
        <source>Event #&lt;b&gt;{0} &lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6364"/>
        <source>BT {0} {1}/min for {2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6380"/>
        <source>ET {0} {1}/min for {2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15871"/>
        <source>{0} to reach ET {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15882"/>
        <source> at {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15884"/>
        <source>{0} to reach BT {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15894"/>
        <source>{0} after FCs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15901"/>
        <source>{0} after FCe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15936"/>
        <source>ET - BT = {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15989"/>
        <source>ET - BT = {0}{1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19327"/>
        <source>Density in: {0} g/l   =&gt;   Density out: {1} g/l</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19365"/>
        <source>Moisture loss: {0}%    Organic loss: {1}%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19385"/>
        <source>({0} g/l)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19871"/>
        <source>Number of errors found {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20538"/>
        <source>Best approximation was made from {0} to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20539"/>
        <source>&lt;b&gt;{0}&lt;/b&gt; {1}/sec, &lt;b&gt;{2}&lt;/b&gt; {3}/min</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../const/UIconst.py" line="48"/>
        <source>Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="50"/>
        <source>Hide Others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="51"/>
        <source>Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="52"/>
        <source>Preferences...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="49"/>
        <source>Hide {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="74"/>
        <source>Quit {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="163"/>
        <source>About {0}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Marker</name>
    <message>
        <location filename="../artisanlib/main.py" line="20661"/>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20662"/>
        <source>Square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20663"/>
        <source>Pentagon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20664"/>
        <source>Diamond</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20665"/>
        <source>Star</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20666"/>
        <source>Hexagon 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20667"/>
        <source>Hexagon 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20668"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20669"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20670"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Menu</name>
    <message>
        <location filename="../const/UIconst.py" line="57"/>
        <source>File</source>
        <translation>Arkiv</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="60"/>
        <source>New</source>
        <translation>Ny</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="61"/>
        <source>Open...</source>
        <translation>Öppna...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="62"/>
        <source>Open Recent</source>
        <translation>Öppna senaste</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="63"/>
        <source>Import</source>
        <translation>Importera</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="64"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="65"/>
        <source>Save As...</source>
        <translation>Spara som...</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="35"/>
        <source>Export...</source>
        <translation type="obsolete">Exportera...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="67"/>
        <source>Save Graph</source>
        <translation>Spara graf</translation>
    </message>
    <message>
        <location filename="const/UIconst.py" line="40"/>
        <source>HTML Report</source>
        <translation type="obsolete">HTML-Rapport</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="70"/>
        <source>Print...</source>
        <translation>Skriv ut...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="85"/>
        <source>Roast</source>
        <translation>Rosta</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="88"/>
        <source>Properties...</source>
        <translation>Egenskaper...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="89"/>
        <source>Background...</source>
        <translation>Bakrund</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="90"/>
        <source>Cup Profile...</source>
        <translation>Koppningsprofil...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="91"/>
        <source>Temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="146"/>
        <source>Calculator</source>
        <translation>Beräkningar</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="99"/>
        <source>Config</source>
        <translation>Konfigurera</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="102"/>
        <source>Device...</source>
        <translation>Apparat...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="103"/>
        <source>Serial Port...</source>
        <translation>Serieport...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="104"/>
        <source>Sampling Interval...</source>
        <translation>Samplingsintervall...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="107"/>
        <source>Colors...</source>
        <translation>Färger...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="110"/>
        <source>Phases...</source>
        <translation>Faser...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="111"/>
        <source>Events...</source>
        <translation>Händelser...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="112"/>
        <source>Statistics...</source>
        <translation>Statistik...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="113"/>
        <source>Axes...</source>
        <translation>Axlar...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="114"/>
        <source>Autosave...</source>
        <translation>Spara automatiskt...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="149"/>
        <source>Extras...</source>
        <translation>Extra...</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="159"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="165"/>
        <source>Documentation</source>
        <translation>Dokumentation</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="168"/>
        <source>Errors</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="167"/>
        <source>Keyboard Shortcuts</source>
        <translation>Tangentbordsgenvägar</translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="169"/>
        <source>Messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="116"/>
        <source>Alarms...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="77"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="80"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="81"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="82"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="68"/>
        <source>Full Size...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="145"/>
        <source>Designer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="92"/>
        <source>Convert to Fahrenheit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="93"/>
        <source>Convert to Celsius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="94"/>
        <source>Fahrenheit Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="95"/>
        <source>Celsius Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7739"/>
        <source>HH506RA...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7743"/>
        <source>K202...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="142"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="147"/>
        <source>Wheel Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="176"/>
        <source>Factory Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="117"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7766"/>
        <source>CSV...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7770"/>
        <source>JSON...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7774"/>
        <source>RoastLogger...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="7747"/>
        <source>K204...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="66"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="69"/>
        <source>Roasting Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="96"/>
        <source>Switch Profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="106"/>
        <source>Oversampling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="115"/>
        <source>Batch...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="148"/>
        <source>LCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="153"/>
        <source>Load Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="154"/>
        <source>Load Recent Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="155"/>
        <source>Save Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="164"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="170"/>
        <source>Serial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="174"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="175"/>
        <source>Platform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="108"/>
        <source>Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../const/UIconst.py" line="109"/>
        <source>Sliders</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../artisanlib/main.py" line="1864"/>
        <source>HUD OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1878"/>
        <source>HUD ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1970"/>
        <source>Alarm notice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2463"/>
        <source>Save the profile, Discard the profile (Reset), or Cancel?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2464"/>
        <source>Profile unsaved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2473"/>
        <source>Action canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2547"/>
        <source>Scope has been reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3757"/>
        <source>Time format error encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3863"/>
        <source>Convert profile data to Fahrenheit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3948"/>
        <source>Convert Profile Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3904"/>
        <source>Profile changed to Fahrenheit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3907"/>
        <source>Unable to comply. You already are in Fahrenheit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3950"/>
        <source>Profile not changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3913"/>
        <source>Convert profile data to Celsius?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3948"/>
        <source>Unable to comply. You already are in Celsius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3954"/>
        <source>Profile changed to Celsius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3959"/>
        <source>Convert Profile Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3959"/>
        <source>No profile data found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3976"/>
        <source>Colors set to defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="3981"/>
        <source>Colors set to grey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4152"/>
        <source>Scope monitoring...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4197"/>
        <source>Scope stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4311"/>
        <source>Scope recording...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4352"/>
        <source>Scope recording stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4405"/>
        <source>Not enough variables collected yet. Try again in a few seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4920"/>
        <source>Scope is OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5104"/>
        <source>Timer is OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5298"/>
        <source>Statistics cancelled: need complete profile [CHARGE] + [DROP]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5483"/>
        <source>Unable to move background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5542"/>
        <source>No finished profile found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5560"/>
        <source>Polynomial coefficients (Horner form):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5563"/>
        <source>Knots:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5566"/>
        <source>Residual:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5569"/>
        <source>Roots:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5573"/>
        <source>Profile information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5734"/>
        <source>Designer Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5734"/>
        <source>Importing a profile in to Designer will decimate all data except the main [points].
Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5778"/>
        <source>Designer Init</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5778"/>
        <source>Unable to start designer.
Profile missing [CHARGE] or [DROP]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6024"/>
        <source>[ CHARGE ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6027"/>
        <source>[ DRY END ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6030"/>
        <source>[ FC START ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6033"/>
        <source>[ FC END ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6036"/>
        <source>[ SC START ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6039"/>
        <source>[ SC END ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6042"/>
        <source>[ DROP ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6305"/>
        <source>New profile created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30658"/>
        <source>Open Wheel Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6492"/>
        <source> added to cupping notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6498"/>
        <source> added to roasting notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6765"/>
        <source>Mouse Cross ON: move mouse around</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="6777"/>
        <source>Mouse cross OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9903"/>
        <source>Do you want to reset all settings?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="9904"/>
        <source>Factory Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10144"/>
        <source>Exit Designer?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10145"/>
        <source>Designer Mode ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10304"/>
        <source>Keyboard moves turned ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10313"/>
        <source>Keyboard moves turned OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10415"/>
        <source>Empty path or box unchecked in Autosave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10422"/>
        <source>&lt;b&gt;[ENTER]&lt;/b&gt; = Turns ON/OFF Keyboard Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10423"/>
        <source>&lt;b&gt;[SPACE]&lt;/b&gt; = Choses current button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10424"/>
        <source>&lt;b&gt;[LEFT]&lt;/b&gt; = Move to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10425"/>
        <source>&lt;b&gt;[RIGHT]&lt;/b&gt; = Move to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10426"/>
        <source>&lt;b&gt;[a]&lt;/b&gt; = Autosave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10427"/>
        <source>&lt;b&gt;[CRTL N]&lt;/b&gt; = Autosave + Reset + START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10428"/>
        <source>&lt;b&gt;[t]&lt;/b&gt; = Mouse cross lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10429"/>
        <source>&lt;b&gt;[d]&lt;/b&gt; = Toggle xy scale (T/Delta)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10430"/>
        <source>&lt;b&gt;[b]&lt;/b&gt; = Shows/Hides Extra Event Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10431"/>
        <source>&lt;b&gt;[s]&lt;/b&gt; = Shows/Hides Event Sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10432"/>
        <source>&lt;b&gt;[i]&lt;/b&gt; = Retrieve Weight In from Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10433"/>
        <source>&lt;b&gt;[o]&lt;/b&gt; = Retrieve Weight Out from Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10434"/>
        <source>&lt;b&gt;[0-9]&lt;/b&gt; = Changes Event Button Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10435"/>
        <source>&lt;b&gt;[;]&lt;/b&gt; = Application ScreenShot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10436"/>
        <source>&lt;b&gt;[:]&lt;/b&gt; = Desktop ScreenShot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10437"/>
        <source>&lt;b&gt;[q,w,e,r + &lt;i&gt;nn&lt;/i&gt;]&lt;/b&gt; = Quick Custom Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10438"/>
        <source>&lt;b&gt;[f]&lt;/b&gt; = Full Screen Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10440"/>
        <source>Keyboard Shotcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10599"/>
        <source>Save</source>
        <translation type="unfinished">Spara</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10611"/>
        <source>Select Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19230"/>
        <source>No profile found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10823"/>
        <source>Invalid artisan format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10951"/>
        <source>Artisan CSV file loaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11570"/>
        <source>To load this profile the extra devices configuration needs to be changed.
Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="11571"/>
        <source>Found a different number of curves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12348"/>
        <source>Save Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12355"/>
        <source>Profile saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15387"/>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12371"/>
        <source>Readings exported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12379"/>
        <source>Export CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12382"/>
        <source>Export JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12385"/>
        <source>Export RoastLogger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12394"/>
        <source>Readings imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12402"/>
        <source>Import CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12405"/>
        <source>Import JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="12408"/>
        <source>Import RoastLogger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15123"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15108"/>
        <source>Oversampling is only active with a sampling interval equal or larger than 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15113"/>
        <source>Sampling Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15113"/>
        <source>Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15123"/>
        <source>A tight sampling interval might lead to instability on some machines. We suggest a minimum of 3s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15196"/>
        <source>Hottop control turned off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15207"/>
        <source>Hottop control turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15308"/>
        <source>Settings loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15368"/>
        <source>artisan-settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15369"/>
        <source>Save Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15372"/>
        <source>Settings saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15489"/>
        <source>Alarm Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15489"/>
        <source>Alarms are not available for device None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15546"/>
        <source>Switch Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15546"/>
        <source>Language successfully changed. Restart the application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15578"/>
        <source>Import K202 CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15630"/>
        <source>K202 file loaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15644"/>
        <source>Import K204 CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15710"/>
        <source>K204 file loaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15724"/>
        <source>Import HH506RA CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15775"/>
        <source>HH506RA file loaded successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15820"/>
        <source>Save Graph as PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15834"/>
        <source>Save Graph as PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15836"/>
        <source>Save Graph as SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16046"/>
        <source>Invalid Wheel graph format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16049"/>
        <source>Wheel Graph succesfully open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16068"/>
        <source>Return the absolute value of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16069"/>
        <source>Return the arc cosine (measured in radians) of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16070"/>
        <source>Return the arc sine (measured in radians) of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16071"/>
        <source>Return the arc tangent (measured in radians) of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16072"/>
        <source>Return the cosine of x (measured in radians).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16073"/>
        <source>Convert angle x from radians to degrees.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16074"/>
        <source>Return e raised to the power of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16075"/>
        <source>Return the logarithm of x to the given base. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16076"/>
        <source>Return the base 10 logarithm of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16077"/>
        <source>Return the minimum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16078"/>
        <source>Return the maximum of x and y.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16079"/>
        <source>Return x**y (x to the power of y).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16080"/>
        <source>Convert angle x from degrees to radians.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16081"/>
        <source>Return the sine of x (measured in radians).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16082"/>
        <source>Return the square root of x.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16083"/>
        <source>Return the tangent of x (measured in radians).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16086"/>
        <source>ET value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16087"/>
        <source>BT value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16088"/>
        <source>Extra #1 T1 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16089"/>
        <source>Extra #1 T2 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16090"/>
        <source>Extra #2 T1 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16091"/>
        <source>Extra #2 T2 value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16093"/>
        <source>current background ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16094"/>
        <source>current background BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16097"/>
        <source>Yn holds values sampled in the actual interval if refering to ET/BT or extra channels from devices listed before, otherwise Yn hold values sampled in the previous interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16100"/>
        <source>MATHEMATICAL FUNCTIONS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16102"/>
        <source>SYMBOLIC VARIABLES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16104"/>
        <source>Symbolic Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16234"/>
        <source>Save Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16238"/>
        <source>Palettes saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16281"/>
        <source>Invalid palettes file format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16284"/>
        <source>Palettes loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16296"/>
        <source>Load Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16317"/>
        <source>Alarms loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17446"/>
        <source>Interpolation failed: no profile available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17455"/>
        <source>Sound turned ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17459"/>
        <source>Sound turned OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19269"/>
        <source>No events found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19566"/>
        <source>Roast properties updated but profile not saved to disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19960"/>
        <source>Autosave OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21457"/>
        <source>&lt;b&gt;Event&lt;/b&gt; hide or show the corresponding slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21458"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action on slider release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21459"/>
        <source>&lt;b&gt;Command&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by &lt;i&gt;value&lt;/i&gt;*&lt;i&gt;factor&lt;/i&gt; + &lt;i&gt;offset&lt;/i&gt;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22256"/>
        <source>Serial Command: ASCII serial command or binary a2b_uu(serial command)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21461"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) or wcoils(slaveId,register,[&amp;lt;bool&amp;gt;,..,&amp;lt;bool&amp;gt;]) or wcoils(slaveId,register,&amp;lt;bool&amp;gt;) writes values to the registers in slaves specified by the given ids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22260"/>
        <source>DTA Command: Insert Data address : value, ex. 4701:1000 and sv is 100. always multiply with 10 if value Unit: 0.1 / ex. 4719:0 stops heating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21463"/>
        <source>&lt;b&gt;Offset&lt;/b&gt; added as offset to the slider value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21464"/>
        <source>&lt;b&gt;Factor&lt;/b&gt; multiplicator of the slider value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22264"/>
        <source>Event custom buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22225"/>
        <source>Event configuration saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22228"/>
        <source>Found empty event type box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22250"/>
        <source>&lt;b&gt;Button Label&lt;/b&gt; Enter \n to create labels with multiple lines.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22251"/>
        <source>&lt;b&gt;Event Description&lt;/b&gt; Description of the Event to be recorded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22252"/>
        <source>&lt;b&gt;Event type&lt;/b&gt; Type of event to be recorded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22253"/>
        <source>&lt;b&gt;Event value&lt;/b&gt; Value of event (1-100) to be recorded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22254"/>
        <source>&lt;b&gt;Action&lt;/b&gt; Perform an action at the time of the event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22255"/>
        <source>&lt;b&gt;Documentation&lt;/b&gt; depends on the action type (&apos;{}&apos; is replaced by the event value):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22257"/>
        <source>Call Program: A program/script path (absolute or relative)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22258"/>
        <source>Multiple Event: Adds events of other button numbers separated by a comma: 1,2,3, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22259"/>
        <source>Modbus Command: write([slaveId,register,value],..,[slaveId,register,value]) or wcoils(slaveId,register,[&amp;lt;bool&amp;gt;,..,&amp;lt;bool&amp;gt;]) writes values to the registers in slaves specified by the given ids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22261"/>
        <source>IO Command: set(n,0), set(n,1), toggle(n) to set Phidget IO digital output n</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22262"/>
        <source>&lt;b&gt;Button Visibility&lt;/b&gt; Hides/shows individual button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22263"/>
        <source>&lt;b&gt;Keyboard Shorcut: &lt;/b&gt; [b] Hides/shows Extra Button Rows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22726"/>
        <source>Background profile not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22731"/>
        <source>Background does not match number of labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23090"/>
        <source>No profile background found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23186"/>
        <source>Reading background profile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25419"/>
        <source>Phidget Temperature Sensor IR attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25423"/>
        <source>Phidget Temperature Sensor IR detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25514"/>
        <source>Phidget Temperature Sensor 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25518"/>
        <source>Phidget Temperature Sensor 4-input detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25716"/>
        <source>Phidget Bridge 4-input attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25720"/>
        <source>Phidget Bridge 4-input detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25829"/>
        <source>Phidget 1018 IO attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25833"/>
        <source>Phidget 1018 IO detached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25962"/>
        <source>Yocto Thermocouple attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="25964"/>
        <source>Yocto PT100 attached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27051"/>
        <source>Designer Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27043"/>
        <source>CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27044"/>
        <source>DRY END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27045"/>
        <source>FC START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27046"/>
        <source>FC END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27047"/>
        <source>SC START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27048"/>
        <source>SC END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27049"/>
        <source>DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27015"/>
        <source>Designer has been reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27418"/>
        <source>The MODBUS device corresponds to input channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27419"/>
        <source>1 and 2.. The MODBUS_34 extra device adds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27420"/>
        <source>input channels 3 and 4. Inputs with slave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27421"/>
        <source>id set to 0 are turned off. Modbus function 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27422"/>
        <source>&apos;read holding register&apos; is the standard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27423"/>
        <source>Modbus function 4 triggers the use of &apos;read </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27424"/>
        <source>input register&apos;. Input registers (fct 4) usually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27425"/>
        <source> are from 30000-39999. Most devices hold data in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27426"/>
        <source>2 byte integer registers. A temperature of 145.2C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27427"/>
        <source>is often sent as 1452. In that case you have to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27428"/>
        <source>use the symbolic assignment &apos;x/10&apos;. Few devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27429"/>
        <source>hold data as 4 byte floats in two registers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27430"/>
        <source>Tick the Float flag in this case.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28654"/>
        <source>External program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28863"/>
        <source>Device not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28989"/>
        <source>Device set to CENTER 305, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30648"/>
        <source>Save Wheel graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30652"/>
        <source>Wheel Graph saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30965"/>
        <source>Load Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31005"/>
        <source>Save Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31037"/>
        <source>&lt;b&gt;Status:&lt;/b&gt; activate or deactive alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31038"/>
        <source>&lt;b&gt;If Alarm:&lt;/b&gt; alarm triggered only if the alarm with the given number was triggered before. Use 0 for no guard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31039"/>
        <source>&lt;b&gt;But Not:&lt;/b&gt; alarm triggered only if the alarm with the given number was not triggered before. Use 0 for no guard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31040"/>
        <source>&lt;b&gt;From:&lt;/b&gt; alarm only triggered after the given event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31041"/>
        <source>&lt;b&gt;Time:&lt;/b&gt; if not 00:00, alarm is triggered mm:ss after the event &apos;From&apos; happend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31042"/>
        <source>&lt;b&gt;Source:&lt;/b&gt; the temperature source that is observed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31043"/>
        <source>&lt;b&gt;Condition:&lt;/b&gt; alarm is triggered if source rises above or below the specified temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31044"/>
        <source>&lt;b&gt;Temp:&lt;/b&gt; the speficied temperature limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31045"/>
        <source>&lt;b&gt;Action:&lt;/b&gt; if all conditions are fulfilled the alarm triggeres the corresponding action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31046"/>
        <source>&lt;b&gt;Description:&lt;/b&gt; the text of the popup, the name of the program, the number of the event button or the new value of the slider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31047"/>
        <source>&lt;b&gt;NOTE:&lt;/b&gt; each alarm is only triggered once</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33957"/>
        <source>OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33950"/>
        <source>CONTINUOUS CONTROL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33963"/>
        <source>ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33962"/>
        <source>STANDBY MODE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31999"/>
        <source>The rampsoak-mode tells how to start and end the ramp/soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32000"/>
        <source>Your rampsoak mode in this pid is:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32008"/>
        <source>Recomended Mode = 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32009"/>
        <source>If you need to change it, change it now and come back later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32010"/>
        <source>Use the Parameter Loader Software by Fuji if you need to

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32011"/>
        <source>Continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34183"/>
        <source>Ramp Soak start-end mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32966"/>
        <source>Load PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33050"/>
        <source>Save PID Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33429"/>
        <source>Change svN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34091"/>
        <source>Pattern did not changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34094"/>
        <source>Ramp/Soak was found ON! Turn it off before changing the pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34096"/>
        <source>Ramp/Soak was found in Hold! Turn it off before changing the pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34439"/>
        <source>Activate PID front buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34439"/>
        <source>Remember SV memory has a finite
life of ~10,000 writes.

Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34546"/>
        <source>RS ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34548"/>
        <source>RS OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34550"/>
        <source>RS on HOLD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34694"/>
        <source>Unable to set sv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34696"/>
        <source>Unable to set new sv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35177"/>
        <source>Load Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35196"/>
        <source>Save Ramp/Soak Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35402"/>
        <source>PID turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35416"/>
        <source>PID turned off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="1962"/>
        <source>Alarm {0} triggered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2006"/>
        <source>Alarm is calling: {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2008"/>
        <source>Calling alarm failed on {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2015"/>
        <source>Alarm trigger button error, description &apos;{0}&apos; not a number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2043"/>
        <source>Alarm trigger slider error, description &apos;{0}&apos; not a valid number [0-100]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4459"/>
        <source>Roast time starts now 00:00 BT = {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4479"/>
        <source>[TP] recorded at {0} BT = {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4540"/>
        <source>[DRY END] recorded at {0} BT = {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4603"/>
        <source>[FC START] recorded at {0} BT = {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4658"/>
        <source>[FC END] recorded at {0} BT = {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4716"/>
        <source>[SC START] recorded at {0} BT = {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4773"/>
        <source>[SC END] recorded at {0} BT = {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4855"/>
        <source>Roast ended at {0} BT = {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4953"/>
        <source>[COOL END] recorded at {0} BT = {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5089"/>
        <source>Event # {0} recorded at BT = {1} Time = {2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="5136"/>
        <source>Computer Event # {0} recorded at BT = {1} Time = {2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10410"/>
        <source>Profile {0} saved in: {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10521"/>
        <source>Event #{0}:  {1} has been updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10660"/>
        <source>{0} has been saved. New roast has started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10704"/>
        <source>{0}  loaded </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="10819"/>
        <source>Background {0} loaded successfully {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15827"/>
        <source>{0}  size({1},{2}) saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15841"/>
        <source>{0} saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="17637"/>
        <source>[ET target 1 = {0}] [BT target 1 = {1}] [ET target 2 = {2}] [BT target 2 = {3}]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19227"/>
        <source>Event #{0} added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19263"/>
        <source> Event #{0} deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19956"/>
        <source>Autosave ON. Prefix: {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20335"/>
        <source>xlimit = ({2},{3}) ylimit = ({0},{1}) zlimit = ({4},{5})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22585"/>
        <source>Phases changed to {0} default: {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23082"/>
        <source>Playback Aid set ON at {0} secs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26841"/>
        <source>Not enough time points for an ET curviness of {0}. Set curviness to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26848"/>
        <source>Not enough time points for an BT curviness of {0}. Set curviness to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="26863"/>
        <source>Incorrect time format. Please recheck {0} time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27050"/>
        <source>Times need to be in ascending order. Please recheck {0} time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27872"/>
        <source>Serial Port Settings: {0}, {1}, {2}, {3}, {4}, {5}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28920"/>
        <source>PID to control ET set to {0} {1} ; PID to read BT set to {2} {3}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29137"/>
        <source>Device set to {0}. Now, check Serial Port settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29275"/>
        <source>Device set to {0}. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29043"/>
        <source>Device set to {0}, which is equivalent to CENTER 309. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29070"/>
        <source>Device set to {0}, which is equivalent to CENTER 303. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29052"/>
        <source>Device set to {0}, which is equivalent to CENTER 306. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29079"/>
        <source>Device set to {0}, which is equivalent to Omega HH506RA. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29180"/>
        <source>Device set to {0}, which is equivalent to Omega HH806AU. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29253"/>
        <source>Device set to {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29110"/>
        <source>Device set to {0}{1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29162"/>
        <source>Device set to {0}. Now, chose Modbus serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29171"/>
        <source>Device set to {0}, which is equivalent to CENTER 302. Now, chose serial port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30003"/>
        <source>Color of {0} set to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32001"/>
        <source>Mode = {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32003"/>
        <source>Start to run from PV value: {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32004"/>
        <source>End output status at the end of ramp/soak: {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32005"/>
        <source>Output status while ramp/soak operation set to OFF: {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32006"/>
        <source>
Repeat Operation at the end: {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33367"/>
        <source>Current sv = {0}. Change now to sv = {1}?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33428"/>
        <source>Current pid = {0}. Change now to pid ={1}?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34089"/>
        <source>Pattern changed to {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34611"/>
        <source>PXG sv#{0} set to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34632"/>
        <source>PXR sv set to {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34667"/>
        <source>SV{0} changed from {1} to {2})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34675"/>
        <source>Unable to set sv{0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34686"/>
        <source>SV changed from {0} to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15220"/>
        <source>PID Standby ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="15225"/>
        <source>PID Standby OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19188"/>
        <source>Alarms from events #{0} created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19190"/>
        <source>No event selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19257"/>
        <source> Events #{0} deleted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageBox Caption</name>
    <message>
        <location filename="artisan.py" line="5129"/>
        <source>Save</source>
        <translation type="obsolete">Spara</translation>
    </message>
</context>
<context>
    <name>Radio Button</name>
    <message>
        <location filename="../artisanlib/main.py" line="27933"/>
        <source>PID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27932"/>
        <source>Meter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27934"/>
        <source>TC4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27935"/>
        <source>Program</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Annotation</name>
    <message>
        <location filename="../artisanlib/main.py" line="878"/>
        <source>Damper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="879"/>
        <source>Fan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="876"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="877"/>
        <source>Heater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="2822"/>
        <source>CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4410"/>
        <source>CHARGE 00:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4471"/>
        <source>TP {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4510"/>
        <source>DE {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4566"/>
        <source>FCs {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4626"/>
        <source>FCe {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4679"/>
        <source>SCs {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4737"/>
        <source>SCe {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4799"/>
        <source>DROP {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4912"/>
        <source>CE {0}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Scope Title</name>
    <message>
        <location filename="../artisanlib/main.py" line="11685"/>
        <source>Roaster Scope</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <location filename="../artisanlib/main.py" line="32349"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23085"/>
        <source>Playback Aid set OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34173"/>
        <source>setting autotune...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34200"/>
        <source>Autotune successfully turned OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34203"/>
        <source>Autotune successfully turned ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34101"/>
        <source>wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31844"/>
        <source>PID OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31847"/>
        <source>PID ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31882"/>
        <source>Empty SV box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31891"/>
        <source>Unable to read SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33996"/>
        <source>Ramp/Soak operation cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33999"/>
        <source>No RX data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34011"/>
        <source>Need to change pattern mode...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34020"/>
        <source>Pattern has been changed. Wait 5 secs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34023"/>
        <source>Pattern could not be changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34054"/>
        <source>RampSoak could not be changed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34067"/>
        <source>RS successfully turned OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32109"/>
        <source>setONOFFrampsoak(): Ramp Soak could not be set OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34169"/>
        <source>Finished reading Ramp/Soak val.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32204"/>
        <source>Finished reading pid values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33388"/>
        <source>Problem setting SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33390"/>
        <source>Cancelled svN change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33411"/>
        <source>setNsv(): bad response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33448"/>
        <source>setNpid(): bad confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33452"/>
        <source>Cancelled pid change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34070"/>
        <source>Ramp Soak could not be set OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34111"/>
        <source>PID set to OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34114"/>
        <source>PID set to ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34117"/>
        <source>Unable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34121"/>
        <source>No data received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34166"/>
        <source>problem reading Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34186"/>
        <source>Autotune cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34205"/>
        <source>UNABLE to set Autotune</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33140"/>
        <source>Decimal position successfully set to 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33143"/>
        <source>Problem setting decimal position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33191"/>
        <source>Thermocouple type successfully set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33194"/>
        <source>Problem setting thermocouple type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33103"/>
        <source>Time Units successfully set to MM:SS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33106"/>
        <source>Problem setting time units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>Ramp (MM:SS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34210"/>
        <source>Soak (MM:SS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34037"/>
        <source>RS ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34057"/>
        <source>RS OFF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32135"/>
        <source>getsegment(): problem reading ramp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32148"/>
        <source>getsegment(): problem reading soak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34280"/>
        <source>Ramp/Soak successfully written</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33558"/>
        <source>setsv(): Unable to set SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33762"/>
        <source>getallpid(): Unable to read pid values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33793"/>
        <source>getallpid(): Unable to read current sv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35278"/>
        <source>Work in Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31871"/>
        <source>SV successfully set to {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34161"/>
        <source>Reading Ramp/Soak {0} ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32247"/>
        <source>{0} successfully sent to pid </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32256"/>
        <source>setpid(): There was a problem setting {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33384"/>
        <source>SV{0} set to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33408"/>
        <source>PID already using sv{0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33445"/>
        <source>pid changed to {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33470"/>
        <source>PID was already using pid {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33473"/>
        <source>setNpid(): Unable to set pid {0} </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33550"/>
        <source>SV{0} successfully set to {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33676"/>
        <source>pid #{0} successfully set to ({1},{2},{3})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33684"/>
        <source>pid command failed. Bad data at pid{0} (8,8,8): ({1},{2},{3}) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33694"/>
        <source>sending commands for p{0} i{1} d{2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33790"/>
        <source>PID is using pid = {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="33857"/>
        <source>PID is using SV = {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="34182"/>
        <source>Current pid = {0}. Proceed with autotune command?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tab</name>
    <message>
        <location filename="../artisanlib/main.py" line="16986"/>
        <source>HUD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16992"/>
        <source>Math</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21448"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35295"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="18716"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23055"/>
        <source>Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23058"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23052"/>
        <source>Config</source>
        <translation type="unfinished">Konfigurera</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16989"/>
        <source>Plotter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29809"/>
        <source>Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="29812"/>
        <source>LCDs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32945"/>
        <source>RS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32948"/>
        <source>SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32954"/>
        <source>Set RS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35165"/>
        <source>PID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32957"/>
        <source>Extra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21433"/>
        <source>Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28457"/>
        <source>ET/BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16995"/>
        <source>UI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21436"/>
        <source>Sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21439"/>
        <source>Quantifiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21442"/>
        <source>Palettes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22430"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="22434"/>
        <source>Espresso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27721"/>
        <source>Modbus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27724"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27727"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28460"/>
        <source>Extra Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28463"/>
        <source>Symb ET/BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28466"/>
        <source>Phidgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="35169"/>
        <source>Ramp/Soak</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Table</name>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>ET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>BT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23212"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23212"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Comm Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Baud Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Byte Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Parity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Stopbits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="27746"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Label 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Label 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Parent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30284"/>
        <source>Opaqueness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Delete Wheel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Edit Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Update Labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Starting angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Color Pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="artisan.py" line="15031"/>
        <source>Temperature</source>
        <translation type="obsolete">Temperatur</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>SV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>Ramp HH:MM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="32263"/>
        <source>Soak HH:MM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Documentation</source>
        <translation type="unfinished">Dokumentation</translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Color 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Color 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>y1(x)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>y2(x)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23301"/>
        <source>DRY END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23304"/>
        <source>FC START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23307"/>
        <source>FC END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23310"/>
        <source>SC START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23313"/>
        <source>SC END</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23316"/>
        <source>DROP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23319"/>
        <source>COOL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>DeltaET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23255"/>
        <source>DeltaBT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23298"/>
        <source>CHARGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19680"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19680"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21743"/>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>LCD 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>LCD 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Curve 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28549"/>
        <source>Curve 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Projection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30451"/>
        <source>Text Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Nr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>If Alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>But Not</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Condition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="31267"/>
        <source>Beep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="23323"/>
        <source>EVENT #{0} {1}{2}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Textbox</name>
    <message>
        <location filename="../artisanlib/main.py" line="560"/>
        <source>Acidity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="518"/>
        <source>Clean Cup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="481"/>
        <source>Head</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="508"/>
        <source>Fragance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="550"/>
        <source>Sweetness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="542"/>
        <source>Aroma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="565"/>
        <source>Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="559"/>
        <source>Body</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="491"/>
        <source>Sour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="536"/>
        <source>Flavor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="493"/>
        <source>Critical
Stimulus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="546"/>
        <source>Aftertaste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="495"/>
        <source>Bitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="496"/>
        <source>Astringency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="497"/>
        <source>Solubles
Concentration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="544"/>
        <source>Mouthfeel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="499"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="500"/>
        <source>Aromatic
Complexity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="501"/>
        <source>Roast
Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="502"/>
        <source>Aromatic
Pungency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="503"/>
        <source>Sweet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="505"/>
        <source>pH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="515"/>
        <source>Dry Fragrance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="516"/>
        <source>Uniformity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="517"/>
        <source>Complexity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="523"/>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="524"/>
        <source>Wet Aroma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="558"/>
        <source>Fragrance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="528"/>
        <source>Taste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="529"/>
        <source>Nose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="534"/>
        <source>Fragrance-Aroma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="553"/>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="545"/>
        <source>Flavour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="555"/>
        <source>Roast Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="556"/>
        <source>Crema Texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="557"/>
        <source>Crema Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="561"/>
        <source>Bitterness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="562"/>
        <source>Defects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="563"/>
        <source>Aroma Intensity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="564"/>
        <source>Aroma Persistence</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tooltip</name>
    <message>
        <location filename="../artisanlib/main.py" line="8369"/>
        <source>Increases the current SV value by 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8377"/>
        <source>Increases the current SV value by 10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8385"/>
        <source>Increases the current SV value by 20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8393"/>
        <source>Decreases the current SV value by 20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8401"/>
        <source>Decreases the current SV value by 10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8409"/>
        <source>Decreases the current SV value by 5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8423"/>
        <source>Turns ON/OFF the HUD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8494"/>
        <source>Timer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8495"/>
        <source>ET Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8496"/>
        <source>BT Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8497"/>
        <source>ET/time (degrees/min)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8498"/>
        <source>BT/time (degrees/min)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8499"/>
        <source>Value of SV in PID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8579"/>
        <source>Number of events found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8589"/>
        <source>Type of event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8595"/>
        <source>Value of event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8607"/>
        <source>Updates the event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16783"/>
        <source>linear: linear interpolation
cubic: 3rd order spline interpolation
nearest: y value of the nearest point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19912"/>
        <source>Automatic generated name = This text + date + time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20618"/>
        <source>Allows to enter a description of the last event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8500"/>
        <source>PID power %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30219"/>
        <source>Save image using current graph size to a png format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20837"/>
        <source>Add new extra Event button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20842"/>
        <source>Delete the last extra Event button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30760"/>
        <source>Show help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28598"/>
        <source>Example: 100 + 2*x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="28599"/>
        <source>Example: 100 + x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30161"/>
        <source>Erases wheel parent hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30165"/>
        <source>Sets graph hierarchy child-&gt;parent instead of parent-&gt;child</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30179"/>
        <source>Increase size of text in all the graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30182"/>
        <source>Decrease size of text in all the graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30186"/>
        <source>Decorative edge beween wheels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30192"/>
        <source>Line thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30197"/>
        <source>Line color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30201"/>
        <source>Apply color pattern to whole graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30207"/>
        <source>Add new wheel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30210"/>
        <source>Rotate graph 1 degree counter clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30213"/>
        <source>Rotate graph 1 degree clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30217"/>
        <source>Save graph to a text file.wg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30225"/>
        <source>open graph file.wg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30228"/>
        <source>Close wheel graph editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4158"/>
        <source>Stop monitoring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8267"/>
        <source>Start monitoring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="4314"/>
        <source>Stop recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8280"/>
        <source>Start recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8329"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16155"/>
        <source>&lt;b&gt;Label&lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16156"/>
        <source>&lt;b&gt;Description &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16158"/>
        <source>&lt;b&gt;Type &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16159"/>
        <source>&lt;b&gt;Value &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16160"/>
        <source>&lt;b&gt;Documentation &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="16161"/>
        <source>&lt;b&gt;Button# &lt;/b&gt;= </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19822"/>
        <source>ON/OFF logs serial communication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19915"/>
        <source>ON/OFF of automatic saving when pressing keyboard letter [a]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19926"/>
        <source>Sets the directory to store batch profiles when using the letter [a]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19974"/>
        <source>Batch prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="19989"/>
        <source>ON/OFF batch counter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20880"/>
        <source>Backup all palettes to a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="20881"/>
        <source>Restore all palettes from a text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21212"/>
        <source>Action Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21217"/>
        <source>Action String</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="21219"/>
        <source>Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30172"/>
        <source>Aspect Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30222"/>
        <source>Sets Wheel graph to view mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="30765"/>
        <source>Clear alarms table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8295"/>
        <source>First Crack Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8302"/>
        <source>First Crack End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8309"/>
        <source>Second Crack Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8316"/>
        <source>Second Crack End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8337"/>
        <source>Charge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8345"/>
        <source>Drop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8360"/>
        <source>Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8432"/>
        <source>Dry End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../artisanlib/main.py" line="8440"/>
        <source>Cool End</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
